from utils import wait
import pytest
global MA_PROMPTS
from setup import get_prompts
from communicator import *

def test_mgr_assistance_initating_request(MA_clients):
    a,b,c,d,e=MA_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    MA_PROMPTS = get_prompts(MA_clients,"manager assistance")
    wait(5)
    a.command("manager assistance")
    assert a.i_wait(MA_PROMPTS['command_response'],10)


def test_verification_of_Activation_response_to_listners(MA_clients):
    a, b, c, d, e = MA_clients
    MA_PROMPTS = get_prompts(MA_clients,"manager assistance")
    assert all((x.i_wait(MA_PROMPTS['activation_response_to_listeners'],10)) for x in MA_clients[1:3])

def test_soundout_for_available_user(MA_clients):
    a, b, c, d, e = MA_clients
    wait(15)
    MA_PROMPTS = get_prompts(MA_clients,"manager assistance")
    assert all(x.soundout == MA_PROMPTS["sound_out_all"] for x in MA_clients[0:3])


def test_already_active_prompt_verification(MA_clients):
    a, b, c, d, e = MA_clients
    MA_PROMPTS = get_prompts(MA_clients,"manager assistance")
    a.command("manager assistance")
    assert a.i_wait(MA_PROMPTS["already_active"],10)

def test_non_member_says_copy_that(MA_clients):
    a, b, c, d, e = MA_clients
    MA_PROMPTS = get_prompts(MA_clients,"manager assistance")
    print "We expect :"+MA_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(MA_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(MA_clients):
    d = MA_clients[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(MA_clients):
    a, b, c, d, e = MA_clients
    MA_PROMPTS = get_prompts(MA_clients,"manager assistance")
    b.command("copythat")
    assert (b.i_wait(MA_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(MA_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(MA_PROMPTS["acceptance_to_listeners"], 10))
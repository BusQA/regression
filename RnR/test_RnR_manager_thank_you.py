from utils import wait
import pytest
global MTY_PROMPTS
from setup import get_prompts
from communicator import *
from config import DEFAULT_ZONE
def test_mty_assistance_initating_request(managers):
    a,b,c,d,e=managers
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    MTY_PROMPTS = get_prompts(managers,"manager thankyou",zone=DEFAULT_ZONE)
    a.command("manager thankyou")
    assert a.i_wait(MTY_PROMPTS['command_response'],20)


def test_verification_of_Activation_response_to_listners(managers):
    a, b, c, d, e = managers
    MTY_PROMPTS = get_prompts(managers,"manager thankyou",zone=DEFAULT_ZONE)
    assert all((x.i_wait(MTY_PROMPTS['activation_response_to_listeners'],10)) for x in managers[1:3])


def test_soundout_for_available_user(managers):
    a, b, c, d, e = managers
    wait(15)
    MTY_PROMPTS = get_prompts(managers,"manager thankyou",to_convert={',':'_'},zone=DEFAULT_ZONE)
    assert all(x.soundout == MTY_PROMPTS["sound_out_all"] for x in managers[0:3])
   
def test_already_active_prompt_verification(managers):
    a, b, c, d, e = managers
    MTY_PROMPTS = get_prompts(managers,"manager thankyou",zone=DEFAULT_ZONE)
    a.command("manager thankyou")
    assert a.i_wait(MTY_PROMPTS["already_active"],10)


def test_soundout_for_engaged_user(managers):
    d = managers[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(managers):
    a, b, c, d, e = managers
    MTY_PROMPTS = get_prompts(managers,"manager thankyou",zone=DEFAULT_ZONE)
    b.command("copythat")
    assert (b.i_wait(MTY_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(MTY_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(MTY_PROMPTS["acceptance_to_listeners"], 10))

from utils import wait
import pytest
global RB_PROMPTS
from setup import get_prompts
import pytest

'''
["andrea birdsong", RB member
"mike diviney", non member RB,managers member
"joanna coffey", RB member
"bev janes", RB member
"angella hilt" non member RB,managers member
]
'''
def test_register_backup_initating_request(RB_ESC_clients):
    a,b,c,d,e=RB_ESC_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    a.command("registerbackup")
    assert a.i_wait(RB_PROMPTS['command_response'],10)
    
def test_verification_of_Activation_response_to_listners(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    #assert all((x.i_wait(RB_PROMPTS['activation_response_to_listeners'],10)) for x in RB_ESC_clients[1:3])
    assert c.i_wait(RB_PROMPTS["activation_response_to_listeners"], 30)

def test_soundout_for_available_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(10)
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    assert all(x.soundout == RB_PROMPTS["sound_out_all"] for x in RB_ESC_clients[0:4:2])

def test_non_member_says_copy_that(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    print "We expect :"+RB_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(RB_PROMPTS["no_active_request"].lower(),10)

def test_escalation_active_prompt_for_requestor(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(15)
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    assert a.i_wait(RB_PROMPTS["escalation_to_requestor"],10)

def test_escalation_active_prompt_for_listeners(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(15)
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    assert c.i_wait(RB_PROMPTS['escalation_to_listeners'],10)

def test_soundout_for_engaged_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(10)
    d = RB_ESC_clients[-2]
    assert d.soundout == "twinkle1"

def test_escalated_soundout_for_available_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    assert (a.soundout == "escalation_sound_out", 10) and (b.soundout == "escalation_sound_out", 10) and (c.soundout == "escalation_sound_out", 10) and (e.soundout == "escalation_sound_out", 10)

def test_accepting_with_available_user(RB_ESC_clients):
    print len(RB_ESC_clients)
    a, b, c, d, e = RB_ESC_clients
    wait(10)
    RB_PROMPTS = get_prompts(RB_ESC_clients)
    b.command("copythat")
    assert (b.i_wait(RB_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10))  and (e.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10))
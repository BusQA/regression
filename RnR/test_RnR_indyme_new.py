from utils import wait
import pytest
global IM_PROMTS
from setup import get_prompts
from communicator import *
from ssh_client import *

def test_indyme_initating_request(IME_clients):
    a,b,c,d,e = IME_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    wait(7)
    d.make_engaged()
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(2)
    IM_PROMTS = get_prompts(IME_clients,"indyme rnr")
    assert all((x.i_wait(IM_PROMTS['activation_response_to_listeners'],10)) for x in IME_clients[0:2])


def test_soundout_for_available_user(IME_clients):
    a, b, c, d, e = IME_clients
    wait(15)
    IM_PROMTS = get_prompts(IME_clients,"indyme rnr")
    assert all(x.soundout == IM_PROMTS["sound_out_all"] for x in IME_clients[0:2])

def test_non_member_says_copy_that(IME_clients):
    a, b, c, d, e = IME_clients
    SB_PROMPTS = get_prompts(IME_clients,"indyme rnr")
    print "We expect :"+SB_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(SB_PROMPTS["no_active_request"].lower(),10)

def test_escalation_response(IME_clients):
    wait(10)
    a, b, c, d, e = IME_clients
    IM_PROMTS = get_prompts(IME_clients,"indyme rnr")
    assert (a.i_wait(IM_PROMTS["escalation_to_listeners"],30) and c.i_wait(IM_PROMTS["activation_response_to_escalation_listeners"],30))

def test_soundout_for_engaged_user(IME_clients):
    d = IME_clients[-2]
    assert d.soundout == "twinkle1"

def test_soundout_for_escalation_user(IME_clients):
    wait(5)
    c = IME_clients[2]
    IM_PROMTS = get_prompts(IME_clients,"indyme rnr")
    assert all(x.soundout == IM_PROMTS["escalation_sound_out"] for x in IME_clients[0:2])

def test_escalation2_response(IME_clients):
    wait(15)
    e = IME_clients[-1]
    IM_PROMTS = get_prompts(IME_clients,"indyme rnr")
    assert (e.i_wait(IM_PROMTS["activation_response_to_level_two_users"],30))

def test_accepting_with_available_user(IME_clients):
    a, b, c, d, e = IME_clients
    IM_PROMTS = get_prompts(IME_clients,"indyme rnr")
    b.command("copythat")
    assert (b.i_wait(IM_PROMTS["acceptances_to_requestor"], 10)) and (a.i_wait(IM_PROMTS["acceptance_to_listeners"], 10)) and (c.i_wait(IM_PROMTS["acceptance_to_listeners"], 10))

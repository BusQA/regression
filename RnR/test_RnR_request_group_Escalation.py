from utils import wait
import pytest
global RG_PROMPTS
from setup import get_prompts
import pytest

'''
["andrea birdsong", RB member
"mike diviney", non member RB,managers member
"joanna coffey", RB member
"bev janes", RB member
"angella hilt" non member RB,managers member
]
'''
def test_request_group_initating_request(RB_ESC_clients):
    a,b,c,d,e=RB_ESC_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    a.cmd("request","registerbackup")
    assert a.i_wait(RG_PROMPTS['command_response'],10)
    
def test_verification_of_Activation_response_to_listners(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    assert c.i_wait(RG_PROMPTS["activation_response_to_listeners"], 20)

def test_soundout_for_available_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(10)
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    assert (a.soundout == RG_PROMPTS["sound_out_all"].replace(",","_") and c.soundout == RG_PROMPTS["sound_out_all"].replace(",","_"))

def test_non_member_says_copy_that(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    print "We expect :"+RG_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(RG_PROMPTS["no_active_request"].lower(),10)

def test_escalation_active_prompt_for_requestor(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(15)
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    assert a.i_wait(RG_PROMPTS["escalation_to_requestor"],10)

def test_escalation_active_prompt_for_listeners(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(15)
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    assert c.i_wait(RG_PROMPTS['activation_response_to_rnr_group'],10)

def test_soundout_for_engaged_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(10)
    d = RB_ESC_clients[-2]
    assert d.soundout == "twinkle1"

def test_escalated_soundout_for_available_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    print a.soundout+"this is validating right"
    assert (a.soundout == RG_PROMPTS["escalation_sound_out"].replace(",","_") and b.soundout == RG_PROMPTS["escalation_sound_out"].replace(",","_") and c.soundout == RG_PROMPTS["escalation_sound_out"].replace(",","_") and e.soundout == RG_PROMPTS["escalation_sound_out"].replace(",","_")) 

def test_accepting_with_available_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    wait(10)
    RG_PROMPTS = get_prompts(RB_ESC_clients,"request")
    b.command("copythat")
    wait(3)
    a.command("1to10")
    b.command("1to10")
    a.tap_out()
    wait(3)
    assert ("conversation_ended" in a.output() and "ended_conversation" in b.output()) 

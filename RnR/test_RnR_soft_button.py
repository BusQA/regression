from utils import wait
import pytest
global SB_PROMPTS
from setup import get_prompts
from communicator import *
from ssh_client import *

def test_soft_button_initating_request(SB_clients):
    a,b,c,d,e = SB_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    wait(7)
    d.make_engaged()
    sshCommand('curl -X POST "http://localhost:8088/softbutton?location=CO&clear=false"')
    wait(2)
    SB_PROMPTS = get_prompts(SB_clients,"soft button")
    assert all((x.i_wait(SB_PROMPTS['activation_response_to_listeners'],10)) for x in SB_clients[1:3])


def test_soundout_for_available_user(SB_clients):
    a, b, c, d, e = SB_clients
    wait(15)
    SB_PROMPTS = get_prompts(SB_clients,"soft button")
    assert all(x.soundout == SB_PROMPTS["sound_out_all"] for x in SB_clients[0:3])


def test_non_member_says_copy_that(SB_clients):
    a, b, c, d, e = SB_clients
    SB_PROMPTS = get_prompts(SB_clients,"soft button")
    print "We expect :"+SB_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(SB_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(SB_clients):
    d = SB_clients[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(SB_clients):
    a, b, c, d, e = SB_clients
    SB_PROMPTS = get_prompts(SB_clients,"soft button")
    b.command("copythat")
    assert (b.i_wait(SB_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(SB_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(SB_PROMPTS["acceptance_to_listeners"], 10))
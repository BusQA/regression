from utils import *
import pytest
from responses import do_you_mean
from DB_HANDLE  import get_feature_prompts

@pytest.fixture(scope="module")
def clients():
    print "re-naming feature_prompt table"
    #rename_table("feature_prompt", "feature_prompt1")
    cii = make_clients(count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    print "logging off clients"
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    print "re-verting feature_prompt table"
    #rename_table("feature_prompt1", "feature_prompt")


#@pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.parametrize("command,response", [
   ("hello",do_you_mean["ask_for_full_name"]),
    ("interrupt",do_you_mean["ask_for_full_name"]),
    ("message",do_you_mean["ask_for_full_name"]),
    ("locate",do_you_mean["ask_for_full_name"])
])
def test_response_no_logged_on(clients,command,response):
    a,b,c,d= clients[:4]
    a.log_on_tool()
    wait(2)
    a.say('menubutton', [command, "gary"])
    wait(2)
    #b.logon_name("gary maasen")
    if command == "message":
        assert a.output() == response
    else:
        assert a.output() == do_you_mean["not_logged_on"]
    wait(2)

#@pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.parametrize("command,response", [
   ("hello",do_you_mean["ask_for_full_name"]),
    ("interrupt",do_you_mean["ask_for_full_name"]),
    ("message",do_you_mean["ask_for_full_name"]),
    ("locate",do_you_mean["ask_for_full_name"])
])
def test_response_1_loggedon(clients,command,response):
    a,b,c,d= clients[:4]
    wait(2)
    b.logon_name("gary maasen")
    wait(5)
    a.say('menubutton', [command, "gary"])
    wait(5)
    if command == "message":
        assert a.output() == response
    elif command == "locate":
        print a.output()
        assert a.output()=="gary,is_available_near,front end"

    else:
        assert (a.output() == "gary maasen,go_ahead") or (a.output() == "interrupt,gary maasen,go_ahead")
        a.tap_out()


#@pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.parametrize("command,response", [
   ("hello",do_you_mean["ask_for_full_name"]),
    ("interrupt",do_you_mean["ask_for_full_name"]),
    ("message",do_you_mean["ask_for_full_name"]),
    ("locate",do_you_mean["ask_for_full_name"])
])
def test_response_2_loggedon(clients,command,response):
    a,b,c,d= clients[:4]
    a.make_available()
    c.logon_name("gary smedley")
    wait(5)
    a.say('menubutton', [command, "gary"])
    wait(2)
    assert a.output() == response

#@pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.parametrize("command,response", [
   ("hello",do_you_mean["ask_for_full_name"]),
    ("interrupt",do_you_mean["ask_for_full_name"]),
    ("message",do_you_mean["ask_for_full_name"]),
    ("locate",do_you_mean["ask_for_full_name"])
])
def test_response_3_logged_on(clients, command, response):
    a, b, c, d = clients[:4]
    d.logon_name("gary geib")
    wait(5)
    a.say('menubutton', [command, "gary"])
    wait(2)
    assert a.output() == response

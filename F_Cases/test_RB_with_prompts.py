from utils import wait
import time
from setup import RB_clients
from DB_HANDLE import Group_NAME,making_prompts,get_feature_prompts
from subprocess import call
import pytest


'''
a= initaior
b=available user
c= available user
d= engaged user
e = non logged on user

'''

def test_register_backup_5_command_reponse(RB_clients):
    a,b,c,d,e=RB_clients
    [x.log_on_tool() for x in a,b,c,d]
    wait(3)
    d.make_engaged()
    dict2 = {"__ACCEPTOR__": a.name, "__FROM__": b.name, ",__ZONE__": "", "__COUNT__": str(5)}
    global RB_NBR
    RB_NBR = making_prompts(get_feature_prompts("registerBackupN"), dict2)
    a.rb("5")
    wait(2)
    assert  a.output() == RB_NBR["command_response"]

def test_register_backup_5_activation_response(RB_clients):
    a, b, c, d, e = RB_clients
    wait(7)
    assert (a.output() == RB_NBR["activation_response_to_requestor"] and all(x.output() == RB_NBR["activation_response_to_listeners"] for x in RB_clients[1:3]))



def test_soundout_for_available_user(RB_clients):
    a, b, c, d, e = RB_clients
    wait(20)
    assert all(x.soundout == RB_NBR["sound_out_all"] for x in RB_clients[:3])

def test_already_active_prompt_verification(RB_clients):
    a, b, c, d, e = RB_clients
    wait(3)
    a.rb("3")
    wait(3)
    #assert a.output() == RB_NBR["already_active"]


def test_soundout_for_engaged_user(RB_clients):
    d = RB_clients[-2]
    #assert d.soundout == "twinkle1"


def test_accepting_with_available_user(RB_clients):
    a, b, c, d, e = RB_clients
    wait(3)
    b.command("copythat")
    wait(3)
    #assert (a.output() == b.name+RB_NBR["accept1"][0]+a.name and b.output() == RB_NBR["accept1"][1]+a.name )
    
def test_accepting_with_already_accepted_user(RB_clients):
    b=RB_clients[1]
    wait(3)
    b.command("copythat")
    wait(3)
    #assert b.output() == RB_NBR["already_accept"]

def test_accepting_with_initiator(RB_clients):
    a, b, c, d, e = RB_clients
    wait(3)
    a.command("copythat")
    wait(3)
    #assert a.output() == RB_NBR["initiator_accept"]


def test_newly_logged_on_user_Accepting(RB_clients):
    a, b, c, d, e = RB_clients
    e.log_on_tool()
    clist=a,e
    wait(15)
    e.command("copythat")
    wait(3)
    #assert a.output() == e.name + RB_NBR["accept1"][0] + a.name and e.output() == RB_NBR["accept1"][1] + a.name and all(x.soundout == RB_NBR["new_user_accept"] for x in clist)



def test_enagaged_user_accepting(RB_clients):
    a, b, c, d, e = RB_clients
    clist = a,d
    d.make_available()
    wait(15)
    d.command("copythat")
    wait(5)
    global t2
    t2 = time.time()
    print t1-t2
    wait(20)
    #assert a.output() == d.name + RB_NBR["accept1"][0] + a.name and e.output() == RB_NBR["accept1"][1] + a.name and all(x.soundout == RB_NBR["engaged_user_accept"] for x in clist)


def test_accepting_escalation(RB_clients):
    a, b, c, d, e = RB_clients
    wait(3)
    #c.command("copythat")
    #assert c.output() == RB_NBR["already_accept"]
    c.command("copythat")
    wait(30)

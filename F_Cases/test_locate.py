from utils import wait
from setup import clients
from communicator import *
from responses import LOCATE_PROMPTS
import pytest

def test_locate_in_conversation_user(clients):
	"""locate the user who is in a conversation"""
	a,b,c=clients[:3]
	[x.log_on_tool() for x in a,b,c]
	wait(2)
	b.cmd('hello', c.name)
	a.cmd1('locate',b.name)
	assert b.fname+LOCATE_PROMPTS["in_conversation"] == a.output()
	b.tap_out()

def test_locate_message_listening_user(clients):
	"""locate a user who is listening messages"""
	a,b,c=clients[:3]
	b.message(c.name)
	wait(5)
	a.cmd1('locate', c.name)
	wait(4)
	assert c.fname+LOCATE_PROMPTS["listening_message"] == a.output()

@pytest.mark.skip(reason="no way of currently testing this")	
def test_locate_inventory_checking_user(clients):
	"""locate user who is checking inventory/sku lookup"""
	a,b,c=clients[:3]
	b.say('menubutton', ['skulookup'])
	wait(2)
	if b.name+LOCATE_PROMPTS["checking_inventory"][0] in b.output():
		b.command("1to10")
		wait(10)
		a.say('menubutton', ['locate', b.name])
		wait(4)
		assert b.fname+LOCATE_PROMPTS["checking_inventory"][1] == a.output()
	else:
		assert False
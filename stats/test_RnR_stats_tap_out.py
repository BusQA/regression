from utils import *
from DB_HANDLE import CO_USERS,get_emp_id,RB_USERS,RB_ESC_USERS,GUN_USERS,GUN_ESC_USERS,MA_USERS,SB_USERS,SRD_USERS,srd_db_update,INDYME_USERS,managers_USERS,Group_NAME
import pytest
global CO_PROMPTS
from setup import get_prompts
from communicator import *
from config import RNR_REQ,RNR_ACC,DEFAULT_ZONE
from time import strftime
def convert_to_str(dict1):
    return {k:str(v) for k,v in dict1.iteritems()}


@pytest.fixture(scope="module")
def clients():
    cii = make_clients(count=5)
    #print"making clinets"
    start_clients(cii)
    yield cii
    [x.log_off_json() for x in cii]
    #print"killing clients"
    kill_clients(cii)

@pytest.mark.parametrize("rnr,users", [
    ('register_backup',RB_USERS),
    ('manager_assistance',MA_USERS),
])
def test_stats(clients,rnr,users):
    expected_prompt_req=dict(RNR_REQ)
    cii=clients
    [x.log_off_json() for x in cii]
    for client,name in zip(cii,users):
        client.logon_name(name)
    a,b,c,d,e=cii
    d.make_engaged()
    wait(5)
    command=rnr.replace('_',' ')
    a.command(command)
    wait(3)
    a.tap_out()
    cnt=1
    tx_id=False
    while cnt < 5 :
        try:
            tx_id=get_rnr_txid_from_tgs(rnr)
            cnt=cnt+1
        except Exception as e:
            assert False
    
    for user,name in zip(cii,users):
        user.id=get_emp_id(name)
    expected_prompt_req['user']=cii[0].id
    expected_prompt_req['device']=cii[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'tapout'))
    print "theatrostats enteries for {0}_request:{1} \n".format(rnr,actual_op_request)
    assert expected_prompt_req['user'] == actual_op_request['user']
    assert expected_prompt_req['server_id'] == actual_op_request['server_id']
    assert expected_prompt_req['responderCount'] == actual_op_request['responderCount']
    assert expected_prompt_req['requestedCount'] == actual_op_request['requestedCount']
    assert expected_prompt_req['device'] == actual_op_request['device']
    assert expected_prompt_req['listeners'] == actual_op_request['listeners']
    [x.log_off_json() for x in cii]    
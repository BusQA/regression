from utils import *
from DB_HANDLE import CO_USERS,get_emp_id,RB_USERS,RB_ESC_USERS,GUN_USERS,GUN_ESC_USERS,MA_USERS,SB_USERS,SRD_USERS,srd_db_update,INDYME_USERS,managers_USERS,Group_NAME
import pytest
global CO_PROMPTS
from setup import get_prompts
from communicator import *
from config import RNR_REQ,RNR_ACC,DEFAULT_ZONE
from time import strftime
def convert_to_str(dict1):
    return {k:str(v) for k,v in dict1.iteritems()}





@pytest.fixture(scope='module')
def clients():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "10",
        "SOFT_BUTTON_NO_RESP_TERMINATE_SEC": "10"})
    cii = make_clients(count=5)
    #print"making clinets"
    start_clients(cii)
    yield cii
    [x.log_off_json() for x in cii]
    #print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120",
        "SOFT_BUTTON_NO_RESP_TERMINATE_SEC": "120"})

@pytest.mark.parametrize("rnr,users", [
    ('carry_out', CO_USERS),
    ('register_backup',RB_USERS),
    ('manager_assistance',MA_USERS),
    ('manager_thank_you',managers_USERS),
    ('indyme_rnr_request',INDYME_USERS),
    ('soft_button',SB_USERS)

])
def test_stats(clients,rnr,users):
    expected_prompt_req=dict(RNR_REQ)
    cii=clients
    [x.log_off_json() for x in cii]
    for client,name in zip(cii,users):
        client.logon_name(name)
    a,b,c,d,e=cii
    d.make_engaged()
    wait(5)
    if rnr=='indyme_rnr_request':
        sshCommand('python2.7 /home/theatro/bin/indyme.py')
    elif rnr == 'soft_button':
        sshCommand('curl -X POST "http://localhost:8088/softbutton?location=CO&clear=false"')
    else:
        command=rnr.replace('_',' ')
        a.command(command)
    
    wait(15)
    cnt=1
    tx_id=False
    while cnt < 5 :
        try:
            tx_id=get_rnr_txid_from_tgs(rnr)
            cnt=cnt+1
        except Exception as e:
            assert False
    
    for user,name in zip(cii,users):
        user.id=get_emp_id(name)
        print "{0} 's id is {1}".format(name,user.id)

    if rnr=='indyme_rnr_request' or rnr=='soft_button':
        #print "inside if"
        expected_prompt_req['user']='-1'
        expected_prompt_req['device']='WEBSOFTCLIENT'
        if rnr=='indyme_rnr_request':expected_prompt_req['location']='east bay'
    else:
        expected_prompt_req['user']=cii[0].id
        expected_prompt_req['device']=cii[0].mac_address
    

    expected_prompt_req['listeners']=[x.id for x in cii]
    if rnr in ['manager_thank_you','indyme_rnr_request','soft_button']:
        actual_op_timeout=convert_to_str(get_Request_Response_stats(tx_id,"timeout"))
    else:
        actual_op_timeout=convert_to_str(get_Request_Response_stats(tx_id,"cancel"))
    print "theatrostats enteries for {0}_request:{1} \n".format(rnr,actual_op_timeout)
    #print "We got location as {0} for this feature{1}".format(expected_prompt_req['location'],rnr)
    #print RNR_REQ
    assert expected_prompt_req['user'] == actual_op_timeout['user']
    if rnr == 'manager_thank_you':
        assert expected_prompt_req['listeners'][4] in actual_op_timeout['listeners']
    else:
        assert expected_prompt_req['listeners'][4] not in actual_op_timeout['listeners']
    
    assert expected_prompt_req['listeners'][0] in actual_op_timeout['listeners']
    assert expected_prompt_req['listeners'][1] in actual_op_timeout['listeners']
    assert expected_prompt_req['listeners'][2] in actual_op_timeout['listeners']
    assert expected_prompt_req['listeners'][3] in actual_op_timeout['listeners']
    assert expected_prompt_req['server_id'] == actual_op_timeout['server_id']
    assert expected_prompt_req['responderCount'] == actual_op_timeout['responderCount']
    assert expected_prompt_req['requestedCount'] == actual_op_timeout['requestedCount']
    assert expected_prompt_req['device'] == actual_op_timeout['device']
    if rnr=='indyme_rnr_request':
        sshCommand('python2.7 /home/theatro/bin/indyme.py "east bay" clear')
    else:
        pass
    [x.log_off_json() for x in cii]



#theatrostats enteries for register_backup_request:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '2', 'listeners': '7576$7583$7585', 'requestedCount': '1', 'user': '7569', 'timeout': '1543999213', 'time': '1543999100', 'device': '0023a729eab1', 'txId': '1543999093000100223', 'type': 'register_backup_escalate_request', 'id': '730', 'location': ''}

#theatrostats enteries for register_backup_accept:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '2', 'listeners': '7583$7585$7569$7563', 'requestedCount': '1', 'user': '7576', 'timeout': '1543999213', 'time': '1543999119', 'device': '0023a7347328', 'txId': '1543999093000100223', 'type': 'register_backup_escalate_accept', 'id': '731', 'location': ''}
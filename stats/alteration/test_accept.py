from utils import *
from DB_HANDLE import CO_USERS,get_emp_id,RB_USERS,RB_ESC_USERS,GUN_USERS,GUN_ESC_USERS,MA_USERS,SB_USERS,SRD_USERS,srd_db_update,INDYME_USERS,managers_USERS,Group_NAME
import pytest
global CO_PROMPTS
from setup import get_prompts
from communicator import *
from config import RNR_CALL,DEFAULT_ZONE
from time import strftime
from stats_utils import *

def convert_to_str(dict1):
    return {k:str(v) for k,v in dict1.iteritems()}


@pytest.fixture(scope="module")
def clients():
    cii = make_clients(count=5,names=RB_USERS)
    #print"making clinets"
    start_clients(cii)
    yield cii
    [x.log_off_json() for x in cii]
    #print"killing clients"
    kill_clients(cii)

def test_alter_accept(clients):
    expected_prompt_req=dict(RNR_CALL)
    a,b,c,d,e=cii=clients
    [x.log_off_json() for x in cii]
    [x.log_on_tool() for x in cii]
    d.make_engaged()
    wait(5)
    sshCommand('/home/theatro/bin/alter_request.sh')
    wait(20)
    b.command("copythat")

    cnt=1
    tx_id=False
    while cnt < 5 :
        try:
            tx_id=get_rnr_txid_from_tgs('alteration',service_rnr=True)
            cnt=cnt+1
            print tx_id
            if tx_id:
                break
        except Exception as e:
            print e
    for user in cii:
        user.id=get_emp_id(user.name)
    
    actual_call_stat=convert_to_str(get_RnR_stat(tx_id))
    for key in expected_prompt_req:
        print "verifying for {0} expected is {1} actual is {2}\n".format(key,expected_prompt_req[key],actual_call_stat[key])
        assert expected_prompt_req[key]==actual_call_stat[key]

    actual_call_event=get_RnR_stat(tx_id,'event')
    assert len(actual_call_event) == 5
    assert verify_so_event(actual_call_event)
    print get_acceptor_data(actual_call_event)
    assert b.mac_address == get_acceptor_data(actual_call_event)[0]
    assert b.id == get_acceptor_data(actual_call_event)[1]
    so_mac_address=get_so_mac_ids(actual_call_event)
    for user in cii[:4]:
        print "veriying {0} in {1}".format(user.mac_address,so_mac_address)
        assert user.mac_address in so_mac_address

    get_so_mac_ids(actual_call_event)


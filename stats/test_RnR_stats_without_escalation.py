from utils import *
from DB_HANDLE import CO_USERS,get_emp_id,RB_USERS,RB_ESC_USERS,GUN_USERS,GUN_ESC_USERS,MA_USERS,SB_USERS,SRD_USERS,srd_db_update,INDYME_USERS,managers_USERS,Group_NAME
import pytest
global CO_PROMPTS
from setup import get_prompts
from communicator import *
from config import RNR_REQ,RNR_ACC,DEFAULT_ZONE
from time import strftime
def convert_to_str(dict1):
    return {k:str(v) for k,v in dict1.iteritems()}


@pytest.fixture(scope="module")
def clients():
    cii = make_clients(count=5)
    #print"making clinets"
    start_clients(cii)
    yield cii
    [x.log_off_json() for x in cii]
    sshCommand('python2.7 /home/theatro/bin/indyme.py "east bay" clear')
    #print"killing clients"
    kill_clients(cii)

@pytest.mark.parametrize("rnr,users", [
    #('carry_out', CO_USERS),
    #('register_backup',RB_USERS),
    #('manager_assistance',MA_USERS),
    #('manager_thank_you',managers_USERS),
    ('indyme',INDYME_USERS),
    #('soft_button',SB_USERS)
])
def test_stats(clients,rnr,users):
    expected_prompt_req=dict(RNR_REQ)
    expected_prompt_acpt=dict(RNR_ACC)
    cii=clients
    #Updating names of the cleints according to paramterized user name for better readablit of code
    for client,name in zip(cii,users):
        client.name=name
        client.id=get_emp_id(client.name)

    [x.log_off_json() for x in cii]
    [x.log_on_tool() for x in cii]
    a,b,c,d,e=cii
    d.make_engaged()
    wait(5)
    if rnr=='indyme':
        sshCommand('python2.7 /home/theatro/bin/indyme.py')
    elif rnr == 'soft_button':
        sshCommand('curl -X POST "http://localhost:9099/softbutton?location=CO&clear=false"')
    elif rnr == 'alteration':
        sshCommand('/home/theatro/bin/alter_request.sh')
    else:
        command=rnr.replace('_',' ')
        a.command(command)
    
    wait(20)
    b.command("copythat")

    cnt=1
    tx_id=False
    while cnt < 5 :
        try:
            if rnr=='alteration':
                tx_id=get_rnr_txid_from_tgs(rnr,service_rnr=True)
            else:
                tx_id=get_rnr_txid_from_tgs(rnr)
            cnt=cnt+1
            if tx_id:
                print tx_id
                break
        except Exception as e:
            assert False    

    if rnr=='indyme' or rnr=='soft_button':
        #print "inside if"
        expected_prompt_req['user']='-1'
        expected_prompt_req['device']='WEBSOFTCLIENT'
        if rnr=='indyme':expected_prompt_req['location']=expected_prompt_acpt['location']='east bay'
    else:
        expected_prompt_req['user']=cii[0].id
        expected_prompt_req['device']=cii[0].mac_address
    

    expected_prompt_acpt['listeners']=[x.id for x in cii]
    expected_prompt_acpt['user']=cii[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    print "theatrostats enteries for {0}_request:{1} \n".format(rnr,actual_op_request)
    print "theatrostats enteries for {0}_accept:{1} \n".format(rnr,actual_op_accept)
    #print "We got location as {0} for this feature{1}".format(expected_prompt_req['location'],rnr)
    #print RNR_REQ
    assert expected_prompt_req['user'] == actual_op_request['user']
    assert expected_prompt_req['server_id'] == actual_op_request['server_id']
    assert expected_prompt_req['responderCount'] == actual_op_request['responderCount']
    assert expected_prompt_req['requestedCount'] == actual_op_request['requestedCount']
    assert expected_prompt_req['device'] == actual_op_request['device']
    assert expected_prompt_acpt['server_id'] == actual_op_accept['server_id']
    assert expected_prompt_acpt['requestedCount'] == actual_op_accept['requestedCount']
    assert expected_prompt_acpt['user'] == actual_op_accept['user']
    assert expected_prompt_acpt['device'] == actual_op_accept['device']    
    assert expected_prompt_acpt['listeners'][0] in actual_op_accept['listeners']
    assert expected_prompt_acpt['listeners'][2] in actual_op_accept['listeners']
    assert expected_prompt_acpt['listeners'][3] in actual_op_accept['listeners']
    assert expected_prompt_acpt['listeners'][1] not in actual_op_accept['listeners']
    if rnr == 'manager_thank_you':
        assert expected_prompt_acpt['listeners'][4] in actual_op_accept['listeners']
        assert DEFAULT_ZONE == actual_op_request['location']
        assert DEFAULT_ZONE == actual_op_accept['location']
    else:
        assert expected_prompt_acpt['listeners'][4] not in actual_op_accept['listeners']
        assert expected_prompt_req['location'] == actual_op_request['location']
        assert expected_prompt_acpt['location'] == actual_op_accept['location']
    if rnr == 'soft_button':
        assert 'SMB' in actual_op_request['remoteTxId']
    else:
        assert expected_prompt_req['remoteTxId'] == actual_op_request['remoteTxId']
    if rnr=='indyme':
        sshCommand('python2.7 /home/theatro/bin/indyme.py "east bay" clear')
    else:
        pass
    [x.log_off_json() for x in cii]
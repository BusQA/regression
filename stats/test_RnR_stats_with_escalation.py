from utils import *
from DB_HANDLE import CO_USERS,get_emp_id,RB_USERS,RB_ESC_USERS,GUN_USERS,GUN_ESC_USERS,MA_USERS,SB_USERS,SRD_USERS,srd_db_update,INDYME_USERS,managers_USERS,Group_NAME
import pytest
global CO_PROMPTS
from setup import get_prompts
from communicator import *
from config import RNR_REQ,RNR_ACC,DEFAULT_ZONE
from time import strftime

'''
RB_ESC_USERS = ["andrea birdsong"RB,"mike diviney" managers,"joanna coffey"RB,"bev janes" RB
,"angella hilt"MA]
andrea birdsong 's id is 7569
mike diviney 's id is 7677
joanna coffey 's id is 7642
bev janes 's id is 7576
angella hilt 's id is 7570
RB
theatrostats enteries for register_backup_request:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '2', 'listeners': '7576$7642', 'requestedCount': '1', 'user': '7569', 'timeout': '1548929575', 'time': '1548929464', 'device': '0023a729eab1', 'txId': '1548929455000100223', 'type': 'register_backup_escalate_request', 'id': '1161', 'location': 'north bay'}
theatrostats enteries for register_backup_accept:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '2', 'listeners': '7576$7642$7570$7569', 'requestedCount': '1', 'user': '7677', 'timeout': '1548929575', 'time': '1548929481', 'device': '0023a7347328', 'txId': '1548929455000100223', 'type': 'register_backup_escalate_accept', 'id': '1162', 'location': 'north bay'}

indyme
theatrostats enteries for indyme_request:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '1', 'listeners': '7569$7576$7642', 'requestedCount': '1', 'user': '-1', 'timeout': '1548930929', 'time': '1548930809', 'device': 'WEBSOFTCLIENT', 'txId': '1548930809000100223', 'type': 'indyme_rnr_escalate_request', 'id': '1164', 'location': 'east bay'}
theatrostats enteries for indyme_accept:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '1', 'listeners': '7569$7576$7642$7570', 'requestedCount': '1', 'user': '7677', 'timeout': '1548930929', 'time': '1548930834', 'device': '0023a7347328', 'txId': '1548930809000100223', 'type': 'indyme_rnr_escalate_accept', 'id': '1165', 'location': 'east bay'}

soft button
theatrostats enteries for soft_button_request:{'server_id': '286', 'remoteTxId': 'SMB-1548931547---127-0-0-1-CO', 'responderCount': '0', 'call_id': '1', 'listeners': '7569$7570$7576$7642$7677', 'requestedCount': '1', 'user': '-1', 'timeout': '0', 'time': '1548931547', 'device': 'WEBSOFTCLIENT', 'txId': '1548931547000100223', 'type': 'soft_button_escalate_request', 'id': '1167', 'location': ''}

theatrostats enteries for soft_button_accept:{'server_id': '286', 'remoteTxId': 'SMB-1548931547---127-0-0-1-CO', 'responderCount': '0', 'call_id': '1', 'listeners': '7569$7570$7576$7642', 'requestedCount': '1', 'user': '7677', 'timeout': '0', 'time': '1548931571', 'device': '0023a7347328', 'txId': '1548931547000100223', 'type': 'soft_button_escalate_accept', 'id': '1168', 'location': ''}
'''

def convert_to_str(dict1):
    return {k:str(v) for k,v in dict1.iteritems()}





@pytest.fixture(scope='module')
def clients():
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"true",
        "ENABLE_REG_BACKUP_ESCALATION":"true",
        "ENABLE_INDYME_ESCALATION":"true",
        "CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5",
        "ENABLE_SOFT_BUTTON_ESCALATION":"true",
        "SOFT_BUTTON_ESCALATION_START_TIME_SEC":"5",
        "REG_BACKUP_MODE":"2"
        })
    cii = make_clients(count=5)
    #print"making clinets"
    start_clients(cii)
    yield cii
    [x.log_off_json() for x in cii]
    #print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"false",
        "ENABLE_REG_BACKUP_ESCALATION":"false",
        "ENABLE_INDYME_ESCALATION":"false",
        "CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"30",
        "ENABLE_SOFT_BUTTON_ESCALATION":"true",
        "SOFT_BUTTON_ESCALATION_START_TIME_SEC":"30",
        "REG_BACKUP_MODE":"1"})

@pytest.mark.parametrize("rnr,users", [
    #('register_backup',RB_ESC_USERS),
    #('indyme',RB_ESC_USERS),
    ('soft_button',RB_ESC_USERS)
])
def test_stats(clients,rnr,users):
    expected_prompt_req=dict(RNR_REQ)
    expected_prompt_acpt=dict(RNR_ACC)
    cii=clients
    [x.log_off_json() for x in cii]
    for client,name in zip(cii,users):
        client.logon_name(name)
    a,b,c,d,e=cii
    d.make_engaged()
    wait(5)
    if rnr=='indyme':
        sshCommand('python2.7 /home/theatro/bin/indyme.py')
    elif rnr == 'soft_button':
        sshCommand('curl -X POST "http://localhost:9099/softbutton?location=CO&clear=false"')
    else:
        command=rnr.replace('_',' ')
        a.command(command)
    
    wait(20)
    b.command("copythat")
    cnt=1
    tx_id=False
    while cnt < 5 :
        try:
            tx_id=get_rnr_txid_from_tgs(rnr)
            cnt=cnt+1
        except Exception as e:
            assert False
    
    for user,name in zip(cii,users):
        user.id=get_emp_id(name)
        print "{0} 's id is {1}".format(name,user.id)

    if rnr=='indyme' or rnr=='soft_button':
        #print "inside if"
        expected_prompt_req['user']='-1'
        expected_prompt_req['device']='WEBSOFTCLIENT'
        if rnr=='indyme':expected_prompt_req['location']=expected_prompt_acpt['location']='east bay'
    else:
        expected_prompt_req['user']=cii[0].id
        expected_prompt_req['device']=cii[0].mac_address
        expected_prompt_req['location']=expected_prompt_acpt['location']=DEFAULT_ZONE
    

    expected_prompt_req['listeners']=expected_prompt_acpt['listeners']=[x.id for x in cii]
    expected_prompt_acpt['user']=cii[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'escalate_request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'escalate_accept'))
    print "theatrostats enteries for {0}_request:{1} \n".format(rnr,actual_op_request)
    print "theatrostats enteries for {0}_accept:{1} \n".format(rnr,actual_op_accept)
    #print "We got location as {0} for this feature{1}".format(expected_prompt_req['location'],rnr)
    #print RNR_REQ
    assert expected_prompt_req['user'] == actual_op_request['user']
    if rnr == 'indyme' or rnr == 'soft_button':
        assert expected_prompt_req['listeners'][0] in actual_op_request['listeners']
    else:
        assert expected_prompt_req['listeners'][0] not in actual_op_request['listeners']
    assert expected_prompt_req['listeners'][2] in actual_op_request['listeners']
    assert expected_prompt_req['listeners'][1] not in actual_op_request['listeners']
    assert expected_prompt_req['listeners'][4] not in actual_op_request['listeners']
    assert expected_prompt_req['listeners'][3] in actual_op_request['listeners']
    assert expected_prompt_req['server_id'] == actual_op_request['server_id']
    assert expected_prompt_req['responderCount'] == actual_op_request['responderCount']
    assert expected_prompt_req['requestedCount'] == actual_op_request['requestedCount']
    assert expected_prompt_req['device'] == actual_op_request['device']
    assert expected_prompt_acpt['server_id'] == actual_op_accept['server_id']
    assert expected_prompt_acpt['requestedCount'] == actual_op_accept['requestedCount']
    assert expected_prompt_acpt['user'] == actual_op_accept['user']
    assert expected_prompt_acpt['device'] == actual_op_accept['device']
    assert expected_prompt_acpt['listeners'][0] in actual_op_accept['listeners']
    assert expected_prompt_acpt['listeners'][2] in actual_op_accept['listeners']
    assert expected_prompt_acpt['listeners'][3] in actual_op_accept['listeners']
    assert expected_prompt_acpt['listeners'][1] not in actual_op_accept['listeners']
    assert expected_prompt_acpt['listeners'][4] in actual_op_accept['listeners']
    assert expected_prompt_req['location'] == actual_op_request['location']
    assert expected_prompt_acpt['location'] == actual_op_accept['location']
    if rnr == 'soft_button':
        assert 'SMB' in actual_op_request['remoteTxId']
    else:
        assert expected_prompt_req['remoteTxId'] == actual_op_request['remoteTxId']
    if rnr=='indyme':
        sshCommand('python2.7 /home/theatro/bin/indyme.py "east bay" clear')
    else:
        pass
    [x.log_off_json() for x in cii]



#theatrostats enteries for register_backup_request:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '2', 'listeners': '7576$7583$7585', 'requestedCount': '1', 'user': '7569', 'timeout': '1543999213', 'time': '1543999100', 'device': '0023a729eab1', 'txId': '1543999093000100223', 'type': 'register_backup_escalate_request', 'id': '730', 'location': ''}

#theatrostats enteries for register_backup_accept:{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '2', 'listeners': '7583$7585$7569$7563', 'requestedCount': '1', 'user': '7576', 'timeout': '1543999213', 'time': '1543999119', 'device': '0023a7347328', 'txId': '1543999093000100223', 'type': 'register_backup_escalate_accept', 'id': '731', 'location': ''}
import sqlite3,sys
from config import TGS_DB_PATH
from ssh_client import *


def to_unicode(unicode_or_str):
    if isinstance(unicode_or_str,str):
        value=unicode_or_str.decode('utf-8')
    else:
        value=unicode_or_str
    return value

def db_connect():
    conn = sqlite3.connect("{0}TGS.db".format(TGS_DB_PATH))

    with conn:
        #conn.row_factory = sqlite3.Row
        c = conn.cursor()
        return c

def get_values_from_tcm(ora_id):
    with SSHTunnelForwarder(('itg0.theatro.com', 22), ssh_password='4help2shoppers', ssh_username='theatro', remote_bind_address=('localhost', 3306)) as server:
       conn = None
       conn = MySQLdb.connect(host='127.0.0.1', port=server.local_bind_port, user='theatro', passwd='theatro',db='theatrostat')
       cursor = conn.cursor(MySQLdb.cursors.DictCursor)
       tables=cursor.execute('''select o.oration_id, o.device,o.user,o.command,o.oration_path,o.duration,o.call_id,o.listeners,o.server_id,o.call_type,o.TXId,oh.system_oration_id,oh.timeToPlay,tc.CompanyID,tc.StoreID,tc.source_user,tc.target_user,tc.target_group,tc.target_all,tc.start_time,tc.end_time,tc.end_cause,tc.start_time_epoch,tc.end_time_epoch,tc.orig_client_type,tc.attemptedTagout,tc.call_duration from Oration o join OrationHeard oh join TheatroCall tc on o.txId=tc.txId where tc.txId in (select txID from Oration where oration_id="{0}") limit 1;'''.format(ora_id))
       results = cursor.fetchall()
       return results[0]

def get_emp_id(name):
    c=db_connect()
    return str((c.execute('''SELECT employeeid from employees where TagOutName="{}";'''.format(name))).fetchall()[0][0])


def get_profile_id():
    c = db_connect()
    dict1 = {}
    [dict1.update({str(k): int(v)}) for k, v in c.execute(
        '''SELECT profile_name,profile_id from profile;''').fetchall()]
    return dict1

def get_names():
    c = db_connect()
    employee_names = c.execute('''SELECT TagOutName from employees where TagOutName !=(SELECT tagoutname from employee_profile_map where profile_id in (SELECT profile_id from profile where profile_name = "command_adv.grxml"));''')
    NAMES = [to_unicode(x[0]) for x in employee_names.fetchall()]
    #print NAMES
    adv_name = c.execute('''SELECT tagoutname from employee_profile_map where profile_id in (SELECT profile_id from profile where profile_name = "command_adv.grxml") limit 1;''')
    profile_id = [str(x[0]) for x in adv_name.fetchall()]
    NAMES = profile_id + NAMES
    return NAMES

def get_basic_users():
    c = db_connect()
    basic_name = c.execute(
        '''SELECT tagoutname from employee_profile_map where profile_id in (SELECT profile_id from profile where profile_name = "command_basic.grxml") limit 1;''')
    name = [str(x[0]) for x in basic_name.fetchall()]
    return name


#Getting prompts from DB
def get_feature_prompts(FeatureName="register backup",type=1):
    c = db_connect()
    dict1 = {}
    #if type==1:
        #[dict1.update({str(k):str(v)}) for k,v in c.execute('''select context_name,prompt from feature_prompt where feature_name = "{0}";'''.format(FeatureName)).fetchall()]
    #else:
    [dict1.update({str(k): str(v)}) for k, v in c.execute('''select context_name,prompt from feature_prompt where feature_name = "{0}" and sequence_id="{1}";'''.format(FeatureName,type)).fetchall()]
    return dict1

def get_help_me_option():
    c = db_connect()
    dict1 = {}
    [dict1.update({str(k): str(v)}) for k, v in c.execute(
        '''select pocket_guide_command,info_wave_file_name from audio_pocket_guide;'''.format(
            FeatureName)).fetchall()]
    return dict1

def get_help_list(id="command_adv.grxml",list_type="listcommands"):
    c = db_connect()
    dict1 = {}
    if list_type=="listcommands":
        [dict1.update({int(k): str(v)}) for k, v in c.execute("select priority, comp_name from apg_component_info where profile_id = ? and pocket_guide_command = ?;",(profile[id],list_type)).fetchall()]
    else:
        [dict1.update({int(k): str(v)}) for k, v in c.execute("select priority, comp_name from apg_component_info where profile_id = '0' and pocket_guide_command = ?;",(list_type,)).fetchall()]
    return dict1



def making_prompts(dict1,dict2):
    for i in dict1:
        dict1[i] = reduce(lambda a, kv: a.replace(*kv), dict2.iteritems(), dict1[i])
    return dict1


#Selecting MOD Group and user names
def get_MOD_USERS():
    c = db_connect()
    temp = c.execute('''SELECT EmployeeID from employeegroups where GroupID = (SELECT GroupID from alias_eligible_groups) limit 4;''')
    MOD_Emp_ID_List = [x[0] for x in temp.fetchall()]
    MOD_USERS = []
    for each in MOD_Emp_ID_List:
        temp = c.execute('''SELECT TagOutName from employees where EmployeeID = {0};'''.format(each))
        MOD_USERS.append([to_unicode(x[0]) for x in temp.fetchall()])
    MOD_USERS = [x[0] for x in MOD_USERS]
    temp=c.execute('''select TagOutName from employees where EmployeeID = (select EmployeeID from employeegroups where GroupID!=(select GroupID from alias_eligible_groups) limit 1);''')
    Non_MOD_MEMBER = [to_unicode(x[0]) for x in temp.fetchall()]
    #print MOD_USERS
    MOD_USERS=MOD_USERS+Non_MOD_MEMBER
    return MOD_USERS


def except_group_user(group_name):
    c = db_connect()
    temp = c.execute('''SELECT a.TagOutName from employees as a, employeegroups as b where a.EmployeeID = b.EmployeeID and b.GroupID = (select GroupID from groups where GName != "{0}") limit 5;'''.format(group_name))
    grp_users=[x[0] for x in temp.fetchall()]
    return grp_users

def select_group_user(group_name,count=5):
    c = db_connect()
    temp = c.execute('''SELECT a.TagOutName from employees as a, employeegroups as b where a.EmployeeID = b.EmployeeID and b.GroupID = (select GroupID from groups where GName = "{0}") limit {1};'''.format(group_name,count))
    grp_users=[x[0] for x in temp.fetchall()]
    return grp_users

def get_group_user_with_non_member(group_name,count=5):
    c = db_connect()
    temp = c.execute('''SELECT a.TagOutName from employees as a, employeegroups as b where a.EmployeeID = b.EmployeeID and b.GroupID = (select GroupID from groups where GName = "{0}") limit {1};'''.format(group_name,count))
    grp_users=[x[0] for x in temp.fetchall()]
    temp1 = c.execute(
        '''SELECT a.TagOutName from employees as a, employeegroups as b where a.EmployeeID = b.EmployeeID and b.GroupID = (select GroupID from groups where GName != "{0}") limit 1;'''.format(
            group_name))
    non_grp_user = [x[0] for x in temp.fetchall()]
    grp_users[-1] = non_grp_user[0]
    return grp_users

def get_rnr_group_user_with_non_member(feature_id,count=5):
    c = db_connect()
    temp = c.execute('''SELECT a.TagOutName from employees as a, employeegroups as b where a.EmployeeID = b.EmployeeID and b.GroupID =(SELECT group_id from feature_group_map where feature_id="{0}") limit {1};'''.format(feature_id,count))
    grp_users=[x[0] for x in temp.fetchall()]
    temp1 = c.execute(
        '''SELECT a.TagOutName from employees as a, employeegroups as b where a.EmployeeID = b.EmployeeID and b.GroupID != (SELECT group_id from feature_group_map where feature_id = "{0}") limit 1;'''.format(
            feature_id))
    non_grp_user = [x[0] for x in temp.fetchall()]
    grp_users[-1] = non_grp_user[0]
    return grp_users

def srd_db_update(start_time):
    [sshCommand(command='/home/theatro/bin/srd_update_db.sh "{0}"'.format(start_time))]
    print "updating DB with store readiness start time as {}\n".format(start_time)
    sshCommand('/home/theatro/bin/restart_tgs.sh')
    print "restarting TGS"


#group name for group call
Group_NAME="hunting"
ESC_Group_NAME="managers"
EXP_GNAME = "tracker"
EXP_USERS = select_group_user(EXP_GNAME)
SR_USERS = select_group_user("store readiness")
RB_USERS = get_group_user_with_non_member("register backup")
NON_RB_USER = except_group_user("register backup")
CO_USERS = get_group_user_with_non_member("carry out")
MA_USERS = get_group_user_with_non_member("manager assistance")
GUN_USERS = get_group_user_with_non_member("gun runner")
RB_ESC_USERS = ["andrea birdsong","mike diviney","joanna coffey","bev janes","angella hilt"]
ESC_USERS = select_group_user("register backup","3") + select_group_user("managers","2")
IND_ESC_USERS = select_group_user("register backup","2") + select_group_user("hunting","2") + select_group_user("managers","1")
MT_USERS = select_group_user("hunting")
#SB_USERS = get_group_user_with_non_member("soft button")
SB_USERS = ["donnie brady","aaron herzog","allen tippet","amanda scott","susan anderson"]
SRD_USERS = get_group_user_with_non_member("store readiness")
managers_USERS=get_group_user_with_non_member("managers")
MOD_USERS=get_MOD_USERS()
NAMES=get_names()
basic_user=get_basic_users()
profile= get_profile_id()
INDYME_USERS = get_rnr_group_user_with_non_member("10")
sws_users = ["donnie brady","amanda peterson","chris gist"]
GUN_ESC_USERS=select_group_user("gun runner","3") + select_group_user("managers","2")
EM_USERS=['kristin clark','sam figueroa','jr sutton','faith arnold','allen tippet','david jason','paul bradley','jessica raybourn','jose jose','joju pj','kesi nani','susan anderson']

#RNR features with feature_id
''' 
    RB Mode-1 = 1
    RB Mode-2 = 2
    Gun Runner = 3
    Manager Assistance = 4
    Carry out = 5
    Soft button = 6
    Store readiness = 7
    Click and pickup = 8
    Manager thank you = 9
    Indyme = 10
    Request group = 11

'''



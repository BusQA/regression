#!/usr/bin/env python
import pytest
import sqlite3
import requests
import argparse
import sys
import os
#from splinter import Browser
from selenium import webdriver
from config import *
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#from selenium import webdriver
#from selenium.webdriver.chrome.options import Options
from selenium.webdriver import ChromeOptions
from os import listdir
from config import driver_path
import datetime
from selenium.webdriver.support.ui import Select
import time

class AddUpdateDeleteCDM():
	def setup(self):
		try:
			chrome_options = ChromeOptions()
			#chrome_options.accept_untrusted_certs = True
			#chrome_options.assume_untrusted_cert_issuer = True
			chrome_options.add_argument("allow-file-access-from-files")
			chrome_options.add_argument("use-fake-device-for-media-stream")
			#chrome_options.add_argument("--disable-user-media-security=true")
			chrome_options.add_argument("use-fake-ui-for-media-stream")
			#opt.add_argument("--disable-extensions")
			chrome_options.add_argument("-disable-extensions")
			chrome_options.add_argument("-disable-infobars")
			chrome_options.add_argument("start-maximized")
			# Pass the argument 1 to allow and 2 to block
			chrome_options.add_experimental_option("prefs", { \
				"profile.default_content_setting_values.media_stream_mic": 1, 
    			"profile.default_content_setting_values.media_stream_camera": 1,
    			"profile.default_content_setting_values.geolocation": 1, 
    			"profile.default_content_setting_values.notifications": 1 
    			})
			self.browser = webdriver.Chrome(driver_path, chrome_options=chrome_options)
			self.browser.maximize_window()
			self.browser.get(url)

			#self.driver = webdriver.Chrome(chrome_options=opt, executable_path='/home/theatro/smoke_tool/TCM_Cases/chromedriver.exe')
			#self.driver.maximize_window()
			#self.driver.visit(url)

		except Exception as e:
			print "Exception while navigating to url"
			raise e


	def login_with_username_and_password(self,user=user,password=password):
		try:
			self.setup()
			WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'twLoginId')))

			if(self.browser.find_element_by_id('twLoginId').is_displayed()):		
				self.browser.find_element_by_name('twLoginId').send_keys(user)
				#self.browser.find_element_by_id('userVerify').click()
				time.sleep(4)
				self.browser.find_element_by_id('twPassword').send_keys(password)
				self.browser.find_element_by_id('loginbtn').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'go-to-new-cdm')))
			else:
				self.browser.quit()
				print "Unable to Navigate to Theatro portal login page"
		

			if(self.browser.find_element_by_id('go-to-new-cdm').is_displayed()):
				self.browser.find_element_by_id('go-to-new-cdm').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(("xpath", "//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']")))
				time.sleep(5)

			else:
				self.browser.quit()
				print "Unable to Navigate to role based login page"
		except Exception as e:
			self.browser.quit()
			raise e

	def tearDown(self):
		self.browser.quit()


	def add_cdm(self):
		try:
			if(self.browser.find_element_by_id('cdm-status-filter').is_displayed()):
				time.sleep(15)
				self.browser.find_element_by_xpath('''//*[@id="main-content"]/section/header//a''').click()
				self.browser.refresh()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-name')))
				self.browser.find_element_by_id('cdm-name').is_displayed()
				b=str(time.time()).split('.')[0]
				cdm = cdmname+b
				self.browser.find_element_by_id('cdm-name').send_keys(cdm)
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-category')))
				time.sleep(4)
				Select(self.browser.find_element_by_id('cdm-category')).select_by_visible_text('Recognition')
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="step-1"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', '''//*[@id="record"]/span[contains(text(),'Start Recording')]''')))
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Start Recording')]''').click()
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Stop Recording')]''').click()
				time.sleep(2)
				self.browser.find_element_by_xpath('''//*[@id="step-2"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', "//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]")))
				self.browser.find_element_by_xpath("//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]").click()
				self.browser.find_element_by_xpath('''//*[@id="step-3"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'immediate-cdm-schedule')))
				time.sleep(4)
				self.browser.find_element_by_id('immediate-cdm-schedule').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-action')))
				time.sleep(4)
				self.browser.find_element_by_id('cdm-action').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(("xpath", "//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']")))
				time.sleep(20)
				createdcdm = self.browser.find_element_by_xpath("//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']").text
				print(createdcdm)
				if(createdcdm==cdm):
					self.tearDown()
					return True
				else:
					self.tearDown()
					return False
			else:
				print "Unable to find the create CDM element in CDM Home Page"
				self.tearDown()
		except Exception as e:
			self.tearDown()
			raise e


	def add_cdm_send_to_allGroups(self):
		try :
			if(self.browser.find_element_by_id('cdm-status-filter').is_displayed()):
				time.sleep(15)
				self.browser.find_element_by_xpath('''//*[@id="main-content"]/section/header//a''').click()
				self.browser.refresh()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-name')))
				self.browser.find_element_by_id('cdm-name').is_displayed()
				b=str(time.time()).split('.')[0]
				cdm = cdmname+b
				self.browser.find_element_by_id('cdm-name').send_keys(cdm)
				Select(self.browser.find_element_by_id('cdm-category')).select_by_visible_text('Recognition')
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="step-1"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', '''//*[@id="record"]/span[contains(text(),'Start Recording')]''')))
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Start Recording')]''').click()
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Stop Recording')]''').click()
				time.sleep(2)
				self.browser.find_element_by_xpath('''//*[@id="step-2"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', "//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]")))
				self.browser.find_element_by_xpath("//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]").click()
				time.sleep(3)
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'select-all-users')))
				self.browser.find_element_by_id('select-all-users').click()
				self.browser.find_element_by_xpath('''//*[@id="step-3"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'immediate-cdm-schedule')))
				time.sleep(4)
				self.browser.find_element_by_id('immediate-cdm-schedule').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-action')))
				time.sleep(4)
				self.browser.find_element_by_id('cdm-action').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(("xpath", "//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']")))
				time.sleep(20)
				createdcdm = self.browser.find_element_by_xpath("//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']").text
				print(createdcdm)
				if(createdcdm==cdm):
					self.tearDown()
					return True
				else:
					self.tearDown()
					return False
			else:
				print "Unable to find the create CDM element in CDM Home Page"
				self.tearDown()
		except Exception as e:
			self.tearDown()
			raise e


	def add_cdm_send_to_singleGroup(self):
		try:
			if(self.browser.find_element_by_id('cdm-status-filter').is_displayed()):
				time.sleep(5)
				self.browser.find_element_by_xpath('''//*[@id="main-content"]/section/header//a''').click()
				self.browser.refresh()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-name')))
				self.browser.find_element_by_id('cdm-name').is_displayed()
				b=str(time.time()).split('.')[0]
				cdm = cdmname+b
				self.browser.find_element_by_id('cdm-name').send_keys(cdm)
				Select(self.browser.find_element_by_id('cdm-category')).select_by_visible_text('Recognition')
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="step-1"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', '''//*[@id="record"]/span[contains(text(),'Start Recording')]''')))
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Start Recording')]''').click()
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Stop Recording')]''').click()
				time.sleep(2)
				self.browser.find_element_by_xpath('''//*[@id="step-2"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', "//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]")))
				self.browser.find_element_by_xpath("//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]").click()
				time.sleep(3)
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'select-all-users')))
				self.browser.find_element_by_id('select-all-users').click()
				time.sleep(3)
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'select-all-static-groups')))
				self.browser.find_element_by_id('select-all-static-groups').click()
				time.sleep(5)
				element = self.browser.find_element_by_xpath("//*[@id='static-groups-list']//label[contains(.,"+"'"+groupName+"'"+")]")
				self.browser.execute_script("return arguments[0].scrollIntoView(true);", element)
				time.sleep(5)
				self.browser.execute_script("scrollBy(0,-250);")
				time.sleep(4)
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', "//*[@id='static-groups-list']//label[contains(.,"+"'"+groupName+"'"+")]")))
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="static-groups-list"]//label/input[@value='''+"'"+groupName+"'"+''']''').click()
				self.browser.find_element_by_xpath('''//*[@id="step-3"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				time.sleep(7)
				element_one = self.browser.find_element_by_id('immediate-cdm-schedule')
				#self.browser.execute_script("return arguments[0].scrollIntoView(false);", element_one)
				element_one.send_keys(Keys.HOME)
				time.sleep(3)
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'immediate-cdm-schedule')))
				self.browser.find_element_by_id('immediate-cdm-schedule').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-action')))
				time.sleep(4)
				self.browser.find_element_by_id('cdm-action').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(("xpath", "//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']")))
				time.sleep(20)
				createdcdm = self.browser.find_element_by_xpath("//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']").text
				print(createdcdm)
				if(createdcdm==cdm):
					self.tearDown()
					return True
				else:
					self.tearDown()
					return False
			else:
				print "Unable to find the create CDM element in CDM Home Page"
				self.tearDown()
		except Exception as e:
			self.tearDown()
			raise e


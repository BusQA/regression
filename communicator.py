# coding=utf-8
import threading
import multiprocessing
import time
import socket
import json
import ast # only used to decode networks message response and voice response from TGS
import random
import Queue
import requests
import config
import tts
import logger
import re
from splinter import Browser
from DB_HANDLE import to_unicode
from requests.exceptions import ConnectionError


##############################################################################

# conditional base class for communicators

# decides base class for implementation of communicators.
# very easily implemented because both libraries use the same API

if config.SCHEME == 'threads':
    communicator_class = threading.Thread
    event_class        = threading.Event
elif config.SCHEME == 'processes':
    communicator_class = multiprocessing.Process
    event_class        = multiprocessing.Event
updated_url = config.url + '/#employees/{0}/{1}'.format(config.COMPANY_ID, config.STORE_ID)
##############################################################################

# settings and constants

CLI_PORTS = {'data':        41101
            ,'auth':        41103
            ,'response':    50008# play port voice from TGS
            }
TGS_PORTS = {'auth':        41102
            ,'command':     50009
            ,'play_credits':50007 # resource info
            ,'clues':       50011# locations
            ,'simulator':   50010
            }
BUTTONS =   ['menubutton'   # primary
            ,'querybutton'  # broadcast
            ]

BUTTON_STATES = ['down'
                ,'up'
                ]

#############################################################################
class Communicator(communicator_class): # communicator_class depends on configuration
    def __init__(self, mac_address, ip_address, tgs_ip_address, name):
        communicator_class.__init__(self)
        # set all properties
        self.finished       = event_class()
        self.mac_address    = mac_address
        self.ip_address     = ip_address
        self.tgs_ip_address = tgs_ip_address
        self.name           = name
        self.fname = name.split()[0]
        self.lname=name.split()[1]
        self.id=12345
        self.gname=''
        self.logger         = logger.get_logger(self.name, stdout=config.STDOUT_LOG, stdout_level=config.STDOUT_LOG_LEVEL, file_level=config.FILE_LOG_LEVEL)
        # make all sockets
        self.response_socket= self.make_socket('response')
        self.auth_socket    = self.make_socket('auth')
        self.data_socket    = self.make_socket('data')
        # set time intervals
        self.auth_interval  = 5
        self.play_credit_interval = 5
        self.clues_interval  = 5
        # clues
        #self.clues           = self.read_clues(config.CLUES_FILE)
        # response holder
        self.last_response_oruid = Queue.Queue()
        self.response_queue  = Queue.Queue()
        self.last_response   = Queue.Queue()
        self.soundout = Queue.Queue()
        self.soundout = "no prompt revived"
        self.last_message="no prompt revived"
        self.last_voice_out  = Queue.Queue()
        self.is_logged_on_as = Queue.Queue()
        # states
        self.is_logged_on   = event_class()
        self.is_available   = event_class()
        self.is_busy        = event_class()

    def run(self):
        self.auth_thread = threading.Thread(name='auth', target=self.keep_authenticating)
        #self.auth_thread.setDaemon(True)
        self.auth_thread.start()
        #self.logger.critical('auth thread started...')

        self.play_credits_thread = threading.Thread(name='play_credits', target=self.keep_sending_play_credits)
        #self.play_credits_thread.setDaemon(True)
        self.play_credits_thread.start()
        #self.logger.critical('play credits thread started...')

        #self.clues_thread = threading.Thread(name='clues', target=self.keep_sending_clues)
        #self.clues_thread.setDaemon(True)
        #self.clues_thread.start()
        #self.logger.critical('clues thread started...')

        self.response_thread = threading.Thread(name='response', target=self.keep_processing_responses)
        #self.response_thread.setDaemon(True)
        self.response_thread.start()
        #self.logger.critical('response thread started...')

        # keep receiving the processsing a response, if any
        while not self.finished.is_set():
            #print "inside the loop"
            try:
                response = self.response_socket.recv(1024)
                #response = self.response_socket.recvfrom(1024, socket.MSG_DONTWAIT)
                #print "insidr"
                #print response
            except:
                time.sleep(0.2)
            else:
                self.response_queue.put_nowait((response))


    def _make_response(self, msg):
        msg = msg.lstrip('_')  # If not logged in, the username starts with an underscore
        msg = ' '.join(msg.lstrip('tts_').split('|')[:-1]).replace('_', ' ').rstrip(',')
        return msg

    def keep_processing_responses(self):
        while not self.finished.is_set():
            if self.response_queue.empty():
                time.sleep(0.2)
            else:
                response = self.response_queue.get()
                #self.logger.info('received response of type {0}'.format(response))
                self.handle_response(response)
                self.handle_soundout(response)

    def _extract_header_from_response(self, response):
        header_string = response[response.find('{'):response.find('}')+1]
        header = ast.literal_eval(header_string)
        #print header
        return header

    def handle_soundout(self,response):
        header = self._extract_header_from_response(response)
        try:
            soundout = header['resourcename']
            self.soundout = soundout
            print '{2}:TGS--{1}--> {0}'.format(self.name,self.soundout,str(time.strftime("%H:%M:%S")))

        except KeyError:
            pass
            #self.logger.error('received unknown header of type {0}'.format(header))
        else:
            self.last_response.put_nowait(response)
            # self.logger.critical('last response is : {0}\n'.format(self.last_response))

    def handle_response(self, response):
        header = self._extract_header_from_response(response)
        ##################################################################
        # the last packet by specification has to have length = 0
        # but right now there is a bug in the system where
        # the last voice call data packet is not renumbered for length = 0 by TGS
        # therefore, we need to use only num = 0 as a delimiter for the incoming packet stream
        ##################################################################
        
        try:
            oruid = header['oruid']
            #print oruid

        except KeyError:
            pass
            #self.logger.error('received unknown header of type {0}'.format(header))
        else:
            if (int(header['num']) == 0)  :
                self.process_response_header(header)
                #print "processing header for message : {0}".format(header)
                print '{2}:TGS--{1}--> {0}'.format(self.name, str(self.output()),str(time.strftime("%H:%M:%S")))
                self.last_response.queue.clear()
            if (int(header['length']) == 0):
                #print '{2}:TGS--play complete{1}--> {0}'.format(self.name, str(self.output()),str(time.strftime("%H:%M:%S")))
                pass
            self.last_response.put_nowait(response)
            #self.logger.info('last response is : {0}\n'.format(header))

    def process_response_header(self, response_header):
        #self.logger.critical('processing response header of type {0}'.format(response_header))
        oruid = response_header['oruid'].lower()
        self.last_response_oruid = oruid
        #self.last_recieved_packet_number = response_header['num']
        #print oruid
        if ('tts_' not in oruid) or ('theatro_smokesuite' not in oruid): 
            self.last_message = oruid
        if 'tts_welcome' in oruid:
            self.is_logged_on.set()
            #self.logger.info('{0} LOGGED ON | GOT ORUID {1}'.format(self.name, oruid))
            self.is_logged_on_as.put_nowait(oruid)
        elif 'tts_okay_goodbye' in oruid:
            self.is_logged_on.clear()
        elif 'tts_available' in oruid:
            self.is_available.set()
        elif 'tts_engaged' in oruid:
            self.is_available.clear()
        elif 'ahead' in oruid:
            self.is_busy.set()
        elif 'ended' in oruid or 'dropped' in oruid or 'posted' in oruid:
            self.is_busy.clear()

    #pattern for oruid
    global pattern,p2
    pattern=re.compile(r',\|_[0-9]{1,3}_.*')

    def output(self):
        try:
            prompt=pattern.sub('',self.last_response_oruid)
            dict2 = config.PATTERN_DLT
            return reduce(lambda a, kv: a.replace(*kv), dict2.iteritems(), prompt)
        except:
            return "No prompt recived"

    def i_wait(self,prompt,sleep=10,exact_match=False):
        count=1
        flag = True
        if exact_match:
            
            while prompt != self.output():
                print "prompt:"+prompt
                time.sleep(0.5)
                print self.output()
                count = count +1
                if count == sleep:
                    flag=False
                    print "Expected: "+prompt
                    print "Recieved: "+self.output()
                    break
            return flag

        while prompt not in self.output() :
            time.sleep(0.5)
            count = count +1
            if config.IS_DEBUG:
                print str(time.strftime("%H:%M:%S"))+"Expected: "+self.name+":"+prompt
                print str(time.strftime("%H:%M:%S"))+"Recieved: "+self.name+":"+self.output()
            if count == sleep:
                flag=False
                print "Expected: " + self.name +":"+ prompt
                print "Recieved: " + self.name +":"+ self.output()
                break
        return flag

    def i_long_prompt_wait(self,prompt,sleep=10,exact_match=False):
        count=1
        flag = True
        if exact_match:
            
            while prompt != self.output():
                print "prompt:"+prompt
                time.sleep(0.5)
                print self.output()
                count = count +1
                if count == sleep:
                    flag=False
                    print "Expected: "+prompt
                    print "Recieved: "+self.output()
                    break
            return flag

        while self.output() not in prompt:
            time.sleep(0.5)
            count = count +1
            if config.IS_DEBUG:
                print str(time.strftime("%H:%M:%S"))+"Expected: "+self.name+":"+prompt
                print str(time.strftime("%H:%M:%S"))+"Recieved: "+self.name+":"+self.output()
            if count == sleep:
                flag=False
                print "Expected: " + self.name +":"+ prompt
                print "Recieved: " + self.name +":"+ self.output()
                break
        return flag

    def i_so_wait(self, prompt, sleep=10, exact_match=False):
        count = 1
        flag = True
        if exact_match:

            while prompt != self.soundout:
                print "prompt:" + prompt
                time.sleep(0.5)
                #print self.output()
                count = count + 1
                if count == sleep:
                    flag = False
                    break
            return flag

        while prompt not in self.soundout:
            time.sleep(0.5)
            count = count + 1
            if count == sleep:
                print "Expected: "+prompt
                print "Recieved: "+self.soundout
                flag = False
                break
        return flag


    def get_last_voice_out(self):
        return list(self.last_voice_out.queue)

    def get_logged_on_as(self):
        return list(self.is_logged_on_as.queue)
    def response_time(self):
        response_got = self.last_response_oruid()
        while ( self.last_response_oruid() == response_got):
            time.sleep(0.1)
            print self.last_response_oruid()
    
    def die(self):
        self.logger.critical('dieing now...')
        self.finished.set()
        self.response_socket.shutdown(socket.SHUT_RDWR)
        self.auth_socket.shutdown(socket.SHUT_RDWR)
        self.data_socket.shutdown(socket.SHUT_RDWR)
        self.response_socket.close()
        self.auth_socket.close()
        self.data_socket.close()
        self.join()

    def read_clues(self, clues_file):
        with open(clues_file, 'r') as in_file:
            clues = [json.loads(line.strip()) for line in in_file.readlines()[:100]]
        #self.logger.info('clues read...')
        return clues

    def make_socket(self, port_key): # port_key in one of the keys from CLI_PORTS dictionary
        new_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        new_socket.setblocking(False)
        new_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        new_socket.bind((self.ip_address, CLI_PORTS[port_key]))
        #self.logger.info('bound socket for {0}'.format(port_key))
        return new_socket

    def move_button(self, button, direction):
        payload =   {'ver': 2
                    ,'mac': self.mac_address
                    ,'time': self.make_time_string(time.time())
                    ,'button': button
                    ,'num': direction
                    ,'length': 0
                    }
        self.data_socket.sendto(json.dumps(payload), (self.tgs_ip_address, TGS_PORTS['command']))
        #self.logger.critical('sent button: {0}, direction: {1} to {2}:{3}'.format(button, direction, self.tgs_ip_address, TGS_PORTS['command']))

    def make_time_string(self, timestamp):
        time_string = repr(timestamp)
        return time_string[:time_string.find('.')]

    def speak(self, button, text):
        payload =   {'mac': self.mac_address
                    ,'button': button
                    }
        for idx, audio in enumerate(tts.yield_speech(text)):
            payload.update({'num': idx + 1, 'time': self.make_time_string(time.time())})
            # the payload header should have "length" field as the last field
            # for the TGS decoder to successfully decode the message
            payload_string = json.dumps(payload)[:-1] + ', "length": ' + repr(len(audio)) + '}'
            self.data_socket.sendto(payload_string + audio, (self.tgs_ip_address, TGS_PORTS['command']))
            # keep a copy of everything outgoing in a queue for possible verification
            self.last_voice_out.put_nowait(payload_string + audio)
            time.sleep(0.025) # 25 millisecond delay between each packet is expected.
        #self.logger.critical('sent speech: {0} to {1}:{2}'.format(text, self.tgs_ip_address, TGS_PORTS['command']))


    def keep_authenticating(self):
        #{"cmd":"auth","ver":2,"mac":"d487d8556633","svnver":"Unversioned directory_0","time":"254402625","volume":0,"battery":28,"chargestat":0,"batterytemp":0}
        if config.WB_SUPPORT:
            auth_message =  {'cmd': 'auth'
                        ,'ver': 2
                        ,'mac': self.mac_address
                        ,'svnver': 'exported_9'
                        ,'time': self.make_time_string(time.time())
                        ,'volume': 70
                        ,'battery': 2100
                        ,'chargestat': 0
                        ,'batterytemp': 12.5
                        ,'SR':"16"
                        ,'capacity':255
                        }
        else:
            auth_message =  {'cmd': 'auth'


                        ,'ver': 2
                        ,'mac': self.mac_address
                        ,'svnver': 'exported_9'
                        ,'time': self.make_time_string(time.time())
                        ,'volume': 70
                        ,'battery': 2100
                        ,'chargestat': 0
                        ,'batterytemp': 12.6
                        ,'capacity':255
                        }
        while not self.finished.is_set():
            auth_message.update({'time': self.make_time_string(time.time())})
            self.auth_socket.sendto(json.dumps(auth_message), (self.tgs_ip_address, TGS_PORTS['auth']))
            #self.logger.debug('auth request sent: {0} to {1}:{2}'.format(auth_message, self.tgs_ip_address, TGS_PORTS['auth']))
            try:
                response, remote_address = self.auth_socket.recv(1024)
            except:
                #self.logger.warning('auth response not received')
                pass
            else:
                #self.logger.debug('auth response received: {0} from {1}:{2}'.format(response, remote_address[0], remote_address[1]))
                # this is used only because the networks message response to auth_message is not JSON. It has "True".
                response = ast.literal_eval(response)
                #self.update_play_credit_interval(response['PlayCreditInterval']/100.0)
            time.sleep(self.auth_interval)

    def keep_sending_play_credits(self):
        seqno = 0
        while not self.finished.is_set():
            credit_message =    {'ver': 2
                                ,'mac': self.mac_address
                                ,'available': 100
                                ,'cmd': 'playcredit'
                                ,'seqno': seqno
                                }
            self.data_socket.sendto(json.dumps(credit_message), (self.tgs_ip_address, TGS_PORTS['play_credits']))
            #self.logger.debug('play credit sent: {0} to {1}:{2}'.format(credit_message, self.tgs_ip_address, TGS_PORTS['play_credits']))
            seqno += 1
            time.sleep(self.play_credit_interval)
           # print self.play_credit_interval

    def keep_sending_clues(self):
        clues_message = {'mac': self.mac_address
                        ,'ver': 2
                        ,'cmd': 'clue'
                        }
        while not self.finished.is_set():
            for clue in self.clues:
                clues_message['clue'] = clue['clues']
                clues_message['myrssi'] = clue['myrssi']
                clues_message['mybssid'] = clue['mybssid']
                self.data_socket.sendto(json.dumps(clues_message), (self.tgs_ip_address, TGS_PORTS['clues']))
                #self.logger.debug('clues sent: {0} to {1}:{2}'.format(clues_message, self.tgs_ip_address, TGS_PORTS['clues']))
                time.sleep(self.clues_interval)

    def update_play_credit_interval(self, interval):
        # the tgs server sends the play credit interval in milliseconds / 10
        # the real communicator code has been frozen.
        # It multiplies the play credit interval by 10 before applying it.
        self.play_credit_interval = interval # this gives is number of seconds
    def say_without_delay(self, button, text_list,skipBtnDn=True,skipBtnUp=True):
        button_down_sent = False
        text_list=[to_unicode(x) for x in text_list]
        #self.move_button(button, 'down')
        #time.sleep(0.6)
        # clear out last_voice_out queue so that speak func can put all fresh payloads there
        self.last_voice_out.queue.clear()
        #for each_text in text_list:
            #self.speak(button, each_text)
        payload =   {'mac': self.mac_address
                    ,'button': button
                    }
        #self.logger.info('{0}------{1}-----> TGS'.format(self.name,text_list))
        print '{2}:{0}------{1}-----> TGS'.format(self.name,str(text_list[0]),str(time.strftime("%H:%M:%S")))
        for idx, audio in enumerate(tts.yield_speech(text_list)):
            #Check whether sending Button event without Dutton Down is enabled
            if not skipBtnDn:
                button_down_sent = True
            if not button_down_sent:
                self.move_button(button, 'down')
                #time.sleep(1)
                button_down_sent = True
            t1 = time.time()*1000 #muliplieng with 100 to get 12 digit accurate time
            payload.update({'num': idx, 'time': self.make_time_string(t1)})
            #print t1
            # the payload header should have "length" field as the last field
            # for the TGS decoder to successfully decode the message
            payload_string = json.dumps(payload)[:-1] + ', "length": ' + repr(len(audio)) + '}'
            self.data_socket.sendto(payload_string + audio, (self.tgs_ip_address, TGS_PORTS['command']))
            # keep a copy of everything outgoing in a queue for possible verification
            self.last_voice_out.put_nowait(payload_string + audio)
            time.sleep(0.025) # 25 millisecond delay between each packet is expected.
        #self.logger.info('sent speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        #time.sleep(0.6)
        #Check whether Button Up flag is True, If yes then only send Button up
        if skipBtnUp:
            self.move_button(button, 'up')

    
    def say(self, button, text_list,skipBtnDn=True,skipBtnUp=True):
        button_down_sent = False
        text_list=[to_unicode(x) for x in text_list]
        #self.move_button(button, 'down')
        time.sleep(0.6)
        # clear out last_voice_out queue so that speak func can put all fresh payloads there
        self.last_voice_out.queue.clear()
        if self.output() == "No prompt recived":
            self.make_available()
        #for each_text in text_list:
            #self.speak(button, each_text)
        payload =   {'mac': self.mac_address
                    ,'button': button
                    }
        #self.logger.info('{0}------{1}-----> TGS'.format(self.name,text_list))
        print '{2}:{0}------{1}-----> TGS'.format(self.name,text_list,str(time.strftime("%H:%M:%S")))
        for idx, audio in enumerate(tts.yield_speech(text_list)):
            #Check whether sending Button event without Dutton Down is enabled
            if not skipBtnDn:
                button_down_sent = True
            if not button_down_sent:
                self.move_button(button, 'down')
                time.sleep(1)
                button_down_sent = True
            t1 = time.time()*1000 #muliplieng with 100 to get 12 digit accurate time
            payload.update({'num': idx, 'time': self.make_time_string(t1)})
            #print t1
            # the payload header should have "length" field as the last field
            # for the TGS decoder to successfully decode the message
            payload_string = json.dumps(payload)[:-1] + ', "length": ' + repr(len(audio)) + '}'
            self.data_socket.sendto(payload_string + audio, (self.tgs_ip_address, TGS_PORTS['command']))
            # keep a copy of everything outgoing in a queue for possible verification
            self.last_voice_out.put_nowait(payload_string + audio)
            time.sleep(0.025) # 25 millisecond delay between each packet is expected.
        #self.logger.info('sent speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        time.sleep(0.6)
        #Check whether Button Up flag is True, If yes then only send Button up
        if skipBtnUp:
            self.move_button(button, 'up')

    def say_ploss(self, button, text_list,skipBtnDn=True,skipBtnUp=True):
        button_down_sent = False
        text_list=[to_unicode(x) for x in text_list]
        #self.move_button(button, 'down')
        time.sleep(0.6)
        # clear out last_voice_out queue so that speak func can put all fresh payloads there
        self.last_voice_out.queue.clear()
        #for each_text in text_list:
            #self.speak(button, each_text)
        payload =   {'mac': self.mac_address
                    ,'button': button
                    }
        #self.logger.info('{0}------{1}-----> TGS'.format(self.name,text_list))
        print '{2}:{0}------{1}-----> TGS'.format(self.name,text_list,str(time.strftime("%H:%M:%S")))
        for idx, audio in enumerate(tts.yield_speech(text_list)):
            #Check whether sending Button event without Dutton Down is enabled
            if not skipBtnDn:
                button_down_sent = True
            if not button_down_sent:
                self.move_button(button, 'down')
                time.sleep(1)
                button_down_sent = True
            t1 = time.time()*1000 #muliplieng with 100 to get 12 digit accurate time
            if  100 <idx < 200:
                print "not sending packets"
            else:
                payload.update({'num': idx, 'time': self.make_time_string(t1)})
                #print idx
                # the payload header should have "length" field as the last field
                # for the TGS decoder to successfully decode the message
                payload_string = json.dumps(payload)[:-1] + ', "length": ' + repr(len(audio)) + '}'
                self.data_socket.sendto(payload_string + audio, (self.tgs_ip_address, TGS_PORTS['command']))
                # keep a copy of everything outgoing in a queue for possible verification
                self.last_voice_out.put_nowait(payload_string + audio)
            
            time.sleep(0.025) # 25 millisecond delay between each packet is expected.
        #self.logger.info('sent speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        time.sleep(0.6)
        #Check whether Button Up flag is True, If yes then only send Button up
        if skipBtnUp:
            self.move_button(button, 'up')
            
    def ma_say(self, button, text_list,skipBtnDn=True,skipBtnUp=True):
        button_down_sent = False
        #self.move_button(button, 'down')
        time.sleep(0.6)
        # clear out last_voice_out queue so that speak func can put all fresh payloads there
        self.last_voice_out.queue.clear()
        #for each_text in text_list:
            #self.speak(button, each_text)
        payload =   {'mac': self.mac_address
                    ,'button': button
                    }
        self.logger.info('{0}------{1}-----> TGS'.format(self.name,text_list))
        print '{0}------{1}-----> TGS'.format(self.name,text_list)
        for idx, audio in enumerate(tts.yield_speech(text_list)):
            #Check whether sending Button event without Dutton Down is enabled
            if not skipBtnDn:
                button_down_sent = True
            if not button_down_sent:
                self.move_button(button, 'down')
                time.sleep(1)
                button_down_sent = True
            t1 = time.time()*1000 #muliplieng with 100 to get 12 digit accurate time
            payload.update({'num': idx, 'time': self.make_time_string(t1)})
            #print t1
            # the payload header should have "length" field as the last field
            # for the TGS decoder to successfully decode the message
            payload_string = json.dumps(payload)[:-1] + ', "length": ' + repr(len(audio)) + '}'
            self.data_socket.sendto(payload_string + audio, (self.tgs_ip_address, TGS_PORTS['command']))
            # keep a copy of everything outgoing in a queue for possible verification
            self.last_voice_out.put_nowait(payload_string + audio)
            time.sleep(0.025) # 25 millisecond delay between each packet is expected.
        self.logger.info('sent speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        time.sleep(0.6)
        #Check whether Button Up flag is True, If yes then only send Button up
        if skipBtnUp:
            self.move_button(button, 'up')


    def say_DnLoss(self, button, text_list):
        button_down_sent = False
        #self.move_button(button, 'down')
        time.sleep(0.6)
        # clear out last_voice_out queue so that speak func can put all fresh payloads there
        self.last_voice_out.queue.clear()
        #for each_text in text_list:
            #self.speak(button, each_text)
        payload =   {'mac': self.mac_address
                    ,'button': button
                    }
        self.logger.critical('sending speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        for idx, audio in enumerate(tts.yield_speech(text_list)):
            #Check whether sending Button event without Dutton Down is enabled
            if not skipBtnDn:
                button_down_sent = True
            if not button_down_sent:
                self.move_button(button, 'down')
                time.sleep(1)
                button_down_sent = True
            t1 = time.time()*1000 #muliplieng with 100 to get 12 digit accurate time
            payload.update({'num': idx, 'time': self.make_time_string(t1)})
            #print t1
            # the payload header should have "length" field as the last field
            # for the TGS decoder to successfully decode the message
            payload_string = json.dumps(payload)[:-1] + ', "length": ' + repr(len(audio)) + '}'
            self.data_socket.sendto(payload_string + audio, (self.tgs_ip_address, TGS_PORTS['command']))
            # keep a copy of everything outgoing in a queue for possible verification
            self.last_voice_out.put_nowait(payload_string + audio)
            time.sleep(0.025) # 25 millisecond delay between each packet is expected.
        self.logger.critical('sent speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        time.sleep(1)
        #Check whether Button Up flag is True, If yes then only send Button up
        if skipBtnUp:
            self.move_button(button, 'up')

    def up_loss(self, button, text_list):
        button_down_sent = False
        #self.move_button(button, 'down')
        time.sleep(0.6)
        # clear out last_voice_out queue so that speak func can put all fresh payloads there
        self.last_voice_out.queue.clear()
        #for each_text in text_list:
            #self.speak(button, each_text)
        payload =   {'mac': self.mac_address
                    ,'button': button
                    }
        self.logger.critical('sending speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        for idx, audio in enumerate(tts.yield_speech(text_list)):
            if not button_down_sent:
                self.move_button(button, 'down')
                button_down_sent = True
            t1 = time.time()*1000 #muliplieng with 100 to get 12 digit accurate time
            payload.update({'num': idx, 'time': self.make_time_string(t1)})
            #print t1
            # the payload header should have "length" field as the last field
            # for the TGS decoder to successfully decode the message
            payload_string = json.dumps(payload)[:-1] + ', "length": ' + repr(len(audio)) + '}'
            self.data_socket.sendto(payload_string + audio, (self.tgs_ip_address, TGS_PORTS['command']))
            # keep a copy of everything outgoing in a queue for possible verification
            self.last_voice_out.put_nowait(payload_string + audio)
            time.sleep(0.025) # 25 millisecond delay between each packet is expected.
        self.logger.critical('sent speech: {0} to {1}:{2}'.format(text_list , self.tgs_ip_address, TGS_PORTS['command']))
        time.sleep(0.6)

    def dn_loss(self, button, text_list):
        time.sleep(0.6)
        # clear out last_voice_out queue so that speak func can put all fresh payloads there
        self.last_voice_out.queue.clear()
        for each_text in text_list:
            self.speak(button, each_text)
        time.sleep(0.6)
        self.move_button(button, 'up')

    
    def log_on(self):
        self.say('menubutton', ['logon', self.name])
        time.sleep(5)

    def log_on_json(self):
        payload = '''{"ver":2,"mac":"'''+self.mac_address+'''","time":"'''+self.make_time_string(time.time())+'''","button":"logon","num":"'''+self.name+'''","length":0}'''
        self.data_socket.sendto(payload, (self.tgs_ip_address, TGS_PORTS['command']))
        time.sleep(1)

    def log_off(self):
        self.say('menubutton', ['logoff'])
        self.is_logged_on_as.queue.clear()

    def log_off_json(self):
        if not self.finished.is_set():
            payload = '''{"ver": 2, "mac": "'''+self.mac_address+'''", "time": "'''+self.make_time_string(time.time())+'''", "button": "logoff", "num": "down", "length": 0}'''
            self.data_socket.sendto(payload, (self.tgs_ip_address, TGS_PORTS['command']))
            time.sleep(0.5)

    def identify(self):
        self.say('menubutton', ['identify'])

    def message(self,client):
        self.say('menubutton', ['message', client])
        time.sleep(3)
        if client + ",go_ahead" in self.output():
            self.command("1to10")
            time.sleep(2)
    def cmd(self,command,client):
        self.say('menubutton', [command, client])
        time.sleep(3)
        if client + ",go_ahead" in self.output():
            self.command("1to10")
            time.sleep(2)
    def cmd1(self,command,client):
        self.say('menubutton', [command, client])
        time.sleep(2)
    def rb(self,number):
        self.say('menubutton', ["registerbackup", number])
        time.sleep(2)

    def make_engaged(self):
        while 'engaged' not in self.output():
            self.tap_out()
            time.sleep(1)
    def make_available(self):
        while 'available' not in self.output():
            self.tap_out()
            time.sleep(1)
            
    def chatin(self, client, text):
        self.say('menubutton', ['chatin', client])
        time.sleep(2)
        if self.is_busy.is_set():
            self.say('menubutton', [text])
        self.is_busy.clear()

    def announcement(self, ttl, text): # ttl can be any of ['now', 'today']
        self.say('menubutton',['announcement{}'.format(ttl)])
        #self.say('menubutton', ['announcement', ttl])
        time.sleep(2)
        if self.is_busy.is_set():
            self.say('menubutton', [text])
        self.is_busy.clear()


    def group_announcement(self, ttl,group,text): # ttl can be any of ['now', 'today']
        self.say('menubutton', ['announcement', ttl, 'for', group])
        time.sleep(2)
        if self.is_busy.is_set():
            self.say('menubutton', [text])
        self.is_busy.clear()


    def addtogroup(self, client, group): # group can be any of ['managers', 'cash register backup']
        self.say('menubutton', ['add',' ',client,'to',' ',group])
        time.sleep(5)
        self.is_busy.clear()



    def broadcast(self , text):
        time.sleep(1)
        self.say('querybutton', [text])

    def menu(self , text):
        time.sleep(1)
        self.say('menubutton', [text])

    def broacast_up_loss(self, text):
        time.sleep(1)
        self.up_loss('querybutton', [text])

    def broacast_dn_loss(self, text):
        time.sleep(1)
        self.dn_loss('querybutton', [text])

    def Interruptall_button_up_loss(self , text):
        self.say('menubutton', ['interruptall'])
        time.sleep(2)
        self.up_loss('menubutton', [text])


    def tap_out(self):
        print '{1}:{0}------TAP_OUT-----> TGS'.format(self.name,str(time.strftime("%H:%M:%S")))
        self.move_button('menubutton', 'down')
        time.sleep(0.2)
        self.move_button('menubutton', 'up')

    def announcement_uloss(self, ttl, text): # ttl can be any of ['now', 'today']
        self.say('menubutton', ['announcement', ttl])
        time.sleep(2)
        if self.is_busy.is_set():
            self.up_loss('menubutton', [text])
        self.is_busy.clear()
    def Hello(self,user):
        self.say('menubutton', ['Hello', user])
        time.sleep(5)
        self.say('menubutton',['1to10'])
    


    def Broadcast_group(self,group_name):
        self.say('menubutton', ['broadcast', group_name])
        time.sleep(3)
        self.say('menubutton',['1to10'])
    
    def log_on_tool(self):
        logon_cont=1
        logon_url = 'http://{0}:{1}/api/logon'.format(config.TGS_IP,config.LOG_ON_PORT)
        headers = {'Content-Type': 'application/json'}
        data = {'mac': self.mac_address, 'tagout': self.name}
        #data={"logon": {"username": self.name, "host_id": self.mac_address, "time": self.make_time_string(time.time()), "txid": "txLT-{0}".format(random.randint(10000000,9999999999))}}
        try:
            response=requests.post(logon_url, data=json.dumps(data), headers=headers)
            while not response:
                if "you_are_already_logged_on" in self.output():
                    break
                logon_cont = logon_cont +1
                time.sleep(10)
                response=requests.post(logon_url, data=json.dumps(data), headers=headers)
                if logon_cont == 3:
                    self.log_on()
                    break

        except ConnectionError as e:
            print "error raised {}".format(e)
            self.log_on()

        

    def logon_name(self,name):
        logon_url = 'http://{0}:{1}/api/logon'.format(config.TGS_IP,config.LOG_ON_PORT)
        headers = {'Content-Type': 'application/json'}
        data = {'mac': self.mac_address, 'tagout': name}
        #data={"logon": {"username": self.name, "host_id": self.mac_address, "time": self.make_time_string(time.time()), "txid": "txLT-{0}".format(random.randint(10000000,9999999999))}}
        return requests.post(logon_url, data=json.dumps(data), headers=headers)

    def command(self,command_name,Ora=None,User=None):
        
        if (not Ora) and (not User):
            #print "inside first if"
            self.say('menubutton',[command_name])
            time.sleep(2)
        elif Ora and (not User):
            print "inside first elif"
            self.say('menubutton',[command_name])
            #time.sleep(3)
            if self.i_wait('go_ahead'):
                self.say('menubutton',[Ora])
            
        else:
            #If commands not recognised
            print "inside first else"
            self.say('menubutton',[command_name,User])
            #time.sleep(4)
            print self.output()
            if not self.i_wait("tts_Sorry_please_say_again"):
                self.say('menubutton',[Ora])
    
    def btn_up(self,command_name,Ora=None,User=None):
        
        if (not Ora) and (not User):
            #print "inside first if"
            self.say('menubutton',[command_name],skipBtnUp=False)
            time.sleep(7)
        elif Ora and (not User):
            print "inside first elif"
            self.say('menubutton',[command_name],skipBtnUp=False)
            time.sleep(3)
            if "go_ahead" in self.output():
                self.say('menubutton',[Ora],skipBtnUp=False)
            
        else:
            #If commands not recognised
            print "inside first else"
            self.say('menubutton',[command_name,User],skipBtnUp=False)
            time.sleep(4)
            print self.output()
            if not ("tts_Sorry_please_say_again" in self.output()):
                self.say('menubutton',[Ora],skipBtnUp=False)
                
    def repeat_command(self,command_name,count):
    	for n in range(count):
    		self.command(command_name)
    		time.sleep(4)


    def setUp(self):
        try:
            self.browser = Browser('chrome')
            self.browser.driver.maximize_window()
            self.browser.visit(config.url)
        except Exception as e:
            print "Exception while navigating to url"
            raise e

    def navigateToDiffStore(self):
        try:
            self.browser.visit("https://itg0.theatro.com/tsg/")
            self.browser.visit(updated_url)
        except Exception as e:
            print "Exception while navigating to different store"
            raise e

    def login_with_username_and_password(self):
        try:
            self.setUp()
            if (self.browser.is_element_present_by_id('twLoginId', wait_time=100)):
                self.browser.find_by_id('twLoginId').first.fill(config.user)
                self.browser.find_by_id('twPassword').first.fill(config.password)
                self.browser.find_by_id('loginbtn').first.click()
                """if(self.browser.is_element_present_by_xpath('''.//*[@id='employeeList']/div''',wait_time=1000)):
                    if(self.browser.is_element_present_by_id('merchant',wait_time=1000)):
                        dropdown = self.browser.find_by_id('merchant')
                        for option in dropdown.find_by_tag('option'):
                            if option.text == all_rows[0].split('/')[0]:
                                self.browser.find_by_id('merchant').first.click()
                                option.click()
                                break
                    if(self.browser.is_element_present_by_xpath('''.//*[@id='storeSelect']/option''',wait_time=1000)):
                        dropdown1 = self.browser.find_by_id('storeSelect')
                        for option1 in dropdown1.find_by_tag('option'):
                            if option1.text == all_rows[0].split('/')[1]:
                                option1.click()
                                break
                else:
                    print "Unable to Login to Theatro portal"""
            else:
                print "Unable to Navigate to Theatro portal login page"
        except Exception as e:
            raise e

    def tearDown(self):
        self.browser.quit()

    def add_employee(self,name):
        name=to_unicode(name)
        if name:
            self.fname = name.split()[0]
            self.lname = name.split()[1]
        print self.fname
        print self.lname

        try:
            self.login_with_username_and_password()
            self.browser.is_element_present_by_xpath('''.//*[@id='employeeList']/div''', wait_time=30)
            self.navigateToDiffStore()
            if (self.browser.is_element_visible_by_xpath('''.//*[@id='employeeList']/div''', wait_time=30)):
                self.browser.find_by_id('newEmployee').first.click()
                if (self.browser.is_element_visible_by_xpath('''.//*[@id='empGroups']/li''', wait_time=30)):
                    self.browser.find_by_id('firstName').first.fill(self.fname)
                    self.browser.find_by_id('lastName').first.fill(self.lname)
                    if self.gname:
                        self.browser.find_by_xpath('//label[contains(text()[normalize-space()],\'' + grpName + '\')]/input').first.check()
                    else:
                        pass
                    self.browser.find_by_id('saveEmployee').first.click()
                    if (self.browser.is_element_visible_by_xpath(
                            '''//div[contains(text(),'Employee Saved Successfully')]''', wait_time=20)):
                        self.browser.find_by_xpath('''//button[contains(text(),'OK')]''').click()

                    else:
                        self.tearDown()
                        return False
                else:
                    print "Unable to Add New employee"
                    self.tearDown()
                    return False
            else:
                print "Unable to click on Add New employee button"
                self.tearDown()
                return False
            self.tearDown()
            return True

        except Exception as e:
            print e
            print "problem in adding employee"
            raise e
#######################################################################################
# test definitions start here ---------------------------------------------------------

if __name__ == '__main__':
    def log_test():
        a = Communicator('test_mac', config.CLI_START_IP, config.SYSTEM_IP, 'test_name')
        a.start()
        #a.join()
        time.sleep(30)
        a.die()

    def communication_test():
        def server():
            test_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            test_socket.bind((config.SYSTEM_IP, TGS_PORTS['command']))
            message = ''
            while message != 'done':
                try:
                    message = test_socket.recv(1024)
                    print message
                except:
                    pass

        def client():
            a = Communicator('test_mac', config.CLI_START_IP, config.SYSTEM_IP, 'test_name')
            for i in range(10):
                print i
                a.data_socket.sendto('this is a test message', (config.SYSTEM_IP, TGS_PORTS['command']))
            a.data_socket.sendto('done', (config.SYSTEM_IP, TGS_PORTS['command']))

        server_thread = threading.Thread(target=server, name='server')
        client_thread = threading.Thread(target=client, name='client')

        server_thread.start()
        client_thread.start()

        server_thread.join()
        client_thread.join()

# tests start here -----------------------------------------------------------------------------------------------
    log_test()
    #communication_test()

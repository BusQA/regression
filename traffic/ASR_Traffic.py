from utils import *
clients = make_clients(count =1)
start_clients(clients)
a=clients[0]
a.log_on_tool()
def ASR_I(Issuer,Option,UserName):
	Issuer.command("asr_hello_option")
	wait(3)
	if "tts_to_contact,|amanda scott,|please_say_one,|or,|to_contact,|amanda peterson,|please_say_two,|or,|tap_out_to_cancel" == Issuer.output():
		Issuer.command(Option)
		wait(3)

		if "tts_{0},|is_not_logged_on".format(UserName) == Issuer.output():
			write_to_file("ASR Case {0} Passed".format(Option),FILE_NAME="Traffic_ASR.txt",MODE="a")
		else:
			write_to_file("ASR Case {1} Failed , User got {0}".format(a.output(),Option),FILE_NAME="Traffic_ASR.txt",MODE="a")
	else:
		write_to_file("ASR Case {1} , Not Recognised User got:{0}".format(a.output(),Option),FILE_NAME="Traffic_ASR.txt",MODE="a")
	wait(5)
def ASR_II(Issuer,UserName):
	Issuer.command("asr_not_sure")
	wait(3)
	if "tts_i_am_not_sure_you_are_trying,|to_contact,|{0},|please_say_yes,|or,|tap_out_to_cancel".format(UserName) == Issuer.output():
		Issuer.command("yes")
		wait(3)
		if "tts_{0},|is_not_logged_on".format(UserName) == Issuer.output():
			write_to_file("ASR Case three Passed",FILE_NAME="Traffic_ASR.txt",MODE="a")
		else:
			write_to_file("ASR Case three Failed , User got {0}".format(a.output()),FILE_NAME="Traffic_ASR.txt",MODE="a")
	else:
		write_to_file("ASR Case three Not Recognised User got:{0}".format(a.output()),FILE_NAME="Traffic_ASR.txt",MODE="a")
def ASR_III(Issuer):
	Issuer.command("asr_sorry")
	wait(5)
	if "tts_Sorry_please_say_again" == Issuer.output():
		write_to_file("ASR Case four Passed",FILE_NAME="Traffic_ASR.txt",MODE="a")
	else:
		write_to_file("ASR Case Four Failed , User got {0}".format(a.output()),FILE_NAME="Traffic_ASR.txt",MODE="a")
	
while True:
	ASR_I(a,"one","amanda scott")
	wait(5)
	ASR_I(a,"two","amanda peterson")
	wait(5)
	ASR_II(a,"amanda peterson")
	wait(5)
	ASR_III(a)
	wait(5)
'''
while True:
	#Case I , contact Amanda scott
	a.command("asr_hello_option")
	wait(3)
	if "tts_to_contact,|amanda scott,|please_say_one,|or,|to_contact,|amanda peterson,|please_say_two,|or,|tap_out_to_cancel" == a.output():
		a.command("one")
		wait(3)
		if "tts_amanda scott,|is_not_logged_on" == a.output():
			write_to_file("ASR Case I Passed",FILE_NAME="Traffic_ASR.txt",MODE="a")
		else:
			write_to_file("ASR Case I Failed , User got {0}".format(a.output()),FILE_NAME="Traffic_ASR.txt",MODE="a")
	else:
		write_to_file("ASR Case I , Not Recognised User got:{0}".format(a.output()),FILE_NAME="Traffic_ASR.txt",MODE="a")
	wait(5)
	#Case II , comatct Amanda Peterson

'''





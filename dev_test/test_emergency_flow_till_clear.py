from utils import wait,DB_UPDATE
import pytest
from communicator import *
from DB_HANDLE import get_feature_prompts,making_prompts
pytest.EM_PROMPTS_1=get_feature_prompts('emergency',1)
pytest.EM_PROMPTS_2=get_feature_prompts('emergency',2)
'''
a ['angella hilt', allowed user #front end 
b'ben sander', allowed user # front end
c'amanda peterson', target group #front end
d'david jason',north bay leader #1 #front end 
e'donnie brady', north bay leader #2 #front end
f'ricky vance', #front end
g'bob bennion'south bay leader #1 # millwork
h'allen tippet'south bay leader #2 #millwork
i'amanda scott' south bay leader#3 #millwork
j'jay bosler'], normal user # millwork
k'david clarke', #millwork
l'holli root' #millwork target group



'''


def test_emergency_first_prompt(emergency_clients):
    for x in emergency_clients:
        x.log_on_tool()
        wait(2)
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt = (making_prompts(pytest.EM_PROMPTS_1, {'__TO__':a.name, '__TO_FN__': a.fname})['select_service']).lower()
    a.command("emergency guide")
    assert a.i_wait(expected_prompt)



def test_evacuation(emergency_clients):
    a=emergency_clients[0]
    a.command('evacuate the store')
    assert a.i_wait((pytest.EM_PROMPTS_1['confirm_activation']).lower())

def test_evacuation_yes(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt = making_prompts(pytest.EM_PROMPTS_1, {'__TO__': a.name, '__TO_FN__': a.fname})['initiator_initial_info']
    a.command('yes')
    assert a.i_wait(expected_prompt)



def test_normal_user_prompts_first_location(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_normal=b,c,e,f
    #attention_to_all,The_nearest_exit_to,__LOCATION_NAME__,is,__EXIT_NAME__,Say_copy_that_to_acknowledge_that_you_have_heard_this_message
    #Added stub to verify script
    expected_prompts=(making_prompts(pytest.EM_PROMPTS_1, {'__LOCATION_NAME__': 'front end', '__EXIT_NAME__':'exit2'})['associates_initial_info']).lower()
    assert all((x.output() in expected_prompts) for x in list_normal)

def test_normal_user_prompts_second_location(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_normal=h,i,j,k,l
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    prompt_4 = (making_prompts(EM_PROMPTS, {'__LOCATION_NAME__': 'millwork', '__EXIT_NAME__': 'exit1'})[
        'associates_initial_info']).lower()
    #assert all (x.i_wait(prompt_4) for x in list_normal)
    assert i.output() in prompt_4
    assert j.output() in prompt_4
    assert k.output() in prompt_4
    assert h.output() in prompt_4
    assert l.output() in prompt_4


def test_floor_leader_prompt(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_lead=d,g
    #__TO__, __TO_FN__, you_are_the_leader_for, __ZONE__
    expected_prompt_1= making_prompts(pytest.EM_PROMPTS_1, {'__TO__': d.name, '__TO_FN__': d.fname,'__ZONE__':'north bay'})['zone_leader_initial_info']
    expected_prompt_2 = making_prompts(pytest.EM_PROMPTS_1, {'__TO__': g.name, '__TO_FN__': g.fname, '__ZONE__': 'south bay'})[
        'zone_leader_initial_info']

    assert d.i_wait(expected_prompt_1)
    assert g.i_wait(expected_prompt_2)


def test_floor_leader_announcememnt(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_lead=d,g
    assert all((x.i_wait(pytest.EM_PROMPTS_2['zone_leader_initial_info'], 10)) for x in list_lead)


def test_initiator_announcememnt(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    assert a.i_wait(pytest.EM_PROMPTS_2['initiator_initial_info'])



def test_clear_prompt_to_initiator(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    wait(45)
    assert a.i_wait((pytest.EM_PROMPTS_1['initiator_subsequent_info']).lower())

def test_manager_announcemment(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_mgr=c,l

    #There_are,__COUNT__,employees_currently_logged_on_in_the_store_At_any_time_say_Update_me_to_get_an_evacuation_update"
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    prompt = (making_prompts(EM_PROMPTS, {'__COUNT__': '12'
                                          }))['target_group_initial_info'].lower()
    assert all((x.i_wait(prompt)) for x in list_mgr)

def test_floor_leader_announcememnt_soundout(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    assert d.i_wait(pytest.EM_PROMPTS_1['north bay_zone_leader_subsequent_info'],60)
    assert g.i_wait(pytest.EM_PROMPTS_1['south bay_zone_leader_subsequent_info'], 60)



def test_target_group_announcememnt_soundout(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_mgr=c,l
    #[x.make_available() for x in list_mgr]
    #__COUNT__,online,__ZONE_LEADER_ACCEPTED_COUNT__,of,__ZONE_LEADER_TOTAL_COUNT__,zones_are_cleared,__ASSOCIATES_ACCEPTED_COUNT__,of,__ASSOCIATES_TOTAL_COUNT__,acknowledged,Say_List_Associates_and_I_will_tell_you_who_remains_online
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    prompt = (making_prompts(EM_PROMPTS, {'__COUNT__': '12',
                                          '__ZONE_LEADER_ACCEPTED_COUNT__': '0',
                                          '__ZONE_LEADER_TOTAL_COUNT__': '2',
                                          '__ASSOCIATES_ACCEPTED_COUNT__': '0',
                                          '__ASSOCIATES_TOTAL_COUNT__': '7',
                                          }))['target_group_subsequent_info'].lower()
    assert all((x.i_wait(prompt,120)) for x in list_mgr)


def test_initiator_announcememnt_soundout(emergency_clients):
    a=emergency_clients[0]
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    prompt = (making_prompts(EM_PROMPTS, {'__COUNT__': '12',
                                          '__ZONE_LEADER_ACCEPTED_COUNT__': '0',
                                          '__ZONE_LEADER_TOTAL_COUNT__': '2',
                                          '__ASSOCIATES_ACCEPTED_COUNT__': '0',
                                          '__ASSOCIATES_TOTAL_COUNT__': '7',
                                          }))['target_group_subsequent_info'].lower()
    assert a.i_wait(prompt,20)



def test_evacuation_intiator_all_clear_initiaor(emergency_clients):
    a=emergency_clients[0]
    #Ok,__TO_FN__,__TO__,I_am_glad_everyone_is_safe!_I_will_send_the_all_clear_announcement_now,tap_out_to_cancel
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt = ((making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname,}))['close_confirmation']).lower()
    a.command('all clear')
    assert a.i_wait(expected_prompt,20)

def test_evacuation_intiator_all_clear_recipient(emergency_clients):
    a=emergency_clients[0]
    #attention_on_all_clear
    wait(10)
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt_1 = ((making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname, }))['close_to_listener']).lower()
    assert all((x.i_wait(expected_prompt_1, 60)) for x in emergency_clients)



def test_evacuation_intiator_all_clear_second_time(emergency_clients):
    a=emergency_clients[0]
    #Ok,__TO_FN__,__TO__,I_am_glad_everyone_is_safe!_I_will_send_the_all_clear_announcement_now,tap_out_to_cancel
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt = (making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname,}))['close_confirmation']
    wait(10)
    a.command('all clear')
    wait(5)
    assert a.output() not in expected_prompt

from utils import wait
from DB_HANDLE import get_feature_prompts,making_prompts,get_help_list
from responses import AG_PROMPTS
import pytest

review_response = "do_you_want_to_review_your_announcement_before_posting_it"
list_command_reponse='would_you_like,conversationcommands,locationcommands,managementcommands,messagecommands,assistancecommands,groupcommands,thasta_all_say_lists_or_teach_me_to_learn_more_or_tap_out_to_exit'
list_command_subset_reponse='i_am_sorry_please_say_one_of_the_following_commands'
list_command_subset_reponse_1='to_hear_how_to_use_another_command_say_next_or_say_list_commands'
retry_next_again='i_am_sorry_please_say_next_list_commands_or_tap_out_to_cancel'

@pytest.mark.parametrize("cmd,next_cmd", [
    ("conversation commands","hello"),
    ("location commands","locate"),
    ("management commands","record huddle"),
    ("message commands","message"),
    ("group commands","hello"),

])
def test_list_commands(clients,command,response):
    b = clients[0]
    b.command("logon kristal baldwin")
    wait(2)
    b.command("list commands")
    assert b.i_wait(list_command_reponse,10)
    b.command(cmd)
    if cmd != "message commands":
        b.command("message")
    else:
        b.command("Hello")
    b.i_wait(list_command_subset_reponse,10)
    b.command("aabra ka dabra helpeme")
    b.i_wait(list_command_subset_reponse,10)
    b.command(next_cmd)
    b.i_wait(list_command_subset_reponse_1,10)
    b.command('more')
    wait(2)
    b.command('more')
    wait(2)
    b.command('more')
    wait(2)
    b.command('more')
    wait(2)
    b.command('more')
    wait(2)
    b.command('next')
    b.i_wait(list_command_subset_reponse_1,10)
    b.command("aabra ka dabra helpeme")
    b.i_wait(retry_next_again,10)
    b.command("message")
    b.i_wait('please_say_next_or_list_cmds_or_tapout',10)
    b.command('next')
    b.command('next')
    b.command('next')
    b.command('next')
from utils import *
from DB_HANDLE import CO_USERS,get_emp_id,RB_USERS,RB_ESC_USERS,GUN_USERS,GUN_ESC_USERS,MA_USERS,SB_USERS,SRD_USERS,srd_db_update,INDYME_USERS,managers_USERS,Group_NAME
import pytest
global CO_PROMPTS
from setup import get_prompts
from communicator import *
from config import RNR_REQ,RNR_ACC
from time import strftime

@pytest.fixture
def carry_out():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120"})
    cii = make_clients(names=CO_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("carry out")
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('carry_out'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


def convert_to_str(dict1):
    return {k:str(v) for k,v in dict1.iteritems()}

#@pytest.mark.skip()
def test_carry_out_stats(carry_out):
    tx_id,cii_list=carry_out
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] == actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']




@pytest.fixture
def register_backup():
    #DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"false"})
    cii = make_clients(names=RB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("register backup")
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('register_backup'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    #DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"true"})


#@pytest.mark.skip()
def test_register_backup_stats(register_backup):
    tx_id,cii_list=register_backup
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] == actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']


@pytest.fixture
def register_backup_escalation():
    DB_UPDATE({"REG_BACKUP_MODE":"1","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("register backup")
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('register_backup'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


#@pytest.mark.skip()
def test_register_backup_escalationa_stats(register_backup_escalation):
    tx_id,cii_list=register_backup_escalation
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] == actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_accept['listeners']


@pytest.fixture
def register_backup_cancel():
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "10"})
    cii = make_clients(names=RB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("register backup")
    wait(20)
    yield get_rnr_txid_from_tgs('register_backup'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"true"})

    
#@pytest.mark.skip()
def test_register_backup_cancel_stats(register_backup_cancel):
    tx_id,cii_list=register_backup_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'cancel'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] == actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_cancel['listeners']


@pytest.fixture
def register_backup_escalation_cancel():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "10","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("register backup")
    wait(20)
    yield get_rnr_txid_from_tgs('register_backup'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


#@pytest.mark.skip()
def test_register_backup_escalation_cancel_stats(register_backup_escalation_cancel):
    tx_id,cii_list=register_backup_escalation_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'cancel'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] == actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_cancel['listeners']

@pytest.fixture
def register_backup_tapout():
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120"})
    cii = make_clients(names=RB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("register backup")
    a.tap_out()
    wait(4)
    yield get_rnr_txid_from_tgs('register_backup'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"true","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})

#@pytest.mark.skip()
def test_register_backup_tapout_stats(register_backup_tapout):
    tx_id,cii_list=register_backup_tapout
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_tapout=convert_to_str(get_Request_Response_stats(tx_id,'tapout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_tapout['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_tapout['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_tapout['requestedCount']
    assert RNR_ACC['user'] == actual_op_tapout['user']
    assert RNR_ACC['device'] == actual_op_tapout['device']
    assert RNR_ACC['location'] == actual_op_tapout['location']
    assert RNR_ACC['listeners'][0] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][2] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][3] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_tapout['listeners']

@pytest.fixture
def gun_runner():
    DB_UPDATE({"REG_BACKUP_MODE":3,"ENABLE_REG_BACKUP_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120"})
    cii = make_clients(names=GUN_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("gun runner")
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('gun_runner'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"3","ENABLE_REG_BACKUP_ESCALATION":"true","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"60"})
    

#@pytest.mark.skip()
def test_gun_runner_stats(gun_runner):
    tx_id,cii_list=gun_runner
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] != actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']


@pytest.fixture
def gun_runner_escalation():
    cii = make_clients(names=GUN_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("gun runner")
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('gun_runner'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    

#@pytest.mark.skip()
def test_gun_runner_escalation_stats(gun_runner_escalation):
    tx_id,cii_list=gun_runner_escalation
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] != actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_accept['listeners']


@pytest.fixture
def gun_runner_cancel():
    DB_UPDATE({"REG_BACKUP_MODE":3,"ENABLE_REG_BACKUP_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    cii = make_clients(names=GUN_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("gun runner")
    wait(20)
    yield get_rnr_txid_from_tgs('gun_runner'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"3","ENABLE_REG_BACKUP_ESCALATION":"true","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})


#@pytest.mark.skip()
def test_gun_runner_cancel_stats(gun_runner_cancel):
    tx_id,cii_list=gun_runner_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'cancel'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] != actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_cancel['listeners']

@pytest.fixture
def gun_runner_escalation_cancel():
    cii = make_clients(names=GUN_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("gun runner")
    wait(20)
    yield get_rnr_txid_from_tgs('gun_runner'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


#@pytest.mark.skip()
def test_gun_runner_escalation_cancel_stats(gun_runner_escalation_cancel):
    tx_id,cii_list=gun_runner_escalation_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'cancel'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] != actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_cancel['listeners']


@pytest.fixture
def gun_runner_tapout():
    DB_UPDATE({"REG_BACKUP_MODE":3,"ENABLE_REG_BACKUP_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"60"})
    cii = make_clients(names=GUN_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("gun runner")
    a.tap_out()
    wait(4)
    yield get_rnr_txid_from_tgs('gun_runner'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"3","ENABLE_REG_BACKUP_ESCALATION":"true","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"60"})


#@pytest.mark.skip()
def test_gun_runner_tapout_stats(gun_runner_tapout):
    tx_id,cii_list=gun_runner_tapout
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_tapout=convert_to_str(get_Request_Response_stats(tx_id,'tapout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_tapout['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_tapout['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_tapout['requestedCount']
    assert RNR_ACC['user'] == actual_op_tapout['user']
    assert RNR_ACC['device'] == actual_op_tapout['device']
    assert RNR_ACC['location'] != actual_op_tapout['location']
    assert RNR_ACC['listeners'][0] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][2] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][3] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_tapout['listeners']



@pytest.fixture
def manager_assistance():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120"})
    cii = make_clients(names=MA_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("manager assistance")
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('manager_assistance'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


#@pytest.mark.skip()
def test_manager_assistancer_stats(manager_assistance):
    tx_id,cii_list=manager_assistance
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] == actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']

@pytest.fixture
def manager_assistance_cancel():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "10"})
    cii = make_clients(names=MA_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("manager assistance")
    wait(20)
    yield get_rnr_txid_from_tgs('manager_assistance'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


#@pytest.mark.skip()
def test_manager_assistance_cancel_stats(manager_assistance_cancel):
    tx_id,cii_list=manager_assistance_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'cancel'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] == actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_cancel['listeners']

@pytest.fixture
def manager_assistance_tapout():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "10"})
    cii = make_clients(names=MA_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("manager assistance")
    a.tap_out()
    wait(4)
    yield get_rnr_txid_from_tgs('manager_assistance'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)



#@pytest.mark.skip()
def test_manager_assistance_tapout_stats(manager_assistance_tapout):
    tx_id,cii_list=manager_assistance_tapout
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_tapout=convert_to_str(get_Request_Response_stats(tx_id,'tapout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_tapout['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_tapout['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_tapout['requestedCount']
    assert RNR_ACC['user'] == actual_op_tapout['user']
    assert RNR_ACC['device'] == actual_op_tapout['device']
    assert RNR_ACC['location'] == actual_op_tapout['location']
    assert RNR_ACC['listeners'][0] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][2] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][3] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_tapout['listeners']




@pytest.fixture
def soft_button():
    DB_UPDATE({"ENABLE_SOFT_BUTTON_ESCALATION":"false","SOFT_BUTTON_ESCALATION_START_TIME_SEC":"60"})
    cii = make_clients(names=SB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('curl -X POST "http://localhost:8088/softbutton?location=CO&clear=false"')
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('soft_button'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"ENABLE_SOFT_BUTTON_ESCALATION":"true"})


#@pytest.mark.skip()
def test_soft_button_stats(soft_button):
    tx_id,cii_list=soft_button
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] != actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] != actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] == actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']


@pytest.fixture
def soft_button_escalation():
    DB_UPDATE({"SOFT_BUTTON_ESCALATION_START_TIME_SEC":"5"})
    cii = make_clients(names=SB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('curl -X POST "http://localhost:8088/softbutton?location=CO&clear=false"')
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('soft_button'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    

#@pytest.mark.skip()
def test_soft_button_escalation_stats(soft_button_escalation):
    tx_id,cii_list=soft_button_escalation
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] != actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] != actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] == actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_accept['listeners']


@pytest.fixture
def soft_button_cancel():
    DB_UPDATE({"ENABLE_SOFT_BUTTON_ESCALATION":"false"})
    cii = make_clients(names=SB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('curl -X POST "http://localhost:8088/softbutton?location=CO&clear=false"')
    wait(20)
    yield get_rnr_txid_from_tgs('soft_button'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"ENABLE_SOFT_BUTTON_ESCALATION":"true"})

@pytest.mark.skip()
def test_soft_button_cancel_stats(soft_button_cancel):
    tx_id,cii_list=soft_button_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'timeout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] != actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] != actual_op_cancel['user']
    assert RNR_ACC['device'] != actual_op_cancel['device']
    assert RNR_ACC['location'] == actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_cancel['listeners']



@pytest.fixture
def soft_button_escalation_cancel():
    DB_UPDATE({"SOFT_BUTTON_ESCALATION_START_TIME_SEC":"5"})
    cii = make_clients(names=SB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('curl -X POST "http://localhost:8088/softbutton?location=CO&clear=false"')
    wait(20)
    yield get_rnr_txid_from_tgs('soft_button'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


@pytest.mark.skip()
def test_soft_button_escalation_cancel_stats(soft_button_escalation_cancel):
    tx_id,cii_list=soft_button_escalation_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'timeout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] != actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] != actual_op_cancel['user']
    assert RNR_ACC['device'] != actual_op_cancel['device']
    assert RNR_ACC['location'] == actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_cancel['listeners']



@pytest.fixture
def store_readiness():
    print "configuring store readiness params"
    com = "date |awk -F ' ' '{print $4}'"
    current_time = sshCommand(command=com)
    current_time=current_time.split(":")
    tmp=current_time[0].split("\n")
    current_time[0]=tmp[1]
    #current_time=strftime("%H:%M:%S").split(":")
    current_time[2]=str(int(current_time[2])+(55)).zfill(2)
    if int(current_time[2])>=60:
        current_time[2]=str(int(current_time[2])-60).zfill(2)
        current_time[1]=str(int(current_time[1])+1).zfill(2)
    start_time=":".join(current_time)
    srd_db_update(start_time)
    cii = make_clients(names=SRD_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(40)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('store_readiness'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    srd_db_update("disable")

#@pytest.mark.skip()
def test_store_readiness_stats(store_readiness):
    tx_id,cii_list=store_readiness
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] == actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] == actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']

@pytest.fixture
def indyme():
    DB_UPDATE({"ENABLE_INDYME_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120"})
    cii = make_clients(names=INDYME_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('indyme'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"true","ENABLE_INDYME_ESCALATION":"true"})


#@pytest.mark.skip()
def test_indyme_stats(indyme):
    tx_id,cii_list=indyme
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] != actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']


@pytest.fixture
def indyme_escalation():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('indyme'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    

#@pytest.mark.skip()
def test_indyme_escalation_stats(indyme_escalation):
    tx_id,cii_list=indyme_escalation
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] != actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_accept['listeners']


@pytest.fixture
def indyme_cancel():
    DB_UPDATE({"ENABLE_INDYME_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "10"})
    cii = make_clients(names=INDYME_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(20)
    yield get_rnr_txid_from_tgs('indyme'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"true","ENABLE_INDYME_ESCALATION":"true","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})


#@pytest.mark.skip()
def test_indyme_cancel_stats(indyme_cancel):
    tx_id,cii_list=indyme_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'timeout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] != actual_op_cancel['user']
    assert RNR_ACC['device'] != actual_op_cancel['device']
    assert RNR_ACC['location'] != actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_cancel['listeners']


@pytest.fixture
def indyme_escalation_cancel():
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(20)
    yield get_rnr_txid_from_tgs('indyme'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


#@pytest.mark.skip()
def test_indyme_escalation_cancel_stats(indyme_escalation_cancel):
    tx_id,cii_list=indyme_escalation_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'timeout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] != actual_op_request['user']
    assert RNR_REQ['device'] != actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] != actual_op_cancel['user']
    assert RNR_ACC['device'] != actual_op_cancel['device']
    assert RNR_ACC['location'] != actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_cancel['listeners']



@pytest.fixture
def manager_thank_you():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120"})
    cii = make_clients(names=managers_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("manager thankyou")
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('manager_thank_you'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)

#@pytest.mark.skip()
def test_manager_thank_you_stats(manager_thank_you):
    tx_id,cii_list=manager_thank_you
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] != actual_op_accept['location']
    assert RNR_ACC['listeners'][0] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_accept['listeners']


@pytest.fixture
def manager_thank_you_cancel():
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    cii = make_clients(names=managers_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command("manager thankyou")
    wait(20)
    yield get_rnr_txid_from_tgs('manager_thank_you'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)

#@pytest.mark.skip()
def test_manager_thank_you_cancel_stats(manager_thank_you_cancel):
    tx_id,cii_list=manager_thank_you_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'timeout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] != actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_cancel['listeners']






@pytest.fixture
def request_group():
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120"})

    cii = make_clients(names=RB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command('request {}'.format('register backup'))
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('request_group'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"true","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})


#@pytest.mark.skip()
def test_request_group_stats(request_group):
    tx_id,cii_list=request_group
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] != actual_op_accept['location']
    assert RNR_ACC['listeners'][0] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_accept['listeners']


@pytest.fixture
def request_group_escalation():
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command('request {}'.format('register backup'))
    wait(20)
    b.command("copythat")
    yield get_rnr_txid_from_tgs('request_group'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


#@pytest.mark.skip()
def test_request_group_escalation_stats(request_group_escalation):
    tx_id,cii_list=request_group_escalation
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[1].id
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_accept=convert_to_str(get_Request_Response_stats(tx_id,'accept'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_accept['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_accept['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_accept['requestedCount']
    assert RNR_ACC['user'] == actual_op_accept['user']
    assert RNR_ACC['device'] == actual_op_accept['device']
    assert RNR_ACC['location'] != actual_op_accept['location']
    assert RNR_ACC['listeners'][0] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_accept['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_accept['listeners']

@pytest.fixture
def request_group_cancel():
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    cii = make_clients(names=RB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command('request {}'.format('register backup'))
    wait(20)
    yield get_rnr_txid_from_tgs('request_group'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"true","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})
    

#@pytest.mark.skip()
def test_request_group_cancel_stats(request_group_cancel):
    tx_id,cii_list=request_group_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'timeout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] != actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_cancel['listeners']


@pytest.fixture
def request_group_escalation_cancel():
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command('request {}'.format('register backup'))
    wait(20)
    yield get_rnr_txid_from_tgs('request_group'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)

#@pytest.mark.skip()
def test_request_group_escalation_cancel_stats(request_group_escalation_cancel):
    tx_id,cii_list=request_group_escalation_cancel
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_cancel=convert_to_str(get_Request_Response_stats(tx_id,'timeout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_cancel['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_cancel['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_cancel['requestedCount']
    assert RNR_ACC['user'] == actual_op_cancel['user']
    assert RNR_ACC['device'] == actual_op_cancel['device']
    assert RNR_ACC['location'] != actual_op_cancel['location']
    assert RNR_ACC['listeners'][0] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][2] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][3] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][1] in actual_op_cancel['listeners']
    assert RNR_ACC['listeners'][4] in actual_op_cancel['listeners']


@pytest.fixture
def request_group_tapout():
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"false"})
    cii = make_clients(names=RB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    a,b,c,d,e=cii
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    a.command('request {}'.format('register backup'))
    a.tap_out()
    wait(4)
    yield get_rnr_txid_from_tgs('request_group'),cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"ENABLE_REQUEST_GROUP_RNR_ESCALATION":"true"})


#@pytest.mark.skip()
def test_request_group_tapout_stats(request_group_tapout):
    tx_id,cii_list=request_group_tapout
    assert tx_id
    for x in cii_list:
        x.id=get_emp_id(x.name)

    RNR_REQ['user']=cii_list[0].id
    RNR_REQ['device']=cii_list[0].mac_address
    RNR_ACC['listeners']=[x.id for x in cii_list]
    RNR_ACC['user']=cii_list[0].id
    RNR_ACC['device']=cii_list[0].mac_address
    actual_op_request=convert_to_str(get_Request_Response_stats(tx_id,'request'))
    actual_op_tapout=convert_to_str(get_Request_Response_stats(tx_id,'tapout'))
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '', 'requestedCount': '1', 'user': '7572', 'timeout': '1543232441', 'time': '1543232321', 'device': '0023a729eab1', 'txId': '1543232321000100223', 'type': 'carry_out_request', 'id': '7', 'location': ''}
    #{'server_id': '286', 'remoteTxId': '', 'responderCount': '0', 'call_id': '20', 'listeners': '7588$7612$7572', 'requestedCount': '1', 'user': '7587', 'timeout': '1543232441', 'time': '1543232347', 'device': '0023a7347328', 'txId': '1543232321000100223', 'type': 'carry_out_accept', 'id': '8', 'location': ''}
    assert RNR_REQ['server_id'] == actual_op_request['server_id']
    assert RNR_REQ['remoteTxId'] == actual_op_request['remoteTxId']
    assert RNR_REQ['responderCount'] == actual_op_request['responderCount']
    assert RNR_REQ['requestedCount'] == actual_op_request['requestedCount']
    assert RNR_REQ['user'] == actual_op_request['user']
    assert RNR_REQ['device'] == actual_op_request['device']
    assert RNR_REQ['location'] != actual_op_request['location']
    assert RNR_ACC['server_id'] == actual_op_tapout['server_id']
    assert RNR_ACC['remoteTxId'] == actual_op_tapout['remoteTxId']
    assert RNR_ACC['requestedCount'] == actual_op_tapout['requestedCount']
    assert RNR_ACC['user'] == actual_op_tapout['user']
    assert RNR_ACC['device'] == actual_op_tapout['device']
    assert RNR_ACC['location'] != actual_op_tapout['location']
    assert RNR_ACC['listeners'][0] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][2] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][3] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][1] not in actual_op_tapout['listeners']
    assert RNR_ACC['listeners'][4] not in actual_op_tapout['listeners']
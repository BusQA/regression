from utils import wait
from setup import MOD_clients
from responses import MOD_PROMPTS,LOCATE_PROMPTS
from communicator import *
import pytest
from ssh_client import *


def test_MOD_check_in(MOD_clients):
	"""check in as MOD"""
	a,b=MOD_clients[:2]
	[x.log_on_tool() for x in a,b]
	wait(2)
	a.command("checkin manager on duty")
	wait(20)
	assert  (a.fname+MOD_PROMPTS["log_on"][0] in a.output() and a.name+MOD_PROMPTS["log_on"][1] in b.output())

#@pytest.mark.skip(reason="no way of currently testing this")
def test_who_is_MOD(MOD_clients):
	"identify who is MOD "
	a,b=MOD_clients[:2]
	wait(2)
	b.command("whoisMOD")
	wait(4)
	assert  a.name+MOD_PROMPTS["who_is_MOD"] in b.output()

def test_hello_MOD(MOD_clients):
	"""make 121 to a MOD"""
	a,b=MOD_clients[:2]
	wait(2)
	b.cmd("hello","mod")
	a.tap_out()
	wait(2)
	assert ('call_ended' in a.output()) and ('manageronduty,ended_call' in b.output())

#@pytest.mark.skip(reason="no way of currently testing this")
def test_interrupt_MOD(MOD_clients):
	"""make 121 to a MOD"""
	a,b=MOD_clients[:2]
	wait(2)
	b.cmd("interrupt","mod")
	a.tap_out()
	wait(2)
	assert ('call_ended' in a.output()) and ('manageronduty,ended_call' in b.output())


def test_message_MOD(MOD_clients):
	"""leave a message to MOD"""
	a,b=MOD_clients[:2]
	wait(2)
	b.message("manageronduty")
	wait(15)
	assert ('posted_at' in a.output() and MOD_PROMPTS["message_mod"] in b.output())
	print a.output()

def test_locate_available_MOD(MOD_clients):
	"""locate a available MOD"""
	a,b=MOD_clients[:2]
	b.cmd1("locate","mod")
	wait(3)
	assert "manageronduty,"+a.name+LOCATE_PROMPTS["available"] in b.output()

def test_MOD_reboot_survival(MOD_clients):
	a = MOD_clients[0]
	sshCommand()
	a.command('whoisMOD')
	wait(5)
	assert  MOD_PROMPTS["self_identify"] in a.output()

def test_MOD_check_out(MOD_clients):
	"""checkout MOD"""
	a,b=MOD_clients[:2]
	wait(2)
	#a.command('playmessages')
	#wait(6)
	a.command("checkout manager on duty")
	wait(15)
	assert ('ok,'+a.fname+MOD_PROMPTS["checkout"][0] in a.output() and MOD_PROMPTS["checkout"][1] in b.output())
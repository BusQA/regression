from utils import wait
from setup import clients


def test_Log_on_command(clients):
    a=clients[0]
    a.log_on()
    wait(5)
    assert a.name in a.output()
    a.log_off_json()
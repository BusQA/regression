
from utils import wait,DB_UPDATE
import pytest
global GUN_PROMPTS
from setup import get_prompts
from communicator import *
from config import DEFAULT_ZONE




def test_gun_runner_initating_request(GUN_ESC_clients):
    a,b,c,d,e = GUN_ESC_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    wait(5)
    GUN_PROMPTS = get_prompts(GUN_ESC_clients,"Gun Runner",zone=DEFAULT_ZONE)
    a.command("gun runner")
    assert a.i_wait(GUN_PROMPTS['command_response'],10)


def test_verification_of_escalation_Activation_response_to_requestor(GUN_ESC_clients):
    a, b, c, d, e = GUN_ESC_clients
    GUN_PROMPTS = get_prompts(GUN_ESC_clients,"Gun Runner",zone=DEFAULT_ZONE)
    assert all((x.i_wait(GUN_PROMPTS['escalation_to_requestor'],60)) for x in GUN_ESC_clients[1:])


def test_verification_of_escalation_Activation_response_to_listners(GUN_ESC_clients):
    a, b, c, d, e = GUN_ESC_clients
    GUN_PROMPTS = get_prompts(GUN_ESC_clients,"Gun Runner",zone=DEFAULT_ZONE)
    assert all((x.i_wait(GUN_PROMPTS['escalation_to_listeners'],10)) for x in GUN_ESC_clients[1:])



def test_soundout_for_available_user(GUN_ESC_clients):
    a, b, c, d, e = GUN_ESC_clients
    wait(15)
    GUN_PROMPTS = get_prompts(GUN_ESC_clients,"Gun Runner",zone=DEFAULT_ZONE)
    assert all(x.soundout == GUN_PROMPTS["escalated,__ZONE__"] for x in GUN_ESC_clients)


def test_already_active_prompt_verification(GUN_ESC_clients):
    a, b, c, d, e = GUN_ESC_clients
    GUN_PROMPTS = get_prompts(GUN_ESC_clients,"Gun Runner",zone=DEFAULT_ZONE)
    a.command("gun runner")
    wait(2)
    assert a.i_wait(GUN_PROMPTS["already_active"],10)


def test_accepting_with_available_user(GUN_ESC_clients):
    a, b, c, d, e = GUN_ESC_clients
    GUN_PROMPTS = get_prompts(GUN_ESC_clients,"Gun Runner",zone=DEFAULT_ZONE)
    b.command("copythat")
    wait(5)
    assert (b.i_wait(GUN_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(GUN_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(GUN_PROMPTS["acceptance_to_listeners"], 10)) and (d.i_wait(GUN_PROMPTS["acceptance_to_listeners"], 10)) and (e.i_wait(GUN_PROMPTS["acceptance_to_listeners"], 10))

def test_gun_runner_timeout(GUN_ESC_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    a, b, c, d, e = GUN_ESC_clients
    [x.log_on_tool() for x in GUN_ESC_clients]
    GUN_PROMPTS = get_prompts(GUN_ESC_clients,"Gun Runner",zone=DEFAULT_ZONE)
    a.command("gun runner")
    assert a.i_wait(GUN_PROMPTS['rnr_timeout'],40)
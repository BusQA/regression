from utils import wait
import pytest
global MTY_PROMPTS
from setup import get_prompts
from communicator import *

def test_mty_assistance_initating_request(clients):
    a,b,c,d,e=clients
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    MTY_PROMPTS = get_prompts(clients,"manager thankyou")
    a.command("manager thankyou")
    assert a.i_wait(MTY_PROMPTS['command_response'],10)


def test_verification_of_Activation_response_to_listners(clients):
    a, b, c, d, e = clients
    MTY_PROMPTS = get_prompts(clients,"manager thankyou")
    assert all((x.i_wait(MTY_PROMPTS['activation_response_to_listeners'],10)) for x in clients[1:3])

@pytest.mark.skip(reason="zone name need to be handled")
def test_soundout_for_available_user(clients):
    a, b, c, d, e = clients
    wait(15)
    MTY_PROMPTS = get_prompts(clients,"manager thankyou")
    assert all(x.soundout == MTY_PROMPTS["sound_out_all"] for x in clients[0:3])
   
def test_already_active_prompt_verification(clients):
    a, b, c, d, e = clients
    MTY_PROMPTS = get_prompts(clients,"manager thankyou")
    a.command("manager thankyou")
    assert a.i_wait(MTY_PROMPTS["already_active"],10)


def test_soundout_for_engaged_user(clients):
    d = clients[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(clients):
    a, b, c, d, e = clients
    MTY_PROMPTS = get_prompts(clients,"manager thankyou")
    b.command("copythat")
    assert (b.i_wait(MTY_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(MTY_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(MTY_PROMPTS["acceptance_to_listeners"], 10))

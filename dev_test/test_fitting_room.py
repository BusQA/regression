from utils import wait
from ssh_client import *
import pytest
global RB_PROMPTS
from setup import get_prompts
import pytest
room = "room one"
def test_pair_me(managers):
    a=managers[0]
    a.log_on_tool()
    wait(3)
    prompts = get_prompts(managers,"roomAssistance",{'__FIRST_NAME__':a.fname,'__TARGET__':room})
    print
    a.cmd1("assign to",room)
    assert a.i_wait(prompts["paired"],20)

def test_activation_of_rnr(managers):
    a = managers[0]
    wait(5)
    prompts = get_prompts(managers, "roomAssistance", {'__ROOM_NAME__': room})
    sshCommand('/home/theatro/bin/rnr.sh')
    print "expected: "+prompts["activation_response_to_listeners"]
    assert a.i_wait(prompts["activation_response_to_listeners"],10)


def test_verification_of_soundouts(managers):
    a = managers[0]
    prompts = get_prompts(managers, "roomAssistance", {'__ROOM_NAME__': room,",":"_"})
    assert a.i_so_wait(prompts["sound_out_group"],30)

def test_verification_of_accepting_request(managers):
    a = managers[0]
    f_prompt = get_prompts(managers, "roomAssistance", {'__ROOM_NAME__': room,",":"_"})["feature_prompt"]
    prompts = get_prompts(managers, "roomAssistance", {'__ROOM_NAME__': room, "__FNAME__": f_prompt})
    a.command("copythat")
    print prompts["acceptances_to_requestor"]
    assert a.i_wait(prompts["acceptances_to_requestor"],10)


def test_verification_of_redirect(managers):
    a,b,c,d,e = managers
    [x.log_on_tool() for x in managers[1:]]
    f_prompt = get_prompts(managers, "roomAssistance", {'__ROOM_NAME__': room,",":"_"})["feature_prompt"]
    prompts = get_prompts(managers, "roomAssistance", {'__ROOM_NAME__': room, "__FNAME__": f_prompt})
    sshCommand('/home/theatro/bin/rnr.sh')
    if a.i_wait(prompts["activation_response_to_listeners"], 10):
        wait(20)
        a.cmd1("redirect",room)
        wait(20)
        a.command("copythat")
        wait(20)

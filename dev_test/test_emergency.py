from utils import wait,DB_UPDATE
import pytest
import threading
from concurrent.futures import ThreadPoolExecutor
from communicator import *
from ssh_client import *
from DB_HANDLE import get_feature_prompts,making_prompts
pytest.EM_PROMPTS_1=get_feature_prompts('emergency',1)
pytest.EM_PROMPTS_2=get_feature_prompts('emergency',2)
pytest.EM_PROMPTS_3=get_feature_prompts('emergency',3)
pytest.EM_PROMPTS_4=get_feature_prompts('emergency',4)
pytest.EM_PROMPTS_5=get_feature_prompts('emergency',5)
pytest.EM_PROMPTS_6=get_feature_prompts('emergency',6)
pytest.EM_PROMPTS_7=get_feature_prompts('emergency',7)
pytest.EM_PROMPTS_8=get_feature_prompts('emergency',8)
pytest.EM_PROMPTS_9=get_feature_prompts('emergency',1)
pytest.EM_PROMPTS_10=get_feature_prompts('emergency',1)
pytest.EM_PROMPTS_11=get_feature_prompts('emergency',1)
pytest.EM_PROMPTS_12=get_feature_prompts('emergency',1)
'''
a ['kristin clark', allowed user #front end 
b'sam figueroa', target group # front end
c'jr sutton', target group #front end
d'faith arnold',allowed user #1 #front end 
e'allen tippet', zone leader #2 #front end
f'david jason', zone leader #front end
g'paul bradley'service desk associate #1 # millwork
h'jessica raybourn'allowed user #2 #millwork
i'jose jose' south bay leader#3 #millwork
j'joju pj'], normal user # millwork
k'kesi nani', #millwork
l'susan anderson' #millwork target group



'''

#@pytest.mark.skip()
def test_emergency_first_prompt(emergency_clients):
    for x in emergency_clients:
        x.log_on_json()
        wait(2)
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt = (making_prompts(pytest.EM_PROMPTS_1, {'__TO__':a.name, '__TO_FN__': a.fname})['select_service']).lower()
    [sshCommand(command='/home/theatro/bin/nping1.sh "{0}" "{1}"'.format(d.mac_address,'millwork'))]
    [sshCommand(command='/home/theatro/bin/nping1.sh "{0}" "{1}"'.format(h.mac_address,'flooring'))]
    a.command('emergency guide')
    assert a.i_wait(expected_prompt)

#@pytest.mark.skip()
def test_evacuation_prompt(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt = (making_prompts(pytest.EM_PROMPTS_1,{})['confirm_activation_evacuation']).lower()
    a.command('evacuation')
    assert a.i_wait(expected_prompt)

#@pytest.mark.skip()
def test_begin_evacuation(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt_1 = (making_prompts(pytest.EM_PROMPTS_1,{'__TO_FN__': a.fname})['initiator_initial_info']).lower()
    #expected_prompt_2 = (making_prompts(pytest.EM_PROMPTS_2,{})['initiator_initial_info']).lower()
    expected_prompt_3 = (making_prompts(pytest.EM_PROMPTS_3,{})['initiator_initial_info']).lower()
    expected_prompt_4 = (making_prompts(pytest.EM_PROMPTS_4,{})['initiator_initial_info']).lower()
    expected_prompt_5 = (making_prompts(pytest.EM_PROMPTS_5,{})['initiator_initial_info']).lower()
    expected_prompt_6 = (making_prompts(pytest.EM_PROMPTS_1,{'__COUNT__': '12'})['200_more_than_one']).lower()
    expected_prompt_7 = (making_prompts(pytest.EM_PROMPTS_7,{})['initiator_initial_info']).lower()
    expected_prompt_8 = (making_prompts(pytest.EM_PROMPTS_8,{})['initiator_initial_info']).lower()
    expected_prompt_9 = (making_prompts(pytest.EM_PROMPTS_1,{})['initiator_subsequent_info']).lower()
    expected_prompt_10 = (making_prompts(pytest.EM_PROMPTS_9,{'__TO_FN__': b.fname})['target_group_initial_info']).lower()
    expected_prompt_11 = (making_prompts(pytest.EM_PROMPTS_10,{'__TO_FN__': c.fname})['target_group_initial_info']).lower()
    expected_prompt_12 = (making_prompts(pytest.EM_PROMPTS_2,{})['target_group_initial_info']).lower()
    expected_prompt_13 = (making_prompts(pytest.EM_PROMPTS_3,{})['target_group_initial_info']).lower()
    expected_prompt_14 = (making_prompts(pytest.EM_PROMPTS_4,{})['target_group_initial_info']).lower()
    expected_prompt_15 = (making_prompts(pytest.EM_PROMPTS_5,{})['target_group_initial_info']).lower()
    expected_prompt_17 = (making_prompts(pytest.EM_PROMPTS_7,{})['target_group_initial_info']).lower()
    expected_prompt_18 = (making_prompts(pytest.EM_PROMPTS_11,{'__TO_FN__': e.fname, '__ZONE__': 'south bay'})['zone_leader_initial_info']).lower()
    expected_prompt_19 = (making_prompts(pytest.EM_PROMPTS_12,{'__TO_FN__': f.fname, '__ZONE__': 'north bay'})['zone_leader_initial_info']).lower()
    expected_prompt_20 = (making_prompts(pytest.EM_PROMPTS_2,{})['zone_leader_initial_info']).lower()
    expected_prompt_21 = (making_prompts(pytest.EM_PROMPTS_3,{})['zone_leader_initial_info']).lower()
    expected_prompt_22 = (making_prompts(pytest.EM_PROMPTS_4,{})['zone_leader_initial_info']).lower()
    expected_prompt_23 = (making_prompts(pytest.EM_PROMPTS_5,{})['zone_leader_initial_info']).lower()
    expected_prompt_25 = (making_prompts(pytest.EM_PROMPTS_7,{})['zone_leader_initial_info']).lower()
    expected_prompt_26 = (making_prompts(pytest.EM_PROMPTS_1,{})['accepted_confirmation']).lower()
    expected_prompt_27 = (making_prompts(pytest.EM_PROMPTS_1,{'__COUNT__': '12'})['210_more_than_one']).lower()
    expected_prompt_28 = (making_prompts(pytest.EM_PROMPTS_1,{})['215_equals_one']).lower()
    expected_prompt_29 = (making_prompts(pytest.EM_PROMPTS_3,{})['dynamic_online_info']).lower()
    expected_prompt_30 = (making_prompts(pytest.EM_PROMPTS_1,{})['service_desk_info']).lower()
    expected_prompt_31 = (making_prompts(pytest.EM_PROMPTS_1,{'__COUNT__': '12'})['530_more_than_one']).lower()
    expected_prompt_32 = (making_prompts(pytest.EM_PROMPTS_1,{})['535_equals_one']).lower()
    expected_prompt_33 = (making_prompts(pytest.EM_PROMPTS_1,{})['540_equals_one']).lower()
    expected_prompt_34 = (making_prompts(pytest.EM_PROMPTS_4,{})['update_info']).lower()
    expected_prompt_35 = (making_prompts(pytest.EM_PROMPTS_1,{})['target_group_subsequent_info']).lower()
    expected_prompt_36 = (making_prompts(pytest.EM_PROMPTS_1,{'__EXIT_NAME__':'exit2'})['associates_initial_info']).lower()
    expected_prompt_37 = (making_prompts(pytest.EM_PROMPTS_1,{'__EXIT_NAME__':'exit2', '__LOCATION_NAME__':'front end'})['associates_subsequent_info']).lower()
    expected_prompt_38 = (making_prompts(pytest.EM_PROMPTS_1,{})['update_after_acknowledgement']).lower()
    expected_prompt_39 = (making_prompts(pytest.EM_PROMPTS_1,{'__FULL_NAME_LOC_LIST__':a.name +',near,front end'})['list_info']).lower()
    expected_prompt_40 = (making_prompts(pytest.EM_PROMPTS_9,{'__EXIT_NAME__':'exit1'})['associates_initial_info']).lower()
    expected_prompt_41 = (making_prompts(pytest.EM_PROMPTS_9,{'__EXIT_NAME__':'exit1', '__LOCATION_NAME__':'millwork'})['associates_subsequent_info']).lower()
    expected_prompt_42 = (making_prompts(pytest.EM_PROMPTS_10,{'__EXIT_NAME__':'exit2'})['associates_initial_info']).lower()
    expected_prompt_43 = (making_prompts(pytest.EM_PROMPTS_10,{'__EXIT_NAME__':'exit2', '__LOCATION_NAME__':'flooring'})['associates_subsequent_info']).lower()

    a.command('yes')

    def assert_initiator_prompts():
        #Verify the prompts of Initiator
        assert a.i_long_prompt_wait(expected_prompt_1,60)
        assert a.i_wait(expected_prompt_3+',no_input',60)
        assert a.i_wait(expected_prompt_4+',no_input',60)
        a.command('start over')
        assert a.i_wait(expected_prompt_3+',no_input',60)
        assert a.i_wait(expected_prompt_4+',no_input',60)
        assert a.i_wait(expected_prompt_5+',no_input',60)
        #Issue Repeat
        a.command('repeat')
        assert a.i_wait(expected_prompt_5+',no_input',60)
        assert a.i_wait(expected_prompt_6+',no_input',60)
        assert a.i_wait(expected_prompt_7+',no_input',60)
        assert a.i_wait(expected_prompt_8+',no_input',60)
        assert a.i_wait(expected_prompt_9+',no_input',60)
        assert a.i_wait(expected_prompt_27+',no_input',60)
        #assert a.i_wait(expected_prompt_28+',no_input',60)
        assert a.i_wait(expected_prompt_29+',no_input',60)
        assert a.i_wait(expected_prompt_9+',no_input',60)

    def assert_managers_prompts_1():
        #Verify the prompts of Managers
        assert b.i_wait(expected_prompt_10+',no_input',60)
        assert b.i_wait(expected_prompt_12+',no_input',60)
        assert b.i_wait(expected_prompt_13+',no_input',60)
        b.command('start over')
        assert b.i_wait(expected_prompt_12+',no_input',60)
        assert b.i_wait(expected_prompt_13+',no_input',60)
        assert b.i_wait(expected_prompt_14+',no_input',60)
        assert b.i_wait(expected_prompt_15+',no_input',60)
        #Issue copy that
        b.command('copy that')
        #Verify the Evacuation Update
        assert b.i_wait(expected_prompt_26+',no_input',60)
        assert b.i_wait(expected_prompt_27+',no_input',80)
        #assert b.i_wait(expected_prompt_28+',no_input',60)
        assert b.i_wait(expected_prompt_29+',no_input',60)
        b.command('update me')
        assert b.i_wait(expected_prompt_31+',no_input',60)
        assert b.i_wait(expected_prompt_32+',no_input',60)
        #assert b.i_wait(expected_prompt_33+',no_input',60)
        assert b.i_wait(expected_prompt_34+',no_input',60)

    def assert_managers_prompts_2():
        #Verify the prompts of Managers
        assert c.i_wait(expected_prompt_11+',no_input',60)
        assert c.i_wait(expected_prompt_12+',no_input',60)
        assert c.i_wait(expected_prompt_13+',no_input',60)
        #Issue Repeat
        c.command('repeat')
        assert c.i_wait(expected_prompt_13+',no_input',60)
        assert c.i_wait(expected_prompt_14+',no_input',60)
        assert c.i_wait(expected_prompt_15+',no_input',60)
        #Verify the Evacuation Update 
        assert c.i_wait(expected_prompt_6+',no_input',60)
        assert c.i_wait(expected_prompt_17+',no_input',60)
        assert c.i_wait(expected_prompt_15+',no_input',80)
        assert c.i_wait(expected_prompt_27+',no_input',60)
        #assert a.i_wait(expected_prompt_28+',no_input',60)
        assert c.i_wait(expected_prompt_29+',no_input',60)

    def assert_zone_leader_prompts_1():
        #Verify the prompts of ZOne leader
        assert e.i_wait(expected_prompt_18+',no_input',60)
        assert e.i_wait(expected_prompt_20+',no_input',60)
        assert e.i_wait(expected_prompt_21+',no_input',60)
        e.command('start over')
        assert e.i_wait(expected_prompt_20+',no_input',60)
        assert e.i_wait(expected_prompt_21+',no_input',60)
        assert e.i_wait(expected_prompt_22+',no_input',60)
        assert e.i_wait(expected_prompt_23+',no_input',60)
        #Issue copy that
        e.command('copy that')
        #Verify the Evacuation Update 
        assert e.i_wait(expected_prompt_26+',no_input',60)
        assert e.i_wait(expected_prompt_27+',no_input',60)
        #assert e.i_wait(expected_prompt_28+',no_input',60)
        assert e.i_wait(expected_prompt_29+',no_input',80)
        assert e.i_wait(expected_prompt_27+',no_input',60)
        #assert e.i_wait(expected_prompt_28+',no_input',60)
        assert e.i_wait(expected_prompt_29+',no_input',80)

    def assert_zone_leader_prompts_2():
        #Verify the prompts of ZOne leader
        assert f.i_wait(expected_prompt_19+',no_input',60)
        assert f.i_wait(expected_prompt_20+',no_input',60)
        assert f.i_wait(expected_prompt_21+',no_input',60)
        #Issue Repeat
        f.command('repeat')
        assert f.i_wait(expected_prompt_21+',no_input',60)
        assert f.i_wait(expected_prompt_22+',no_input',60)
        #verify the initial entry into Evacuation Update
        assert f.i_wait(expected_prompt_23+',no_input',60)
        assert f.i_wait(expected_prompt_6+',no_input',60)
        assert f.i_wait(expected_prompt_25+',no_input',60)
        #Verify the subsequent entry into Evacuation Update
        assert f.i_wait(expected_prompt_23+',no_input',80)
        assert f.i_wait(expected_prompt_27+',no_input',60)
        #assert f.i_wait(expected_prompt_28+',no_input',60)
        assert f.i_wait(expected_prompt_29+',no_input',80)
        f.command('list associates')
        assert f.i_wait(expected_prompt_39,80)
        assert f.i_wait(expected_prompt_23+',no_input',80)

    def assert_service_desk_prompts():
        assert g.i_wait(expected_prompt_30,60)
        g.command('copy that')
        assert g.i_wait(expected_prompt_36+',no_input',60)
        assert g.i_wait(expected_prompt_37+',no_input',80)
        g.command('copy that')
        assert g.i_wait(expected_prompt_26+',no_input',60)
        assert g.i_wait(expected_prompt_38,60)
    
    def assert_other_user_prompts():
        assert d.i_wait(expected_prompt_40+',no_input',60)
        assert h.i_wait(expected_prompt_42+',no_input',60)
        assert d.i_wait(expected_prompt_41+',no_input',80)
        assert h.i_wait(expected_prompt_43+',no_input',80)

        

    #executor = ThreadPoolExecutor(max_workers=2)
    with ThreadPoolExecutor(max_workers=8) as executor:
        executor.submit(assert_initiator_prompts)
        executor.submit(assert_managers_prompts_1)
        executor.submit(assert_managers_prompts_2)
        executor.submit(assert_zone_leader_prompts_1)
        executor.submit(assert_zone_leader_prompts_2)
        executor.submit(assert_service_desk_prompts)
        executor.submit(assert_other_user_prompts)

#@pytest.mark.skip()
def test_all_clear(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt_1 = (making_prompts(pytest.EM_PROMPTS_1,{})['users_are_online']).lower()
    expected_prompt_2 = (making_prompts(pytest.EM_PROMPTS_1,{})['close_confirmation']).lower()
    expected_prompt_3 = (making_prompts(pytest.EM_PROMPTS_1,{})['close_to_listener']).lower()
    expected_prompt_4 = (making_prompts(pytest.EM_PROMPTS_1,{})['assigne_all_clear']).lower()
    d.command('all clear')
    assert d.i_wait(expected_prompt_1,60)
    d.command('yes')
    assert d.i_wait(expected_prompt_2)
    assert a.i_wait(expected_prompt_3,60)
    assert b.i_wait(expected_prompt_3,60)
    assert c.i_wait(expected_prompt_3,60)
    assert e.i_wait(expected_prompt_3,60)
    assert f.i_wait(expected_prompt_3,60)
    assert g.i_wait(expected_prompt_3,60)
    assert h.i_wait(expected_prompt_3,60)
    assert i.i_wait(expected_prompt_3,60)
    assert j.i_wait(expected_prompt_3,60)
    assert k.i_wait(expected_prompt_3,60)
    assert l.i_wait(expected_prompt_3,60)



'''def test_normal_user_prompts_first_location(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_normal=b,c,e,f
    wait(15)
    #attention_to_all,The_nearest_exit_to,__LOCATION_NAME__,is,__EXIT_NAME__,Say_copy_that_to_acknowledge_that_you_have_heard_this_message
    #Added stub to verify script
    expected_prompts=(making_prompts(pytest.EM_PROMPTS_1, {'__LOCATION_NAME__': 'front end', '__EXIT_NAME__':'exit2'})['    ']).lower()
    assert all((x.output() in expected_prompts) for x in list_normal)

def test_normal_user_prompts_second_location(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_normal=h,i,j,k
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    prompt_4 = (making_prompts(EM_PROMPTS, {'__LOCATION_NAME__': 'millwork', '__EXIT_NAME__': 'exit1'})[
        'associates_initial_info']).lower()
    #assert all (x.i_wait(prompt_4) for x in list_normal)
    assert i.output() in prompt_4
    assert j.output() in prompt_4
    assert k.output() in prompt_4
    assert h.output() in prompt_4


def test_floor_leader_prompt(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_lead=d,g
    #__TO__, __TO_FN__, you_are_the_leader_for, __ZONE__
    expected_prompt_1= making_prompts(pytest.EM_PROMPTS_1, {'__TO__': d.name, '__TO_FN__': d.fname,'__ZONE__':'north bay'})['zone_leader_initial_info']
    expected_prompt_2 = making_prompts(pytest.EM_PROMPTS_1, {'__TO__': g.name, '__TO_FN__': g.fname, '__ZONE__': 'south bay'})[
        'zone_leader_initial_info']

    assert d.i_wait(expected_prompt_1)
    assert g.i_wait(expected_prompt_2)


def test_floor_leader_announcememnt(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_lead=d,g
    assert all((x.i_wait(pytest.EM_PROMPTS_2['zone_leader_initial_info'], 10)) for x in list_lead)

@pytest.mark.skip()
def test_manager_announcemment(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_mgr=c,l

    #__ASSOCIATES_TOTAL_COUNT__, online, __ZONE_LEADER_ACCEPTED_COUNT__, of, __ZONE_LEADER_TOTAL_COUNT__, zones_are_cleared, __ASSOCIATES_ACCEPTED_COUNT__, of, __ASSOCIATES_TOTAL_COUNT__, acknowledged, Say_List_Associates_and_I_will_tell_you_who_remains_online
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    prompt = (making_prompts(EM_PROMPTS, {'__ASSOCIATES_TOTAL_COUNT__': '11',
                                          '__ZONE_LEADER_ACCEPTED_COUNT__': '0',
                                          '__ZONE_LEADER_TOTAL_COUNT__': '2',
                                          '__ASSOCIATES_TOTAL_COUNT__': '11',
                                          '__ASSOCIATES_ACCEPTED_COUNT__': '0',
                                          }))['target_group_subsequent_info'].lower()
    assert all((x.i_wait(prompt)) for x in list_mgr)

def test_floor_leader_announcememnt_repeat(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_lead=d,g
    [x.make_available() for x in list_lead]

    assert all((x.i_wait(pytest.EM_PROMPTS_2['zone_leader_initial_info'], 60)) for x in list_lead)


def test_clear_prompt(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    wait(30)
    assert a.i_wait(pytest.EM_PROMPTS_1['initiator_subsequent_info'])


def test_target_group_announcememnt_repeat(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    list_mgr=c,l
    [x.make_available() for x in list_mgr]
    #__ASSOCIATES_TOTAL_COUNT__, online, __ZONE_LEADER_ACCEPTED_COUNT__, of, __ZONE_LEADER_TOTAL_COUNT__, zones_are_cleared, __ASSOCIATES_ACCEPTED_COUNT__, of, __ASSOCIATES_TOTAL_COUNT__, acknowledged, Say_List_Associates_and_I_will_tell_you_who_remains_online
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    prompt = (making_prompts(EM_PROMPTS, {'__ASSOCIATES_TOTAL_COUNT__': '11',
                                          '__ZONE_LEADER_ACCEPTED_COUNT__': '0',
                                          '__ZONE_LEADER_TOTAL_COUNT__': '2',
                                          '__ASSOCIATES_TOTAL_COUNT__': '7',
                                          '__ASSOCIATES_ACCEPTED_COUNT__': '0',
                                          }))['target_group_subsequent_info'].lower()
    assert all((x.i_wait(prompt,60)) for x in list_mgr)

@pytest.mark.skip()
def test_evacuation_intiator_copy_that(emergency_clients):
    a=emergency_clients[0]
    a.command('copy that')
    wait(10)
    print a.output()

def test_evacuation_intiator_repeat(emergency_clients):
    a=emergency_clients[0]
    a.command('repeat')
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt = making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname})[
        'initiator_initial_info']
    assert a.i_wait(expected_prompt)



def test_evacuation_intiator_all_clear(emergency_clients):
    a=emergency_clients[0]
    list_all=emergency_clients[1:]
    #Ok,__TO_FN__,__TO__,I_am_glad_everyone_is_safe!_I_will_send_the_all_clear_announcement_now,tap_out_to_cancel
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt = ((making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname,}))['close_confirmation']).lower()
    a.command('all clear')
    assert a.i_wait(expected_prompt,20)
    assert all((x.i_wait(pytest.EM_PROMPTS_1['close_to_listener'])) for x in list_all)

def test_evacuation_intiator_all_clear_second_time(emergency_clients):
    a=emergency_clients[0]
    #Ok,__TO_FN__,__TO__,I_am_glad_everyone_is_safe!_I_will_send_the_all_clear_announcement_now,tap_out_to_cancel
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt = (making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname, }))['close_confirmation']
    a.command('all clear')
    assert a.i_wait(expected_prompt)'''
from utils import wait
from DB_HANDLE import get_feature_prompts,making_prompts,get_help_list
from responses import AG_PROMPTS
import pytest

review_response = "do_you_want_to_review_your_announcement_before_posting_it"
list_command_reponse='would_you_like,conversationcommands,locationcommands,managementcommands,messagecommands,assistancecommands,groupcommands,thasta_all_say_lists_or_teach_me_to_learn_more_or_tap_out_to_exit'
list_command_subset_reponse='i_am_sorry_please_say_one_of_the_following_commands'
list_command_subset_reponse_1='to_hear_how_to_use_another_command_say_next_or_say_list_commands'
retry_next_again='i_am_sorry_please_say_next_list_commands_or_tap_out_to_cancel'


def test_helpme(clients):
    a=clients[0]
    a.log_on_tool()
    a.command("help me")
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["help_me_command_list"],10)
    
from utils import wait,DB_UPDATE
import pytest
from communicator import *
from DB_HANDLE import get_feature_prompts
pytest.EM_PROMPTS=get_feature_prompts('emergency')

'''
['angella hilt', allowed user #front end
'ben sander', allowed user # front end
'amanda peterson', target group #front end
'david jason',north bay leader #1 #plumping
'donnie brady', north bay leader #2 #plumping
'ricky vance', #plumping

'bob bennion'south bay leader #1 # millwork
'allen tippet'south bay leader #2 #millwork
'amanda scott' south bay leader#3 #millwork
'jay bosler'], normal user # millwork
'david clarke', #millwork
'holli root' #millwork



'''


def test_emergency_guide_tap_out(emergency_clients):
    a = emergency_clients[0]
    a.log_on_tool()
    a.command('emergency guide')
    wait(2)
    a.tap_out()
    EM_PROMPTS=get_feature_prompts('emergency')
    assert a.i_wait(EM_PROMPTS['info_canceled'])

def test_evacuation_tap_out(emergency_clients):
    a = emergency_clients[0]
    a.command('evacuate the store')
    wait(2)
    a.tap_out()
    EM_PROMPTS = get_feature_prompts('emergency')
    assert a.i_wait(EM_PROMPTS['request_canceled'])

@pytest.mark.skip()
def test_time_out(emergency_clients):
    a = emergency_clients[0]
    a.command("emergency guide")
    wait(15)
    EM_PROMPTS = get_feature_prompts('emergency')
    assert False

@pytest.mark.skip()
def test_non_member_issue_emergency(emergency_clients):
    a = emergency_clients[-1]
    a.command("emergency guide")
    EM_PROMPTS = get_feature_prompts('emergency')
    assert False

@pytest.mark.skip()
def test_evacuation(emergency_clients):
    a=emergency_clients[0]
    a.command('evacuate the store')
    assert a.i_wait(pytest.EM_PROMPTS['confirm_activation'])

@pytest.mark.skip()
def test_evacuation_no(emergency_clients):
    a=emergency_clients[0]
    a.command('no')
    assert a.i_wait(pytest.EM_PROMPTS['list_options'])

@pytest.mark.skip()
def test_evacuation_no_folowed_tap_out(emergency_clients):
    a=emergency_clients[0]
    a.tap_out()
    assert a.i_wait(pytest.EM_PROMPTS['confirm_activation'])
import paramiko
from config import TGS_IP
from config import SUDO_PASSWORD


def sshCommand(command='/home/theatro/bin/restart_tgs.sh',hostname=TGS_IP, port=22, username='theatro', password=SUDO_PASSWORD):
    sshClient = paramiko.SSHClient()                                   # create SSHClient instance
    sshClient.set_missing_host_key_policy(paramiko.AutoAddPolicy())    # AutoAddPolicy automatically adding the hostname and new host key
    sshClient.load_system_host_keys()
    print command
    sshClient.connect(hostname, port, username, password)
    stdin, stdout, stderr = sshClient.exec_command(command, get_pty=True)
    stdin.write(SUDO_PASSWORD + '\n')
    stdin.flush()
    #print(stdout.read())
    print(stderr.read())
    return stdout.read() 

if __name__ == '__main__':
    sshCommand('ls -lrth','172.16.16.25', 22, 'theatro', 'Theatro1')

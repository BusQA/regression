import pytest
from config import *
from utils import *
from addcdm import *
from ssh_client import *
import time
from DB_HANDLE import *

pytest.CDM_ANNC='no message'


@pytest.fixture(scope="module")
def group_clients():
    #CLEAR_ANNOUNCEMENTS()
    cii = make_clients(count=1)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    [x.log_off_json() for x in cii]
    yield cii
    print"killing clients"
    [x.log_off_json() for x in cii]
    kill_clients(cii)
    #CLEAR_ANNOUNCEMENTS()


#@pytest.mark.skip()
@pytest.mark.parametrize("kudos,type", [
    ('kudos1','all_groups'),
    ('kudos2','all'),
    ('kudos3','group')
])
def test_users(group_clients,kudos,type):
    a = group_clients[0]
    
    wait(10)
    t=AddUpdateDeleteCDM()
    t.login_with_username_and_password('chida','Developer342!')
    if type=='all_groups':
       send_cdm_flag = t.add_cdm_send_to_allGroups()
    elif type == 'all': 
        send_cdm_flag = t.add_cdm()
    else:
        send_cdm_flag = t.add_cdm_send_to_singleGroup()


    if send_cdm_flag:
        cdm_msg=get_rnr_txid_from_tgs('play_audio','wave')
        a.log_on_tool()
        wait(10)
        print "We have recived {} as CDM message\n".format(cdm_msg)
        assert a.i_wait(cdm_msg,100)
    a.log_off_json()

#@pytest.mark.skip()
#def test_CDM_kudos_Announcements_available_user(clients):
    
'''
@pytest.mark.skip()
def test_CDM_kudos_Announcements_engaged_Groupuser(group_clients):
    a,b,c,d,e = group_clients
    wait(10)
    b.tap_out()
    assert b.i_wait("posted_at",100)

@pytest.mark.skip()
def test_CDM_kudos_Announcements_newly_logged_on_user(group_clients):
    a,b,c,d,e = group_clients
    wait(4)
    c.log_on_tool()
    assert c.i_wait("posted_at",100)

@pytest.mark.skip()
def test_CDM_kudos_Announcements_replay_last_message(group_clients):
    a,b,c,d,e = group_clients
    wait(10)
    a.command('replay last message')
    assert a.i_wait("posted_at",100)

@pytest.mark.skip()
def test_CDM_kudos_Announcements_reboot_survival(group_clients):
    a,b,c,d,e = group_clients
    sshCommand()
    wait(10)
    d.log_on_tool()
    assert a.i_wait("posted_at",100)

@pytest.mark.skip()
def test_CDM_kudos_Announcements_reboot_survival_replay_message(group_clients):
    a,b,c,d,e = group_clients
    a.command('replay last message')
    assert a.i_wait("posted_at",100)
    '''
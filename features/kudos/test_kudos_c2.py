import pytest
from config import *
from utils import *
from ssh_client import *
import time
from DB_HANDLE import *
from addcdm import *


pytest.CDM_ANNC='no message'


@pytest.fixture(scope="module")
def group_clients():
    CLEAR_ANNOUNCEMENTS()
    cii = make_clients(count=2)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    print"killing clients"
    kill_clients(cii)
    #CLEAR_ANNOUNCEMENTS()


#@pytest.mark.skip()
@pytest.mark.parametrize("kudos,type", [
    ('kudos1','all'),
    ('kudos2','group'),
    ('kudos3','user')
])

def test_C2_users(group_clients,kudos,type):
	a,b = group_clients[:2]
	[x.log_on_tool() for x in a,b]
	wait(5)
	if type == 'all':	
		prompt=get_feature_prompts('kudos_all')
		a.command("kudos")
		assert a.i_wait(prompt["go_ahead"],20)
		a.command("1to10")
		wait(3)
		assert a.i_wait(prompt["review_before_playing"],20)
		a.command("yes")
		wait(5)
		assert a.i_wait(prompt['re_record_confirm'],20)
		a.command('yes')
		wait(3)
		assert a.i_wait(prompt['re_record_go_ahead'],20)
		a.command('1to10')
		a.command('no')
		#wait(3)
		assert a.i_wait(prompt['posted'],20)
		#assert b.i_wait("posted_at",40)
		c2_msg=(get_rnr_txid_from_tgs('oration_created','oraPath')).lower()
		print "We have recived {} as Kudos message\n".format(c2_msg)
		assert b.i_wait(c2_msg,60)

	elif type == 'group':
		prompt=get_feature_prompts('kudos_grp')
		a.command('kudos {}'.format(Group_NAME))
		wait(3)
		pytest.last_message=a.last_message
		pytest.last_message
		a.command('1to10')
		assert a.i_wait(prompt['posted'],20)
		wait(5)
		c2_msg=(get_rnr_txid_from_tgs('oration_created','oraPath')).lower()
		print "We have recived {} as Kudos message\n".format(c2_msg)
		assert b.i_wait(c2_msg,60)

	elif type == 'user':
		prompt=get_feature_prompts('kudos_user')
		a.command('kudos {}'.format(a.name))
		wait(3)
		assert a.i_wait(prompt['self_message'],20)
		a.command('kudos {}'.format(b.name))
		pytest.last_message=a.last_message
		print pytest.last_message
		a.command('1to10')
		wait(3)
		assert a.i_wait(prompt['posted'],20)
		c2_msg=(get_rnr_txid_from_tgs('oration_created','oraPath')).lower()
		print "We have recived {} as Kudos message\n".format(c2_msg)
		assert b.i_wait(c2_msg,60)
		pytest.last_message=a.last_message
		print pytest.last_message

	[x.log_off_json() for x in a,b]

	

@pytest.mark.parametrize("kudos,type", [
    ('kudos1','all'),
    ('kudos2','group'),
    ('kudos3','user')
])

def test_kudos_message_cancel(group_clients,kudos,type):
	a,b = group_clients[:2]
	[x.log_on_tool() for x in a,b]
	wait(3)
	if type == 'all':
		prompt=get_feature_prompts('kudos_all')
		a.command("kudos")
		assert a.i_wait(prompt["go_ahead"],20)
		assert a.i_wait(prompt['message_cancelled'],40)

	elif type == 'group':
		prompt=get_feature_prompts('kudos_grp')
		a.command('kudos {}'.format(Group_NAME))
		wait(3)
		assert a.i_wait(prompt['message_cancelled'],40)

	else:
		prompt=get_feature_prompts('kudos_user')
		a.command('kudos {}'.format(b.name))
		wait(3)
		assert a.i_wait(prompt['message_cancelled'],40)

	[x.log_off_json() for x in a,b]

'''
	#@pytest.mark.skip()
@pytest.mark.parametrize("kudos,type", [
    ('kudos1','all_groups'),
    ('kudos2','all'),
    ('kudos3','group')
])
def test_users(group_clients,kudos,type):
    a = group_clients[0]
    
    wait(10)
    t=AddUpdateDeleteCDM()
    t.login_with_username_and_password('chida','Developer342!')
    if type=='all_groups':
       send_cdm_flag = t.add_cdm_send_to_allGroups()
    elif type == 'all': 
        send_cdm_flag = t.add_cdm()
    else:
        send_cdm_flag = t.add_cdm_send_to_singleGroup()


    if send_cdm_flag:
        cdm_msg=get_rnr_txid_from_tgs('play_audio','wave')
        a.log_on_tool()
        wait(10)
        print "We have recived {} as CDM message\n".format(cdm_msg)
        assert a.i_wait(cdm_msg,100)
        a.log_off_json()
    else:
        #t.tearDown()
        assert False
        a.log_off_json()
        '''
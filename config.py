import logging
import os
import sys
#from DB_HANDLE import get_names

#SCHEME          = 'processes' # one of ['processes', 'threads'].
SCHEME          = 'threads'
# Decides base class of communicator from the "threading" or "multiprocessing" libraries

SUDO_PASSWORD   = '4help2shoppers'
BLOCKED_IPS     = ['192.16.25.25']
SYSTEM_IP       = '8.8.25.50'
TGS_IP          = '8.8.26.26'
CLI_START_IP    = '8.8.26.50'
CLI_COUNT       =  12
LOG_ON_PORT     = 9099
WB_SUPPORT     = False
DEFAULT_LOCATION = "front end"
DEFAULT_ZONE = "north bay"
remote_rnr = ["soft button","indyme rnr","store readiness"]

# CRITICAL > ERROR > WARNING > INFO > DEBUG > NOTSET
STDOUT_LOG_LEVEL= logging.CRITICAL
FILE_LOG_LEVEL  = logging.DEBUG
STDOUT_LOG      = True
IS_DEBUG        = False
RnR_Config = {"RB":["register backup","RB_USERS","code3"],
              "CO":["carry out","CO_USERS","carry out"],
              "MA":["manager assistance","MA_USERS","manager assistance"],
              "MT":["manager thankyou","MT_USERS","manager thankyou"],
              "SB":["soft button"],
              "CP":["click and pickup"]}
PATTERN_DLT = {"tts_":"","|":""}
Record_Replay_Prams = {"ENABLE_ANNC_HOUR_RECORD_AND_REPLAY":"true",
                       "ENABLE_ANNC_NOW_RECORD_AND_REPLAY":"true",
                       "ENABLE_ANNC_TODAY_RECORD_AND_REPLAY":"true",
                      "ENABLE_HUDDLE_RECORD_AND_REPLAY":"true"}
# project base directory path
BASE_DIR        = os.path.dirname(os.path.realpath(__file__))

#TTS_DIR         = os.path.join(BASE_DIR, 'support/waves/')
TTS_ENGINE      = 'festival'    # one of ['festival']
TTS_AMPLITUDE   = 2.00          # float scale for festival

CLUES_FILE      = os.path.join(BASE_DIR,'support/clues.txt')
  
LOGS_PATH       = os.path.join(BASE_DIR, 'logs/')
LOGS_FORMAT     =   {'fmt': '%(asctime)s [%(levelname)s] %(funcName)s | %(name)s-%(threadName)s ->>>- %(message)s'
                    ,'datefmt': '%Y-%m-%d %H:%M:%S'
                    }
driver_path = os.path.join(BASE_DIR, 'drivers\\chromedriver.exe')

#store configuration
USER_NAMES = ["donnie brady","elizabeth hunter","elizabeth roy"]
COMPANY_ID= 1
STORE_ID= 615
url = 'https://8.8.26.26'
user="chida"
password="Developer342!"
cdmname="kudos_"
storeName="SmokeSuite"
groupName="hunting"
scheduleType="Absolute"
scheduleTimeZone="(GMT+05:30) India Standard Time"
updatecdmname="cdmname"
#driver_path=r"E:\smoke_tool_21May18\TCM_Cases\chromedriver.exe"
# remote configuration

CLOUD_URL           = 'https://itg3.theatro.com/restapi/audit/'
MAIL_URL            = 'https://itg3.theatro.com/restapi/mail/send'
#TW_LOGIN            = 'auditapi'
#TW_PASSWORD         = 'd1a98e54ca892633b02a94df140db9c0'
TW_LOGIN            = 'adminnew'
TW_PASSWORD         = 'ec9e9edfaba9f6fea36671144ca9816a'

###############################################################################
# store configuration
M_DIR = os.getcwd()
if sys.platform == 'win32':
  TGS_DB_PATH = M_DIR + '\\db\\'
  AUDIO_PATH = M_DIR + '\\names'
  if WB_SUPPORT:
    TTS_DIR = M_DIR + '\\support\\waves_16k\\'
  else:
    TTS_DIR = M_DIR + '\\support\\waves_8k\\'


else:
  TGS_DB_PATH = '/opt/theatro/etc/'
  GRAMMAR_PATH = '/opt/theatro/etc/Sounds/GrammarFiles/'
  AUDIO_PATH = '/opt/theatro/etc/Sounds_Rel1_1/Announcements/names/'
  LOGGEDON_GRXML = 'persons_loggedon.grxml'
  NONLOGGEDON_GRXML = 'persons_nonloggedon.grxml'
  ALL_GRXML = 'persons_all.grxml'
  GROUPS_GRXML = 'groups.grxml'
  TTS_DIR = os.path.join(BASE_DIR, 'support/waves/')

###############################################################################
# mail configuration

#MAIL_RECIPIENTS     = ['bipin@theatrolabs.com','ramya@theatrolabs.com','kiran@theatrolabs.com','vijay@theatrolabs.com','srinivasarao@theatrolabs.com','pavan@theatrolabs.com','chidanand@theatrolabs.com']


MAIL_RECIPIENTS     = ['bipin@theatrolabs.com','chidanand@theatrolabs.com','abhinandan@theatrolabs.com']
MAIL_HEADER         =   '''
                        <!DOCTYPE html>
                        <html>
                        <head>
                            <meta charset="UTF-8">
                            <title></title>
                        </head>
                        <body>
                        <table border="1" cellpadding="5" cellspacing="0">
                        '''
MAIL_FOOTER = '''
                        </table>
                        <br/>
                        <br/>
                        <i>NOTE: This is a computer generated mail. This mail is sent as an indication of the results found from an audit of the respective customer store. Any attempt to reply to this email address will not result in any further action.</i>
                        </body>
                        </html>
                        '''
#



NAMES = {"donnie brady"}
#NAMES=get_names()
RNR_REQ={'server_id': '286', 
          'remoteTxId': '', 
          'responderCount': '0', 
          'listeners': '', 
          'requestedCount': '1', 
          'user': '1234', 
          'device': '0023a729eab1', 
          'location': ''}

RNR_ACC={'server_id': '286', 
'remoteTxId': '', 
'responderCount': '0', 
'listeners': [], 
'requestedCount': '1', 
'user': '1234', 
'device': '0023a7347328',
'type': 'carry_out_accept',
 'location': ''}

RNR_CALL={'audience_count': '3','duration': '25','initiator_id': '0','initiator_type': '5','node_id': 'room1','node_name': 'room1','node_type': '1','reason_code': '201','requested_count': '1','responded_count': '1','server_id': '286','service_id': '210','service_trigger_type': '0','service_type': '2','session_id': '','status': '4','type': 'alteration'}

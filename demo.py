import sys
from config import TGS_DB_PATH
import sqlite3


def db_connect():
    conn = sqlite3.connect("{0}TGS.db".format(TGS_DB_PATH))
    with conn:
        c = conn.cursor()
        return c


def to_unicode(unicode_or_str):
    if isinstance(unicode_or_str,str):
        value=unicode_or_str.decode('utf-8')
    else:
        value=unicode_or_str
    return value

def get_values_from_tcm(ora_id):
    with SSHTunnelForwarder(('itg0.theatro.com', 22), ssh_password='4help2shoppers', ssh_username='theatro', remote_bind_address=('localhost', 3306)) as server:
       conn = None
       conn = MySQLdb.connect(host='127.0.0.1', port=server.local_bind_port, user='theatro', passwd='theatro',db='theatrostat')
       cursor = conn.cursor(MySQLdb.cursors.DictCursor)
       tables=cursor.execute('''select o.oration_id, o.device,o.user,o.command,o.oration_path,o.duration,o.call_id,o.listeners,o.server_id,o.call_type,o.TXId,oh.system_oration_id,oh.timeToPlay,tc.CompanyID,tc.StoreID,tc.source_user,tc.target_user,tc.target_group,tc.target_all,tc.start_time,tc.end_time,tc.end_cause,tc.start_time_epoch,tc.end_time_epoch,tc.orig_client_type,tc.attemptedTagout,tc.call_duration from Oration o join OrationHeard oh join TheatroCall tc on o.txId=tc.txId where tc.txId in (select txID from Oration where oration_id="{0}") limit 1;'''.format(ora_id))
       results = cursor.fetchall()
       return results[0]



def get_feature_prompts(FeatureName="register backup"):
	c=db_connect()
	dict1 = {}
	[dict1.update({str(k):str(v)}) for k,v in c.execute('''select context_name,prompt from feature_prompt where feature_name = "{0}";'''.format(FeatureName)).fetchall()]
	return dict1


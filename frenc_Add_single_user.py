import pytest
from config import NAMES
from utils import wait


@pytest.mark.dependency()

def test_add_employee(clients):
    a=clients[0]
    assert a.add_employee("black raven")


@pytest.mark.dependency(depends=["test_add_employee"])
def test_verify_logon(clients):
    a=clients[0]
    wait(5)
    a.logon_name("black raven")
    assert a.i_wait("black raven",20)

from utils import *
from DB_HANDLE import MT_USERS,MOD_USERS,RB_USERS,RB_ESC_USERS,ESC_Group_NAME
import pytest
from DB_HANDLE import get_feature_prompts,making_prompts
from config import NAMES,DEFAULT_ZONE,remote_rnr
ENABLE_RECORD_REPLAY = {"ENABLE_ANNC_HOUR_RECORD_AND_REPLAY":"true","ENABLE_ANNC_NOW_RECORD_AND_REPLAY":"true","ENABLE_ANNC_TODAY_RECORD_AND_REPLAY":"true","ENABLE_HUDDLE_RECORD_AND_REPLAY":"true"}
DISABLE_RECORD_REPLAY = {"ENABLE_ANNC_HOUR_RECORD_AND_REPLAY":"false","ENABLE_ANNC_NOW_RECORD_AND_REPLAY":"false","ENABLE_ANNC_TODAY_RECORD_AND_REPLAY":"false","ENABLE_HUDDLE_RECORD_AND_REPLAY":"false"}



def get_prompts(clients,feature='register backup',to_convert="",zone=''):
    global ESC_Group_NAME
    prompts = get_feature_prompts(feature)
    initiator_name=clients[0].name
    acceptor_name=clients[1].name
    first_name=clients[0].fname 
    if feature in remote_rnr:
        initiator_name="soft client"
    if feature == "request":
        ESC_Group_NAME = 'register backup'
    
    if zone:
    	d2 = {'__ZONE__': zone, '__FROM__': initiator_name, '__ACCEPTOR__': acceptor_name, '__TO__': initiator_name, '__GROUP__':ESC_Group_NAME}
    else:
		d2 = {',__ZONE__': zone, '__FROM__': initiator_name, '__ACCEPTOR__': acceptor_name, '__TO__': initiator_name, '__GROUP__':ESC_Group_NAME}
    if to_convert:
        d2.update(to_convert)
    p = making_prompts(prompts, d2)
    p.update(get_feature_prompts('common feature'))
    return  p



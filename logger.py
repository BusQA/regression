import logging
import config
import os
import sys

def get_logger(name, stdout=True, stdout_level=logging.DEBUG, file_level=logging.DEBUG):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    # formatter
    log_formatter = logging.Formatter(fmt=config.LOGS_FORMAT['fmt'], datefmt=config.LOGS_FORMAT['datefmt'])
    # file handler
    log_file = os.path.join(config.LOGS_PATH, name + '.log')
    file_handler = logging.FileHandler(log_file, mode='a')
    file_handler.setFormatter(log_formatter)
    file_handler.setLevel(file_level)
    logger.addHandler(file_handler)
    # stream handler
    if stdout:
        stream_handler = logging.StreamHandler(sys.stdout)
        stream_handler.setFormatter(log_formatter)
        stream_handler.setLevel(stdout_level)
        logger.addHandler(stream_handler)
    #
    return logger

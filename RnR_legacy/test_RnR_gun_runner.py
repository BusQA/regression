from utils import wait,DB_UPDATE
import pytest
global GUN_PROMPTS
from setup import get_prompts
from communicator import *
from config import DEFAULT_ZONE

def test_gun_runner_initating_tap_out(GUN_clients):
    a,b,c,d,e = GUN_clients
    a.log_on_tool()
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    a.command("gun runner")
    wait(2)
    a.tap_out()
    assert a.i_wait(GUN_PROMPTS['cancell_request'],10)


def test_gun_runner_initating_request(GUN_clients):
    a,b,c,d,e = GUN_clients
    [x.log_on_tool() for x in b,c,d,e]
    d.make_engaged()
    wait(5)
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    a.command("gun runner")
    assert a.i_wait(GUN_PROMPTS['command_response'],10)


def test_verification_of_Activation_response_to_listners(GUN_clients):
    a, b, c, d, e = GUN_clients
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    assert all((x.i_wait(GUN_PROMPTS['activation_response_to_listeners'],30)) for x in GUN_clients[1:3])

def test_soundout_for_available_user(GUN_clients):
    a, b, c, d, e = GUN_clients
    wait(15)
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    assert all(x.soundout == GUN_PROMPTS["sound_out_all"] for x in GUN_clients[0:3])


def test_already_active_prompt_verification(GUN_clients):
    a, b, c, d, e = GUN_clients
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    a.command("gun runner")
    wait(2)
    assert a.i_wait(GUN_PROMPTS["already_active"],10)

def test_non_member_says_copy_that(GUN_clients):
    a, b, c, d, e = GUN_clients
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    print "We expect :"+GUN_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(GUN_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(GUN_clients):
    d = GUN_clients[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(GUN_clients):
    a, b, c, d, e = GUN_clients
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    b.command("copythat")
    wait(5)
    assert (b.i_wait(GUN_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(GUN_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(GUN_PROMPTS["acceptance_to_listeners"], 10))

def test_gun_runner_timeout(GUN_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    a, b, c, d, e = GUN_clients
    [x.log_on_tool() for x in GUN_clients]
    GUN_PROMPTS = get_prompts(GUN_clients,"Gun Runner",zone=DEFAULT_ZONE)
    a.command("gun runner")
    assert a.i_wait(GUN_PROMPTS['rnr_timeout'],40)
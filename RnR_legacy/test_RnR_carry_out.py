from utils import wait,DB_UPDATE
import pytest
global CO_PROMPTS
from setup import get_prompts
from communicator import *

def test_carry_out_initating_request(CO_clients):
    a,b,c,d,e=CO_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    CO_PROMPTS = get_prompts(CO_clients,"carry out")
    a.command("carry out")
    assert a.i_wait(CO_PROMPTS['activation_response_to_requestor'],10)


def test_verification_of_Activation_response_to_listners(CO_clients):
    a, b, c, d, e = CO_clients
    CO_PROMPTS = get_prompts(CO_clients,"carry out")
    assert all((x.i_wait(CO_PROMPTS['activation_response_to_listeners'],10)) for x in CO_clients[1:3])

def test_soundout_for_available_user(CO_clients):
    a, b, c, d, e = CO_clients
    wait(15)
    CO_PROMPTS = get_prompts(CO_clients,"carry out")
    assert all(x.soundout == CO_PROMPTS["sound_out_all"] for x in CO_clients[0:3])


def test_already_active_prompt_verification(CO_clients):
    a, b, c, d, e = CO_clients
    CO_PROMPTS = get_prompts(CO_clients,"carry out")
    a.command("carry out")
    assert a.i_wait(CO_PROMPTS["already_active"],10)

def test_non_member_says_copy_that(CO_clients):
    a, b, c, d, e = CO_clients
    CO_PROMPTS = get_prompts(CO_clients,"carry out")
    print "We expect :"+CO_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(CO_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(CO_clients):
    d = CO_clients[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(CO_clients):
    a, b, c, d, e = CO_clients
    CO_PROMPTS = get_prompts(CO_clients,"carry out")
    b.command("copythat")
    assert (b.i_wait(CO_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(CO_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(CO_PROMPTS["acceptance_to_listeners"], 10))

def test_carry_out_timeout(CO_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    a, b, c, d, e = CO_clients
    [x.log_on_tool() for x in CO_clients]
    CO_PROMPTS = get_prompts(CO_clients, "carry out")
    a.command("carry out")
    assert a.i_wait(CO_PROMPTS['rnr_timeout'],40)
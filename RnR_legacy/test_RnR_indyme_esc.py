from utils import wait,DB_UPDATE
import pytest,os
global IM_PROMTS
from setup import get_prompts
from communicator import *
from ssh_client import *

'''
["andrea birdsong", RB member
"mike diviney", non member RB,managers member
"joanna coffey", RB member
"bev janes", RB member
"angella hilt" non member RB,managers member
]'''

def test_indyme_initating_request(RB_ESC_clients):
    a,b,c,d,e = RB_ESC_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    wait(7)
    d.make_engaged()
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(2)
    IM_PROMTS = get_prompts(RB_ESC_clients,"indyme rnr")
    assert a.i_wait(IM_PROMTS['activation_response_to_listeners'],10) and c.i_wait(IM_PROMTS['activation_response_to_listeners'],10)


def test_escalation_response_to_target_group(RB_ESC_clients):
    wait(30)
    a, b, c, d, e = RB_ESC_clients
    IM_PROMTS = get_prompts(RB_ESC_clients,"indyme rnr")
    assert a.i_wait(IM_PROMTS["escalation_to_listeners"])
    assert c.i_wait(IM_PROMTS["escalation_to_listeners"])

def test_escalation_response_to_escalation_group(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    IM_PROMTS = get_prompts(RB_ESC_clients,"indyme rnr")
    assert e.i_wait(IM_PROMTS["activation_response_to_escalation_listeners"])
    assert b.i_wait(IM_PROMTS["activation_response_to_escalation_listeners"])

def test_soundout_for_engaged_user(RB_ESC_clients):
    d = RB_ESC_clients[-2]
    assert d.soundout == "twinkle1"

def test_soundout_for_escalation_user(RB_ESC_clients):
    wait(10)
    a, b, c, d, e = RB_ESC_clients
    IM_PROMTS = get_prompts(RB_ESC_clients,"indyme rnr")
    assert e.soundout == IM_PROMTS["escalation_sound_out"]
    list_so=[a,b,e,c]
    assert all((x.i_so_wait(IM_PROMTS["escalation_sound_out"],10)) for x in list_so)

def test_accepting_with_available_user(RB_ESC_clients):
    a, b, c, d, e = RB_ESC_clients
    IM_PROMTS = get_prompts(RB_ESC_clients,"indyme rnr")
    b.command("copythat")
    assert (b.i_wait(IM_PROMTS["acceptances_to_requestor"], 10)) and (a.i_wait(IM_PROMTS["acceptance_to_listeners"], 10)) and (e.i_wait(IM_PROMTS["acceptance_to_listeners"], 10))

def test_indyme_timeout(RB_ESC_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5"})
    a, b, c, d, e = RB_ESC_clients
    [x.log_on_tool() for x in RB_ESC_clients]
    IM_PROMTS = get_prompts(RB_ESC_clients,"indyme rnr")
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(30)
    b.command('copy that')
    assert b.i_wait(IM_PROMTS["no_active_request"].lower(),10)
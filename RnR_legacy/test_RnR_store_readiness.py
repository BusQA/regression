from utils import wait
import pytest
global SRD_PROMPTS
from setup import get_prompts
from communicator import *
from ssh_client import *

def test_srd_initating_request(SRD_clients):
    a,b,c,d,e = SRD_clients
    [x.log_on() for x in a,b,c,d,e]
    wait(4)
    d.make_engaged()
    SRD_PROMPTS = get_prompts(SRD_clients,"store readiness")
    assert all((x.i_wait(SRD_PROMPTS['activation_response_to_listeners'],70)) for x in SRD_clients[1:3])


def test_soundout_for_available_user(SRD_clients):
    a, b, c, d, e = SRD_clients
    wait(15)
    SRD_PROMPTS = get_prompts(SRD_clients,"store readiness")
    assert all(x.soundout == SRD_PROMPTS["sound_out_all"] for x in SRD_clients[0:3])


def test_non_member_says_copy_that(SRD_clients):
    a, b, c, d, e = SRD_clients
    SRD_PROMPTS = get_prompts(SRD_clients,"store readiness")
    print "We expect :"+SRD_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(SRD_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(SRD_clients):
    d = SRD_clients[-2]
    wait(10)
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(SRD_clients):
    a, b, c, d, e = SRD_clients
    SRD_PROMPTS = get_prompts(SRD_clients,"store readiness")
    b.command("copythat")
    assert (b.i_wait(SRD_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(SRD_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(SRD_PROMPTS["acceptance_to_listeners"], 10))
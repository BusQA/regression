from utils import wait,DB_UPDATE
import pytest
global RB_PROMPTS
from setup import get_prompts
import pytest

def test_register_backup_tap_out(RB_clients):
    a,b,c,d,e=RB_clients
    a.log_on_tool()
    RB_PROMPTS = get_prompts(RB_clients)
    a.command("registerbackup")
    wait(2)
    a.tap_out()
    assert a.i_wait(RB_PROMPTS['cancell_request'],10)


def test_register_backup_initating_request(RB_clients):
    a,b,c,d,e=RB_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    RB_PROMPTS = get_prompts(RB_clients)
    a.command("registerbackup")
    assert a.i_wait(RB_PROMPTS['command_response'],10)


def test_verification_of_Activation_response_to_listners(RB_clients):
    a, b, c, d, e = RB_clients
    RB_PROMPTS = get_prompts(RB_clients)
    assert all((x.i_wait(RB_PROMPTS['activation_response_to_listeners'],30)) for x in RB_clients[1:3])


def test_soundout_for_available_user(RB_clients):
    a, b, c, d, e = RB_clients
    wait(15)
    RB_PROMPTS = get_prompts(RB_clients)
    assert all(x.soundout == RB_PROMPTS["sound_out_all"] for x in RB_clients[0:3])



def test_already_active_prompt_verification(RB_clients):
    a, b, c, d, e = RB_clients
    RB_PROMPTS = get_prompts(RB_clients)
    a.command("registerbackup")
    assert a.i_wait(RB_PROMPTS["already_active"],10)


def test_non_member_says_copy_that(RB_clients):
    a, b, c, d, e = RB_clients
    RB_PROMPTS = get_prompts(RB_clients)
    print "We expect :"+RB_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(RB_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(RB_clients):
    d = RB_clients[-2]
    assert d.soundout == "twinkle1"


def test_accepting_with_available_user(RB_clients):
    a, b, c, d, e = RB_clients
    RB_PROMPTS = get_prompts(RB_clients)
    b.command("copythat")
    assert (b.i_wait(RB_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10))


def test_register_backup_timeout(RB_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    a, b, c, d, e = RB_clients
    [x.log_on_tool() for x in RB_clients]
    RB_PROMPTS = get_prompts(RB_clients)
    a.command("register backup")
    assert a.i_wait(RB_PROMPTS['rnr_timeout'],40)
    


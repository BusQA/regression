from utils import wait,DB_UPDATE
import pytest
global MA_PROMPTS
from setup import get_prompts
from communicator import *
from config import DEFAULT_ZONE

def test_mgr_assistance_request_tapout(MA_Loc_clients):
    a,b,c,d,e = MA_Loc_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    MA_PROMPTS = get_prompts(MA_Loc_clients,"manager assistance",zone=DEFAULT_ZONE)
    wait(5)
    a.command("manager assistance")
    wait(2)
    a.tap_out()
    assert a.i_wait(MA_PROMPTS['cancell_request'],10)

def test_mgr_assistance_initating_request(MA_Loc_clients):
    a,b,c,d,e=MA_Loc_clients
    MA_PROMPTS = get_prompts(MA_Loc_clients,"manager assistance",zone=DEFAULT_ZONE)
    d.make_engaged()
    wait(2)
    a.command("manager assistance")
    assert a.i_wait(MA_PROMPTS['command_response'],10)


def test_verification_of_Activation_response_to_listners(MA_Loc_clients):
    a, b, c, d, e = MA_Loc_clients
    MA_PROMPTS = get_prompts(MA_Loc_clients,"manager assistance",zone=DEFAULT_ZONE)
    assert all((x.i_wait(MA_PROMPTS['activation_response_to_listeners'],20)) for x in MA_Loc_clients[1:3])

def test_soundout_for_available_user(MA_Loc_clients):
    a, b, c, d, e = MA_Loc_clients
    wait(15)
    MA_PROMPTS = get_prompts(MA_Loc_clients,"manager assistance",zone=DEFAULT_ZONE)
    assert all(x.soundout == MA_PROMPTS["sound_out_all"].replace(',','_') for x in MA_Loc_clients[0:3])


def test_already_active_prompt_verification(MA_Loc_clients):
    a, b, c, d, e = MA_Loc_clients
    MA_PROMPTS = get_prompts(MA_Loc_clients,"manager assistance",zone=DEFAULT_ZONE)
    a.command("manager assistance")
    assert a.i_wait(MA_PROMPTS["already_active"],10)

def test_non_member_says_copy_that(MA_Loc_clients):
    a, b, c, d, e = MA_Loc_clients
    MA_PROMPTS = get_prompts(MA_Loc_clients,"manager assistance",zone=DEFAULT_ZONE)
    print "We expect :"+MA_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(MA_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(MA_Loc_clients):
    d = MA_Loc_clients[-2]
    wait(10)
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(MA_Loc_clients):
    a, b, c, d, e = MA_Loc_clients
    MA_PROMPTS = get_prompts(MA_Loc_clients,"manager assistance",zone=DEFAULT_ZONE)
    b.command("copythat")
    assert (b.i_wait(MA_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(MA_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(MA_PROMPTS["acceptance_to_listeners"], 10))

def test_manager_assiatance_time_out(MA_Loc_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    a, b, c, d, e = MA_Loc_clients
    [x.log_on_tool() for x in MA_Loc_clients]
    MA_PROMPTS = get_prompts(MA_Loc_clients, "manager assistance", zone=DEFAULT_ZONE)
    a.command("manager assistance")
    assert a.i_wait(MA_PROMPTS['rnr_timeout'],40)

from utils import wait,DB_UPDATE
import pytest
from setup import get_prompts
from communicator import *
from config import DEFAULT_ZONE

def test_carry_out_initating_request(CO_Loc_clients):
    a,b,c,d,e=CO_Loc_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    d.make_engaged()
    wait(5)
    CO_PROMPTS = get_prompts(CO_Loc_clients,"carry out",zone=DEFAULT_ZONE)
    a.command("carry out")
    assert a.i_wait(CO_PROMPTS['activation_response_to_requestor'],10)


def test_verification_of_Activation_response_to_listners(CO_Loc_clients):
    a, b, c, d, e = CO_Loc_clients
    CO_PROMPTS = get_prompts(CO_Loc_clients,"carry out",zone=DEFAULT_ZONE)
    assert all((x.i_wait(CO_PROMPTS['activation_response_to_listeners'],10)) for x in CO_Loc_clients[1:3])

def test_soundout_for_available_user(CO_Loc_clients):
    a, b, c, d, e = CO_Loc_clients
    wait(15)
    CO_PROMPTS = get_prompts(CO_Loc_clients,"carry out",zone=DEFAULT_ZONE)
    assert all(x.soundout == CO_PROMPTS["sound_out_all"].replace(',','_') for x in CO_Loc_clients[0:3])

def test_already_active_prompt_verification(CO_Loc_clients):
    a, b, c, d, e = CO_Loc_clients
    CO_PROMPTS = get_prompts(CO_Loc_clients,"carry out",zone=DEFAULT_ZONE)
    a.command("carry out")
    assert a.i_wait(CO_PROMPTS["already_active"],10)

def test_non_member_says_copy_that(CO_Loc_clients):
    a, b, c, d, e = CO_Loc_clients
    CO_PROMPTS = get_prompts(CO_Loc_clients,"carry out",zone=DEFAULT_ZONE)
    print "We expect :"+CO_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(CO_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(CO_Loc_clients):
    d = CO_Loc_clients[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(CO_Loc_clients):
    a, b, c, d, e = CO_Loc_clients
    CO_PROMPTS = get_prompts(CO_Loc_clients,"carry out",zone=DEFAULT_ZONE)
    b.command("copythat")
    assert (b.i_wait(CO_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(CO_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(CO_PROMPTS["acceptance_to_listeners"], 10))

def test_carry_out_time_out(CO_Loc_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    a, b, c, d, e = CO_Loc_clients
    [x.log_on_tool() for x in CO_Loc_clients]
    CO_PROMPTS = get_prompts(CO_Loc_clients,"carry out",zone=DEFAULT_ZONE)
    a.command("carry out")
    assert a.i_wait(CO_PROMPTS['rnr_timeout'],40)
    wait(5)
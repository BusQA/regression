from utils import wait,DB_UPDATE
import pytest
global IM_PROMTS
from setup import get_prompts
from communicator import *
from ssh_client import *

def test_indyme_initating_request(IM_clients):
    a,b,c,d,e = IM_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    wait(7)
    d.make_engaged()
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(2)
    IM_PROMTS = get_prompts(IM_clients,"indyme rnr")
    assert all((x.i_wait(IM_PROMTS['activation_response_to_listeners'],10)) for x in IM_clients[1:3])


def test_soundout_for_available_user(IM_clients):
    a, b, c, d, e = IM_clients
    wait(15)
    IM_PROMTS = get_prompts(IM_clients,"indyme rnr")
    assert all(x.soundout == IM_PROMTS["sound_out_all"] for x in IM_clients[0:3])

def test_non_member_says_copy_that(IM_clients):
    a, b, c, d, e = IM_clients
    SB_PROMPTS = get_prompts(IM_clients,"indyme rnr")
    print "We expect :"+SB_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(SB_PROMPTS["no_active_request"].lower(),10)


def test_soundout_for_engaged_user(IM_clients):
    d = IM_clients[-2]
    assert d.soundout == "twinkle1"

def test_accepting_with_available_user(IM_clients):
    a, b, c, d, e = IM_clients
    IM_PROMTS = get_prompts(IM_clients,"indyme rnr")
    b.command("copythat")
    assert (b.i_wait(IM_PROMTS["acceptances_to_requestor"], 10)) and (a.i_wait(IM_PROMTS["acceptance_to_listeners"], 10)) and (c.i_wait(IM_PROMTS["acceptance_to_listeners"], 10))
    wait(5)

def test_indyme_timeout(IM_clients):
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"10"})
    a, b, c, d, e = IM_clients
    [x.log_on_tool() for x in IM_clients]
    IM_PROMTS = get_prompts(IM_clients,"indyme rnr")
    sshCommand('python2.7 /home/theatro/bin/indyme.py')
    wait(30)
    b.command('copy that')
    assert b.i_wait(IM_PROMTS["no_active_request"].lower(),10)



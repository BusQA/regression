from utils import wait
import pytest
global RB_PROMPTS
from setup import get_prompts
import pytest
from config import DEFAULT_ZONE

def test_register_backup2_tapout(RB2_ESC_clients):
    a,b,c,d,e=RB2_ESC_clients
    [x.log_on_tool() for x in a,b,c,d,e]
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    a.command("registerbackup")
    wait(2)
    a.tap_out()
    assert a.i_wait(RB_PROMPTS['cancell_request'],10)

def test_register_backup2_initating_request(RB2_ESC_clients):
    a,b,c,d,e=RB2_ESC_clients
    d.make_engaged()
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    a.command("registerbackup")
    assert a.i_wait(RB_PROMPTS['command_response'],30)
    
def test_verification_of_Activation_response_to_listners(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    assert c.i_wait(RB_PROMPTS["activation_response_to_listeners"], 30)

def test_soundout_for_available_user(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    wait(10)
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    assert (a.soundout == RB_PROMPTS["sound_out_all"].replace(",","_") and c.soundout == RB_PROMPTS["sound_out_all"].replace(",","_"))

def test_non_member_says_copy_that(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    print "We expect :"+RB_PROMPTS["no_active_request"]
    e.command("copythat")
    assert e.i_wait(RB_PROMPTS["no_active_request"].lower(),10)

def test_escalation_active_prompt_for_requestor(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    wait(15)
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    assert a.i_wait(RB_PROMPTS["escalation_to_requestor"],10)

def test_escalation_active_prompt_for_listeners(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    wait(15)
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    assert c.i_wait(RB_PROMPTS['escalation_to_listeners'],10)

def test_soundout_for_engaged_user(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    wait(10)
    d = RB2_ESC_clients[-2]
    assert d.soundout == "twinkle1"

def test_escalated_soundout_for_available_user(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    assert (a.soundout == RB_PROMPTS["escalation_sound_out"].replace(",","_") and b.soundout == RB_PROMPTS["escalation_sound_out"].replace(",","_") and c.soundout == RB_PROMPTS["escalation_sound_out"].replace(",","_") and e.soundout == RB_PROMPTS["escalation_sound_out"].replace(",","_"))

def test_accepting_with_available_user(RB2_ESC_clients):
    a, b, c, d, e = RB2_ESC_clients
    wait(10)
    RB_PROMPTS = get_prompts(RB2_ESC_clients,zone=DEFAULT_ZONE)
    b.command("copythat")
    assert (b.i_wait(RB_PROMPTS["acceptances_to_requestor"], 10)) and (a.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10)) and (c.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10))  and (e.i_wait(RB_PROMPTS["acceptance_to_listeners"], 10))

from utils import wait
from DB_HANDLE import get_feature_prompts,making_prompts,get_help_list
from responses import AG_PROMPTS
import pytest
from setup import *


def test_helpme(clients):
    a=clients[0]
    a.log_on_tool()
    a.command("help me")
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide",{'__FIRST_NAME__':a.fname})
    assert a.i_wait(HLP_ME_PROMPTS["help_me_command_list"],10)

def test_listing_commands(clients):
    a=clients[0]
    a.command("commands")
    HLP_ME_PROMPTS = get_prompts(clients,"commands",{
        '__COMMAND_LIST__':"conversationcommands,locationcommands,managementcommands,messagecommands,assistancecommands,groupcommands"
        })
    assert a.i_wait(HLP_ME_PROMPTS["commands_cmd_response"],10)

def test_first_message_command_info(clients):
    a=clients[0]
    a.command("message commands")
    HLP_ME_PROMPTS = get_prompts(clients,"messagecommands",{'__COMMAND__':'messagecommands',
        '__COMMAND_LIST__':get_feature_prompts('messagecommands')["AnnouncementNow"],
        '__SAY_NEXT__':get_feature_prompts('messagecommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["first_prompt_for_cmd_or_tip"],10)

def test_second_command_info(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"messagecommands",{'__COMMAND__':'messagecommands',
        '__COMMAND_LIST__':(get_feature_prompts('messagecommands')["AnnouncementToday"]).lower(),
        '__SAY_NEXT__':get_feature_prompts('messagecommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)


def test_verification_prompt_for_more_than_threshold(clients):
    a=clients[0]
    a.command("Hello")
    HLP_ME_PROMPTS = get_prompts(clients,"teach_me")
    assert a.i_wait(HLP_ME_PROMPTS["sorry_please_say"],10)

def test_third_command_info(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"messagecommands",{'__COMMAND__':'messagecommands',
        '__COMMAND_LIST__':(get_feature_prompts('messagecommands')["GroupMessage"]).lower(),
        '__SAY_NEXT__':get_feature_prompts('messagecommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_fourth_command_info(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"messagecommands",{'__COMMAND__':'messagecommands',
        '__COMMAND_LIST__':(get_feature_prompts('messagecommands')["Message"]).lower(),
        '__SAY_NEXT__':get_feature_prompts('messagecommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)


def test_fifth_command_info(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"messagecommands",{'__COMMAND__':'messagecommands',
        '__COMMAND_LIST__':(get_feature_prompts('messagecommands')["PlayMessages"]).lower(),
        '__SAY_NEXT__':get_feature_prompts('messagecommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_sixth_command_info(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"messagecommands",{'__COMMAND__':'messagecommands',
        '__COMMAND_LIST__':(get_feature_prompts('messagecommands')["offthefloor"]).lower(),
        '__SAY_NEXT__':get_feature_prompts('messagecommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)


def test_seventh_command_info(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"messagecommands",{'__COMMAND__':'messagecommands',
        '__COMMAND_LIST__':(get_feature_prompts('messagecommands')["onthefloor"]).lower(),
        '__SAY_NEXT__':get_feature_prompts('messagecommands')["cmd_or_tip_list_complete"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)


def test_no_more_commands_to_play(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["no_more_commands"],10)


def test_coming_out_of_tips_and_tricks(clients):
    a=clients[0]
    wait(5)
    a.tap_out()
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["audio_pocket_guide_canceled"],10)
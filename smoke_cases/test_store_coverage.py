from ssh_client import *
from responses import COVERAGE_PROMPTS
from utils import wait
import pytest


#@pytest.mark.skip(reason="no way of currently testing this")

def test_coverage_single_location(clients):
    a= clients[0]
    [x.log_on_tool() for x in clients]
    wait(2)
    print COVERAGE_PROMPTS["Location"]
    [sshCommand(command='/home/theatro/bin/nping1.sh "{0}" "{1}"'.format(x.mac_address,L)) for x,L in zip(clients, COVERAGE_PROMPTS["Location"])]
    a.say('menubutton', ['store coverage'])
    wait(5)
    assert COVERAGE_PROMPTS["TEST_CASE_1"][1] == a.output()

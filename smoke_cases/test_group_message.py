from utils import wait
from DB_HANDLE import Group_NAME
from ssh_client import *
import pytest
pytest.last_message=''

#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_message_to_avialble_users(group_clients):
    a,b,c,d,e = group_clients
    [x.log_on_tool() for x in a,b,c,e ]
    wait(2)
    c.make_engaged()
    e.make_engaged()
    a.cmd("message",Group_NAME)
    #assert b.i_wait("posted_at",40) and a.i_wait(b.fname+",has_heard_your_message",40)
    assert b.i_wait("posted_at",40)
    pytest.last_message=b.last_message
    print "last message is:"+str(pytest.last_message)
    #assert (b.fname+",has_heard_your_message" in a.output() and "posted_at" in b.output())





#@pytest.mark.skip(reason="no way of currently testing this")
def test_replay_last_message(group_clients):
    b = group_clients[1]
    wait(5)
    b.command('replay last message')
    assert b.i_wait(pytest.last_message)

def test_group_message_to_engaged_user(group_clients):
    a, b, c, d, e = group_clients
    c.tap_out()
    assert c.i_wait("posted_at",40)


#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_message_to_non_logged_on_user(group_clients):
    a, b, c, d, e = group_clients
    d.log_on_tool()
    assert d.i_wait("posted_at",50)


#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_message_reboot_survival(group_clients):
    a, b, c, d, e = group_clients
    sshCommand()
    e.tap_out()
    #assert e.i_wait("posted_at",40) and a.i_wait(e.fname+",has_heard_your_message",40)
    assert e.i_wait("posted_at",40)



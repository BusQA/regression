from utils import wait
import pytest
import pytest
pytest.last_message=''

review_response = "do_you_want_to_review_your_announcement_before_posting_it"


@pytest.mark.parametrize("command,response", [
    ("announcementnow","announcement_now"),
    ("announcementhour","announcement_hour"),
    ("post it","announcement_today"),
    ("record huddle","morning_huddle"),

])
def test_announcements(clients,command,response):
    a,b,c,d,e = clients
    [x.log_on_tool() for x in clients]
    #a.log_on()
    wait(2)
    a.command(command,"1to10")
    #wait(10)
    assert (a.i_wait("posted",40) and b.i_wait("posted_at",40) and c.i_wait("posted_at",40) and d.i_wait("posted_at",40) and e.i_wait("posted_at",40))
    pytest.last_message=b.last_message
    wait(5)
    b.command('replay last message')
    assert b.i_wait(pytest.last_message,20) ,"Replay last message is failed"




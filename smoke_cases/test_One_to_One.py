import pytest
from utils import wait
from ssh_client import *
from DB_HANDLE import *
from time import strftime
pytest.posted_time='zero'

def test_non_logged_on_user(clients):
    a,b = clients[:2]
    a.log_on_tool()
    wait(2)
    a.say('menubutton', ['hello', b.name])
    wait(5)
    #andrea birdsong,|is_not_logged_on
    assert  b.name+',is_not_logged_on' in a.output()

def test_last_conatct_without_history(clients):
    a,b = clients[:2]
    wait(2)
    a.command('last contact')
    LASTCALL_PROMPTS=making_prompts(get_feature_prompts('CallHistory'),{'__TO__':a.name,'__FROM__':a.name})
    assert a.i_wait(LASTCALL_PROMPTS["no_active_calls"])
    

def test_to_available_user(clients):
    a,b = clients[:2]
    b.log_on_tool()
    wait(2)
    a.say('menubutton', ['hello', b.name])
    pytest.posted_time=strftime("%#I,%M,%p").lower()
    wait(5)
    #andrea birdsong,|go_ahead
    if b.name+",go_ahead" in a.output():
        a.command("1to10")
        a.tap_out()
        wait(2)
        assert "ended_call" in b.output()
    else:
        assert False

def test_last_conatct(clients):
    a,b = clients[:2]
    wait(2)
    LASTCALL_PROMPTS=making_prompts(get_feature_prompts('CallHistory'),{'__TIME__':pytest.posted_time,'__FROM__':a.name})
    b.command('last contact')
    assert b.i_wait(LASTCALL_PROMPTS['last_call'])


def test_one_to_engaged_user(clients):
    a,b = clients[:2]
    wait(2)
    b.tap_out()
    a.say('menubutton', ['hello', b.name])
    wait(5)
    assert  b.fname+',is_engaged_near' in a.output()

#@pytest.mark.skip()




def test_last_conatct_reboot_survival(clients):
    sshCommand()
    a,b = clients[:2]
    wait(20)
    LASTCALL_PROMPTS=making_prompts(get_feature_prompts('CallHistory'),{'__TIME__':pytest.posted_time,'__FROM__':a.name})
    b.command('last contact')
    assert b.i_wait(LASTCALL_PROMPTS['last_call'],20)


def test_one_to_unreachable(clients):
    a,b = clients[:2]
    b.die()
    wait(10)
    a.say('menubutton', ['hello', b.name])
    wait(5)
    assert  b.name+',is_out_of_range' in a.output()
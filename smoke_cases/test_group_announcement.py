from utils import wait
from DB_HANDLE import Group_NAME
from ssh_client import *
import pytest
pytest.last_message=''


#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_post_it_to_avialble_users(group_clients):
    a,b,c,d,e = group_clients
    [x.log_on_tool() for x in a,b,c,e ]
    wait(2)
    c.make_engaged()
    e.make_engaged()
    a.cmd("post it",Group_NAME)
    #wait(5)
    #assert ("posted" in a.output() and "posted_at" in b.output())
    assert b.i_wait("posted_at",20) and a.i_wait("posted",20)
    pytest.last_message=b.last_message

def test_replay_last_group_post_message(group_clients):
    b = group_clients[1]
    b.command('replay last message')
    assert b.i_wait(pytest.last_message,20)

#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_post_it_to_engaged_user(group_clients):
    a, b, c, d, e = group_clients
    c.tap_out()
    assert c.i_wait("posted_at",40)


#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_post_it_to_non_logged_on_user(group_clients):
    a, b, c, d, e = group_clients
    d.log_on_tool()
    assert d.i_wait("posted_at",60)

#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_post_it_reboot_survival(group_clients):
    a, b, c, d, e = group_clients
    sshCommand()
    e.tap_out()
    assert e.i_wait("posted_at",60)

from utils import wait
import pytest
from ssh_client import *
from DB_HANDLE import *
pytest.last_message=''
#@pytest.mark.skip(reason="no way of currently testing this")


def test_message_to_avialble_user(clients):
    a,b = clients[:2]
    [x.log_on_tool() for x in a,b]
    wait(2)
    a.cmd('message',b.name)
    #wait(15)
    #assert (b.fname+',has_heard_your_message' in a.output() and 'posted_at' in b.output())
    assert b.i_wait("posted_at",30) and a.i_wait(b.fname+',has_heard_your_message',30)
    pytest.last_message=b.last_message
    print pytest.last_message

def test_replay_last_message_with_no_message(clients):
    a,b = clients[:2]
    a.command('replay last message')
    LASTMSG_PROMPTS=making_prompts(get_feature_prompts('CallHistory'),{'__TO__':a.name})
    assert a.i_wait(LASTMSG_PROMPTS["no_active_messages"])

def test_replay_last_message(clients):
    a,b = clients[:2]
    b.command('replay last message')
    assert b.i_wait(pytest.last_message,20)


#@pytest.mark.skip(reason="no way of currently testing this")
def test_message_to_non_logged_on_user(clients):
    a,b,c,d,e = clients
    a.cmd('message',e.name)
    e.log_on_tool()
    #wait(25)
    #assert (e.fname+',has_heard_your_message' in a.output() and 'posted_at' in e.output())
    assert e.i_wait("posted_at",50) and a.i_wait(e.fname+',has_heard_your_message',50)

#@pytest.mark.skip(reason="no way of currently testing this")
def test_message_to_engaged_user(clients):
    a,b,c,d,e = clients
    c.log_on_tool()
    wait(2)
    c.make_engaged()
    a.message(c.name)
    c.tap_out()
    #wait(15)
    #assert (c.fname+',has_heard_your_message' in a.output() and 'posted_at' in c.output())
    assert c.i_wait("posted_at",50) and a.i_wait(c.fname+',has_heard_your_message',50)

#@pytest.mark.skip(reason="no way of currently testing this")
def test_message_reboot_survival(clients):
    a,b,c,d,e = clients
    d.log_on_tool()
    d.make_engaged()
    wait(5)
    a.message(d.name)
    sshCommand()
    d.make_available()
    #wait(15)
    #assert (d.fname+',has_heard_your_message' in a.output() and 'posted_at' in c.output())
    assert d.i_wait("posted_at",60) and a.i_wait(d.fname+',has_heard_your_message',60)

#@pytest.mark.skip(reason="no way of currently testing this")
def test_replay_last_message_reboot_survival(clients):
    a,b = clients[:2]
    b.command('replay last message')
    assert b.i_wait(pytest.last_message,20)

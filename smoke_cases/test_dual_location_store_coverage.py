from ssh_client import *
from responses import COVERAGE_PROMPTS
from utils import wait
import pytest

def test_coverage_double_location(clients):
    a= clients[0]
    [x.log_on_tool() for x in clients]
    #[call(['./nping1.sh', str(x.mac_address), str(L)]) for x, L in zip(clients, COVERAGE_PROMPTS["Dual_Location"])]
    [sshCommand(command='/home/theatro/bin/nping1.sh "{0}" "{1}"'.format(x.mac_address,L)) for x, L in zip(clients, COVERAGE_PROMPTS["Dual_Location"])]
    wait(7)
    a.say('menubutton', ['store coverage'])
    #wait(5)
    #assert COVERAGE_PROMPTS["TEST_CASE_2"][1] == a.output()
    assert a.i_wait(COVERAGE_PROMPTS["TEST_CASE_2"][1],10)
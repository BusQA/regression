from utils import wait
from DB_HANDLE import Group_NAME
import pytest

#@pytest.mark.skip(reason="no way of currently testing this")

def test_group_call_with_member_not_logged_on(group_clients):
    a= group_clients[0]
    a.log_on_tool()
    wait(2)
    a.say('menubutton', ['hello', Group_NAME])
    assert a.i_wait('no_one_available,'+Group_NAME,10)

#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_call_single_recipeint(group_clients):
    a,b,c,d,e = group_clients
    b.log_on_tool()
    wait(2)
    a.say('menubutton', ['hello', Group_NAME])
    if a.i_wait(Group_NAME+',go_ahead',10):
        a.command("1to10")
        b.command("1to10")
        b.tap_out()
        print b.name+"tapping out"
        assert b.i_wait("group_call_ended",10)
        assert (a.i_wait(b.name+',tapped_out,group_call_ended',10) and b.i_wait("group_call_ended",10))
    else:
        assert False,"Failed as user command not recognized. User got: "+str(a.output())

#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_call_to_available_users(group_clients):
    a,b,c,d,e = group_clients
    [x.log_on_tool() for x in c, d, e]
    wait(2)
    a.make_available()
    a.say('menubutton', ['hello', Group_NAME])
    if a.i_wait(Group_NAME+',go_ahead',10):
        a.command("1to10")
        b.command("1to10")
        c.tap_out()
        print c.name+"tapping out"
        assert c.i_wait("group_call_ended",10)
        print d.name+"tapping out"
        d.tap_out()
        assert d.i_wait("group_call_ended",10)
        print a.name+"tapping out"
        a.tap_out()
        assert a.i_wait("group_call_ended",10)
        assert all([a.name+',tapped_out,group_call_ended' in x.output() for x in b,e])
    else:
        assert False,"Failed as user command not recognized. User got: "+str(a.output())

#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_call_to_some_enagaged_users(group_clients):
    a,b,c,d,e = group_clients
    wait(2)
    d.make_engaged()
    e.make_engaged()
    a.say('menubutton', ['hello', Group_NAME])
    
    if a.i_wait(Group_NAME+',go_ahead',10):
        a.command("1to10")
        a.tap_out()
        assert a.i_wait("group_call_ended",10)
        assert all([a.name+',tapped_out,group_call_ended' in x.output() for x in b,c])
        assert all(['engaged' in x.output() for x in d,e])
    else:
        assert False,"Failed as user command not recognized. User got: "+str(a.output())

#@pytest.mark.skip(reason="no way of currently testing this")
def test_group_call_to_some_unreachable_users(group_clients):
    a,b,c,d,e = group_clients
    wait(2)
    [x.die() for x in  d, e]
    wait(30)
    a.say('menubutton', ['hello', Group_NAME])
    if a.i_wait(Group_NAME+',go_ahead',10):
        a.command("1to10")
        a.tap_out()
        assert a.i_wait("group_call_ended",10)
        assert all([a.name+',tapped_out,group_call_ended' in x.output() for x in b,c])
        assert all(['engaged' in x.output() for x in d,e])
    else:
        assert False,"Failed as user command not recognized. User got: "+str(a.output())
from utils import *
import pytest
from responses import do_you_mean
from DB_HANDLE  import get_feature_prompts



def test_response_no_one_logged_on(clients):
    a= clients[0]
    a.log_on_tool()
    a.say('menubutton', ['hello', "gary"])
    assert a.i_wait(do_you_mean["not_logged_on"])


def test_response_1_loggedon(clients):
    a,b= clients[:2]
    b.logon_name("gary maasen")
    wait(1)
    a.say('menubutton', ['hello', "gary"])
    assert a.i_wait("gary maasen,go_ahead")
    a.tap_out()


def test_response_2_loggedon(clients):
    a,b,c= clients[:3]
    c.logon_name("gary smedley")
    wait(2)
    a.say('menubutton', ['hello', "gary"])
    assert a.i_wait(do_you_mean["ask_for_full_name"])


def test_response_3_logged_on(clients):
    a, b, c, d = clients[:4]
    d.logon_name("gary geib")
    wait(2)
    a.say('menubutton', ['hello', "gary"])
    assert a.i_wait(do_you_mean["ask_for_full_name"])

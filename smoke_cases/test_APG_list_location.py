
from utils import wait
from DB_HANDLE import get_feature_prompts,making_prompts,get_help_list
from responses import AG_PROMPTS
import pytest
from setup import *



def test_list_location(APG_clients):
    a=APG_clients[0]
    a.command("list locations")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide",{
        '__LIST__':"break room,kitchens,millwork,office,shelving",
        '__SAY_MORE__':get_feature_prompts('audiopocketguide')["say_more_to_hear_more"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_top_x_listlocations"],10)


def test_more_inside_list_groups(APG_clients):
    a=APG_clients[0]
    a.command("more")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide",{
        '__COUNT__':'2',
        '__LIST__':"stock room,front end",
        '__SAY_MORE__':get_feature_prompts('audiopocketguide')["complete_listlocations"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_x_listlocations"],10)




def test_no_more_commands_to_play(APG_clients):
    a=APG_clients[0]
    a.command("more")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["no_more_listlocations"],10)


def test_verification_of_tap_out(APG_clients):
    a=APG_clients[0]
    a.tap_out()
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["audio_pocket_guide_canceled"],10)


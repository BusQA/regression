from utils import wait
import pytest

def test_non_logged_on_user(clients):
    a,b = clients[:2]
    a.log_on_tool()
    wait(2)
    a.say('menubutton', ['interrupt', b.name])
    assert  a.i_wait(b.name+',is_not_logged_on',10)


def test_to_available_user(clients):
    a,b = clients[:2]
    a.log_on_tool()
    b.log_on_tool()
    wait(2)
    a.say('menubutton', ['interrupt', b.name])
    #andrea birdsong,|go_ahead
    if a.i_wait(b.name+",go_ahead",10):
        a.command("1to10")
        a.tap_out()
        assert b.i_wait("ended_call",4)
    else:
        assert False


def test_interrupt_to_engaged_user(clients):
    a,b = clients[:2]
    [x.log_on_tool() for x in a,b]
    wait(2)
    b.tap_out()
    a.say('menubutton', ['interrupt', b.name])
    # andrea birdsong,|go_ahead
    if a.i_wait(b.name+",go_ahead",10):
        a.command("1to10")
        a.tap_out()
        assert b.i_wait("ended_call",4)
    else:
        assert False


def test_interrupt_unreachable(clients):
    a = clients[0]
    b = clients[-1]
    [x.log_on_tool() for x in a,b]
    b.die()
    wait(10)
    a.say('menubutton', ['interrupt', b.name])
    assert  a.i_wait(b.name+',is_out_of_range',6)
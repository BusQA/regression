from utils import wait
from DB_HANDLE import get_feature_prompts,making_prompts,get_help_list
from responses import AG_PROMPTS
import pytest
from setup import *


def test_helpme(clients):
    a=clients[0]
    a.log_on_tool()
    a.command("help me")
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide",{'__FIRST_NAME__':a.fname})
    assert a.i_wait(HLP_ME_PROMPTS["help_me_command_list"],10)

def test_teach_me(clients):
    a=clients[0]
    a.command("teach me")
    HLP_ME_PROMPTS = get_prompts(clients,"teachme")
    assert a.i_wait(HLP_ME_PROMPTS["teach_me_cmd_response"],10)


def test_tips_and_tricks_first_tip(clients):
    a=clients[0]
    a.command("tips and tricks")
    HLP_ME_PROMPTS = get_prompts(clients,"tipsandtricks",{'__COMMAND__':'tipsandtricks',
        '__COMMAND_LIST__':get_feature_prompts('tipsandtricks')["replay last message"],
        '__SAY_NEXT__':get_feature_prompts('tipsandtricks')["say_next_for_another_tip_or_cmd"]
        })
    print a.output()
    assert a.i_wait(HLP_ME_PROMPTS["first_prompt_for_cmd_or_tip"],10)

def test_tips_and_tricks_seconds_tip(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"tipsandtricks",{'__COMMAND__':'tipsandtricks',
        '__COMMAND_LIST__':get_feature_prompts('tipsandtricks')["time"],
        '__SAY_NEXT__':get_feature_prompts('tipsandtricks')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_tips_and_tricks_third_tip(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"tipsandtricks",{'__COMMAND__':'tipsandtricks',
        '__COMMAND_LIST__':get_feature_prompts('tipsandtricks')["reply"],
        '__SAY_NEXT__':get_feature_prompts('tipsandtricks')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_tips_and_tricks_fourth_tip(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"tipsandtricks",{'__COMMAND__':'tipsandtricks',
        '__COMMAND_LIST__':get_feature_prompts('tipsandtricks')["who is my last contact"],
        '__SAY_NEXT__':get_feature_prompts('tipsandtricks')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

@pytest.mark.skip()
def test_verification_prompt_for_less_than_threshold(clients):
    a=clients[0]
    a.command("Abcdbe helpeme")
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["retry_next_again"],10)

@pytest.mark.skip()
def test_verification_prompt_for_more_than_threshold(clients):
    a=clients[0]
    a.command("announcement now")
    #HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide")
    assert a.i_wait("please_say_next_or_list_cmds_or_tapout",10)
    
def test_tips_and_tricks_fifth_tip(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"tipsandtricks",{'__COMMAND__':'tipsandtricks',
        '__COMMAND_LIST__':get_feature_prompts('tipsandtricks')["tapout"],
        '__SAY_NEXT__':get_feature_prompts('tipsandtricks')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_tips_and_tricks_six_tip(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"tipsandtricks",{'__COMMAND__':'tipsandtricks',
        '__COMMAND_LIST__':get_feature_prompts('tipsandtricks')["bargein"],
        '__SAY_NEXT__':get_feature_prompts('tipsandtricks')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_tips_and_tricks_last_tip(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"tipsandtricks",{'__COMMAND__':'tipsandtricks',
        '__COMMAND_LIST__':get_feature_prompts('tipsandtricks')["engaged"],
        '__SAY_NEXT__':get_feature_prompts('tipsandtricks')["cmd_or_tip_list_complete"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_no_more_tips_to_play(clients):
    a=clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["no_more_commands"],10)


def test_coming_out_of_tips_and_tricks(clients):
    a=clients[0]
    wait(5)
    a.tap_out()
    HLP_ME_PROMPTS = get_prompts(clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["audio_pocket_guide_canceled"],10)

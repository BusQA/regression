from utils import wait
from communicator import *
from responses import LOCATE_PROMPTS
import pytest
 

def test_locate_non_logged_on_user(clients):
	"""locate a non logged on user"""
	a,b=clients[:2]
	a.log_on_tool()
	wait(2)
	a.cmd1('locate',b.name)
	assert a.i_wait(b.fname+LOCATE_PROMPTS["non_logged_on"],10)



def test_locate_available_user(clients):
	"""locate a available user"""
	a,b=clients[:2]
	b.log_on_tool()
	wait(2)
	a.cmd1('locate',b.name)
	assert a.i_wait(b.fname+LOCATE_PROMPTS["available"],10)	

   

def test_locate_engaged_user(clients):
	"""locate a engaged user"""
	a,b=clients[:2]
	b.tap_out()
	wait(2)
	a.cmd1('locate',b.name)
	assert a.i_wait(b.fname+LOCATE_PROMPTS["engaged"],10,True)	


def test_locate_self(clients):
	"""locate himself"""
	a,b=clients[:2]
	a.cmd1('locate',a.name)
	assert a.i_wait(LOCATE_PROMPTS["self"],10,True)


def test_locate_unreachable(clients):
	a,b=clients[:2]
	b.die()
	wait(10)
	a.cmd1('locate',b.name)
	assert a.i_wait(b.name+LOCATE_PROMPTS["unreachable"],10)
from utils import wait
import pytest
pytest.last_message=''

def test_sales_update_non_member(managers):
    a = managers[-1]
    a.log_on_tool()
    wait(3)
    a.command("sales update")
    assert a.i_wait("sorry_you_are_not_allowed_to_use_this_command",20)
    a.log_off_json()


def test_sales_update(managers):
    a,b = managers[:2]
    [x.log_on_tool() for x in a,b]
    wait(2)
    a.command("sales update")
    if a.i_wait("sales_update,go_ahead",10):
        a.command("1to10")
        if a.i_wait("do_you_want_to_review_your_sales_update_before_posting_it,yes_or_no",10):
            a.command("yes")
            if a.i_wait("do_you_want_to_rerecord_it",20):
                a.command("no")
                assert a.i_wait("posted",10) and b.i_wait("posted_at",30)
                pytest.last_message=b.last_message
                print "last message is:"+str(pytest.last_message)

            else:
                assert False,"User got: "+str(a.output())
        else:
            assert False,"User got: "+str(a.output())
    else:
        assert False,"User got: "+str(a.output())

def test_sales_update_replay(managers):
    a,b = managers[:2]
    wait(5)
    b.command('replay last message')
    assert b.i_wait(pytest.last_message)
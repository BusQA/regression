from utils import wait
from responses import MOD_PROMPTS,LOCATE_PROMPTS
from communicator import *
import pytest
from ssh_client import *


def test_MOD_check_in(MOD_clients):
	"""check in as MOD"""
	a,b=MOD_clients[:2]
	[x.log_on_tool() for x in a,b]
	wait(2)
	a.command("checkin manager on duty")
	assert (a.i_wait(a.fname+MOD_PROMPTS["log_on"][0],40) and b.i_wait(MOD_PROMPTS["log_on"][1],40))

#@pytest.mark.skip(reason="no way of currently testing this")
def test_who_is_MOD(MOD_clients):
	"identify who is MOD "
	a,b=MOD_clients[:2]
	wait(2)
	b.command("whoisMOD")
	assert b.i_wait(a.name+MOD_PROMPTS["who_is_MOD"],10)

def test_hello_MOD(MOD_clients):
	"""make 121 to a MOD"""
	a,b=MOD_clients[:2]
	wait(2)
	b.cmd("hello","mod")
	a.tap_out()
	assert (a.i_wait('call_ended',10) and b.i_wait('manageronduty,ended_call'))

#@pytest.mark.skip(reason="no way of currently testing this")
def test_interrupt_MOD(MOD_clients):
	"""make 121 to a MOD"""
	a,b=MOD_clients[:2]
	wait(2)
	b.cmd("interrupt","mod")
	a.tap_out()
	assert (a.i_wait('call_ended', 10) and b.i_wait('manageronduty,ended_call'))

def test_locate_available_MOD(MOD_clients):
	"""locate a available MOD"""
	a,b=MOD_clients[:2]
	wait(5)
	b.cmd1("locate","mod")
	assert  b.i_wait("manageronduty,"+a.name+LOCATE_PROMPTS["available"],10)

def test_message_MOD(MOD_clients):
	"""leave a message to MOD"""
	a,b=MOD_clients[:2]
	wait(2)
	b.message("manageronduty")
	assert  (a.i_wait('posted_at',30) and b.i_wait(MOD_PROMPTS["message_mod"],30))



#@pytest.mark.skip(reason="no way of currently testing this")
def test_MOD_reboot_survival(MOD_clients):
	a = MOD_clients[0]
	sshCommand()
	a.command('whoisMOD')
	assert a.i_wait(MOD_PROMPTS["self_identify"],10)

#@pytest.mark.skip(reason="no way of currently testing this")
def test_MOD_check_out(MOD_clients):
	"""checkout MOD"""
	a,b=MOD_clients[:2]
	a.command("checkout manager on duty")
	assert (a.i_wait('ok,'+a.fname+MOD_PROMPTS["checkout"][0],30) and b.i_wait(MOD_PROMPTS["checkout"][1],30))

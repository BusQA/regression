from utils import wait
from DB_HANDLE import get_feature_prompts,making_prompts,get_help_list
from responses import AG_PROMPTS
import pytest
from setup import *



def test_list_commands(APG_clients):
    a=APG_clients[0]
    a.command("list commands")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"commands",{
        '__COMMAND_LIST__':"conversationcommands,locationcommands,managementcommands,messagecommands,assistancecommands,groupcommands"
        })
    expected_prompt=HLP_ME_PROMPTS["commands_cmd_response"]+","+get_feature_prompts('audiopocketguide')["complete_listcommands_subset"]
    assert a.i_wait(expected_prompt,10)


def test_list_conversation_commands(APG_clients):
    a=APG_clients[0]
    a.command("conversation commands")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide",{
        '__LIST__':'hello,interrupt,urgentbulletin',
        '__SAY_MORE__':get_feature_prompts('audiopocketguide')["complete_conversationcommands"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_top_x_conversationcommands"],10)

def test_verification_of_issuing_out_of_context_command(APG_clients):
    a=APG_clients[0]
    a.command("message")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"conversationcommands",{
        '__LIST__':'hello,interrupt,urgentbulletin'
        })
    assert a.i_wait(HLP_ME_PROMPTS["sorry_please_say"],10)

def test_selecting_first_command(APG_clients):
    a=APG_clients[0]
    a.command("hello")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide",{
       '__COMMAND_LIST__':get_feature_prompts('conversationcommands')["Hello"],
       '__SAY_NEXT__':get_feature_prompts('conversationcommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["command_is"],10)


def test_selecting_next_command(APG_clients):
    a=APG_clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"conversationcommands",{
       '__COMMAND_LIST__':(get_feature_prompts('conversationcommands')["Interrupt"]).lower(),
       '__COMMAND__':"conversationcommands",
       '__SAY_NEXT__':get_feature_prompts('conversationcommands')["say_next_for_another_tip_or_cmd"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_issuning_out_of_context_command(APG_clients):
    a=APG_clients[0]
    wait(3)
    a.command("announcment")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"teach_me")
    assert a.i_wait(HLP_ME_PROMPTS["sorry_please_say"],10)

def test_third_command_info(APG_clients):
    a=APG_clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"conversationcommands",{'__COMMAND__':'conversationcommands',
        '__COMMAND_LIST__':get_feature_prompts('conversationcommands')["urgentbulletin"],
        '__SAY_NEXT__':get_feature_prompts('conversationcommands')["cmd_or_tip_list_complete"]
        })
    assert a.i_wait(HLP_ME_PROMPTS["the_next_tip_or_command_is"],10)

def test_no_more_commands_to_play(APG_clients):
    a=APG_clients[0]
    a.command("next")
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["no_more_commands"],10)


def test_verification_of_audio_packet_guide_time_out(APG_clients):
    a=APG_clients[0]
    wait(20)
    HLP_ME_PROMPTS = get_prompts(APG_clients,"audiopocketguide")
    assert a.i_wait(HLP_ME_PROMPTS["audio_pocket_guide_canceled"],10)


from responses import FB_PROMPTS,VM_PROMPTS,LP_PROMPTS
from utils import wait
import pytest

@pytest.mark.parametrize("command,response", [
    ("feedback",FB_PROMPTS),
    ("verbal memo",VM_PROMPTS),
    ("loss prventaion",LP_PROMPTS)

])
def test_command_posting(one_liner_cii,command,response):
    a=one_liner_cii
    wait(2)
    a.say('menubutton', [command])
    if a.i_wait(response["TEST_CASE_1"][1],10):
        if command != "loss prventaion":
            a.say('menubutton', ['1to10'])
            assert a.i_wait(response["TEST_CASE_1"][2],10)
        else:
            wait(7)
            if a.i_wait(response["TEST_CASE_1"][2],10):
                a.say('menubutton', ['1to10'])
                assert a.i_wait(response["TEST_CASE_1"][3],10)
            else:
                assert False, "User got:"+str(a.output)

    else:
        assert False, "Failed as user command not recognized. User got: "+str(a.output())

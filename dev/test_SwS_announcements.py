from utils import wait
import pytest

review_response = "do_you_want_to_review_your_announcement_before_posting_it"


@pytest.mark.parametrize("command,response", [
    ("announcementnow","announcement_now"),
    ("announcementhour","announcement_hour"),
    ("post it","announcement_today"),
    ("record huddle","morning_huddle"),

])
def test_announcements(sws_clients,command,response):
    a,b,c = sws_clients
    [x.log_on_tool() for x in a,b,c]
    #a.log_on()
    wait(2)
    c.command(command,"1to10")
    wait(10)
    a.command(command,"1to10")
    #wait(10)
    assert (a.i_wait("posted",40) and b.i_wait("posted_at",40) and c.i_wait("sorry_you_are_not_allowed_to_use_this_command",40))
    wait(5)

import pytest
from config import NAMES
from utils import wait
from addcdm import *

@pytest.mark.skip()
def test_send_CDM(clients):
    a=clients[0]
    t=AddUpdateDeleteCDM()
    t.login_with_username_and_password('aayush','Developer342!')
    flag = t.add_cdm()   
    if flag:
        a.log_on_tool()
        assert a.i_wait("posted_at",100)
        t.tearDown()
    else:
        assert False
        t.tearDown()
    
#@pytest.mark.skip()
def test_send_cdm_send_to_allGroups(group_clients):
    a=group_clients[0]
    t=AddUpdateDeleteCDM()
    flag = t.add_cdm_send_to_allGroups()
    if flag:
        a.log_on_tool()
        #wait(10)
        assert a.i_wait("posted_at",100)
    else:
        assert False

import pytest
from utils import wait


def test_non_logged_on_user(clients):
    a,b = clients[:2]
    a.log_on_tool()
    wait(2)
    a.say('menubutton', ['hello', b.name])
    wait(5)
    #andrea birdsong,|is_not_logged_on
    assert  b.name+',is_not_logged_on' in a.output()


def test_to_available_user(clients):
    a,b = clients[:2]
    b.log_on_tool()
    wait(2)
    a.say('menubutton', ['hello', b.name])
    wait(5)
    #andrea birdsong,|go_ahead
    if b.name+",go_ahead" in a.output():
        a.command("1to10")
        a.tap_out()
        wait(2)
        assert "ended_call" in b.output()
    else:
        assert False


def test_one_to_engaged_user(clients):
    a,b = clients[:2]
    wait(2)
    b.tap_out()
    a.say('menubutton', ['hello', b.name])
    wait(5)
    assert  b.fname+',is_engaged_near' in a.output()


def test_one_to_unreachable(clients):
    a,b = clients[:2]
    b.die()
    wait(10)
    a.say('menubutton', ['hello', b.name])
    wait(5)
    assert  b.name+',is_out_of_range' in a.output()
    a.log_off_json()
    b.log_off_json()

import pytest
from config import NAMES
from utils import wait,tool


@pytest.mark.parametrize('name',NAMES,scope='session')
def test_add_employee(E2E_CIIs,name):
    a,b=E2E_CIIs
    if a.add_employee(name):
        a.make_available()
        wait(5)
        tool(name)
        if a.i_wait(name, 20):
            pytest.assume(True)
            wait(10)
        else:
            pytest.assume(False)
        b.say('menubutton', ['hello', name])
        wait(5)
        # andrea birdsong,|go_ahead
        if name + ",go_ahead" in b.output():
            b.command("1to10")
            b.tap_out()
            wait(2)
            pytest.assume("ended_call" in a.output())
        else:
            pytest.assume(False)

    else:
        pytest.assume(False)

    a.log_off_json()
    wait(10)

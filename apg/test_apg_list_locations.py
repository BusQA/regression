from utils import wait
from DB_HANDLE import get_feature_prompts,making_prompts,get_help_list
from responses import AG_PROMPTS
import pytest


command_list = get_help_list(list_type="listlocations")
@pytest.mark.run(order=60)
def test_help_me_command(single_client):
    a=single_client[0]
    a.log_on_tool()
    APG_PROMPTS = get_feature_prompts("audiopocketguide")
    dict2={'__TO__':a.name}
    dict2.update(AG_PROMPTS)
    #print dict2
    new_prompt=making_prompts(APG_PROMPTS,dict2)
    wait(2)
    a.command("help me")
    assert  a.i_wait(new_prompt['help_me_command_list'],10)

@pytest.mark.run(order=61)
def test_list_locations(single_client):
    a = single_client[0]
    APG_PROMPTS = get_feature_prompts("audiopocketguide")
    #renaming list of string comma sepetaed string [a,b,c]-->a,b,c
    top_5=",".join([command_list[x] for x in range(1,6)])
    #coverting in lowercase letters
    top_5_lower = top_5.lower()
    dict2 = {'__TO__': a.name,'__LOCATION_LIST__':top_5_lower,'__COUNT__':str(5),'__SAY_MORE__':APG_PROMPTS["say_more_to_hear_more"]}
    # print dict2
    new_prompt = making_prompts(APG_PROMPTS, dict2)
    wait(2)
    a.command("list locations")
    assert a.i_wait(new_prompt['the_top_x_locations_are'],10)


@pytest.mark.run(order=63)
def test_list_locations_next_2(single_client):
    a = single_client[0]
    APG_PROMPTS = get_feature_prompts("audiopocketguide")

    last_command=command_list[6]
    #coverting in lowercase letters
    dict2 = {'__TO__': a.name,'__LOCATION_LIST__':last_command,'__COUNT__':str(1),',__SAY_MORE__':''}
    # print dict2
    new_prompt = making_prompts(APG_PROMPTS, dict2)
    wait(2)
    a.command("more")
    assert a.i_wait(new_prompt['the_next_x_locations_are'],10)
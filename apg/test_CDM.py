import pytest
from config import NAMES
from utils import wait
from addcdm import *

def test_send_CDM(clients):
    a=clients[0]
    t=AddUpdateDeleteCDM()
    t.login_with_username_and_password('aayush','Developer342!')
    flag = t.add_cdm()   
    if flag:
        a.log_on_tool()
        #wait(120)
        assert a.i_wait('posted_at',100)
    else:
        assert False
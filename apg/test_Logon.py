from utils import wait
import pytest


def test_Log_on_command(single_client):
    a=single_client[0]

    a.log_on()
    wait(5)
    assert a.name in a.output()
    a.log_off_json()
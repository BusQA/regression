from utils import wait
from setup import record_replay_c2 as clients
import pytest

e_resp_review = "oops_i_didnt_get_that_do_you_want_to_review_it_yes_or_no"
e_resp_rerecord = "oops_i_didnt_get_that_do_you_want_to_rerecord_it_yes_or_no"
review_response = "do_you_want_to_review_your_announcement_before_posting_it"


@pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.parametrize("command,response", [
   ("announcementnow","announcement_now"),
    ("announcementhour","announcement_hour"),
    ("post it","announcement_today"),
    ("record huddle","morning_huddle"),
])
def test_TGS_638_review(clients,command,response):
    a = clients[0]
    a.log_on_tool()
    wait(2)
    a.command(command)
    wait(3)
    #andrea birdsong,|go_ahead
    if response+",go_ahead" in a.output():
        a.command("1to10")
        wait(2)
        if command == "record huddle":
            global review_response
            review_response = "do_you_want_to_review_your_huddle_before_posting_it"
        if review_response in a.output():
            a.command(a.name)
            wait(3)
            assert e_resp_review in a.output()
            a.tap_out()
            a.log_off_json()
        else:
            print  review_response
            print a.output()
            assert False
    else:
        assert  False
    a.log_off_json()


#@pytest.mark.skip(reason="no way of currently testing this")
@pytest.mark.parametrize("command,response", [
    ("announcementnow","announcement_now"),
    ("announcementhour","announcement_hour"),
    ("post it","announcement_today"),
    ("record huddle","morning_huddle"),

])
def test_TGS_645(clients,command,response):
    a = clients[0]
    a.log_on_tool()
    wait(2)
    a.command(command)
    wait(3)
    #andrea birdsong,|go_ahead
    if response+",go_ahead" in a.output():
        a.command("1to10")
        wait(2)

        if command == "record huddle":
            global review_response
            review_response = "do_you_want_to_review_your_huddle_before_posting_it"

        if review_response in a.output():
            a.command("Z18591")
            wait(3)
            a.command("s300")
            wait(10)
            a.command("n300")
            assert "posted" in a.output()
            a.log_off_json()
        else:
            print a.output()
            print review_response
            assert False
    else:
        assert False
    a.log_off_json()

#!/usr/bin/env python
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from smtplib import SMTP
import smtplib
import sys
from config import MAIL_RECIPIENTS

recipients = MAIL_RECIPIENTS
emaillist = [elem.strip().split(',') for elem in recipients]
msg = MIMEMultipart()
msg['Subject'] = "Smoke results"
msg['From'] = 'bipin@theatrolabs.com'
msg['Reply-to'] = 'bipin@theatrolabs.com,ramya@theatrolabs.com,srinivasarao@theatrolabs.com,pavan@theatrolabs.com,bhaskar@theatrolabs.com,chidanand@theatrolabs.com,chaitanya@theatrolabs.com'
#Result_File = str('E:\\smoke_tool\\results\\smoke.html')
Result_File = str('E:\\smoke_tool\\results\\RnR.html')
msg.preamble = 'Multipart massage.\n'

part = MIMEText("Hi, please find the attached Smoke results Report")
msg.attach(part)

#part = MIMEApplication(open(str(sys.argv[2]), "rb").read())
part = MIMEApplication(open(Result_File, "rb").read())
part.add_header('Content-Disposition', 'attachment', filename=Result_File)
msg.attach(part)

server = smtplib.SMTP("smtp.gmail.com:587")
server.ehlo()
server.starttls()
server.login("theatroindia@gmail.com", "Theatro1")

server.sendmail(msg['From'], emaillist, msg.as_string())
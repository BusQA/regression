from utils import *
from DB_HANDLE import *
from time import strftime
import pytest


ENABLE_RECORD_REPLAY = {"ENABLE_ANNC_HOUR_RECORD_AND_REPLAY":"true","ENABLE_ANNC_NOW_RECORD_AND_REPLAY":"true","ENABLE_ANNC_TODAY_RECORD_AND_REPLAY":"true","ENABLE_HUDDLE_RECORD_AND_REPLAY":"true"}
DISABLE_RECORD_REPLAY = {"ENABLE_ANNC_HOUR_RECORD_AND_REPLAY":"false","ENABLE_ANNC_NOW_RECORD_AND_REPLAY":"false","ENABLE_ANNC_TODAY_RECORD_AND_REPLAY":"false","ENABLE_HUDDLE_RECORD_AND_REPLAY":"false"}

@pytest.fixture(scope="module")
def clients():
    cii = make_clients()
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    [x.log_off_json() for x in cii]
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)

@pytest.fixture(scope="module")
def APG_clients():
    cii = make_clients(count=2)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    [x.log_on_tool() for x in cii]
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
@pytest.fixture(scope="module")
def one_to_one():
    cii = make_clients()
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)


@pytest.fixture(scope="module")
def E2E_CIIs():
    cii = make_clients(names=["test test","donnie brady"],count=2)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    b=cii[-1]
    b.log_on_json()
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)

@pytest.fixture(scope="module")
def record_replay_c2():
    print "enable record and replay"
    DB_UPDATE(ENABLE_RECORD_REPLAY)
    cii = make_clients(count=1)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    print"killing clients"
    [x.log_off_json() for x in cii]
    kill_clients(cii)
    print "disabling record and replay"
    DB_UPDATE(DISABLE_RECORD_REPLAY)


@pytest.fixture(scope="module")
def group_clients():
    cii = make_clients(names=MT_USERS)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    print"killing clients"
    [x.log_off_json() for x in cii]
    kill_clients(cii)
    CLEAR_ANNOUNCEMENTS()

@pytest.fixture(scope="module")
def single_client():
    cii = make_clients(names=MOD_USERS,count =1)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    cii[0].log_off_json()
    print"killing clients"
    kill_clients(cii)

@pytest.fixture(scope="module")
def single_client_basic():
    cii = make_clients(names=basic_user,count =1)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    cii[0].log_off_json()
    print"killing clients"
    kill_clients(cii)

@pytest.fixture(scope="module")
def dbl_client():
    cii = make_clients(count =2)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii[:2]]
    print"killing clients"
    kill_clients(cii)
    CLEAR_ANNOUNCEMENTS()

@pytest.fixture(scope="module")
def sws_clients():
    cii = make_clients(names=sws_users,count =3)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii[:2]]
    print"killing clients"
    kill_clients(cii)
    CLEAR_ANNOUNCEMENTS()

@pytest.fixture(scope="module")
def MOD_clients():
    cii = make_clients(names=MOD_USERS,count =2)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    CLEAR_ANNOUNCEMENTS()


@pytest.fixture(scope="module")
def RB_clients():
    print "setting RB mode as 1"
    #system("./update_db.sh 3 REG_BACKUP_MODE")
    #system("./restart_tgs.sh")
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"false"})
    cii = make_clients(names=RB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120"})

@pytest.fixture(scope="module")
def RB_ESC_clients():
    print "setting RB mode as 1"
    #system("./update_db.sh 3 REG_BACKUP_MODE")
    #system("./restart_tgs.sh")
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"true","ENABLE_INDYME_ESCALATION":"true"})
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"30"})

@pytest.fixture(scope="module")
def RB2_ESC_clients():
    print "setting RB mode as 2"
    DB_UPDATE({"REG_BACKUP_MODE":"2","ENABLE_REG_BACKUP_ESCALATION":"true"})
    cii = make_clients(names=RB_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)

@pytest.fixture(scope="module")
def CO_clients():
    print "disabling location based carry out"
    DB_UPDATE({"ENABLE_LOCATION_BASED_CARRY_OUT":"false"})
    cii = make_clients(names=CO_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "120"})
@pytest.fixture(scope="module")
def CO_Loc_clients():
    print "Enabling location based carry out"
    DB_UPDATE({"ENABLE_LOCATION_BASED_CARRY_OUT":"true"})
    cii = make_clients(names=CO_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"ENABLE_LOCATION_BASED_CARRY_OUT":"false","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120"})

@pytest.fixture(scope="module")
def MA_clients():
    print "disabling location based manager assistance"
    DB_UPDATE({"ENABLE_LOCATION_BASED_MANAGER_ASSISTANCE":"false"})
    cii = make_clients(names=MA_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)

@pytest.fixture(scope="module")
def MA_Loc_clients():
    print "Enabling location based manager assistance"
    DB_UPDATE({"ENABLE_LOCATION_BASED_MANAGER_ASSISTANCE":"true"})
    cii = make_clients(names=MA_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC": "10","ENABLE_LOCATION_BASED_MANAGER_ASSISTANCE":"false"})

@pytest.fixture(scope="module")
def managers():
    cii = make_clients(names=managers_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120"})

@pytest.fixture(scope="module")
def GUN_clients():
    print "Configuring RB mode=3"
    DB_UPDATE({"REG_BACKUP_MODE":3,"ENABLE_REG_BACKUP_ESCALATION":"false"})
    cii = make_clients(names=GUN_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120"})

@pytest.fixture(scope="module")
def GUN_ESC_clients():
    print "Configuring RB mode=3"
    DB_UPDATE({"REG_BACKUP_MODE":"3","ENABLE_REG_BACKUP_ESCALATION":"true","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"5","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"60"})
    cii = make_clients(names=GUN_ESC_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    DB_UPDATE({"REG_BACKUP_MODE":"1","ENABLE_REG_BACKUP_ESCALATION":"false","CASH_REG_BACKUP_ESCALATION_START_TIME_SEC":"30","CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120"})

@pytest.fixture(scope="module")
def one_liner_cii():
    cii = make_clients(count =1)
    a=cii[0]
    print"making clinets"
    a.start()
    a.log_on_tool()
    print "starting clients"
    yield a
    a.log_off_json()
    a.die()

@pytest.fixture(scope="module")
def SB_clients():
    print "disabling location based softbutton"
    DB_UPDATE({"ENABLE_LOCATION_BASED_SOFT_BUTTON":"false"})
    cii = make_clients(names=SB_USERS, count=5)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    [x.log_off_json() for x in cii]
    print"killing clients"
    kill_clients(cii)
    
@pytest.fixture(scope="module")
def IM_clients():
    print "configuring indyme params"
    DB_UPDATE({"ENABLE_INDYME_ESCALATION":"false","LEVEL_ONE_ESCALATION_USER":""})
    cii = make_clients(names=INDYME_USERS)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    print"killing clients"
    [x.log_off_json() for x in cii]
    kill_clients(cii)
    DB_UPDATE({"CASH_REG_BACKUP_NO_RESP_TERMINATE_SEC":"120","ENABLE_INDYME_ESCALATION":"true"})

@pytest.fixture(scope="module")
def IME_clients():
    print "configuring indyme params"
    DB_UPDATE({"ENABLE_INDYME_ESCALATION":"true","LEVEL_ONE_ESCALATION_USER":0})
    cii = make_clients(names=IND_ESC_USERS)
    print"making clinets"
    start_clients(cii)
    print "starting clients"
    yield cii
    print"killing clients"
    [x.log_off_json() for x in cii]
    kill_clients(cii)

@pytest.fixture(scope="module")
def SRD_clients():
    print "configuring store readiness params"
    com = "date |awk -F ' ' '{print $4}'"
    current_time = sshCommand(command=com)
    current_time=current_time.split(":")
    tmp=current_time[0].split("\n")
    current_time[0]=tmp[1]
    #current_time=strftime("%H:%M:%S").split(":")
    current_time[2]=str(int(current_time[2])+(55)).zfill(2)
    if int(current_time[2])>=60:
        current_time[2]=str(int(current_time[2])-60).zfill(2)
        current_time[1]=str(int(current_time[1])+1).zfill(2)
    start_time=":".join(current_time)
    srd_db_update(start_time)
    cii = make_clients(names=SRD_USERS)
    print"making clinets"
    start_clients(cii)
    [x.log_off_json() for x in cii]
    print "starting clients"
    yield cii
    print"killing clients"
    srd_db_update("disable")
    [x.log_off_json() for x in cii]
    kill_clients(cii)


@pytest.fixture(scope="module")
def emergency_clients():
    cii = make_clients(names=EM_USERS)
    print"making clinets"
    start_clients(cii)
    [x.log_off_json() for x in cii]
    print "starting clients"
    yield cii
    print"killing clients"
    [x.log_off_json() for x in cii]
    kill_clients(cii)
import os,time
import sys
import logging
import config
import random
from add_ip_addresses import get_ips
from communicator import Communicator
#from subprocess import call
from ssh_client import *
from splinter import Browser
import DB_HANDLE
from parse import search
from ssh_client import *
from sshtunnel import SSHTunnelForwarder
import MySQLdb

def write_to_file(message,FILE_NAME="Smoke.txt",MODE="a"):
    file = open(FILE_NAME,MODE)
    if message is not "":
        file.write(message)
        file.write("\n")
        file.close()

def make_threads(user_list,action):
    threadlist = []
    for user_name in user_list:
        threadlist.append(threading.Thread(target=action, args=(user_name,)))
    [x.start() for x in threadlist]
    
def get_random_mac_address():
    mac = [ 0x00, 0x00, 0x00,
    random.randint(0x00, 0x7f),
    random.randint(0x00, 0xff),
    random.randint(0x00, 0xff) ]
    return ':'.join(map(lambda x: "%02x" % x, mac))

def wait(interval):
        time.sleep(interval)


def tool(name, boardID='A0700'):
    try:
        browser = Browser('chrome')
        browser.driver.maximize_window()
        browser.visit('http://{0}:{1}/LogonSupport.html'.format(config.TGS_IP, config.LOG_ON_PORT))
        wait(2)
        tag_name = '-'.join(name.split(" "))
        browser.find_by_xpath('''//*[@id="demoOne-nav"]/div[2]/a[1]''').click()
        wait(1)
        browser.find_by_xpath('''//*[@id="{}"]/a'''.format(tag_name)).click()
        wait(1)
        browser.find_by_xpath('''//*[@id="employeeModal"]/div/div/div[2]/span[1]/input''').fill(boardID)
        wait(1)
        browser.find_by_xpath('''//*[@id="employeeModal"]/div/div/div[2]''').click()
        wait(1)
        browser.find_by_xpath('''//*[@id="submit"]''').click()
        wait(1)
        browser.find_by_xpath('''//*[@id="employeeModalsuccess"]/div/div/div[3]/button''').click()
        wait(1)
        browser.quit()
    except Exception as e:
        print e



macs = ['0023a729eab1',
    '0023a7347328',
    '0023a729eaa0',
    '0023a7347307',
    '0023a7347314',
    '0023a729ea92',
    '0023a72ef9ac',
    '0023a729ea8e',
    '0023a7347324',
    '0023a734730e',
    '0023a729eaa2',
    '0023a72ef9b0',
    '0023a729ea8f',
    '0023a73472f9',
    '0023a729eaa8',
    '0023a729eaa5',
    '0023a7347316',
    '0023a734731e',
    '0023a7347318',
    '0023a72ef9b7',
    '0023a734730d',
    '0023a7347326',
    '0023a7347322',
    '0023a7347313',
    '0023a73472fe',
    '0023a72ef9ad',
    '0023a7347316',
    '0023a7347305',
    '0023a729ea90',
    '0023a7347304',
    '0023a729ea9c',
    '0023a729eabb',
    '0023a7347319',
    '0023a73472ff',
    '0023a72ef9b9',
    '0023a729ea8c',
    '0023a729eab6',
    '0023a729eabc',
    '0023a729ea87',
    '0023a729eab2',
    '0023a72ef9af',
    '0023a729ea9f',
    '0023a729eab7',
    '0023a7347310',
    '0023a7347323',
    '0023a72942eb',
    '0023a734732a',
    '0023a72ef9b3',
    '0023a7347317',
    '0023a7347311',
    '0023a734730b',
    '0023a729eab8',
    '0023a729eaac',
    '0023a729ea94',
    '0023a73472fd',
    '0023a7347325',
    '0023a729ea93',
    '0023a729ea99',
    '0023a729ea8b',
    '0023a729eaa6',
    '0023a72ef9b1',
    '0023a729eaab',
    '0023a72ef9b8',
    '0023a729ea98',
    '0023a7347320',
    '0023a7347329',
    '0023a729ea9e',
    '0023a729eab9',
    '0023a734730a',
    '0023a734731c',
    '0023a72ef9ba',
    '0023a7347306',
    '0023a729eab0',
    '0023a729ea91',
    '0023a73b155f',
    '0023a7347315',
    '0023a734730f',
    '0023a7347321',
    '0023a734731b',
    '0023a73b1545',
    '0023a73b154f',
    '0023a73b1559',
    '0023a7347308',
    '0023a73b1565',
    '0023a73b1546',
    '0023a73b1578',
    '0023a73b1583',
    '0023a73b157e',
    '0023a73b1576',
    '0023a73b1552',
    '0023a73b153e',
    '0023a73b155c',
    '0023a73b1556',
    '0023a73b155e',
    '0023a73b1551',
    '0023a73b157f',
    '0023a73b1572',
    '0023a73b156b',
    '0023a73b1575',
    '0023a73b1573',
    '0023a73b1543',
    '0023a73b1544',
    '0023a73b1553',
    '0023a73b155b',
    '0023a73b1475',
    '0023a73b0b2f',
    '0023a73b156c',
    '0023a73b156e',
    '0023a73b1564',
    '0023a73b154c',
    '0023a73b1557',
    '0023a73b1542',
    '0023a73b1548',
    '0023a73b154b',
    '0023a73b1577',
    '0023a73b1579',
    '0023a73b156d',
    '0023a73b155d',
    '0023a73b1558',
    '0023a73b157a',
    '0023a73b1554',
    '0023a73b153f',
    '0023a73b1547',
    '0023a73b1561',
    '0023a73b1570',
    '0023a73b1569',
    '0023a73b1563',
    '0023a73b1562',
    '0023a7xxxxxx',
    '0023a73b157d',
    '0023a73b1571',
    '0023a73b156f',
    '0023a73b0c72',
    '0023a73b1567',
    '0023a73b147f',
    '0023a73b157b',
    '0023a73b1568',
    '0023a73b1555',
    '0023a73b1581',
    '0023a73b148d',
    '0023a73b1490',
    '0023a73b148c',
    '0023a73b146f',
    '0023a7xxxxxx',
    '0023a729eaa7',
    '0023a72ef9b4',
    '0023a73b1549',
    '0023a72ef9ae',
    '0023a7xxxxxx',
    '0023a734731f',
    '0023a73b147b',
    '0023a73b147e',
    '0023a73b1492',
    '0023a73b1494',
    '0023a729eab5',
    '0023a73b149b',
    '0023a72ef9b2',
    '0023a73b1482',
    '0023a73b1498',
    '0023a72ef9b5',
    '0023a73b1487',
    '0023a73b1491',
    '0023a73b1497',
    '0023a73b1496',
    '0023a729ea9a',
    '0023a729eaa4',
    '0023a7xxxxxx',
    '0023a73b149e',
    '0023a73b149c',
    '0023a73b1489',
    '0023a73b1495',
    '0023a73b148a',
    '0023a7347301',
    '0023a734731a',
    '0023a73b148e',
    '0023a73b149a',
    '0023a73b1493',
    '0023a73b0b32',
    '0023a73b155a',
    '0023a73b13db',
    '0023a729ea9d',
    '0023a72ef9bb',
    '0023a734730c',
    '0023a73b1582',
    '0023a73b157c',
    '0023a729eaa9',
    '0023a73b148f',
    '0023a729eaaa',
    '0023a7347327',
    '0023a73b154a',
    '0023a73b1560',
    '0023a73b1566',
    '0023a73b1574',
    '0023a73b156a',
    '0023a73b154e',
    '0023a73b154d',
    '0023a73b1541',
    '0023a73b1540',
    '0023a73b1584',
    '0023a729e9b1',
    '0023a72942e6',
    '0023a729e9ba',
'0023a72942e7',
'0023a72a0401',
'0023a72a03fb',

'0023a729ea32',

'0023a72a03fe',

'0023a72941e1',

'0023a72942fc',

'0023a72a03f2',

'0023a729ea9b',

'0023a72a03f8',

'0023a72941e2',

'0023a72a03e8',

'0023a72a0400',

'0023a72941e4',

'0023a72a03ef',

'0023a72a03e0',

'0023a72a03d8',

'0023a72a03e6',

'0023a72a03c4',

'0023a72a03ca',

'0023a72a03d6',

'0023a72a03cd',

'0023a72a03fa',

'0023a72941e5',

'0023a72a03d2',

'0023a729430e',

'0023a729ea57',

'0023a729eabd',

'0023a7294325',

'0023a7294307',

'0023a7294301',

'0023a7294319',

'0023a7294305',

'0023a729430c',

'0023a72a03fc',

'0023a72a03f6',

'0023a7xxxxxx',

'0023a729431f',

'0023a7294320',

'0023a7294308',

'0023a7294326',

'0023a72a03e9',

'0023a72a03ea',

'0023a72a03e4',

'0023a7294314',

'0023a72a03d7',

'0023a72a03f0',

'0023a72a03d0',

'0023a72903de',

'0023a7294302',

'0023a7294303',

'0023a729e8bb',

'0023a729ed1f',

'0023a72942f3',

'0023a72942f1',

'0023a729ed4b',

'0023a729ed20',

'0023a729e8c1',

'0023a729ed1d',

'0023a729f8ca',

'0023a72942ef',

'0023a729430a',

'0023a7294310',

'0023a72942e4',

'0023a72a03f3',

'0023a72942f2',

'0023a72a03f9',

'0023a7294304',

'0023a72942ec',

'0023a72942e3',

'0023a729e8c2',

'0023a729e8b8',

'0023a729e9a6',

'0023a729e8be',

'0023a72a03d9',

'0023a72a03c1',

'0023a729431b',

'0023a729eaa1',

'0023a7294312',

'0023a729430f',

'0023a7294316',

'0023a7294321',

'0023a7294318',

'0023a729431e',

'0023a72942f5',

'0023a72942f7',

'0023a72942e0',

'0023a7294324',

'0023a729430b',

'0023a72942fb',

'0023a72a03f4',

'0023a729e8b9',

'0023a72a03ee',

'0023a72a03dc',

'0023a72941e8',


]
def to_str(str_or_unicode):
    if isinstance(str_or_unicode,unicode):
        value=str_or_unicode.encode('utf-8')
    else:
        value=str_or_unicode
    return value


def make_clients(start_ip=config.CLI_START_IP, count=config.CLI_COUNT, tgs_ip=config.TGS_IP, names=DB_HANDLE.NAMES):
    if len(names) < count:
        count = len(names)
        print 'not enough names in config for given count... will make only as many as the names are. --> ', count
    clients = []
    for ip_addr, name, index in zip(get_ips(start_ip, count), names, range(count)):
        clients.append(Communicator(macs[index], ip_addr, tgs_ip, to_str(name)))
    return clients
    

def make_clients_with_or_without_names(start_ip=config.CLI_START_IP, count=config.CLI_COUNT, tgs_ip=config.TGS_IP, names=config.NAMES):
    ips = get_ips(start_ip, count)
    clients = []
    for ip_addr, name, index in zip(ips, names, range(count)):
        clients.append(Communicator(get_random_mac_address(), ip_addr, tgs_ip, name))
    raw_input('made {} clients with names'.format(len(clients)))
    remaining_ips = ips[len(names):]
    for ip_addr in remaining_ips:
        clients.append(Communicator(get_random_mac_address().replace(':', ''), ip_addr, tgs_ip, 'noname'))
    raw_input('made {} clients without names'.format(len(remaining_ips)))
    return clients

def start_clients(clients):
    for each in clients:
        each.start()
        print "starting socket for {}".format(each.ip_address)


def kill_clients(clients):
    for each in clients:
        each.die()
        print "killing socket for {}".format(each.ip_address)
    wait(2)

def strip_headers(response):
    without_header = response[response.find('}') + 1 :]
    return without_header

def has_voice_travelled(sender, receiver):
    results = []
    sender_voice_full = ''
    receiver_voice_full = ''
    for sender_voice, receiver_voice in zip(sender.get_last_voice_out(), receiver.get_last_response()):
        sender_voice, receiver_voice = map(strip_headers, [sender_voice, receiver_voice])
        results.append(sender_voice == receiver_voice)
        sender_voice_full += sender_voice
        receiver_voice_full += receiver_voice
    return all(results)
    #return results, sender_voice_full, receiver_voice_full

def DB_UPDATE(para_dict):
    [sshCommand(command='/home/theatro/bin/update_db.sh "{0}" "{1}"'.format(value,param)) for param,value in zip(para_dict.keys(),para_dict.values())]
    print "updating DB with {}\n".format(para_dict)
    sshCommand('/home/theatro/bin/backup.sh')
    print "restarting TGS"

def rename_table(old,new):
    sshCommand(command='/home/theatro/bin/rename_feature_prompt_table.sh "{0}" "{1}"'.format(old,new))
    sshCommand('/home/theatro/bin/restart_tgs.sh')

def CLEAR_ANNOUNCEMENTS():
    sshCommand('/home/theatro/bin/backup.sh')


def in_words(num):
    num_words_single =  {1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five'
                        ,6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten'
                        ,11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen'
                        ,15: 'fifteen', 16: 'sixteen', 17: 'seventeen', 18: 'eighteen', 19: 'nineteen'
                        }
    num_words_double =  ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

    if 1 <= num < 19:
        return num_words_single[num]
    elif 20 <= num <= 99:
        tens, below_ten = divmod(num, 10)
        return num_words_double[tens - 2] + '-' + num_words_single[below_ten]
    else:
        return str(num)

def get_stats_from_tag(pattern):
    grep_cmd='cat /opt/theatro/logs/tag_tsm.log | grep "{}" | tail -1'.format(pattern)
    return sshCommand(command=grep_cmd)



def get_rnr_txid_from_tgs(rnr,type='txid',service_rnr=False):
    #grep_cmd='cat /opt/theatro/logs/tag_tsm.log | grep "{}" | tail -1'.format(rnr)
    result=get_stats_from_tag(rnr)
    if service_rnr:
        pattern = '''"txID": "{:d}"'''
    else:
        pattern = '''"transaction_id": "{:d}"'''
    if type=='wave':
        pattern = '''"filename": "{}"'''
    
    elif type=='oraPath':
        pattern = '''"orpath": "{}"'''

    try:
        if type == 'txid':
            return str(search(pattern,result).fixed[0])
        elif type == 'wave':
            pattern2='/opt/theatro/TheatroLabsIncProgramFiles/tgs/Systemtgs/S3_Audio/{}.wav'
            wave_file_path=str(search(pattern,result).fixed[0])
            return str(search(pattern2,wave_file_path).fixed[0])
        else:
            pattern2='/opt/theatro/logs/HEAR_ALL/donnie brady/{}.wav'
            wave_file_path=str(search(pattern,result).fixed[0])
            return str(search(pattern2,wave_file_path).fixed[0])

    except Exception as AttributeError:
        print "Not able to grep"
        return False
    

def get_Request_Response_stats(id,type):
    #id=get_rnr_txid_from_tgs(rnr)
    with SSHTunnelForwarder(('8.8.26.26', 22), ssh_password='4help2shoppers', ssh_username='theatro', remote_bind_address=('localhost', 3306)) as server:
       conn = None
       conn = MySQLdb.connect(host='127.0.0.1', port=server.local_bind_port, user='theatro', passwd='theatro',db='theatrostat')
       cursor = conn.cursor(MySQLdb.cursors.DictCursor)
       tables=cursor.execute('''select * from Request_Response where txId='{0}'and type like '%{1}%';'''.format(id,type))
       results = cursor.fetchall()
       return results[0]

def get_RnR_stat(id,type='stat'):
    #id=get_rnr_txid_from_tgs(rnr)
    with SSHTunnelForwarder(('8.8.26.26', 22), ssh_password='4help2shoppers', ssh_username='theatro', remote_bind_address=('localhost', 3306)) as server:
       conn = None
       conn = MySQLdb.connect(host='127.0.0.1', port=server.local_bind_port, user='theatro', passwd='theatro',db='theatrostat')
       cursor = conn.cursor(MySQLdb.cursors.DictCursor)
       if type=='stat':
            tables=cursor.execute('''select * from rnr_service_call where tx_id='{0}';'''.format(id))
            results = cursor.fetchall()
            return results[0]
       else:
            tables=cursor.execute('''select * from rnr_service_event where tx_id='{0}';'''.format(id))
            results = cursor.fetchall()
            return results

       
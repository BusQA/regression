import pexpect
import config
import sys

# change "eth0" to "wlan0" if connected through the wireless adaptor
#command = 'sudo ifconfig eth0:{0} {1} netmask 255.255.0.0 up'

command = 'sudo ifconfig en0:{0} {1} netmask 255.255.255.0 up'

def get_ips(start_ip=config.CLI_START_IP, count=config.CLI_COUNT):
    base_octet_1, base_octet_2, base_octet_3, base_octet_4 = map(int, start_ip.split('.'))
    ips = []
    for idx in range(count):
        if (base_octet_4 + idx) % 256 == 0:
            print 'mod recycled'
            base_octet_3 += 1
        ip_to_add = [base_octet_1, base_octet_2, base_octet_3, (base_octet_4 + idx) % 256]
        ips.append('.'.join(map(str, ip_to_add)))
    return ips

def add_ips(start_ip=config.CLI_START_IP, count=config.CLI_COUNT):
    ips = get_ips(start_ip, count)
    for idx, ip_to_add in enumerate(ips):
        if ip_to_add not in config.BLOCKED_IPS:
            task = pexpect.spawn(command.format(idx + 1, ip_to_add))
            print command.format(idx + 1, ip_to_add)
            task.logfile = sys.stdout
            task.expect('password')
            task.sendline(config.SUDO_PASSWORD)
            task.wait()

if __name__ == '__main__':
   add_ips()

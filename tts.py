import subprocess
import audioop
import os
import wave
import struct
import config
from tts_polly import bulk
if os.name == 'nt':
    import winsound
    from win32com.client import Dispatch
    import pythoncom
    SAFT8kHz16BitMono = 6
    SAFTCCITT_uLaw_8kHzMono = 48
    SSFMCreateForWrite = 3

def store_tts_to_wav(text, out_filename):
    if os.name == 'nt':
        pythoncom.CoInitialize()
        voice = Dispatch("SAPI.SpVoice")
        stream = Dispatch("SAPI.SpFileStream")
        Format = Dispatch("SAPI.SpAudioFormat")
        Format.Type = SAFT8kHz16BitMono
        stream.Format = Format
        stream.Open(out_filename, SSFMCreateForWrite)
        voice.AudioOutputStream = stream
        voice.speak(text)
        stream.Close()
    elif os.name == 'posix':
        subprocess.call('echo "'+text+'" | text2wave -F 8000 -otype ulaw -scale '+str(config.TTS_AMPLITUDE)+' -o '+out_filename , shell=True)
    else:
        exit('''

            Unknown Operating System...

            os.name not in ['nt', 'posix']

            ''')

def yield_speech(text_list, directory=config.TTS_DIR, keep_file=True, play_file=False):
    '''
    generates text-to-speech and yields speech chunks of 200 bytes
    '''
    # assert that the given .wav exists
    #speech_filename = text.replace(' ', '_') + '.wav'
    speech_files = []
    #print text_list
    for text in text_list:
        speech_filename = text + '.wav'
        #print speech_filename
        speech_filepath = os.path.join(directory, speech_filename)
        #print speech_filepath
        if speech_filename not in os.listdir(directory):
            bulk(text,speech_filepath)
            #speech_filepath = os.path.join(config.AUDIO_PATH, speech_filename)
        speech_files.append(speech_filepath)
        #print speech_files
        # optionally play the file
        if play_file:
            if os.name == 'posix':
                subprocess.call(['aplay', '-f', 'MU_LAW', speech_filepath])
            elif os.name == 'nt':
                winsound.PlaySound(speech_filepath, winsound.SND_FILENAME | winsound.SND_ASYNC)

    ulaw_data = ""
    #print speech_files
    for each_file in speech_files:
        with open(each_file, 'rb') as in_file:
            data = in_file.read()
        wav_data = data[(data.find('data') + 4):]
        data_len = struct.unpack('<L', wav_data[:4])[0]
        raw_data = wav_data[4:][:data_len]
        ulaw_data += raw_data
        ''' for linux
        if os.name == 'nt':
            ulaw_data += audioop.lin2ulaw(raw_data, 2)
        elif os.name == 'posix':
            ulaw_data += raw_data
            '''

    if config.WB_SUPPORT:
        while ulaw_data:
            chunk, ulaw_data = ulaw_data[:400], ulaw_data[400:]
            yield chunk
    else:
        while ulaw_data:
            chunk, ulaw_data = ulaw_data[:200], ulaw_data[200:]
            yield chunk

    if not keep_file:
        for each_file in speech_files:
            os.remove(each_file)

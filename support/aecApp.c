#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>

/* Prototype declaration */
int sendData(const unsigned char *buffer, unsigned int bufferLen,
	     const char *pSendToIp, unsigned int sendToPortNo,int VoiceLength);
int main(int argc , char * argv[])
{
   FILE *fp = NULL;
   FILE *fout = NULL;
   int chCnt = 0;
   int outCnt = 0;
   int filesize = 0;
   int bytesRead = 0;
   int colonCnt = 0;
   int fromServer = 0;
   int i;
   char c;
   char c1;
   int k = 0;
   int j = 0;
   char length[100];
   char firststring[100];
   char pStrOr[100] =
       { '{', '"', 'o', 'r', 'u', 'i', 'd', '"', ':', '\0' };
   int len = -1;
   int numColCnt = 0;
   unsigned int size = 2000000;
   char colon = ':';
   char *charctr = (char *) malloc(size * sizeof(char));
   char *pVoice = (char *) malloc(size * sizeof(char));
   char *pStVoice;

#if 1
   int pktCount = 0;
   int jj;
   int orPktCnt = 0;
   char *kkptr;
   char orPkt[1000];
   int prevCnt = 0;
   int presCnt = 0;
  
   memset(orPkt, 0, 1000);

#endif				/* */
    //  fp = fopen("pcapraw_ChoppyVoice_choppy_voice_50008_100", "rb");
    fp = fopen(argv[1], "rb");

   //fp = fopen("pcapraw_ChoppyVoice_voice_chop_broadc_100", "rb");
   //fp = fopen("pcapraw_GoodVoice_powersave_104_50008", "rb");
   if (fp == NULL) {
      printf("Error Opening pcapbin file\n");
      exit(0);
   }
   fout = fopen("g711", "wb");
   if (fout == NULL) {
      printf("Error Opening pcapbin file\n");
      exit(0);
   }
   fseek(fp, 0L, SEEK_END);
   filesize = ftell(fp);
   fseek(fp, 0L, SEEK_SET);
   bytesRead = fread(charctr, sizeof(char), filesize, fp);
   pStVoice = pVoice;
   i = 0;
   colonCnt = 0;

#if 1
   kkptr = &charctr[i];

#endif				/* */
//Check if server to client or vice versa
   while (colonCnt != 1) {
      c = charctr[i++];
      firststring[k++] = c;

#if 0
      orPkt[orPktCnt++] = c;

#endif				/* */
      if (c == colon)
	 colonCnt++;
   }
   firststring[k] = '\0';
   if (strcmp(pStrOr, firststring) == 0) {
      fromServer = 1;
      printf(" Server to Client Oration File.\n");
   } else {
      fromServer = 0;
      printf("Client to Server Voice.\n");
   }
   i = 0;
   colonCnt = 0;
   k = 0;
   while (1) {

#if 1
      memset(orPkt, 0, 1000);

#endif				/*  */
      colonCnt = 0;
      if (fromServer) {
	 while (colonCnt != 4) {
	    c = charctr[i++];
	    if (c == colon)
	       colonCnt++;
	 }
      } else {
	 if (!chCnt)
	    numColCnt = 6;

	 else
	    numColCnt = 5;
	 while (colonCnt != numColCnt) {
	    c = charctr[i++];
	    if (c == colon)
	       colonCnt++;
	 }
      }
      k = 0;
      if (fromServer) {
	 while ((c1 = charctr[i++]) != ',')
	    length[k++] = c1;
	 length[k] = '\0';
	 len = atoi(length);
	 while ((c1 = charctr[i++]) != '}');
      } else {
	 while ((c1 = charctr[i++]) != '}')
	    length[k++] = c1;
	 length[k] = '\0';
	 len = atoi(length);
      }

// Extract voice
      for (j = 0; j < len; j++) {
	 pVoice[j] = charctr[i++];
	 outCnt++;
      }
      pVoice += len;
	printf("The length of voice is %d\n",len);

#if 1
// Send pkt over UDP socket
      memcpy(orPkt, kkptr, (i - prevCnt));
      pktCount++;
      sendData(orPkt, (i - prevCnt), "192.168.1.6", 50008,len);
      usleep(1000);
      printf(" Pkt Number = %d Pkt Sent of size %d\n", pktCount,(i - prevCnt));
      prevCnt = i;
      kkptr = &charctr[i];

#endif				/* */
      if (i >= bytesRead) {
	 break;
      }
      colonCnt = 0;
      chCnt++;
   }
   fwrite(pStVoice, sizeof(char), outCnt, fout);
   fclose(fout);
   fclose(fp);
   return 0;
}

int
sendData(const unsigned char *buffer, unsigned int bufferLen,
	 const char *pSendToIp, unsigned int sendToPortNo,int voiceLength)
{
   static int count;
   struct sockaddr_in serverAddr;
   int sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
   memset((char *) &serverAddr, 0, sizeof(serverAddr));
   serverAddr.sin_family = AF_INET;
   serverAddr.sin_port = htons(sendToPortNo);
   if (inet_aton(pSendToIp, &serverAddr.sin_addr) == 0) {
      fprintf(stderr, "inet_aton() failed\n");
      return -1;
   }
   if (sendto
       (sockfd, buffer, bufferLen, 0, (struct sockaddr *) &serverAddr,
	sizeof(serverAddr)) == -1) {
      fprintf(stderr, "sendto() failed\n");
      return -1;
   }

   close(sockfd);
   return 0;
}

def get_acceptor_data(t2):
    for ele in t2:
        if ele['event_method'] == 8:
            return (str(ele['userHostID']),str(ele['userId']))

def get_so_mac_ids(t2):
	lista=[]
	for ele in t2:
		if ele['event_method'] == 11:
			lista.append(str(ele['userHostID']))
	return lista
	
def verify_so_event(t1):
    cnt=0
    for ele in t1:
        if ele['event_method'] == 11:
            cnt+=1
    if cnt==4:
        return True
    else:
        return False

def verify_accept_event(t1):
    cnt=0
    for ele in t1:
        if ele['event_method'] == 8:
            cnt+=1
    if cnt==1:
        return True
    else:
        return False
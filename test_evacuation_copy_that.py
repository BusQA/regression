from utils import wait,DB_UPDATE
import pytest
from communicator import *
from DB_HANDLE import get_feature_prompts,making_prompts
pytest.EM_PROMPTS_1=get_feature_prompts('emergency',1)
pytest.EM_PROMPTS_2=get_feature_prompts('emergency',2)
'''
a ['angella hilt', allowed user #front end 
b'ben sander', allowed user # front end
c'amanda peterson', target group #front end
d'david jason',north bay leader #1 #front end 
e'donnie brady', north bay leader #2 #front end
f'ricky vance', #front end
g'bob bennion'south bay leader #1 # millwork
h'allen tippet'south bay leader #2 #millwork
i'amanda scott' south bay leader#3 #millwork
j'jay bosler'], normal user # millwork
k'david clarke', #millwork
l'holli root' #millwork target group



'''


def test_emergency_first_prompt(emergency_clients):
    for x in emergency_clients:
        x.log_on_tool()
        wait(2)
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt = (making_prompts(pytest.EM_PROMPTS_1, {'__TO__':a.name, '__TO_FN__': a.fname})['select_service']).lower()
    a.command("emergency guide")
    assert a.i_wait(expected_prompt)



def test_evacuation(emergency_clients):
    a=emergency_clients[0]
    a.command('evacuate the store')
    assert a.i_wait((pytest.EM_PROMPTS_1['confirm_activation']).lower())

def test_evacuation_yes(emergency_clients):
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt = making_prompts(pytest.EM_PROMPTS_1, {'__TO__': a.name, '__TO_FN__': a.fname})['initiator_initial_info']
    a.command('yes')
    assert a.i_wait(expected_prompt)




def test_evacuation_first_copy_that(emergency_clients):
    wait(120)
    a, b, c, d, e, f, g, h, i, j, k, l = emergency_clients
    expected_prompt = making_prompts(pytest.EM_PROMPTS_1, {'__TO__': a.name, '__TO_FN__': a.fname})[
        'target_group_initial_info']
    b.command('copy that')
    b.i_wait(expected_prompt)
    prompt = (making_prompts(EM_PROMPTS, {'__COUNT__': '12',
                                          '__ZONE_LEADER_ACCEPTED_COUNT__': '0',
                                          '__ZONE_LEADER_TOTAL_COUNT__': '2',
                                          '__ASSOCIATES_ACCEPTED_COUNT__': '1',
                                          '__ASSOCIATES_TOTAL_COUNT__': '7',
                                          }))['update_info'].lower()
    a.command('update me')
    a.i_wait()


    #Ok,__TO_FN__,__TO__,I_am_glad_everyone_is_safe!_I_will_send_the_all_clear_announcement_now,tap_out_to_cancel
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt = ((making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname,}))['close_confirmation']).lower()
    expected_prompt_1 = ((making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname, }))['close_to_listener']).lower()
    a.command('all clear')
    assert a.i_wait(expected_prompt,20)
    assert all((x.i_wait(expected_prompt_1, 60)) for x in emergency_clients)

def jmj bnnnnnnnnnnnnnnnnnnnnnnnbnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnb n                                   bnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnjn             bn(emergency_clients):
    a=emergency_clients[0]
    #Ok,__TO_FN__,__TO__,I_am_glad_everyone_is_safe!_I_will_send_the_all_clear_announcement_now,tap_out_to_cancel
    EM_PROMPTS = get_feature_prompts('emergency', 1)
    expected_prompt = (making_prompts(EM_PROMPTS, {'__TO__': a.name, '__TO_FN__': a.fname,}))['close_confirmation']
    wait(10)
    a.command('all clear')
    wait(5)
    assert a.output() not in expected_prompt

#!/usr/bin/python
from termcolor import colored
import utils,config,time
global clients
clients = utils.make_clients()
utils.start_clients(clients)
a,b,c,d,e= clients
#Log in One by one
for each in clients:
   each.log_on_tool()

#Broadcast calltype 1
a.broadcast('atoz')
print colored('Broadcast calltype 1 is done','red')

#One to One call call type2
a.say('menubutton',['Hello',b.name])
time.sleep(5)
print a.output()
if 'go_ahead' in a.output():
	a.say('menubutton',['atoz'])
	time.sleep(3)
	b.say('menubutton',['atoz'])
	a.tap_out()
	time.sleep(10)



#Message call type 4
a.say('menubutton',['message',b.name])
time.sleep(7)
a.say('menubutton',['atoz'])
time.sleep(30)

#Announcment Now type 5,21
a.say('menubutton',['announcementnow'])
time.sleep(2)
a.say('menubutton',['atoz'])
time.sleep(10)


#Announcment Now type 22
a.say('menubutton',['announcementhour'])
time.sleep(2)
a.say('menubutton',['atoz'])
time.sleep(10)

#Announcment Now type 23
a.say('menubutton',['announcementtoday'])
time.sleep(2)
a.say('menubutton',['atoz'])
time.sleep(10)

#One to One call call type2
a.say('menubutton',['Interrupt',b.name])
time.sleep(5)
print a.output()
if 'go_ahead' in a.output():
	a.say('menubutton',['atoz'])
	time.sleep(3)
	b.say('menubutton',['atoz'])
	a.tap_out()
	time.sleep(10)

a.say('menubutton',['interruptall'])
time.sleep(2)

if 'go_ahead' in a.output():
	a.say('menubutton',['atoz'])


for each in clients:
	each.log_off_json()
time.sleep(20)

utils.kill_clients(clients)

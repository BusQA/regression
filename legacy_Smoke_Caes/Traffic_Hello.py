#!/usr/bin/python
#Date:11th Feb 2016
#Author:Bipin
#Objective:Run Traffic Continously
from termcolor import colored
import utils,config,time,threading,thread,time
def message(a,b):
	while True:
		Start_Message(a,b)
		Start_Message(b,a)
def Start_Message(caller_party,called_party):
	caller_party.say('menubutton',['message',called_party.name])
	time.sleep(7)
	if "go_ahead" in caller_party.output():
		caller_party.say('menubutton',['1to10'])
		time.sleep(30)
		if 'has_heard_your_message' in caller_party.output():
			utils.write_to_file("Message Passed","Traffic_Message.txt")
		else:
			utils.write_to_file("Message Failed","Traffic_Message.txt")
	else:
		utils.write_to_file("Message Failed. User got: "+str(caller_party.output()),"Traffic_Message.txt")

def Start_One_to_One(caller_party,called_party,call="Hello"):
	caller_party.say('menubutton',[call,called_party.name])
	time.sleep(5)
	if "go_ahead" in caller_party.output():
		caller_party.say('menubutton',['1to10'])
		caller_party.tap_out()
		time.sleep(10)
		if 'ended_call' in called_party.output():
			utils.write_to_file(call+" Passed","Traffic_One_2_One.txt")
		else:
			utils.write_to_file(call+" Failed","Traffic_One_2_One.txt")
	else:
		utils.write_to_file(call+" Failed. User got: "+str(caller_party.output()),"Traffic_One_2_One.txt")


def One_2_One(a,b):
	while True:
		Start_One_to_One(a,b)
		Start_One_to_One(b,a)

def Interrupt(a,b):
	while True:
		Start_One_to_One(a,b,"Interrupt")
		Start_One_to_One(b,a,"Interrupt")

def Oration(initiator,E_output,command,file_name):
	initiator.say('menubutton',['1to10'])	
	time.sleep(10)
	if (E_output in initiator.output()):
		utils.write_to_file(command+" Passed",file_name)
	else:
		utils.write_to_file(command+" Failed",file_name)


def announcement(initiator):
	while True:
		initiator.say('menubutton',['announcementnow'])
		time.sleep(2)
		initiator.say('menubutton',['1to10'])
		time.sleep(10)
		initiator.say('menubutton',['announcementhour'])
		time.sleep(2)
		initiator.say('menubutton',['1to10'])
		time.sleep(10)
		initiator.say('menubutton',['announcementtoday'])
		time.sleep(2)
		initiator.say('menubutton',['1to10'])
		time.sleep(10)
def group_cases(initiator):
	while True:
		initiator.say('menubutton',['message','hunting'])
		time.sleep(5)
		if "go_ahead" in initiator.output():
			Oration(initiator,"tts_posted","Group Message","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group Message Failed. User got: "+str(initiator.output()),"Traffic_Group_Cases.txt")			

		initiator.say('menubutton',['announcementnow','hunting'])
		time.sleep(7)
		if "go_ahead" in initiator.output():
			Oration(initiator,"tts_posted","Group Announcement","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group Announcement Failed. User got: "+str(initiator.output()),"Traffic_Group_Cases.txt")			
		
		initiator.say('menubutton',['broadcast','managers'])
		time.sleep(5)
		if "go_ahead" in initiator.output():
			Oration(initiator,"tts_4","Group Broadcast","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group Broadcast Failed. User got: "+str(initiator.output()),"Traffic_Group_Cases.txt")			
		
		initiator.say('menubutton',['contact','GEAR'])
		time.sleep(5)
		if "go_ahead" in initiator.output():
			Oration(initiator,"tts_group_call_ended","Group Call","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group Broadcast Failed. User got: "+str(initiator.output()),"Traffic_Group_Cases.txt")			
		
def pairs(items):
        return zip(items[::2], items[1::2])
def Cont_one_2_one(lista):
    threadlist = []
    for fobx, foby in pairs(lista):
    	threadlist.append(threading.Thread(target=One_2_One, args=(fobx,foby,)))
    	print threadlist
    [x.start() for x in threadlist]
    print "started thread"

def Cont_Interrupt(lista):
    threadlist = []
    for fobx, foby in pairs(lista):
    	threadlist.append(threading.Thread(target=Interrupt, args=(fobx,foby,)))
    [x.start() for x in threadlist]



def Cont_message(lista):
    threadlist = []
    for fobx, foby in pairs(lista):
        threadlist.append(threading.Thread(target=message, args=(fobx,foby,)))
    [x.start() for x in threadlist]
def stability():
	global clients
	clients = utils.make_clients_with_or_without_names()
	print "clients:" + str(clients)
	One_2_One_Users = clients[1:(config.CLI_COUNT -1)]
	print One_2_One_Users
	utils.start_clients(One_2_One_Users)
	[x.log_on_tool() for x in One_2_One_Users]
	print "calling Cont"
	Cont_one_2_one(One_2_One_Users)
	print "called"
if __name__ == '__main__':
	stability()

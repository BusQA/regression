#!/usr/bin/python
#Date:11th Feb 2016
#Author:Bipin
#Objective:zendesk issue 18591
from termcolor import colored
import utils,config,time,threading,thread,time,multiprocessing

def announcement(initiator):
	while True:
		initiator.command("announcementnow","yes")
		initiator.command("Z18591")


def stability():
	global clients
	clients = utils.make_clients(count=1)
	utils.start_clients(clients)
	a= clients[0]
	a.log_on_tool()
	announcement(a)
if __name__ == '__main__':
	stability()

#!/usr/bin/python
from termcolor import colored
import utils,config,time
from os import system
global clients
clients = utils.make_clients()
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'


utils.write_to_file("##########################Message################################")


print colored(' Message to logged on and available user','red')
print clear
a.log_on_tool()
b.log_on_tool()
time.sleep(2)
a.say('menubutton',['message',b.name])
time.sleep(7)
a.say('menubutton',['1to10'])
time.sleep(30)
if 'has_heard_your_message' in a.output():
	utils.write_to_file("Message to logged on and available user Passed")
else:
	utils.write_to_file("Message to logged on and available user Failed")
##########################One to one message reboot Survival################################
print colored('One to one message reboot Survival','red')
print clear
b.tap_out()
a.say('menubutton',['message',b.name])
time.sleep(7)
a.say('menubutton',['1to10'])
#Restarting TGS
system("./restart_tgs.sh")
b.tap_out()
time.sleep(30)

if 'has_heard_your_message' in a.output():
	utils.write_to_file("One to one message reboot Survival Passed")
else:
	utils.write_to_file("One to one message reboot Survival Failed")


##########################Message to non logged on user################################3
print colored(' Message to non logged on user','red')
print clear
a.say('menubutton',['message',c.name])
time.sleep(7)
a.say('menubutton',['1to10'])
c.log_on_tool()
time.sleep(30)
if 'has_heard_your_message' in a.output():
	utils.write_to_file("Message to non logged on user Passed")
else:
	utils.write_to_file("Message to non logged on user Failed")
##########################Message to engaged user################################3
print colored(' Message to engaged user','red')
print clear

b.tap_out()
time.sleep(15)
a.say('menubutton',['message',b.name])
time.sleep(7)
a.say('menubutton',['1to10'])
time.sleep(10)
b.tap_out()
time.sleep(30)

if 'has_heard_your_message' in a.output():
	utils.write_to_file("Message to engaged user Passed")
else:
	utils.write_to_file("Message to engaged user Failed")

for each in clients:
	each.log_off_json()

utils.kill_clients(clients)

#!/usr/bin/python
from termcolor import colored
import utils,config,time
from os import system
global clients
clients = utils.make_clients()
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
for each in clients:
	each.log_on_tool()
a.say('menubutton',['record huddle'])
time.sleep(5)
if '''tts_morning_huddle,|go_ahead,|tap_out_to_cancel''' in a.output():
	a.say('menubutton',['1to10'])
	time.sleep(20)
	if ('tts_posted' in a.output()) and ('tts_posted_at' in b.output()) and ('tts_posted_at' in c.output()) and ('tts_posted_at' in d.output()) and ('tts_posted_at' in e.output()):
		utils.write_to_file("Record Huddle Passed")
	else:
		utils.write_to_file("Record Huddle Failed")
else:
	utils.write_to_file("Record Huddle command not succeded, TGS Response is %s",a.output())

for each in clients:
	each.log_off_json()

utils.kill_clients(clients)

#!/usr/bin/python
from termcolor import colored
import utils,config,time,threading,thread,time

def message(caller,called):
	while True:
		caller.say('menubutton',['message',called.name])
		time.sleep(5)
		caller.say('menubutton',['1to10'])
		time.sleep(20)
		if 'has_heard_your_message' in caller.output():
			utils.write_to_file("Message direct play success","message.txt")
		else:
			utils.write_to_file("Message direct play Failed","message.txt")

def message_play(caller,called):
	while True:
		called.log_off_json()
		time.sleep(5)
		caller.say('menubutton',['message',called.name])
		time.sleep(5)
		caller.say('menubutton',['1to10'])
		called.log_on_tool()
		time.sleep(2)
		called.say('menubutton',['playmessage'])
		time.sleep(20)
		if 'has_heard_your_message' in caller.output():
			utils.write_to_file("play message success","message_play.txt")
		else:
			utils.write_to_file("play message success Failed","message_play.txt")

def message_ttl(caller,called):
	while True:
		called.log_off_json()
		time.sleep(5)
		caller.say('menubutton',['message',called.name])
		time.sleep(5)
		caller.say('menubutton',['1to10'])
		time.sleep(70)
		called.log_on_tool()
		time.sleep(2)
		called.say('menubutton',['playmessage'])
		if 'tts_no_messages' in caller.output():
			utils.write_to_file("Message Timeout success","message_ttl.txt")
		else:
			utils.write_to_file("Message Timeout Failed","message_ttl.txt")

def Suite():
	clients = utils.make_clients(count=6)
	utils.start_clients(clients)
	a,b,c,d,e,f= clients
	clear = 10 * '\n'
	for each in clients:
		each.log_on_tool()
		time.sleep(3)
	stab1_task = threading.Thread(target=message_play, args=(a,b))
	stab1_task.start()
	stab2_task = threading.Thread(target=message, args=(c,d))
	stab2_task.start()
	stab3_task = threading.Thread(target=message_ttl, args=(e,f))
	stab3_task.start()
if __name__ == '__main__':
	Suite()

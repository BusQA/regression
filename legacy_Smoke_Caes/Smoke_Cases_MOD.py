#!/usr/bin/python
from termcolor import colored
from os import system
import utils,config,time
global clients
from DB_HANDLE import MOD_USERS
clients = utils.make_clients(names=MOD_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
time.sleep(5)
for each in clients:
	each.log_on_tool()
	time.sleep(3)
utils.write_to_file("##########################Manager on Duty################################")
print colored('MOD logon','red')
print clear
a.say('menubutton',['checkin manager on duty'])
time.sleep(20)
if ('you_are_now_manager_on_duty' in a.output()) and ('is_now_the_manager_on_duty' in b.output()) and ('is_now_the_manager_on_duty' in c.output()) and ('is_now_the_manager_on_duty' in d.output()) :
	utils.write_to_file("MOD logon Passed")
else:
	utils.write_to_file("MOD logon Failed"+a.output())
##########################Hello MOD################################
print colored('Hello MOD','red')
print clear
time.sleep(5)
b.say('menubutton',['Hello','MOD'])
time.sleep(5)
if 'go_ahead' in b.output():
	b.say('menubutton',['Hello','MOD'])
	a.tap_out()
	time.sleep(10)
	if 'call_ended' in a.output():
		utils.write_to_file("Hello MOD Passed")
	else:
		utils.write_to_file("Hello MOD Failed"+a.output())
##########################MOD stealing################################3
print colored('MOD stealing','red')
print clear
time.sleep(10)
b.say('menubutton',['checkin manager on duty'])
time.sleep(20)

if ('has_just_checked_in_and_is_now_manager_on_duty' in a.output()) and ('you_are_now_manager_on_duty' in b.output()):
	utils.write_to_file("MOD stealing Passed")
else:
	utils.write_to_file("MOD stealing Failed"+a.output())
##########################Message MOD################################
print colored('Message MOD','red')
print clear
a.say('menubutton',['message','MOD'])
time.sleep(5)
a.say('menubutton',['message','1to10'])
time.sleep(30)
if 'has_heard_your_message' in a.output():
	utils.write_to_file("Message MOD Passed")
else:
	utils.write_to_file("Message MOD Failed"+a.output())


##########################who is MOD################################3
print colored('who is MOD','red')
print clear
a.say('menubutton',['whoisMOD'])
time.sleep(5)

if (b.name in a.output()):
	utils.write_to_file("who is MOD Passed")
else:
	utils.write_to_file("who is MOD Failed"+a.output())
##########################Checkin MOD from Non Manager Member################################3
print colored('Checkin MOD from Non Manager Member','red')
print clear
e.say('menubutton',['checkin manager on duty'])
time.sleep(5)

if ('you_do_not_have_access_to_this_command,you_are_not_a_member_of,managers' in e.output()):
	utils.write_to_file("Checkin MOD from Non Manager Member Passed")
else:
	utils.write_to_file("Checkin MOD from Non Manager Member Failed"+e.output())
##########################MOD Reboot Survival################################
print colored('MOD Reboot Survival','red')
print clear
b.log_off_json()
time.sleep(5)
e.say('menubutton',['message','MOD'])
time.sleep(5)
e.say('menubutton',['message','1to10'])
system("./restart_tgs.sh")
a.log_on_tool()
time.sleep(3)
a.say('menubutton',['checkin manager on duty'])
time.sleep(60)
if 'posted_at' in a.output():
	utils.write_to_file("Message MOD Reboot Survival Passed")
else:
	utils.write_to_file("Message MOD Reboot Survival Failed"+a.output())


for each in clients:
	each.log_off_json()
	time.sleep(2)

utils.kill_clients(clients)

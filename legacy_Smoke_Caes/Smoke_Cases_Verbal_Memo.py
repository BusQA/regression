#!/usr/bin/python
from termcolor import colored
from responses import VM_PROMPTS
import utils,config,time
global clients
from DB_HANDLE import MOD_USERS
clients = utils.make_clients(names=MOD_USERS,count=1)
utils.start_clients(clients)
a=clients[0]

clear = 10 * '\n'

utils.write_to_file("##########################Issuing Verbal Memo command################################")
print colored(VM_PROMPTS["TEST_CASE_1"][0],'red')
print clear
a.log_on_tool()
time.sleep(5)
a.say('menubutton',['verbal memo'])
time.sleep(5)
if VM_PROMPTS["TEST_CASE_1"][1] in a.output():
	a.say('menubutton',['1to10'])
	time.sleep(5)
	if VM_PROMPTS["TEST_CASE_1"][2] in a.output():
		utils.write_to_file(VM_PROMPTS["TEST_CASE_1"][0]+" Passed")
	else:
		utils.write_to_file(VM_PROMPTS["TEST_CASE_1"][0]+" Failed"+"\n"+"User got:"+a.output()+"\n"+"Expected is:"+VM_PROMPTS["TEST_CASE_1"][2])
else:
	utils.write_to_file(VM_PROMPTS["TEST_CASE_1"][0]+" Failed"+"\n"+"User got:"+a.output()+"\n"+"Expected is:"+VM_PROMPTS["TEST_CASE_1"][1])
	
	
for each in clients:
	each.log_off_json()
utils.kill_clients(clients)

#!/usr/bin/python
from termcolor import colored
import utils,config,time
from responses import MA_PROMPTS
from DB_HANDLE import MA_USERS
from os import system
from subprocess import call

clients = utils.make_clients(names= MA_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
for each in clients:
	each.log_on_tool()
clear = 10 * '\n'


print colored(MA_PROMPTS["TEST_CASE_1"][0],'red')
print clear
a.say('menubutton',['manager assistance'])
time.sleep(20)
b.say('menubutton',['copythat'])
time.sleep(10)
if (MA_PROMPTS["TEST_CASE_1"][1] in b.output()) and (MA_PROMPTS["TEST_CASE_1"][2] in a.output()) and (MA_PROMPTS["TEST_CASE_1"][2] in c.output()) and (MA_PROMPTS["TEST_CASE_1"][2] in d.output()) and (MA_PROMPTS["TEST_CASE_1"][2] in e.output()):
	utils.write_to_file(MA_PROMPTS["TEST_CASE_1"][0]+" Passed")
else:
	utils.write_to_file(MA_PROMPTS["TEST_CASE_1"][0]+" Failed"+"\n actual response is:" + str([x.output() for x in a,b,c,d,e]) +"\nExpected output is:"+str(MA_PROMPTS["TEST_CASE_1"])) 
	
##########################manager assistance mode I with Members only logged in################################
print colored(MA_PROMPTS["TEST_CASE_2"][0],'red')
print clear
system("./update_db.sh 'true' 'ENABLE_LOCATION_BASED_MANAGER_ASSISTANCE'")
system("./restart_tgs.sh")
a.say('menubutton',['manager assistance'])
time.sleep(10)
system("./nping.sh")
time.sleep(5)
a.say('menubutton',['manager assistance'])
time.sleep(30)
b.say('menubutton',['copythat'])
time.sleep(5)
if (MA_PROMPTS["TEST_CASE_2"][1] in b.output()):
	b.say('menubutton',['north bay'])
	time.sleep(12)
	if (MA_PROMPTS["TEST_CASE_2"][2] in b.output()) and (MA_PROMPTS["TEST_CASE_2"][3] in a.output()) and (MA_PROMPTS["TEST_CASE_2"][3] in c.output()) and (MA_PROMPTS["TEST_CASE_2"][3] in d.output()) and (MA_PROMPTS["TEST_CASE_2"][3] in e.output()):
		utils.write_to_file(MA_PROMPTS["TEST_CASE_2"][0]+" Passed")
	else:
		utils.write_to_file(MA_PROMPTS["TEST_CASE_2"][0]+" Failed"+"\n actual response is:" + str([x.output() for x in a,b,c,d,e]) +"\nExpected output is:"+str(MA_PROMPTS["TEST_CASE_2"]))  
else:
	utils.write_to_file(MA_PROMPTS["TEST_CASE_2"][0]+" Failed"+"\n actual response is :"+ str(b.output())+ "\n Expected response:"+MA_PROMPTS["TEST_CASE_2"][1])

b.say('menubutton',['copythat'])
time.sleep(12)
if (MA_PROMPTS["TEST_CASE_2"][5] in b.output()) and (MA_PROMPTS["TEST_CASE_2"][6] in a.output()) and (MA_PROMPTS["TEST_CASE_2"][6] in c.output()) and (MA_PROMPTS["TEST_CASE_2"][6] in d.output()) and (MA_PROMPTS["TEST_CASE_2"][6] in e.output()):
	utils.write_to_file(MA_PROMPTS["TEST_CASE_2"][4]+" Passed")
else:
	utils.write_to_file("{0} Failed /\n Actual response:{1}/\n Expected Response:{2}".format(MA_PROMPTS["TEST_CASE_2"][4],[str (x.output()) for x in a,b,c,d,e],[MA_PROMPTS["TEST_CASE_2"][x] for x in 5,6]))

##########################manager assistance mode II################################

for each in clients:
	each.log_off_json()
	time.sleep(1)

utils.kill_clients(clients)

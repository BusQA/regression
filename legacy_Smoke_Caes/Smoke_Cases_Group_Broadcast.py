#!/usr/bin/python
from termcolor import colored
import utils,config,time
global clients
from DB_HANDLE import Group_Users,Group_NAME
clients = utils.make_clients(names=Group_Users)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
time.sleep(5)
for each in clients:
	each.log_on_tool()
	time.sleep(2)

clear = 10 * '\n'

##########################Test Case 27 Group Broadcast################################
print colored('Smoke Test Case 27 Group Broadcast','red')
print clear
a.say('menubutton',['broadcast',Group_NAME])
time.sleep(2)
a.say('menubutton',['broadcast','1to10'])
time.sleep(5)
if '4' in a.output():
	file = open('Smoke.txt','a')
	file.write("Test Case 27 Group Broadcast Passed")
	file.write("\n")
	file.close()
else:
	file = open('Smoke.txt','a')
	file.write("Test Case 27 Group Broadcast Failed")
	file.write("\n")
	file.close()

##########################Test Case 28 Group Broadcast with engaged and unreachable users################################
print colored('Smoke Test Test Case 28 Group Broadcast with engaged and unreachable users','red')
print clear
b.tap_out()
e.die()
time.sleep(60)
a.say('menubutton',['broadcast',Group_NAME])
time.sleep(2)
a.say('menubutton',['broadcast','1to10'])
time.sleep(10)
if '2' in a.output():
	file = open('Smoke.txt','a')
	file.write("Test Case 28 Group Broadcast with engaged and unreachable users Passed")
	file.write("\n")
	file.close()
else:
	file = open('Smoke.txt','a')
	file.write("Test Case 28 Group Broadcast with engaged and unreachable users Failed")
	file.write("\n")
	file.close()
for each in clients:
	each.log_off_json()


utils.kill_clients(clients)

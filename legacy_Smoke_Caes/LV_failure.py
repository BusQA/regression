#!/usr/bin/python
#Date:11th Feb 2016
#Author:Bipin
#Objective:To issue comands continously with contnous update in grammar
from termcolor import colored
import utils,config,time,threading,thread,time

def One_2_One(caller_party,called_party):
	while True:
		caller_party.say('menubutton',['Hello',called_party.name])
		time.sleep(3)
		caller_party.tap_out()
		time.sleep(2)

def message(caller_party,called_party):
	while True:
		caller_party.say('menubutton',['message',called_party.name])
		time.sleep(3)
		caller_party.tap_out()
		time.sleep(2)

def Interrupt(caller_party,called_party):
	while True:
		caller_party.say('menubutton',['interrupt',called_party.name])
		time.sleep(3)
		caller_party.tap_out()
		time.sleep(2)

def log_on_off(username):
	print "Inside module"
	while True:
		#print "Inside loop"
		username.log_off_json()
		time.sleep(2)
		username.log_on_json()
		time.sleep(2)
	
def stability():
	global clients
	clients = utils.make_clients(count =10)
	utils.start_clients(clients)
	a,b,c,d,e,f,g,h,i,j=clients
	for each in clients:
		each.log_on_json()
	clear = 10 * '\n'
	stab1_task = threading.Thread(target=log_on_off, args=(b,))
	stab1_task.start()
	stab2_task = threading.Thread(target=One_2_One, args=(a,b))
	stab2_task.start()
	stab3_task = threading.Thread(target=message, args=(c,d))
	stab3_task.start()
	stab4_task = threading.Thread(target=Interrupt, args=(e,d))
	stab4_task.start()
	stab5_task = threading.Thread(target=One_2_One, args=(d,b))
	stab5_task.start()
	stab6_task = threading.Thread(target=message, args=(f,d))
	stab6_task.start()
	stab7_task = threading.Thread(target=Interrupt, args=(g,d))
	stab7_task.start()
	stab8_task = threading.Thread(target=One_2_One, args=(h,b))
	stab8_task.start()
	stab9_task = threading.Thread(target=message, args=(i,d))
	stab9_task.start()
	stab10_task = threading.Thread(target=Interrupt, args=(j,d))
	stab10_task.start()
	
if __name__ == '__main__':
    stability()
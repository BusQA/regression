from responses import FB_PROMPTS
from utils import wait
from setup import single_client
import pytest

@pytest.mark.parametrize("command,response", [
    ("","announcement_now"),
    ("announcementhour","announcement_hour"),
    ("post it","announcement_today"),
    ("record huddle","morning_huddle"),

])
def test_command_posting(single_client):
    a= single_client[0]
    a.log_on_tool()
    wait(2)
    a.say('menubutton', ['feedback'])
    wait(5)
    if FB_PROMPTS["TEST_CASE_1"][1] in a.output():
        a.say('menubutton', ['1to10'])
        wait(5)
        assert FB_PROMPTS["TEST_CASE_1"][2] in a.output()
    else:
        assert False, "Failed as user command not recognized. User got: "+str(a.output())

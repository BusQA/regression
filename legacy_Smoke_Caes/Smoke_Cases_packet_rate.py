#!/usr/bin/python
from termcolor import colored
import utils,config,time
from os import system
from responses import PT_RATE
global clients
clients = utils.make_clients(count=1)
utils.start_clients(clients)
a=clients[0]
clear = 10 * '\n'

utils.write_to_file("########################"+PT_RATE["TEST_CASE_1"][0]+"################################")
print colored('Verification of Packet Rate','red')
print clear
a.say('menubutton',['Hello'])
time.sleep(3)
if PT_RATE["TEST_CASE_1"][1] in a.get_last_response()[-1]:
	utils.write_to_file("Packet rate is 20 ms Passed")
else:
	utils.write_to_file("Packet rate is 20 ms Failed"+"\n"+"Actual output is:"+PT_RATE["TEST_CASE_1"][1]+"\nExpect was:"+a.get_last_response()[-1])


time.sleep(5)

for each in clients:
	each.log_off_json()

utils.kill_clients(clients)

#!/usr/bin/python
from termcolor import colored
import utils,config,time
global clients
from DB_HANDLE import Group_Users,Group_NAME
clients = utils.make_clients(names=Group_Users)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
[x.log_on_tool() for x in clients]
'''
##########################group call to available user################################3
print colored(' group call to available user','red')
print clear
for each in clients:
	each.log_on_tool()
	
a.say('menubutton',['Hello',Group_NAME])
time.sleep(5)
a.say('menubutton',['atoz'])
time.sleep(5)
a.tap_out()
time.sleep(5)


##########################group message to available user################################3
print colored(' group message to available user','red')
[x.tap_out() for x in d,e]
b.log_off_json()
a.say('menubutton',['message',Group_NAME])
time.sleep(5)
a.say('menubutton',['atoz'])
time.sleep(5)
b.log_on_tool()
time.sleep(30)
[x.tap_out() for x in d,e]
time.sleep(60)



##########################group broadcast to available user################################3
print colored(' group broadcast to available user','red')
print clear
a.say('menubutton',['broadcast',Group_NAME])
time.sleep(5)
a.say('menubutton',['atoz'])
time.sleep(5)

'''

##########################group announcement to available user################################3
print colored(' announcement to available user','red')
print clear

[x.tap_out() for x in d,e]
b.log_off_json()
a.say('menubutton',['announcementnow',Group_NAME])
time.sleep(5)
a.say('menubutton',['atoz'])
time.sleep(5)
b.log_on_tool()
[x.tap_out() for x in d,e]

time.sleep(60)

for each in clients:
	each.log_off_json()

utils.kill_clients(clients)

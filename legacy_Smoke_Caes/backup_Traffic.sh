#!/bin/bash -x
rm /tmp/upgrade_tgs.sh
#rm /tmp/upgrade_tag.sh
cp /opt/theatro/bin/TGS_AWS_Gateway/TAG_Config.py_Traffic /opt/theatro/bin/TGS_AWS_Gateway/TAG_Config.py
cp /opt/theatro/etc_Traffic/Sounds/GrammarFiles/persons_loggedon.grxml_bkp /opt/theatro/etc_Traffic/Sounds/GrammarFiles/persons_loggedon.grxml
cp /opt/theatro/etc_Traffic/Sounds/GrammarFiles/persons_all.grxml /opt/theatro/etc_Traffic/Sounds/GrammarFiles/persons_nonloggedon.grxml
sqlite3 /opt/theatro/etc_Traffic/TGS.db "delete from tgssoftstart;delete from TGS_SOFT_START_RETRIEVE_ANNC_INFO;delete from TGS_SOFT_START_RETRIEVE_MSG_INFO;"
rm -rf /opt/theatro/var/logs/TGS.log
ps -aef | grep tgsserver | awk {'print $2'} > process.txt
kill -9 $(cat process.txt)
#ps -aef | grep TAG | awk {'print $2'} > process.txt
#kill -9 $(cat process.txt)


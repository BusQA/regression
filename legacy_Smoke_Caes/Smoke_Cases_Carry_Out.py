#!/usr/bin/python
from termcolor import colored
import utils,config,time
from DB_HANDLE import CO_USERS
from os import system
from subprocess import call
clients = utils.make_clients(names= CO_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
for each in clients:
	each.log_on_tool()
clear = 10 * '\n'

##########################carry out mode I with Members only logged in################################
print colored('Smoke Test Case 29 carry out','red')
print clear
system("./update_db.sh 'false' 'ENABLE_LOCATION_BASED_CARRY_OUT'")
system("./restart_tgs.sh")
a.say('menubutton',['carry out'])
time.sleep(20)
b.say('menubutton',['copythat'])
time.sleep(5)
if ('tts_thank_you_for_responding_to' in b.output()) and ('is_responding_to' in a.output()) and ('is_responding_to' in c.output()) and ('is_responding_to' in d.output()) and ('is_responding_to' in e.output()):
	utils.write_to_file("carry out mode I with Members only logged in Passed")
else:
	utils.write_to_file("carry out mode I with Members only logged in Failed")
##########################carry out mode II################################
print colored('Smoke Test Case 29 carry out','red')
print clear

call(['./update_db.sh','true','ENABLE_LOCATION_BASED_CARRY_OUT'])
system("./restart_tgs.sh")
a.say('menubutton',['carry out'])
time.sleep(5)
system("./nping.sh")
a.say('menubutton',['carry out'])
time.sleep(10)
b.say('menubutton',['copythat'])
time.sleep(5)
if ('''tts_ok_you_are_responding_to,|carry_out,|please_say,|north bay,|or,|south bay,''' in b.output()):
	b.say('menubutton',['north bay'])
	time.sleep(12)
	if ('''tts_thank_you_for_responding_to,|carry_out,|north bay''' in b.output()) and ('''is_responding_to,|carry_out,|north bay''' in a.output()) and ('''is_responding_to,|carry_out,|north bay''' in c.output()) and ('''is_responding_to,|carry_out,|north bay''' in d.output()) and ('''is_responding_to,|carry_out,|north bay''' in e.output()):
		utils.write_to_file("carry out mode II north bay accepted Passed")
	else:
		utils.write_to_file("carry out mode II north bay accepted  Failed")
else:
	utils.write_to_file("Dual Location carry out is not activated Failed")
time.sleep(10)
b.say('menubutton',['copythat'])
time.sleep(12)
if ('''tts_thank_you_for_responding_to,|carry_out,|south bay''' in b.output()) and ('''is_responding_to,|carry_out,|south bay''' in a.output()) and ('''is_responding_to,|carry_out,|south bay''' in c.output()) and ('''is_responding_to,|carry_out,|south bay''' in d.output()) and ('''is_responding_to,|carry_out,|south bay''' in e.output()):
	utils.write_to_file("carry out mode II south bay accepted  Passed")
else:
	utils.write_to_file("carry out mode II south bay accepted Failed")
for each in clients:
	each.log_off_json()
	time.sleep(1)

utils.kill_clients(clients)

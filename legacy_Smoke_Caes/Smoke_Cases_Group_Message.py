#!/usr/bin/python
from termcolor import colored
from os import system
import utils,config,time
global clients
from DB_HANDLE import MT_USERS,Group_NAME
clients = utils.make_clients(names=MT_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
time.sleep(5)
utils.write_to_file("##########################Group Message################################")
print colored('Group Message to available users','red')
print clear
a.log_on_tool()
b.log_on_tool()
c.log_on_tool()
d.log_on_tool()
time.sleep(5)
d.tap_out()
time.sleep(2)
if 'available' in d.output():
	d.tap_out()

a.say('menubutton',['message',Group_NAME])
time.sleep(2)
a.say('menubutton',['1to10'])
time.sleep(30)
if ('posted_at' in b.output()) and ('posted_at' in c.output()):
	utils.write_to_file("group Message to available users Passed")
else:
	utils.write_to_file("group Message to available users Failed")

##########################Group Message to Engaged user################################3
print colored('Group Message to Engaged user','red')
print clear
d.tap_out()
time.sleep(2)
if 'engaged' in d.output():
	d.tap_out()

time.sleep(20)
if 'posted_at' in d.output():
	utils.write_to_file("Group Message to Engaged user Passed")
else:
	utils.write_to_file("Group Message to Engaged user Failed")
##########################16 group Message to Non Logged on user################################3
print colored('Group Message to Non Logged on user','red')
print clear
e.log_on_tool()
time.sleep(30)
if 'posted_at' in e.output():
	utils.write_to_file("Group Message to Non Logged on user Passed")
else:
	utils.write_to_file("Group Message to Non Logged on user Failed")
time.sleep(5)

#########################Group Message reboot Survival################################
print colored('Group Message reboot Survival','red')
print clear
b.tap_out()
a.say('menubutton',['message',Group_NAME])
time.sleep(2)
a.say('menubutton',['1to10'])
system("./restart_tgs.sh ")
b.tap_out()
time.sleep(20)
if 'posted_at' in b.output():
	utils.write_to_file("Group Message reboot Survival Passed")
else:
	utils.write_to_file("Group Message reboot Survival Failed")


for each in clients:
	each.log_off_json()

time.sleep(5)

utils.kill_clients(clients)

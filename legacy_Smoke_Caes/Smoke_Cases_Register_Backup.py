#!/usr/bin/python
from termcolor import colored
from utils import *
from DB_HANDLE import NON_RB_USER,get_feature_prompts
from os import system
from subprocess import call

#system("./update_db.sh 1 REG_BACKUP_MODE")
#system("./restart_tgs.sh")
global clients
clients = make_clients(names=NON_RB_USER)
start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'

for each in clients:
	each.log_on_tool()
clear = 10 * '\n'
Test_Params = {"__ACCEPTOR__":b.name,"__FROM__":a.name,",__ZONE__":""}

RB_PROMPTS =get_feature_prompts("register backup",Test_Params)
##########################Register backup mode I with RB USERS not logged on################################
print colored('Register backup mode I with RB USERS not logged on','red')
print clear
a.say('menubutton',['code3'])
wait(7)
try:
	
	print a.output()
	print RB_PROMPTS["command_response"]
	if a.output() == RB_PROMPTS["command_response"]:
		wait(10)
		if a.output() == RB_PROMPTS["sound_out_all"]:
			wait(10)
			b.say('menubutton',['copythat'])
			wait(5)

			E_Output_Acceptor = RB_PROMPTS["acceptances_to_requestor"]
			E_Output_Listener = RB_PROMPTS["acceptance_to_listeners"]

			try:
				if (E_Output_Acceptor == b.output() and E_Output_Listener == a.output()):
					write_to_file("Register backup mode I with RB USERS not logged on Passed")
				else:
					write_to_file("Register backup mode I with RB USERS not logged on Failed As Acceptor got:{0}\nListener got:{1}\nBut Expected Response:{2}\n{3}".format(a.output(),b.output(),E_Output_Acceptor,E_Output_Listener))

			except e as AttributeError:
				print e
		else:
			write_to_file("Register backup mode I with RB USERS not logged on Failed As Acceptor got:{0}/\n ANd Expected :{1}".format(a.output(),RB_PROMPTS["sound_out_all"]))
	else:
		write_to_file("Register backup mode I with RB USERS not logged on Failed As Acceptor got:{0}\n ANd Expected :{1}".format(a.output(),RB_PROMPTS["command_response"]))

except AttributeError, e:
	print e

for each in clients:
	each.log_off_json()
wait(5)

kill_clients(clients)

#!/usr/bin/python
from termcolor import colored
import utils,config,time
from responses import GUN_PROMPTS
from DB_HANDLE import GUN_USERS
from os import system
from subprocess import call

system("./update_db.sh '3' 'REG_BACKUP_MODE'")
system("./restart_tgs.sh")

clients = utils.make_clients(names= GUN_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
for each in clients:
	each.log_on_tool()
clear = 10 * '\n'


time.sleep(10)
print colored(GUN_PROMPTS["TEST_CASE_1"][0],'red')
print clear
a.say('menubutton',['gun runner'])
time.sleep(10)
system("./nping.sh")
time.sleep(5)
a.say('menubutton',['gun runner'])
time.sleep(30)
b.say('menubutton',['copythat'])
time.sleep(5)
if (GUN_PROMPTS["TEST_CASE_1"][1] in b.output()):
	b.say('menubutton',['north bay'])
	time.sleep(12)
	if (GUN_PROMPTS["TEST_CASE_1"][2] in b.output()) and (GUN_PROMPTS["TEST_CASE_1"][3] in a.output()) and (GUN_PROMPTS["TEST_CASE_1"][3] in c.output()) and (GUN_PROMPTS["TEST_CASE_1"][3] in d.output()) and (GUN_PROMPTS["TEST_CASE_1"][3] in e.output()):
		utils.write_to_file(GUN_PROMPTS["TEST_CASE_1"][0]+" Passed")
	else:
		utils.write_to_file(GUN_PROMPTS["TEST_CASE_1"][0]+" Failed"+"\n actual response is:" + str([x.output() for x in a,b,c,d,e]) +"\nExpected output is:"+str(GUN_PROMPTS["TEST_CASE_1"]))  
else:
	utils.write_to_file(GUN_PROMPTS["TEST_CASE_1"][0]+" Failed"+"\n actual response is :"+ str(b.output())+ "\n Expected response:"+GUN_PROMPTS["TEST_CASE_1"][1])

b.say('menubutton',['copythat'])
time.sleep(12)
if (GUN_PROMPTS["TEST_CASE_1"][5] in b.output()) and (GUN_PROMPTS["TEST_CASE_1"][6] in a.output()) and (GUN_PROMPTS["TEST_CASE_1"][6] in c.output()) and (GUN_PROMPTS["TEST_CASE_1"][6] in d.output()) and (GUN_PROMPTS["TEST_CASE_1"][6] in e.output()):
	utils.write_to_file(GUN_PROMPTS["TEST_CASE_1"][4]+" Passed")
else:
	utils.write_to_file("{0} Failed /\n Actual response:{1}/\n Expected Response:{2}".format(GUN_PROMPTS["TEST_CASE_1"][4],[str (x.output()) for x in a,b,c,d,e],[GUN_PROMPTS["TEST_CASE_1"][x] for x in 5,6]))

##########################manager assistance mode II################################

for each in clients:
	each.log_off_json()
	time.sleep(1)
utils.kill_clients(clients)
system("./update_db.sh '1' 'REG_BACKUP_MODE'")
system("./restart_tgs.sh")


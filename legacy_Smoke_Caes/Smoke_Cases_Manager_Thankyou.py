#!/usr/bin/python
from termcolor import colored
import utils,config,time
from responses import MT_PROMPTS
from DB_HANDLE import MT_USERS
from os import system
from subprocess import call

clients = utils.make_clients(names= MT_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
for each in clients:
	each.log_on_tool()
clear = 10 * '\n'


print colored(MT_PROMPTS["TEST_CASE_1"][0],'red')
print clear

a.say('menubutton',['manager thankyou'])
time.sleep(10)
system("./nping.sh")
time.sleep(5)
a.say('menubutton',['manager thankyou'])
time.sleep(30)
b.say('menubutton',['copythat'])
time.sleep(5)
if (MT_PROMPTS["TEST_CASE_1"][1] in b.output()):
	b.say('menubutton',['north bay'])
	time.sleep(12)
	if (MT_PROMPTS["TEST_CASE_1"][2] in b.output()) and (MT_PROMPTS["TEST_CASE_1"][3] in a.output()) and (MT_PROMPTS["TEST_CASE_1"][3] in c.output()) and (MT_PROMPTS["TEST_CASE_1"][3] in d.output()) and (MT_PROMPTS["TEST_CASE_1"][3] in e.output()):
		utils.write_to_file(MT_PROMPTS["TEST_CASE_1"][0]+" Passed")
	else:
		utils.write_to_file("{0} Failed /\n Actual response:{1}/\n Expected Response:{2}".format(MT_PROMPTS["TEST_CASE_1"][0],[str (x.output()) for x in a,b,c,d,e],[MT_PROMPTS["TEST_CASE_1"][x] for x in 2,3]))
else:
	utils.write_to_file("{0} Failed /\n Actual response:{1}/\n Expected Response:{2}".format(MT_PROMPTS["TEST_CASE_1"][0],[str (x.output()) for x in a,b,c,d,e],[MT_PROMPTS["TEST_CASE_1"][x] for x in 1]))

b.say('menubutton',['copythat'])
time.sleep(12)
if (MT_PROMPTS["TEST_CASE_1"][5] in b.output()) and (MT_PROMPTS["TEST_CASE_1"][6] in a.output()) and (MT_PROMPTS["TEST_CASE_1"][6] in c.output()) and (MT_PROMPTS["TEST_CASE_1"][6] in d.output()) and (MT_PROMPTS["TEST_CASE_1"][6] in e.output()):
	utils.write_to_file(MT_PROMPTS["TEST_CASE_1"][4]+" Passed")
else:
	utils.write_to_file("{0} Failed /\n Actual response:{1}/\n Expected Response:{2}".format(MT_PROMPTS["TEST_CASE_1"][4],[str (x.output()) for x in a,b,c,d,e],[MT_PROMPTS["TEST_CASE_2"][x] for x in 5,6]))

##########################manager assistance mode II################################

for each in clients:
	each.log_off_json()
	time.sleep(1)

utils.kill_clients(clients)

#!/usr/bin/python
#Date:11th Feb 2016
#Author:Bipin
#Objective:Run Traffic Continously
from termcolor import colored
import utils,config,time,threading,thread,time
def message(caller_party,called_party):
	while True:
		caller_party.say('menubutton',['message',called_party.name])
		time.sleep(7)
		caller_party.say('menubutton',['1to10'])
		time.sleep(30)
		if 'has_heard_your_message' in caller_party.output():
			utils.write_to_file("Message Passed","Traffic_Message.txt")
		else:
			utils.write_to_file("Message Failed","Traffic_Message.txt")

def One_2_One(caller_party,called_party):
	while True:
		caller_party.say('menubutton',['Hello',called_party.name])
		time.sleep(5)
		caller_party.say('menubutton',['1to10'])
		caller_party.tap_out()
		time.sleep(10)
		if 'ended_call' in called_party.output():
			utils.write_to_file("One to one Passed","Traffic_One_2_One.txt")
		else:
			utils.write_to_file("One to one Failed","Traffic_One_2_One.txt")

def announcement(initiator):
	while True:
		initiator.say('menubutton',['announcementnow'])
		time.sleep(2)
		initiator.say('menubutton',['1to10'])
		time.sleep(10)
		initiator.say('menubutton',['announcementhour'])
		time.sleep(2)
		initiator.say('menubutton',['1to10'])
		time.sleep(10)
		initiator.say('menubutton',['announcementtoday'])
		time.sleep(2)
		initiator.say('menubutton',['1to10'])
		time.sleep(10)
def group_cases(initiator):
	while True:
		initiator.say('menubutton',['message','hunting'])
		time.sleep(5)
		initiator.say('menubutton',['1to10'])
		time.sleep(30)
		if ('tts_posted' in initiator.output()):
			utils.write_to_file("Group message Passed","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group message Failed","Traffic_Group_Cases.txt")
		initiator.say('menubutton',['announcementnow','hunting'])
		time.sleep(2)
		initiator.say('menubutton',['1to10'])
		time.sleep(20)
		if ('tts_posted' in initiator.output()):
			utils.write_to_file("Group announcement Passed","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group announcement Failed","Traffic_Group_Cases.txt")
		initiator.say('menubutton',['broadcast','managers'])
		time.sleep(2)
		initiator.say('menubutton',['broadcast','1to10'])
		time.sleep(5)
		if 'tts_4' in initiator.output():
			utils.write_to_file("Group broadcast Passed","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group broadcast Failed","Traffic_Group_Cases.txt")
		initiator.say('menubutton',['contact','GEAR'])
		time.sleep(5)
		initiator.say('menubutton',['1to10'])
		initiator.tap_out()
		time.sleep(10)
		if ('tts_group_call_ended' in initiator.output()):
			utils.write_to_file("Group Call Passed","Traffic_Group_Cases.txt")
		else:
			utils.write_to_file("Group Call Failed","Traffic_Group_Cases.txt")

def stability():
	global clients
	clients = utils.make_clients()
	utils.start_clients(clients)
	b,c,d,e,p=clients[:5]
	for each in clients:
		each.log_on_tool()
	clear = 10 * '\n'
	stab1_task = threading.Thread(target=message, args=(b,c))
	stab1_task.start()
	stab2_task = threading.Thread(target=One_2_One, args=(d,e))
	stab2_task.start()
	#stab3_task = threading.Thread(target=announcement, args=(a,))
	#stab3_task.start()
	stab4_task = threading.Thread(target=group_cases, args=(p,))
	stab4_task.start()

if __name__ == '__main__':
    stability()

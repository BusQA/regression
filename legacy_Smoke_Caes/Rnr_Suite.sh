#!/bin/bash -x
./update_db.sh 1 REG_BACKUP_MODE
./update_db.sh false ENABLE_LOCATION_BASED_MANAGER_ASSISTANCE
./update_db.sh false ENABLE_LOCATION_BASED_CARRY_OUT
./restart_tgs.sh
python2.7 Smoke_Cases_RnR_RBI.py RB
python2.7 Smoke_Cases_RnR_RBI.py MA
python2.7 Smoke_Cases_RnR_RBI.py CO

./update_db.sh 2 REG_BACKUP_MODE
./update_db.sh true ENABLE_LOCATION_BASED_MANAGER_ASSISTANCE
./update_db.sh true ENABLE_LOCATION_BASED_CARRY_OUT
./restart_tgs.sh
python2.7 Smoke_Cases_RnR_RBII.py RB
python2.7 Smoke_Cases_RnR_RBII.py MA
python2.7 Smoke_Cases_RnR_RBII.py CO
python2.7 Smoke_Cases_RnR_RBII.py MT
python2.7 Smoke_Cases_RnR_playaudio.py SB
python2.7 Smoke_Cases_RnR_playaudio.py CP

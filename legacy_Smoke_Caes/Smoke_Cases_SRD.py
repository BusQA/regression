#!/usr/bin/python
from termcolor import colored
import utils,config,time
from os import system
global clients
from DB_HANDLE import SR_USERS
clients = utils.make_clients(names=SR_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
for each in clients:
	each.log_on_json()

time.sleep(20)
clear = 10 * '\n'

##########################Verify SRD R and R request is working################################
print colored('SRD R and R request is working','red')
print clear
b.say('menubutton',['copythat'])
time.sleep(8)
if ('''tts_thank_you_for_completing_the_store_readiness_check''' in b.output()) and ('''tts_ben sander,|has_completed_the_store_readiness_check''' in a.output()) and ('''tts_ben sander,|has_completed_the_store_readiness_check''' in c.output()) and ('''tts_ben sander,|has_completed_the_store_readiness_check''' in d.output()) and ('''tts_ben sander,|has_completed_the_store_readiness_check''' in e.output()):
	utils.write_to_file("SRD R and R request is working Passed")
else:
	utils.write_to_file("SRD R and R request is working Failed")

system("./restart_tgs.sh")


##########################Verify SRD R and R request for already accepted before restart################################
print colored('SRD R and R request for already accepted before restart','red')
print clear
b.say('menubutton',['copythat'])
time.sleep(5)
if ('''tts_there_is_no_active_request''' in b.output()):
	utils.write_to_file("SRD R and R request for already accepted before restart Passed")
else:
	utils.write_to_file("SRD R and R request for already accepted before restart Failed")




for each in clients:
	each.log_off_json()
time.sleep(5)

utils.kill_clients(clients)

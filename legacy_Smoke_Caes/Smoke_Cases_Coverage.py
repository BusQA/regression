#!/usr/bin/python
from termcolor import colored
from subprocess import call
from responses import COVERAGE_PROMPTS
import utils,config,time
global clients
from DB_HANDLE import MOD_USERS
clients = utils.make_clients()
utils.start_clients(clients)
a,b,c,d,e=clients

clear = 10 * '\n'

utils.write_to_file("##########################"+COVERAGE_PROMPTS["TEST_CASE_1"][0]+"################################")
print colored(COVERAGE_PROMPTS["TEST_CASE_1"][0],'red')
print clear
for each in  clients:
	each.log_on_tool()


time.sleep(5)
print COVERAGE_PROMPTS["Location"]
[call(['./nping1.sh',str(x.mac_address),str(L)]) for x,L in zip(clients,COVERAGE_PROMPTS["Location"])]

a.say('menubutton',['store coverage'])
time.sleep(7)


if COVERAGE_PROMPTS["TEST_CASE_1"][1] == a.output():
	utils.write_to_file(COVERAGE_PROMPTS["TEST_CASE_1"][0]+" Passed")
else:
	utils.write_to_file(COVERAGE_PROMPTS["TEST_CASE_1"][0]+" Failed"+"\n"+"User got:"+a.output()+"\n"+"Expected is:"+COVERAGE_PROMPTS["TEST_CASE_1"][1])
	
time.sleep(60)
[call(['./nping1.sh',str(x.mac_address),str(L)]) for x,L in zip(clients,COVERAGE_PROMPTS["Dual_Location"])]


a.say('menubutton',['store coverage'])
time.sleep(7)


if COVERAGE_PROMPTS["TEST_CASE_2"][1] == a.output():
	utils.write_to_file(COVERAGE_PROMPTS["TEST_CASE_2"][0]+" Passed")
else:
	utils.write_to_file(COVERAGE_PROMPTS["TEST_CASE_2"][0]+" Failed"+"\n"+"User got:"+a.output()+"\n"+"Expected is:"+COVERAGE_PROMPTS["TEST_CASE_2"][1])


	


for each in clients:
	each.log_off_json()
utils.kill_clients(clients)

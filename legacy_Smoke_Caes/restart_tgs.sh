#!/bin/bash -x
ps -aef | grep tgsserver | awk {'print $2'} > /tmp/process.txt
kill -9 $(cat /tmp/process.txt)
sleep 30

#!/usr/bin/python
from termcolor import colored
import utils,config,time
global clients
from DB_HANDLE import MT_USERS,Group_NAME
clients = utils.make_clients(names=MT_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
utils.write_to_file("##########################Group Call################################")
print colored(' group call to no one logged on','red')
print clear
time.sleep(10)
a.log_on_tool()
time.sleep(5)
a.say('menubutton',['Hello',Group_NAME])
time.sleep(5)

if 'no_one_available_in' in a.output():
	utils.write_to_file("group call to no one logged on Passed")
else:
	utils.write_to_file("group call to no one logged on Failed")


##########################group call to available user################################3
print colored(' group call to available user','red')
print clear
for each in clients:
	each.log_on_tool()
	time.sleep(2)

a.Hello(Group_NAME)
time.sleep(5)
a.tap_out()
time.sleep(5)

if ('group_call_ended' in a.output()) and ('group_call_ended' in b.output()):
	utils.write_to_file("group call to available user Passed")
else:
	utils.write_to_file("group call to available user Failed")
##########################group call to some engaged user################################3
import time
print colored(' group call to some engaged user','red')
print clear

time.sleep(5)
b.tap_out()
time.sleep(2)
a.Hello(Group_NAME)
time.sleep(5)
a.tap_out()
time.sleep(5)

if ('group_call_ended' in a.output()) and ('engaged' in b.output()):
	utils.write_to_file("group call to some engaged user Passed")
else:
	utils.write_to_file("group call to some engaged user Failed")
##########################group call to some unrachable user################################3
print colored(' group call to some Unreachable user','red')
print clear
b.die()
time.sleep(15)
a.Hello(Group_NAME)
time.sleep(5)
a.tap_out()
time.sleep(5)

if ('group_call_ended' in a.output()) and ('engaged' in b.output()):
	utils.write_to_file("group call to some Unreachable user Passed")
else:
	utils.write_to_file("group call to some Unreachable user Failed")
time.sleep(15)

for each in clients:
	each.log_off_json()

utils.kill_clients(clients)

#!/usr/bin/python
from termcolor import colored
import utils,config,time
global clients
from DB_HANDLE import GEAR_USERS,GEAR_NAME
clients = utils.make_clients(names=GEAR_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients

clear = 10 * '\n'

utils.write_to_file("##########################GEAR Feature################################")
print colored('Hello gear when no one is logged on and issuer taps out','red')
print clear
a.log_on_tool()
time.sleep(5)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)

if 'would_like_to_leave_a_message' in a.output():
	a.tap_out()
	time.sleep(5)
	if 'tts_message_cancelled_please_say_contact' in a.output():
		utils.write_to_file("Hello gear when no one is logged on and issuer taps out Passed")
	
else:
	utils.write_to_file("Hello gear when no one is logged on and issuer taps out Failed")
	

##########################Hello Gear with all available user################################3
print colored('Hello Gear with all available user','red')
print clear
for each in clients:
	each.log_on_tool()
	time.sleep(3)

time.sleep(5)

a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
a.tap_out()
time.sleep(5)
if ('tts_group_call_ended' in a.output()) and ('ended_group_chat' in b.output()) and ('ended_group_chat' in c.output()) and ('ended_group_chat' in d.output()) and ('ended_group_chat' in e.output()):
	utils.write_to_file("Hello Gear with all available user Passed")
else:
	utils.write_to_file("Hello Gear with all available user Failed")

##########################Tap out one user at a time while in a gear group################################3
print colored('Tap out one user at a time while in a gear group','red')
print clear
time.sleep(10)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
if 'tts_Sorry_please_say_again' in a.output():
	a.say('menubutton',['contact',GEAR_NAME])
	time.sleep(5)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
b.tap_out()
c.tap_out()
d.tap_out()
e.tap_out()
time.sleep(3)
if ('tts_you_are_leaving_the_group_chat' in b.output()) and ('tts_you_are_leaving_the_group_chat' in c.output()) and ('tts_you_are_leaving_the_group_chat' in d.output()) and ('tts_you_are_leaving_the_group_chat' in e.output()) and ('ended_group_chat' in a.output()):
	utils.write_to_file("Tap out one user at a time while in a gear group Passed")
else:
	utils.write_to_file("Tap out one user at a time while in a gear group Failed")


##########################Issue Hello Gear when all member in gear group are in engaged state################################3
import time
print colored('Issue Hello Gear when all member in gear group are in engaged state','red')
print clear

for each in clients:
	each.tap_out()

a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
if ('is_engaged_with_another_outfitter' in a.output()):
	utils.write_to_file("Issue Hello Gear when all member in gear group are in engaged state Passed")
else:
	utils.write_to_file("Issue Hello Gear when all member in gear group are in engaged state Failed")
##########################Issue Hello Gear when all member in gear group are in conversation################################3
print colored('Issue Hello Gear when all member in gear group are in conversation','red')
print clear

a.say('menubutton',['yes'])
time.sleep(3)
a.say('menubutton',['atoz'])
for each in clients:
	each.tap_out()
time.sleep(3)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
if ('is_in_a_conversation' in a.output()) :
	utils.write_to_file("Issue Hello Gear when all member in gear group are in conversation Passed")
else:
	utils.write_to_file("Issue Hello Gear when all member in gear group are in conversation Failed")

time.sleep(15)

for each in clients:
	each.log_off_json()

utils.kill_clients(clients)

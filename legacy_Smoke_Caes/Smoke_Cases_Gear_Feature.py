#!/usr/bin/python
from termcolor import colored
from responses import GEAR_PROMPTS
import utils,config,time
global clients
from DB_HANDLE import GEAR_USERS
clients = utils.make_clients(names=GEAR_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients

clear = 10 * '\n'

utils.write_to_file("##########################GEAR Feature################################")
print colored(GEAR_PROMPTS["TEST_CASE_1"][0],'red')
print clear
a.log_on_tool()
time.sleep(5)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)

if GEAR_PROMPTS["TEST_CASE_1"][1] in a.output():
	a.tap_out()
	time.sleep(5)
	
	if GEAR_PROMPTS["TEST_CASE_2"] in a.output():
		utils.write_to_file( GEAR_PROMPTS["TEST_CASE_1"][0]+" Passed")
	else:
		utils.write_to_file(GEAR_PROMPTS["TEST_CASE_1"][0] + " Failed" + "Expected is:"+GEAR_PROMPTS["TEST_CASE_2"] +"and actaul is:" +str(a.output()))
	
else:
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_1"][0] + "Failed "+"Expected is:"+GEAR_PROMPTS["TEST_CASE_1"][1] +"and actaul is:" +str(a.output()))


##########################Hello Gear with all available user################################3
print colored('Hello Gear with all available user','red')
print clear
for each in [b,c,d,e]:
	each.log_on_tool()

time.sleep(5)

a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
a.tap_out()
time.sleep(5)
if (GEAR_PROMPTS["TEST_CASE_3"][1] in a.output()) and (GEAR_PROMPTS["TEST_CASE_3"][2] in b.output()) and (GEAR_PROMPTS["TEST_CASE_3"][2] in c.output()) and (GEAR_PROMPTS["TEST_CASE_3"][2] in d.output()) and (GEAR_PROMPTS["TEST_CASE_3"][2] in e.output()):
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_3"][0]+" Passed")
else:
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_3"][0]+" Failed"+"\n"+"actual response is:" + str([x.output() for x in a,b,c,d,e]) +"\n"+ "Expected output is:"+str(GEAR_PROMPTS["TEST_CASE_3"]))

##########################Tap out one user at a time while in a gear group################################3
print colored(GEAR_PROMPTS["TEST_CASE_4"][0],'red')
print clear
time.sleep(10) 
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
b.tap_out()
c.tap_out()
d.tap_out()
e.tap_out()
time.sleep(3)
if (GEAR_PROMPTS["TEST_CASE_4"][2] in b.output()) and (GEAR_PROMPTS["TEST_CASE_4"][2]  in c.output()) and (GEAR_PROMPTS["TEST_CASE_4"][2]  in d.output()) and (GEAR_PROMPTS["TEST_CASE_4"][2]  in e.output()) and (GEAR_PROMPTS["TEST_CASE_4"][1]  in a.output()):
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_4"][0]+" Passed")
else:
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_4"][0] +" Failed"+"\n"+"actual response is:" + str([x.output() for x in a,b,c,d,e]) +"\n"+ "Expected output is:"+str(GEAR_PROMPTS["TEST_CASE_4"]))


##########################Issue Hello Gear when all member in gear group are in engaged state################################3
import time
print colored(GEAR_PROMPTS["TEST_CASE_5"][0],'red')
print clear

for each in clients:
	each.tap_out()

a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
if (GEAR_PROMPTS["TEST_CASE_5"][1] in a.output()):
	utils.write_to_file( GEAR_PROMPTS["TEST_CASE_5"][0]+" Passed")
else:
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_5"][0] + " Failed" + "\n"+"Expected is:"+GEAR_PROMPTS["TEST_CASE_5"][1] + "\n"+"and actaul is:" +str(a.output()))
	##########################Issue Hello Gear when all member in gear group are in conversation################################3
print colored(GEAR_PROMPTS["TEST_CASE_6"][0],'red')
print clear

a.say('menubutton',['yes'])
time.sleep(3)
a.say('menubutton',['atoz'])
for each in clients:
	each.tap_out()
time.sleep(3)
a.say('menubutton',['contact',GEAR_NAME])
time.sleep(5)
if (GEAR_PROMPTS["TEST_CASE_6"][1] in a.output()) :
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_6"][0] +" Passed")
else:
	utils.write_to_file(GEAR_PROMPTS["TEST_CASE_6"][0] + "Failed "+"Expected is:"+GEAR_PROMPTS["TEST_CASE_6"][1] +"and actaul is:" +str(a.output()))
time.sleep(5)
for each in clients:
	each.log_off_json()
utils.kill_clients(clients)

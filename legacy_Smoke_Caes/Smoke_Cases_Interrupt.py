#!/usr/bin/python
from termcolor import colored
import utils,config,time
#global clients
clients = utils.make_clients()
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
utils.write_to_file("##########################Interrupt################################")

print colored(' Interrupt to no one logged on','red')
a.log_on_tool()
time.sleep(5)
a.say('menubutton',['make call',b.name])
time.sleep(5)
if 'is_not_logged_on' in a.output():
	utils.write_to_file("Interrupt to no one logged on Passed")
else:
	utils.write_to_file("Interrupt to no one logged on Failed")

##########################Interrupt to available user################################3
print colored(' Interrupt to available user','red')
print clear
for each in clients:
	each.log_on_tool()

time.sleep(10)
a.command('make call','1to10',b.name)
time.sleep(25)

if ('call_ended' in a.output()) and ('ended_call' in b.output()):
	utils.write_to_file("Interrupt to available user Passed")
else:
	utils.write_to_file("Interrupt to available user Failed")
##########################Interrupt to some engaged user################################3
import time
print colored(' Interrupt to some engaged user','red')
print clear

time.sleep(5)
c.tap_out()
time.sleep(2)
a.command('make call','1to10',c.name)
time.sleep(25)

if ('call_ended' in a.output()) and ('ended_call' in b.output()):
	utils.write_to_file("Interrupt to some engaged user Passed")
else:
	utils.write_to_file("Interrupt to some engaged user Failed")

##########################Interrupt to some unrachable user################################3

print colored(' Interrupt to some Unreachable user','red')
print clear
b.die()
time.sleep(20)
a.say('menubutton',['make call',b.name])
time.sleep(5)


if 'is_in_a_conversation' in a.output():
	utils.write_to_file("Interrupt to some unrachable user Passed")
else:
	utils.write_to_file("Interrupt to some unrachable user Failed")
time.sleep(10)

##########################Interrupt all################################3
print colored(' Interrupt all','red')
print clear

time.sleep(10)
a.say('menubutton',['disturb all'])
time.sleep(2)

if 'go_ahead' in a.output():
	a.say('menubutton',['1to10'])
	utils.write_to_file("Interrupt all Passed")
else:
	utils.write_to_file("Interrupt all Failed")
for each in clients:
	each.log_off_json()

time.sleep(10)
utils.kill_clients(clients)
time.sleep(5)

#!/usr/bin/python
from termcolor import colored
import utils,config,time
from os import system
global clients
clients = utils.make_clients()
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'

utils.write_to_file("########################Logon################################")
print colored('Logon via log_on_tool','red')
print clear
b.log_on_tool()
time.sleep(3)
if b.name in b.output():
	utils.write_to_file("Logon via log_on_tool Passed")
else:
	utils.write_to_file("Logon via log_on_tool Failed")

a.say('menubutton',['logon',a.name])
time.sleep(15)
if a.name in a.output():
	utils.write_to_file("Logon via Logon command Passed")
else:
	utils.write_to_file("Logon via Logon command Failed")
utils.write_to_file("##########################One to one Call################################")
c.log_on_tool()
d.log_on_tool()
print colored('One to one to available user','red')
print clear
time.sleep(5)
a.say('menubutton',['Hello',b.name])
time.sleep(5)
d.die()
print a.output()
if 'go_ahead' in a.output():
	a.say('menubutton',['Hello',b.name])
	time.sleep(3)
	b.say('menubutton',['Hello',a.name])
	a.tap_out()
	time.sleep(10)
	if 'ended_call' in b.output():
		utils.write_to_file("One to one to available user Passed")
	else:
		utils.write_to_file("One to one to available user Failed")
else:
	utils.write_to_file("One to one to available user Failed")

##########################One to one to engage user################################3
print colored('One to one to engaged user','red')
print clear
b.tap_out()
time.sleep(5)
a.say('menubutton',['Hello',b.name])
time.sleep(5)
if 'is_engaged_near' in a.output():
	time.sleep(10)
	utils.write_to_file("One to one to engage user Passed")
else:
	utils.write_to_file("One to one to engage user Failed")
b.tap_out()
##########################One to one to unreachable user user################################3
print colored(' One to one to unreachable user','red')
print clear
time.sleep(20)
a.say('menubutton',['Hello',d.name])
time.sleep(5)
if 'destination_is_no_longer_available' in a.output():
	utils.write_to_file("One to one to unreachable user Passed")
else:
	utils.write_to_file("One to one to unreachable user Failed"+"User got"+str(a.output()))
##########################One to one to non logged on user################################3
print colored(' One to one to non logged on user','red')
print clear
b.log_off_json()
time.sleep(10)
a.say('menubutton',['Hello',b.name])
time.sleep(10)
if 'is_not_logged_on' in a.output():
	time.sleep(10)
	utils.write_to_file("One to one to non logged on user Passed")
else:
	utils.write_to_file("One to one to non logged on user Failed")

for each in clients:
	each.log_off_json()
	time.sleep(2)


for each in clients:
	each.log_off_json()

utils.kill_clients(clients)

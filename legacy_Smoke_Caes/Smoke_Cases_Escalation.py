#!/usr/bin/python
from termcolor import colored
import utils,config,time
from DB_HANDLE import ESC_USERS
from responses import ESC_PROMPTS
from os import system
from subprocess import call
#Enabling Escalation feature
system("./update_db.sh 'true' 'ENABLE_REG_BACKUP_ESCALATION'")
system("./restart_tgs.sh")


global clients
clients = utils.make_clients(names=ESC_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
for each in clients:
	each.log_on_tool()

clear = 10 * '\n'
##########################Test CAse Execution################################

print colored(ESC_PROMPTS["TEST_CASE_1"][0],'red')
print clear
a.say('menubutton',['code3'])
time.sleep(30)
if (ESC_PROMPTS["TEST_CASE_1"][2] in b.output()) and (ESC_PROMPTS["TEST_CASE_1"][1] in a.output()) and (ESC_PROMPTS["TEST_CASE_1"][2] in c.output()):
	time.sleep(20)
	e.say('menubutton',['copythat'])
	time.sleep(7)
	if (ESC_PROMPTS["TEST_CASE_1"][4] in b.output()) and (ESC_PROMPTS["TEST_CASE_1"][4] in a.output()) and (ESC_PROMPTS["TEST_CASE_1"][4] in c.output()) and (ESC_PROMPTS["TEST_CASE_1"][4] in d.output()) and (ESC_PROMPTS["TEST_CASE_1"][3] in e.output()):
		utils.write_to_file(ESC_PROMPTS["TEST_CASE_1"][0]+" Passed")
	else:
		utils.write_to_file("{0} Failed /\n Actual response:{1}/\n Expected Response:{2}".format(ESC_PROMPTS["TEST_CASE_1"][0],[str (x.output()) for x in a,b,c,d,e],[ESC_PROMPTS["TEST_CASE_1"][x] for x in 3,4]))	
else:
	utils.write_to_file("{0} Failed /\n Actual response:{1}/\n Expected Response:{2}".format(ESC_PROMPTS["TEST_CASE_1"][0],[str (x.output()) for x in a,b,c],[ESC_PROMPTS["TEST_CASE_1"][x] for x in 1,2,3]))	



for each in clients:
	each.log_off_json()
time.sleep(5)

utils.kill_clients(clients)
#Disabling Escalation feature
system("./update_db.sh 'false' 'ENABLE_REG_BACKUP_ESCALATION'")
system("./restart_tgs.sh")

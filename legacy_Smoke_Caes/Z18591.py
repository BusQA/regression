#!/usr/bin/python
#Date:11th Feb 2016
#Author:Bipin
#Objective:zendesk issue 18591
from termcolor import colored
import utils,config,time,threading,thread,time,multiprocessing

def announcement(initiator):
	while True:
		initiator.command("announcementnow","yes")
		initiator.command("junk")

def Cont(lista):
    threadlist = []
    for x in lista:
    	threadlist.append(threading.Thread(target=announcement, args=(x,)))
    [x.start() for x in threadlist]


def stability():
	global clients
	clients = utils.make_clients(count=5)
	utils.start_clients(clients)
	[x.log_on_tool() for x in clients]
	Cont(clients)
if __name__ == '__main__':
	stability()

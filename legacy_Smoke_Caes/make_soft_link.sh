#!/bin/bash

rm -rf /opt/theatro/bin/handle_grammar.py
ln -s `pwd`/handle_grammar.py /opt/theatro/bin/handle_grammar.py 

rm -rf /opt/theatro/bin/tgs/tgsserver
ln -s `pwd`/bin/tgsserver /opt/theatro/bin/tgs/tgsserver

rm /opt/theatro/etc_Traffic/tgscompilation.sh
ln -s `pwd`/tgs_environment.sh /opt/theatro/etc_Traffic/tgscompilation.sh 

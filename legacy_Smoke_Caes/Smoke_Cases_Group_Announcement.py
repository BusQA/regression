#!/usr/bin/python
from termcolor import colored
import utils,config,time
from os import system
global clients
from DB_HANDLE import MT_USERS,Group_NAME
clients = utils.make_clients(names=MT_USERS)
utils.start_clients(clients)
a,b,c,d,e= clients
clear = 10 * '\n'
time.sleep(10)
utils.write_to_file("##########################Group Announcement################################")
print colored('Group Announcement to available users','red')
print clear
for each in clients:
	each.log_on_tool()

e.log_off_json()
time.sleep(2)
d.tap_out()
time.sleep(3)
if 'available' in d.output():
	d.tap_out()

a.say('menubutton',['announcementnow',Group_NAME])
time.sleep(2)
a.say('menubutton',['1to10'])
time.sleep(20)
if ('posted_at' in b.output()) and ('posted_at' in c.output()):
	utils.write_to_file("Group Announcement to available users Passed")
else:
	utils.write_to_file("Group Announcement to available users Failed")
##########################45 Group Announcement to Engaged users################################3
print colored('Group Announcement to Engaged user','red')
print clear

d.tap_out()
time.sleep(2)
if 'engaged' in d.output():
	d.tap_out()

time.sleep(20)
if 'posted_at' in d.output():
	utils.write_to_file("Group Announcement to Engaged user Passed")
else:
	utils.write_to_file("Group Announcement to Engaged user Failed")
##########################Group Announcement to Non Logged on user################################3
print colored('Group Announcement to Non Logged on user','red')
print clear
e.log_on_tool()
time.sleep(60)
if 'posted_at' in e.output():
	utils.write_to_file("Group Announcement to Non Logged on user Passed")
else:
	utils.write_to_file("Group Announcement to Non Logged on user Failed")
time.sleep(5)

#########################Group Announcement reboot Survival################################
print colored('Group Announcement Now reboot Survival','red')
print clear
b.tap_out()
a.say('menubutton',['announcementnow',Group_NAME])
time.sleep(2)
a.say('menubutton',['1to10'])
time.sleep(10)
system("./restart_tgs.sh ")
b.tap_out()
time.sleep(30)

if 'posted_at' in b.output():
	utils.write_to_file("Group Announcement reboot Survival Passed")
else:
	utils.write_to_file("Group Announcement reboot Survival Failed")

for each in clients:
	each.log_off_json()

time.sleep(5)

utils.kill_clients(clients)

#!/usr/bin/python
#Date:11th Feb 2016
#Author:Bipin
#Objective:Run Traffic Continously
from termcolor import colored
import utils,config,time,threading,thread,time,multiprocessing

def delay_cal(caller_party,t1,r1):
	while r1 is caller_party.output():
		time.sleep(0.5)
	time2 = time.time()
	return time2 - t1

def Start_One_to_One(caller_party,called_party,call="Hello"):
	caller_party.say('menubutton',[call,called_party.name])
	delay=delay_cal(caller_party,time.time(),caller_party.output())
	utils.write_to_file(str(caller_party.name)+"response time is"+str(delay))
	
def One_2_One(a,b):
	#a.log_on_tool()
	#b.log_on_tool()
	for num in range(1):
		Start_One_to_One(a,b)
		#time.sleep(10)
		utils.kill_clients([a,b])

def Oration(initiator,E_output,command,file_name):
	initiator.say('menubutton',['1to10'])	
	time.sleep(10)
	if (E_output in initiator.output()):
		utils.write_to_file(command+" Passed",file_name)
	else:
		utils.write_to_file(command+" Failed",file_name)

def pairs(items):
        return zip(items[::2], items[1::2])
def Cont(lista,action):
    threadlist = []
    for fobx, foby in pairs(lista):
    	threadlist.append(threading.Thread(target=action, args=(fobx,foby,)))
    [x.start() for x in threadlist]

def Cont_P(lista,action):
	plist = []
	for fobx, foby in pairs(lista):
		plist.append(multiprocessing.Process(target=action, args=(fobx,foby,)))
	[x.start() for x in plist]

def stability():
	global clients
	clients = utils.make_clients_with_or_without_names()
	One_2_One_Users = clients[0:100]
	utils.start_clients(One_2_One_Users)
	Cont(One_2_One_Users,One_2_One)
if __name__ == '__main__':
	stability()

ALTER TABLE feature_prompt rename to feature_prompt_bkp;

CREATE TABLE IF NOT EXISTS "feature_prompt" (
      feature_name varchar(150) NOT NULL,
      context_name varchar(150) NOT NULL,
      prompt varchar(256) NOT NULL,
      sequence_id int(11) DEFAULT 1);

INSERT INTO feature_prompt( feature_name, context_name, prompt) SELECT feature_name, context_name, prompt FROM feature_prompt_bkp;

DROP TABLE feature_prompt_bkp;

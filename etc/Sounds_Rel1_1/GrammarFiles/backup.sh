#!/bin/bash -x
cp persons_loggedon.grxml_bkp persons_loggedon.grxml
cp persons_all.grxml persons_nonloggedon.grxml
sqlite3 /opt/theatro/etc_Traffic/TGS.db "delete from tgssoftstart;delete from TGS_SOFT_START_RETRIEVE_ANNC_INFO;delete from TGS_SOFT_START_RETRIEVE_MSG_INFO;"
rm -rf /opt/theatro/var/logs/TGS.log
ps -aef | grep tgsserver | awk {'print $2'} > process.txt
kill -9 $(cat process.txt)
ps -aef | grep TAG | awk {'print $2'} > process.txt
kill -9 $(cat process.txt)


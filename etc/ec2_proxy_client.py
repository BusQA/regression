import socket
import SocketServer
import threading
import thread
import time
import logging
import sys
import ssl
import random
import sys
import Queue
import dns.resolver
import urllib2
import urllib
import requests
import json
import re
import uuid
import os
import sqlite3

import logging.handlers

max_payload = 1350

client_svn_information = '''$Revision: 15430 $  $Date: 2016-08-13 02:58:03 +0530 (Sat, 13 Aug 2016) $ $HeadURL: https://pl3.projectlocker.com/TheatroLabs/Main/svn/trunk/utilities/ec2_proxy_client.py $ $Id: ec2_proxy_client.py 15430 2016-08-12 21:28:03Z gerry@theatrolabs.com $'''
theatrowildcard_pem = """Bag Attributes
    friendlyName: theatrowildcard
    localKeyID: 54 69 6D 65 20 31 34 36 39 32 32 36 37 38 37 32 31 33
Key Attributes: <No Attributes>
Bag Attributes
    friendlyName: theatrowildcard
    localKeyID: 54 69 6D 65 20 31 34 36 39 32 32 36 37 38 37 32 31 33
subject=/C=US/ST=Texas/L=Dallas/O=Theatro Labs, Inc./CN=*.theatro.com
issuer=/C=US/O=GeoTrust Inc./CN=GeoTrust SSL CA - G3
-----BEGIN CERTIFICATE-----
MIIG2DCCBcCgAwIBAgIQS3aZEWjfuRYfv4pW2pxRTDANBgkqhkiG9w0BAQsFADBE
MQswCQYDVQQGEwJVUzEWMBQGA1UEChMNR2VvVHJ1c3QgSW5jLjEdMBsGA1UEAxMU
R2VvVHJ1c3QgU1NMIENBIC0gRzMwHhcNMTYwNzIyMDAwMDAwWhcNMTkwOTAyMjM1
OTU5WjBjMQswCQYDVQQGEwJVUzEOMAwGA1UECAwFVGV4YXMxDzANBgNVBAcMBkRh
bGxhczEbMBkGA1UECgwSVGhlYXRybyBMYWJzLCBJbmMuMRYwFAYDVQQDDA0qLnRo
ZWF0cm8uY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmxlO4Yo1
AbXKwchOnkk4cz9RKRwfmXRoeBQhb1N2YR1RcQCoclF7owlKdVn0LxjO5n8XiBjJ
HxAYnvUnwZxdLd/4p6cAX+E69Fvck/pG34TaoIrEhcxpTQ6XLhMTKSJLcmkt6ElR
IMesK/BPOPM+CTgdfeKdi798mjUzfzg92eSi+eKw3cVZgLJz9WIk0RodRP/hpCLL
uNHB5B+PUDAEJ0NuupWmESOCEYbuvzunxNKAPqauOg1TssYrJdsdsNvn08J9i0tG
ovlNwUk4FMyfIti8ao2yfcpyMApkadc6ESNSah14hDrDP65fU9rTynav5Qi6+tPG
bY43RH3QULopDQIDAQABo4IDpTCCA6EwJQYDVR0RBB4wHIINKi50aGVhdHJvLmNv
bYILdGhlYXRyby5jb20wCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBaAwKwYDVR0f
BCQwIjAgoB6gHIYaaHR0cDovL2duLnN5bWNiLmNvbS9nbi5jcmwwgZ0GA1UdIASB
lTCBkjCBjwYGZ4EMAQICMIGEMD8GCCsGAQUFBwIBFjNodHRwczovL3d3dy5nZW90
cnVzdC5jb20vcmVzb3VyY2VzL3JlcG9zaXRvcnkvbGVnYWwwQQYIKwYBBQUHAgIw
NQwzaHR0cHM6Ly93d3cuZ2VvdHJ1c3QuY29tL3Jlc291cmNlcy9yZXBvc2l0b3J5
L2xlZ2FsMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAfBgNVHSMEGDAW
gBTSb/eW9IU/cjwwfSPahXibo3xafDBXBggrBgEFBQcBAQRLMEkwHwYIKwYBBQUH
MAGGE2h0dHA6Ly9nbi5zeW1jZC5jb20wJgYIKwYBBQUHMAKGGmh0dHA6Ly9nbi5z
eW1jYi5jb20vZ24uY3J0MIIB9QYKKwYBBAHWeQIEAgSCAeUEggHhAd8AdQDd6x0r
eg1PpiCLga2BaHB+Lo6dAdVciI09EcTNtuy+zAAAAVYUsbhFAAAEAwBGMEQCIDMs
S6oc8ukVInyw10kcPhM0qZAIaeD4oh5L+CeB+uKqAiA7u4U/HbDBkYqf9uW+6D6G
EV6CcysbrATwdBOwDkqsSAB1AKS5CZC0GFgUh7sTosxncAo8NZgE+RvfuON3zQ7I
DdwQAAABVhSxuHoAAAQDAEYwRAIgeUyZt6PqUbIMvPbpQpvitV3PaWkSa+kiZ6HE
US0NSJ8CIARY/B3yoc2v/kLC6J/wCgM6bnKTMtVp4iNgnAw1lM0XAHYAaPaY+B9k
gr46jO65KB1M/HFRXWeT1ETRCmesu09P+8QAAAFWFLG4hgAABAMARzBFAiAQ6o/j
rqMW25XXkN9U0QoEBuydJjZtSKiH8HaXNPO0qwIhAJgQxhjHk+S7AKd5YFEsBRqS
s+h2lxhDxqGh2BgfRG0sAHcA7ku9t3XOYLrhQmkfq+GeZqMPfl+wctiDAMR7iXqo
/csAAAFWFLG6mAAABAMASDBGAiEAitdhiyfOXiVQM4am05W/syareVkjhMsfPqPf
ALbuDpsCIQCDR6qwdxf03WAz5cvs97dNI3G5dYW+fmtAEBg/UsslvzANBgkqhkiG
9w0BAQsFAAOCAQEAV5x/DvOq1RLSmW3qS6v7OyqXSRDoJJv3irxBwCcwaYrXdI3X
YroxGUhNnHwnOFpIiAjlG14iPfG6kHP0Ju+QR6YdvCGCEKe+ZnD9830lEqBd+ft3
5SSTaPqefWADuuk7eqvtl21ULqrNOqEWFmYePgK/6LRK+ORAbrBcD+3/OCP3+wUn
STS4vAgAVA1Py+HrqRcsAQedwwPnRUC6DpKUOu1MtPov9wj5zP3vQAG7SX/sk7+w
fFjMhkGjT6wG26k2V7y3ajiUlK9t2HdKc8nhbLVJ45Zh1xF9GA5cTsP/UscRvka7
MMI0I2UQoa4GkojYdH6EAKefNTpUx4SCe9CGzA==
-----END CERTIFICATE-----
Bag Attributes
    friendlyName: CN=GeoTrust SSL CA - G3,O=GeoTrust Inc.,C=US
subject=/C=US/O=GeoTrust Inc./CN=GeoTrust SSL CA - G3
issuer=/C=US/O=GeoTrust Inc./CN=GeoTrust Global CA
-----BEGIN CERTIFICATE-----
MIIETzCCAzegAwIBAgIDAjpvMA0GCSqGSIb3DQEBCwUAMEIxCzAJBgNVBAYTAlVT
MRYwFAYDVQQKEw1HZW9UcnVzdCBJbmMuMRswGQYDVQQDExJHZW9UcnVzdCBHbG9i
YWwgQ0EwHhcNMTMxMTA1MjEzNjUwWhcNMjIwNTIwMjEzNjUwWjBEMQswCQYDVQQG
EwJVUzEWMBQGA1UEChMNR2VvVHJ1c3QgSW5jLjEdMBsGA1UEAxMUR2VvVHJ1c3Qg
U1NMIENBIC0gRzMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDjvn4K
hqPPa209K6GXrUkkTdd3uTR5CKWeop7eRxKSPX7qGYax6E89X/fQp3eaWx8KA7UZ
U9ulIZRpY51qTJEMEEe+EfpshiW3qwRoQjgJZfAU2hme+msLq2LvjafvY3AjqK+B
89FuiGdT7BKkKXWKp/JXPaKDmJfyCn3U50NuMHhiIllZuHEnRaoPZsZVP/oyFysx
j0ag+mkUfJ2fWuLrM04QprPtd2PYw5703d95mnrU7t7dmszDt6ldzBE6B7tvl6QB
I0eVH6N3+liSxsfQvc+TGEK3fveeZerVO8rtrMVwof7UEJrwEgRErBpbeFBFV0xv
vYDLgVwts7x2oR5lAgMBAAGjggFKMIIBRjAfBgNVHSMEGDAWgBTAephojYn7qwVk
DBF9qn1luMrMTjAdBgNVHQ4EFgQU0m/3lvSFP3I8MH0j2oV4m6N8WnwwEgYDVR0T
AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAQYwNgYDVR0fBC8wLTAroCmgJ4Yl
aHR0cDovL2cxLnN5bWNiLmNvbS9jcmxzL2d0Z2xvYmFsLmNybDAvBggrBgEFBQcB
AQQjMCEwHwYIKwYBBQUHMAGGE2h0dHA6Ly9nMi5zeW1jYi5jb20wTAYDVR0gBEUw
QzBBBgpghkgBhvhFAQc2MDMwMQYIKwYBBQUHAgEWJWh0dHA6Ly93d3cuZ2VvdHJ1
c3QuY29tL3Jlc291cmNlcy9jcHMwKQYDVR0RBCIwIKQeMBwxGjAYBgNVBAMTEVN5
bWFudGVjUEtJLTEtNTM5MA0GCSqGSIb3DQEBCwUAA4IBAQCg1Pcs+3QLf2TxzUNq
n2JTHAJ8mJCi7k9o1CAacxI+d7NQ63K87oi+fxfqd4+DYZVPhKHLMk9sIb7SaZZ9
Y73cK6gf0BOEcP72NZWJ+aZ3sEbIu7cT9clgadZM/tKO79NgwYCA4ef7i28heUrg
3Kkbwbf7w0lZXLV3B0TUl/xJAIlvBk4BcBmsLxHA4uYPL4ZLjXvDuacu9PGsFj45
SVGeF0tPEDpbpaiSb/361gsDTUdWVxnzy2v189bPsPX1oxHSIFMTNDcFLENaY9+N
QNaFHlHpURceA1bJ8TCt55sRornQMYGbaLHZ6PPmlH7HrhMvh+3QJbBo+d4IWvMp
zNSS
-----END CERTIFICATE-----
Bag Attributes
    friendlyName: CN=GeoTrust Global CA,O=GeoTrust Inc.,C=US
subject=/C=US/O=GeoTrust Inc./CN=GeoTrust Global CA
issuer=/C=US/O=GeoTrust Inc./CN=GeoTrust Global CA
-----BEGIN CERTIFICATE-----
MIIDVDCCAjygAwIBAgIDAjRWMA0GCSqGSIb3DQEBBQUAMEIxCzAJBgNVBAYTAlVT
MRYwFAYDVQQKEw1HZW9UcnVzdCBJbmMuMRswGQYDVQQDExJHZW9UcnVzdCBHbG9i
YWwgQ0EwHhcNMDIwNTIxMDQwMDAwWhcNMjIwNTIxMDQwMDAwWjBCMQswCQYDVQQG
EwJVUzEWMBQGA1UEChMNR2VvVHJ1c3QgSW5jLjEbMBkGA1UEAxMSR2VvVHJ1c3Qg
R2xvYmFsIENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2swYYzD9
9BcjGlZ+W988bDjkcbd4kdS8odhM+KhDtgPpTSEHCIjaWC9mOSm9BXiLnTjoBbdq
fnGk5sRgprDvgOSJKA+eJdbtg/OtppHHmMlCGDUUna2YRpIuT8rxh0PBFpVXLVDv
iS2Aelet8u5fa9IAjbkU+BQVNdnARqN7csiRv8lVK83Qlz6cJmTM386DGXHKTubU
1XupGc1V3sjs0l44U+VcT4wt/lAjNvxm5suOpDkZALeVAjmRCw7+OC7RHQWa9k0+
bw8HHa8sHo9gOeL6NlMTOdReJivbPagUvTLrGAMoUgRx5aszPeE4uwc2hGKceeoW
MPRfwCvocWvk+QIDAQABo1MwUTAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBTA
ephojYn7qwVkDBF9qn1luMrMTjAfBgNVHSMEGDAWgBTAephojYn7qwVkDBF9qn1l
uMrMTjANBgkqhkiG9w0BAQUFAAOCAQEANeMpauUvXVSOKVCUn5kaFOSPeCpilKIn
Z57QzxpeR+nBsqTP3UEaBU6bS+5Kb1VSsyShNwrrZHYqLizz/Tt1kL/6cdjHPTfS
tQWVYrmm3ok9Nns4d0iXrKYgjy6myQzCsplFAMfOEVEiIuCl6rYVSAlk6l5PdPcF
PseKUgzbFbS9bZvlxrFUaKnjaZC2mqUPuLk/IH2uSrW4nOQdtqvmlKXBx4Ot2/Un
hw4EbNX/3aBd7YdStysVAq45pmp06drE57xNNB6pXE0zX5IJL4hmXXeXxx12E6nV
5fEWCRE11azbJHFwLJhWC9kXtNHjUStedejV0NxPNO3CBWaAocvmMw==
-----END CERTIFICATE-----
"""

theatrowildcard_key = """Bag Attributes
    friendlyName: theatrowildcard
    localKeyID: 54 69 6D 65 20 31 34 36 39 32 32 36 37 38 37 32 31 33
Key Attributes: <No Attributes>
-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCbGU7hijUBtcrB
yE6eSThzP1EpHB+ZdGh4FCFvU3ZhHVFxAKhyUXujCUp1WfQvGM7mfxeIGMkfEBie
9SfBnF0t3/inpwBf4Tr0W9yT+kbfhNqgisSFzGlNDpcuExMpIktyaS3oSVEgx6wr
8E848z4JOB194p2Lv3yaNTN/OD3Z5KL54rDdxVmAsnP1YiTRGh1E/+GkIsu40cHk
H49QMAQnQ266laYRI4IRhu6/O6fE0oA+pq46DVOyxisl2x2w2+fTwn2LS0ai+U3B
STgUzJ8i2LxqjbJ9ynIwCmRp1zoRI1JqHXiEOsM/rl9T2tPKdq/lCLr608ZtjjdE
fdBQuikNAgMBAAECggEAOldOwLAIOFh1qGb8i7XnMhLDKL+Xc1XCZkjMOuAeS3GH
zQX+VuInq7A2/eWUYJLoYPO1mlAYd0LKwCBfkAiNVFBHbGj5Txed4YUcd91RA4sp
4fHQ8rNteLrgS0iJkJOB5cuCpTnO9pGlRfHjGv0EfNtK3Kt9r6+b4zXX5Sb2vRsX
Q3xdgnF0KLNENF/E+HywLLiFUeTdVsIDhSJnXwPAbhArK8Huvt1mdl+ty6gmtUKu
uzdxJW6ZzKDhIwFVJT6GStopVGLd8YgjpHJUcqs4IUnhLXgjSVILtiUSagK1imsH
bhoBNVFnNWRL9hx8KgTpnOCGrRRgZiPWP4yuDxnf0QKBgQDw9jK3KNQbTUatJlxQ
k7fBgSQsn6GglmG6mYMoyVE8Ec6iUkPVBBG0U9YA7Ij4/OrkeLEmimMU/5Vt+0oN
UDndlwEGMKWvZ9JAVqxxQbumu4a2lCpJcEUdk8VpIqYI/ARr0dQI1gfY5l1SFZc7
W0k+NrBYgcLIV6729hbkTlL8RwKBgQCkx0txTsTlCZkV3BDn4ns7pDLqiQfCoNhZ
S/Yza4wVsyutq4RXtavwLbKMFyydR1RbCXpISjCWZSdf32X8vqMqlM/gywwVNlqh
c9rd822STCIvzD1FO1LZ0k1zFXWReyH0lN2m2amLB/1r155VMw9/s85bmjlE7Goc
AhuDax8eCwKBgCapKbvvZhC3Vlajp/4ZpUr7wGUUe6c7WZTJ2pxpqIn9xWyPzGha
DckkBlsBmHxyf/GGV836OCl/VGdgASKfs5kVCEoI9hxtiPNftK07QbNftR6IKTy9
MamsMulKcN9SKBdwhBei14F6fJCV/Dy+ycKcnbClqS3hu6XOGCArXO8zAoGAK6co
k2H8w3ihd6sPR382inxbiMq4pZUa+70mAkVRnUYUIjQ4IUxduolAreBNz3TgIiYG
oTQWXyF5Jur2B9kxPMyjrbfdAZmetPsOVpJwyrskq2j+KmMm9dHC9uQOe3YVbsgp
WFR9l/mL54/9wDCAwKIP3zCiT2IaUkU0qMIuQjUCgYB4huBN8FSsqaAFHMxErumN
60qIKg5cfU0Jsh5NbI75q42m5qhvfso6do1+1YyUp1/zoZL3trRJiNFpVc3dDZs+
WvLZSeEK/vuPGNe8ihGkNdLYeexdld7AJ6wppVZs3U78QKT3Wj4h4AXnqDN7L5IB
/R3akpl4Y5wRd21gA5rLBA==
-----END PRIVATE KEY-----
"""

if sys.platform == 'win32':
    logfile = '\\Theatro\\logs\\proxy.log'
    certfilename = '\\Theatro\\theatrowildcard.pem'
    keyfilename = '\\theatro\\theatrowildcard.key'
    udpfilename = '\\theatro\udp_echo.exe'
else:
    logfile = '/opt/theatro/logs/proxy.log'
    certfilename = '/opt/theatro/etc/theatrowildcard.pem'
    keyfilename = '/opt/theatro/etc/theatrowildcard.key'
    udpfilename = '/opt/theatro/bin/udp_echo'
    

if not os.path.exists(certfilename):   
    with open(certfilename, "w") as text_file:
        text_file.write(theatrowildcard_pem) 
os.chmod(certfilename, 0400) 

if not os.path.exists(keyfilename):
    with open(keyfilename, "w") as text_file:
        text_file.write(theatrowildcard_key) 
os.chmod(keyfilename, 0400) 

if os.path.exists(udpfilename):  
    os.chmod(udpfilename, 0755)

format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(format)

f = logging.handlers.TimedRotatingFileHandler( logfile, when='midnight')
f.setLevel(logging.DEBUG)
f.setFormatter(format)

logger = logging.getLogger('ec2_proxy_logger')
logger.setLevel(logging.DEBUG)
logger.addHandler(f)
logger.addHandler(ch)

ServerPort = 443
ServerIP = 'c2proxy.theatro.com'
tgs_socket = None
MAC2TGSIP = {}

PeerCerts = [{'notAfter': 'Sep  3 23:59:59 2015 GMT', 'subjectAltName': (('DNS', '*.theatro.com'), ('DNS', 'theatro.com')), 'subject': ((('countryName', u'US'),), (('stateOrProvinceName', u'Texas'),), (('localityName', u'Dallas'),), (('organizationName', u'Theatro Labs, Inc.'),), (('commonName', u'*.theatro.com'),))},
             {'notAfter': 'Sep  2 23:59:59 2016 GMT', 'subjectAltName': (('DNS', '*.theatro.com'), ('DNS', 'theatro.com')), 'subject': ((('countryName', u'US'),), (('stateOrProvinceName', u'Texas'),), (('localityName', u'Dallas'),), (('organizationName', u'Theatro Labs, Inc.'),), (('commonName', u'*.theatro.com'),))},
             {'notAfter': 'Sep  2 23:59:59 2019 GMT', 'subjectAltName': (('DNS', '*.theatro.com'), ('DNS', 'theatro.com')), 'subject': ((('countryName', u'US'),), (('stateOrProvinceName', u'Texas'),), (('localityName', u'Dallas'),), (('organizationName', u'Theatro Labs, Inc.'),), (('commonName', u'*.theatro.com'),))}]

storeinfo = 'unknown'
chainname ='unknown'
storename = 'unknown'
connected = False

Tunnelsocket = None
TunnelPort = 22
ToServerQ = Queue.Queue()
#ToNTPQ = Queue.Queue()

alarm_host_name = 'central.theatro.com'   

def cmpcert(cert, PeerCerts):
    #        print 'cert', type(cert), repr(cert)
    #        print 'PerrCert', type(PeerCert), repr(PeerCert)
        for servercert in PeerCerts:
            good = True
            for item in cert.keys():
        #            print item
        #            print PeerCert[item], cert[item]
                if item in servercert.keys():
                    if servercert[item] <> cert[item]:
                        good = False
                        continue
            if good : return True
        return False

  
    
def sendAlarm(alarm, detail=""):
    global chainname, storename, dev_system
    def send_thread(post_data):
        logger.info( 'sending alarm %s', post_data )
        url = 'https://'+ alarm_host_name + '/restapi/alarms'
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(url, data=json.dumps(post_data), headers=headers, verify= not dev_system) 
        except Exception as e:
            if dev_system and 'Insecure' in repr(e): return
            logger.error('Send Alarm Error %s', repr(e))
        
        
    post_data =  {
      'type': "alarm",
      'chain': chainname, 
      'store': storename,
      'msg_type': "alarm",
      'alarm_time': time.time(), 
      'source': "",
      'summary': alarm,
      'detail': detail,
      'severity': 3
       }
    
#    threading.Thread(target=send_thread,args=(post_data,)).start()   
    
    
def get_external_ip():
    try:
        site = urllib2.urlopen("http://checkip.dyndns.org/").read()
        extip = re.findall('([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)', site)[0]
        sendAlarm( 'Proxy Client External IP', 'External IP=' + extip )
        logger.info( 'Proxy Client External IP %s', extip )
    except Exception as e:
        sendAlarm( "Can't Determine Proxy Client External IP", repr(e))
        logger.info( "Can't Determine Proxy Client External IP %s", repr(e) )
        
def get_store_info():
    global chainnname, storename
    if sys.platform == 'win32':
        configfile = open('C:\\Theatro\\tgsconfig.txt', 'r')
        lines = configfile.readlines()
        for line in lines : 
            parts = line.split('=')
            if len(parts) > 1:
                option = parts[0].strip()
                values = parts[1].split('#')
                value = values[0].strip()
#                print option, value
                if option == 'CHAINNAME':
                    chainname = value
                if option == 'STORENAME' :
                    storename = value
        if storename == '' :
            return chainname
        else:
            return chainname + '/' + storename
    
    else:  ### Assume Linux
        dbfilename = '/opt/theatro/etc/TGS.db'
        if os.path.exists(dbfilename):
            try:
                con = sqlite3.connect(dbfilename)
                cur = con.cursor()    
                cur.execute('SELECT ParamValue FROM configparams where ParaName = "STORE_ID";')
                value = cur.fetchone()[0].strip()
                chainname, storename = value.split('/')
                return value
            except Exception as e:
                logger.error('Failed to get STORE_ID from %s %s %s', dbfilename, repr(value), repr(e))
                exit()
        else:
            configfile = open('/opt/theatro/etc/TGSConfig.ini', 'r')
            lines = configfile.readlines()
            for line in lines : 
                parts = line.split('=')
                if len(parts) == 2:
                    option = parts[0].strip()
                    value = parts[1].replace('"','').strip()
    #                print option, value
                    if option == 'STORE_ID':
                        chainname, storename = value.split('/')
                        return value

    
def send2TGS(data, port):
    try: 
        startmacidx = data.find('"mac":"' ) + 7
        if startmacidx == 6:
            startmacidx = data.find("'mac':'" ) + 7
            endmacidx = data.find("'", startmacidx)
        else:
            endmacidx = data.find('"', startmacidx)
        mac = data[startmacidx:endmacidx]    
    except Exception as e:
            logger.error('Error in send2TGS %s %d %s', repr(e), port, data )

    try:
        ip = MAC2TGSIP[mac]
    except:
        logger.error("Can't find TGS IP Address for %s", mac )
        ip = '127.0.0.1'
        
    tgs_socket.sendto( data, (ip, port))
               
class AudioOutHandler(SocketServer.BaseRequestHandler): 
    def handle(self):
            global connected
            if connected: send_to_server('AO', self.request[0])

class EchoHandler(SocketServer.BaseRequestHandler): 
    def handle(self):
            global connected, echo_client
            if connected: 
                echo_client = str(self.client_address)
#                print echo_client, self.request[0]
                send_to_server('EC', echo_client + self.request[0])

class AuthResponseHandler(SocketServer.BaseRequestHandler): 
    def handle(self):
        global connected
        if connected:
#            print 'AuthResponse', self.request[0]
            send_to_server('AU', self.request[0])
        else:
            logger.warn( 'AU Not Connected' )

def send_to_server( port, data ):
#    print 'Send to Server:', port, data
    length = '%04d' % len(data)

    if len(data) > max_payload:
        logger.error('Packet too big %s', port+length)
        return
    
    try:   
        ToServerQ.put(port+length+data)
    except Exception as e:
        logging.warn('Could not send to serverQ %s', repr(e))
        

def send_auth(data):
#    print 'send Auth to TGS ', data
    try:         
        startmacidx = data.find('"mac":"' ) + 7
        if startmacidx == 6:
            startmacidx = data.find("'mac':'" ) + 7
            endmacidx = data.find('"', startmacidx)
        else:
            endmacidx = data.find('"', startmacidx)
        mac = data[startmacidx:endmacidx]

        startdestidx = data.find('"Dest":"' ) + 7
        if startdestidx == 6:
            startdestidx = data.find("'Dest':'" ) + 7
            enddestidx = data.find('"', startdestidx)
        else:
            enddestidx = data.find("'", startdestidx)
        dest = data[startdestidx:enddestidx].split('/')
        if len(dest) == 3:
            MAC2TGSIP[mac] = dest[2][:-1]
        else: 
            MAC2TGSIP[mac] = '127.0.0.1'
            
        send2TGS( data, 41102)
    except Exception as e:
        logging.warn('Could not send auth to TGS %s %s', repr(e), dest )

def send_play_credit(data):
            send2TGS( data, 50007)

def send_clue(data):
    send2TGS( data, 50011)


def send_audio(data):
    send2TGS( data, 50009)
    
def register_response(data):
    global connected
    logger.info( 'Register Response %s', data )
    if 'Success' in data :
        connected = True
        sendAlarm( 'Proxy Client Connected')
        return
    else:
        connected = False
        raise Exception('Registration Failure ', data)

def Tunnel_in_stream():
    global Tunnelsocket
#    print 'Starting Tunnel listener'
    while True:
        try:
            data=Tunnelsocket.recv(max_payload)
            if len(data) > 0:
                send_to_server('SD', data)
            else:
                raise Exception("Tunnel Connection Dropped")
        except Exception as e:
            logger.info( 'Tunnel In Exception %s' % repr(e))
            send_to_server( 'SC', 'Disconnect')
            if Tunnelsocket :
                try:
                    Tunnelsocket.shutdown(socket.SHUT_RDWR)
                    Tunnelsocket.close()
                    Tunnelsocket = None
                except Exception as e:
                    Tunnelsocket = None
                    logger.warn('Error Shutting Down Tunnel Socket %s' % repr(e) )
            return
                
            
def Tunnel_control(data):
    global Tunnelsocket
    if 'Connect' in data:
        params = data.split(' ')
        TunnelPort = int(params[1])
        logger.info('Starting Tunnel Tunnel Server on port %d' % TunnelPort )
        try:
            Tunnelsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            Tunnelsocket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
            Tunnelsocket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 10)
            Tunnelsocket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 10)
            Tunnelsocket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 6)


            Tunnelsocket.connect(('127.0.0.1', TunnelPort))
            Tunnel_server = threading.Thread(target=Tunnel_in_stream)
            Tunnel_server.start()
            logger.info( 'Server Started')
        except Exception as e:
            logger.warn( 'Start Failed %s', repr(e)) 
    elif data == 'Disconnect':
        logger.info( 'Stopping Tunnel Server' )
        try:
            Tunnelsocket.shutdown(socket.SHUT_RDWR)
            Tunnelsocket.close()
            Tunnelsocket = None
        except Exception as e:
            Tunnelsocket = None
            logger.warn( 'Error Closing Tunnel socket %s', repr(e))
    else:
        logger.warn( 'Unknown Tunnel control option %s', data  )

    
def Tunnel_data(data):
    global Tunnelsocket
#    print 'Sending Tunnel data ', data
#    print Tunnelsocket
    try:
        Tunnelsocket.sendall(data)
    except Exception as e:
        logger.error( 'Tunnel Send Failed %s', repr(e) )
#        raise   

def Echo(data):
    global echo_client
    try:
        eoa = data.find(')') + 1
        echo_client = data[:eoa]
        data = data[eoa:]
#        print 'sending to ', echo_client, data
        echo_socket.sendto( data, eval(echo_client))
    except Exception as e:
        logger.error( 'Echo Send Failed %s', repr(e) )
        raise   

# def NTPQHandler():
#     global ToNTPQ, ToServerQ
#     NTPsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#     NTPsocket.settimeout(1)
#     while True:
#         try:
#             data = toNTPQ.get()
#             mac_address = data[-12:]
#             print mac_address
#             NTPsocket.sendto(data[:-12], ('127.0.0.1', 123 ))
#             response = NTPsocket.recv(1024)
#             print 'got ntp response'
#             ToServerQ.put( response + mac_address )
#         except Exception as e :
#             logger.warning('Problem getting NTP response %s', repr(e))
        
# def NTP_Query(data):
#     global ToNTPQ
#     ToNTPQ.put(data)
#     return
    
def OutGoingQHandler():
    global ToServerQ
#    print 'Out Going Q Handler'
    while True:
        try:   
            data = ToServerQ.get()
            server.sendall(data)
        except Exception as e:
            logging.warn('Could not send to server %s', repr(e))       


        
print "Running"
      
print 'Starting Logging'
if 'win32' in sys.platform :
    logging.basicConfig(filename='c:\\theatro\\logs\\client.log', format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO)

else: # Assume Linux
    logging.basicConfig(filename='/opt/theatro/logs/client.log', format="%(asctime)s - %(levelname)s - %(message)s", level=logging.INFO)
logger.info('Started')
logger.info('Proxy Client SVN INFORNATION %s', client_svn_information)
   
storeinfo = get_store_info()
chainname = storeinfo.split('/')[0]
try:
    ServerIP = sys.argv[1]
except:
    ServerIP = chainname + '.proxy.theatro.com'
    
try:
    ServerPort = int(sys.argv[2])
except:
    # Look up SRV record for port number
    query = '_proxy._tcp.' + ServerIP
    try:
        answers = dns.resolver.query( query, 'SRV')
        ServerIP = str(answers[0].target)
        ServerPort = int(answers[0].port)  
        logger.info( 'SVR Lookup Successful. Redirecting to %s:%d', ServerIP, ServerPort)
    except:
        ServerPort = 443
        
    try:
        resolver = dns.resolver.Resolver()
        for rdata in resolver.query(chainname + '.central.theatro.com', 'CNAME') :
            alarm_host_name = rdata.to_text()[:-1]
    except:
        pass     

dev_system = 'Theatro' in chainname or 'ITG' in chainname 
 
logger.info('Starting External IP Lookup')
extip = threading.Thread(target=get_external_ip)
extip.start() 

logger.info('Starting SendToServer')
ToServer = threading.Thread(target=OutGoingQHandler)
ToServer.start() 

# logger.info('Starting NTP Handler')
# ToServer = threading.Thread(target=NTPQHandler)
# ToServer.start() 

    
logger.info('Starting Audio Out')
AudioOutServer = SocketServer.UDPServer(('127.0.0.1', 50008), AudioOutHandler)
audioout_server = threading.Thread(target=AudioOutServer.serve_forever)
audioout_server.start()

logger.info( 'Starting Auth Response' )
AuthResponseHandleServer = SocketServer.UDPServer(('127.0.0.1', 41103), AuthResponseHandler)
tgs_socket = AuthResponseHandleServer.socket
authresponse_server = threading.Thread(target=AuthResponseHandleServer.serve_forever)
authresponse_server.start()

logger.info( 'Starting Echo Server' )
EchoHandleServer = SocketServer.UDPServer(('127.0.0.1', 50013), EchoHandler)
echo_socket = EchoHandleServer.socket
echoresponse_server = threading.Thread(target=EchoHandleServer.serve_forever)
echoresponse_server.start()


sendAlarm( 'Proxy Client Started', client_svn_information )

while True:
    connected = False
    while not connected:
        try:
            logger.info( 'Registering with Server %s:%s %s', ServerIP, ServerPort, storeinfo )
            server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            server.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            server = ssl.wrap_socket(server,
                                     ssl_version=ssl.PROTOCOL_TLSv1,
                                     cert_reqs=ssl.CERT_REQUIRED,
                                     certfile=certfilename,
                                     keyfile=keyfilename,
                                     ca_certs=certfilename )
            server.settimeout(10.0)
            server.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
            server.connect((ServerIP, ServerPort)) 
            cert = server.getpeercert()
            logger.info( 'Server Cert %s', cert )
            if not cmpcert( cert, PeerCerts):
                logger.warn('PeerCert does not match %s', cert)
                server.close()
                raise 'PeerCert does not match'
            else:
               logger.info('Server Certificate is good.') 

            server.settimeout(None)
            try:
                local_ipaddr = socket.gethostbyname(socket.gethostname())
            except:
                local_ipaddr = 'IPAddr Lookup Failure'
                
            server.sendall( """GET  /%s HTTP/1.1
            Host: %s
            User-Agent: Theatro C2 Proxy
            Accept: text
            Accept-Language: en-US,en;q=0.5
            Accept-Encoding: gzip, deflate
            Connection: keep-alive
            Cache-Control: max-age=0,
            Revision Info: %s""" % (storeinfo, ServerIP, client_svn_information + ' ' + socket.gethostname() + ' ' + local_ipaddr + ' ' + str(uuid.uuid4())))
            
            portmap = {'AU' : send_auth,
                       'PC' : send_play_credit,
                       'CU' : send_clue,
                       'AI' : send_audio,
                       'RE' : register_response,
                       'SC' : Tunnel_control,
                       'SD' : Tunnel_data,
#                       'NT' : NTP_query,
                       'EC' : Echo }
            while True:
                try:
                    data = server.recv(6)
                    if len(data) == 0:
                        raise Exception('Proxy Server Closed Connection')
                    port = data[:2]
                    length = data[2:]
                    data = server.recv(int(length))
            #        print port, length, data
                    portmap[port](data)
                except Exception as e:
#                    print 'Message Handling Error %s' % repr(e)
                    raise                       
        except Exception as e:
            logger.warn( 'Lost Connection to Proxy Server %s',repr(e))
            sendAlarm( 'Proxy Client DisConnected', repr(e))
            connected = False
            server.close()
            time.sleep(random.uniform(0.5, 5.0))
            break 
    


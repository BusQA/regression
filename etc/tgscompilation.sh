#!/bin/bash
ulimit -c unlimited
export TGS_BUILD_BASE_DIR=/opt/theatro
export TGS_BUILD_DIR=/opt/theatro
export TGS_DB_PATH=$TGS_BUILD_BASE_DIR/etc
export TGS_CONFIG_PATH=$TGS_BUILD_BASE_DIR/etc
export NUANCE_DIR=$TGS_CONFIG_PATH/Nuance
export XML_DIR=$TGS_CONFIG_PATH/xml_config
export LD_LIBRARY_PATH=${TGS_BUILD_BASE_DIR}/lib:$NUANCE_DIR/lib:$LD_LIBRARY_PATH:/usr/local/lib:$XML_DIR/xml_libs
export READTGSCONFIGINI=false
export GRAMMAR_PATH=$TGS_CONFIG_PATH/Sounds_Rel1_1/GrammarFiles
export TGS_BNF_PATH=$NUANCE_DIR/grammars
export HANDLE_GR_LOG_PATH=/opt/theatro/logs/

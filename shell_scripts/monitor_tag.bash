while true
do
  date >> /opt/theatro/logs/tag_monitor.log
  echo "Starting TAG App" >> /opt/theatro/logs/tag_monitor.log
  if [ ! -f /tmp/upgrade_tag.sh ]; then
    #python2.7 -u /opt/theatro/bin/TGS_AWS_Gateway/TAG.py | tee /opt/theatro/logs/tag.log
    /opt/theatro/bin/tag/venv/bin/python -u /opt/theatro/bin/tag/TAG.py | tee /opt/theatro/logs/tag.log 
  fi  
  date >> /opt/theatro/logs/tag_monitor.log

  echo "Running upgrade_tag.sh" >>  /opt/theatro/logs/tag_monitor.log
  /bin/sh < /tmp/upgrade_tag.sh
  date >> /opt/theatro/logs/tag_monitor.log
  echo "Finished upgrade_tag.sh" >> /opt/theatro/logs/tag_monitor.log
  sleep 10
done

#!/bin/bash -x
ps -aef | grep tgsserver | awk {'print $2'} > process.txt
kill -9 $(cat process.txt)
sleep 20


#!/bin/bash
rm -rf /opt/theatro/bin/handle_grammer.py
ln -s `pwd`/handle_grammer.py /opt/theatro/bin/handle_grammer.py 

rm -rf /opt/theatro/bin/tgs/tgsserver
ln -s `pwd`/bin/tgsserver /opt/theatro/bin/tgs/tgsserver

rm /opt/theatro/etc/tgscompilation.sh
ln -s `pwd`/tgscompilation.sh /opt/theatro/etc/tgscompilation.sh 

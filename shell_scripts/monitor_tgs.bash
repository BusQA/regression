while true
do
  date >> /opt/theatro/logs/tgs_monitor.log
  echo "Starting TGS App" >> /opt/theatro/logs/tgs_monitor.log
  if [ ! -f /tmp/upgrade_tgs.sh ]; then
   cd /opt/theatro/logs
   source /opt/theatro/etc/tgscompilation.sh
   /opt/theatro/bin/tgs/tgsserver | tee /opt/theatro/logs/tgsconsole.log
  fi  
  date >> /opt/theatro/logs/tgs_monitor.log

  echo "Running upgrade_tgs.sh" >>  /opt/theatro/logs/tgs_monitor.log
  /bin/sh < /tmp/upgrade_tgs.sh
  date >> /opt/theatro/logs/tgs_monitor.log
  echo "Finished upgrade_tgs.sh" >> /opt/theatro/logs/tgs_monitor.log
  sleep 10
done

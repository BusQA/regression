#!/bin/bash -x
rm /tmp/upgrade_tgs.sh
touch /tmp/theatrotest
sed -i "/WEB_URL/s/dallascentral.theatro.com/itg0.theatro.com/g" /opt/theatro/bin/tag/TAG_Config.py | echo "file changed"
sed -i "/WEB_URL/s/central.theatro.com/itg0.theatro.com/g" /opt/theatro/bin/tag/TAG_Config.py | echo "file changed"
cp /opt/theatro/etc/Sounds_Rel1_1/GrammarFiles/persons_loggedon.grxml_bkp /opt/theatro/etc/Sounds_Rel1_1/GrammarFiles/persons_loggedon.grxml
cp /opt/theatro/etc/Sounds_Rel1_1/GrammarFiles/persons_all.grxml /opt/theatro/etc/Sounds_Rel1_1/GrammarFiles/persons_nonloggedon.grxml
sqlite3 /opt/theatro/etc/TGS.db "delete from tgssoftstart;delete from TGS_SOFT_START_RETRIEVE_ANNC_INFO;delete from TGS_SOFT_START_RETRIEVE_MSG_INFO;"
#rm -rf /opt/theatro/logs/TGS.log
ps -aef | grep tgsserver | awk {'print $2'} > process.txt
kill -9 $(cat process.txt)
sleep 25


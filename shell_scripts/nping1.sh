#Author:Bipin
#Date:29th Jan 2016
#Objective :- Send Location packet for CII mac IDs like location server
#Syntax : ./nping.sh <CII IP adress> <CII mac ID> <CII new location>
#!/bin/bash -x
 sudo nping -c1 --udp -g 41103 -p 9997 -S 127.0.0.1 --dest-ip 127.0.0.1 --data-string "{\"hostid\": \"$1\", \"ver\": 2, \"mybssid\": \"c04a00406f65\", \"location\": \"$2\", \"myrssi\": -63, \"position\": \"19,19\", \"prob\": 0.0016521342009936007, \"momentum\": \"stationary\"}";

from config import DEFAULT_LOCATION as location
#All the Expected prompts for gear feature
GEAR_PROMPTS = {"TEST_CASE_1":["Hello gear when no one is logged on and issuer taps out","gear,is_not_logged_on,would_like_to_leave_a_message,please_say_yes_or,tap_out_to_cancel"],
"TEST_CASE_2":"message_cancelled_please_say_contact,gear,to_try_again_later",
"TEST_CASE_3":["Hello Gear with all available user","group_call_ended","ended_group_chat"],
"TEST_CASE_4":["Tap out one user at a time while in a gear group","has_ended_group_chat","you_are_leaving_the_group_chat"],
"TEST_CASE_5":["Issue Hello Gear when all member in gear group are in engaged state","gear,is_engaged_with_another_outfitter,would_like_to_leave_a_message,please_say_yes_or,tap_out_to_cancel"],
"TEST_CASE_6":["Issue Hello Gear when all member in gear group are in conversation","is_in_a_conversation"]}
PT_RATE = {"TEST_CASE_1":['Verification of Packet Rate','"num":"91"']}
VM_PROMPTS= {"TEST_CASE_1":["Issuing Verbal Memo comand","verbal_memo,go_ahead","posted"]}
FB_PROMPTS= {"TEST_CASE_1":["Issuing Feedback comand","feedback_for,system,go_ahead","thank_you_for_leaving_feedback"]}
LP_PROMPTS= {"TEST_CASE_1":["Issuing loss prevention comand","you_are_reporting_an_lp_event_in_the_store,tap_out_to_cancel","ok_please_describe_in_detail_the_lp_event_you_saw","posted"]}
GRP_MSG = {"TEST_CASE_1":["Group Message to available users","tts_available","tts_posted_at"],
			"TEST_CASE_2":["Group Message to Engaged user","tts_engaged","tts_posted_at"],
			"TEST_CASE_3":["Group Message to Non Logged on user","tts_posted_at"],
			"TEST_CASE_4":["Group Message reboot Survival","tts_posted_at"] }
SK_PROMPTS= {"TEST_CASE_1":["Issuing SKULOOKUP comand","please_say_the_sku_number_one_digit_at_a_time","checking_inventory_for,|1,|2,|3,|4,|5,|6,|7,|8,|9,|say_no_if_i_have_it_wrong","checking_inventory_request_timed_out,|please_say_check_inventory_to_try_again"]}
MA_PROMPTS={"TEST_CASE_1":["Manager Assistance without location","tts_thank_you_for_responding_to,|manager_assistance","is_responding_to,|manager_assistance"],
			"TEST_CASE_2":["Manager Assistance with Location","tts_ok_you_are_responding_to,|manager_assistance,|please_say,|north bay,|or,|south bay","tts_thank_you_for_responding_to,|manager_assistance,|north bay","is_responding_to,|manager_assistance,|north bay","Location bases manager Assistance for second Location","tts_thank_you_for_responding_to,|manager_assistance,|south bay,","is_responding_to,|manager_assistance,|south bay"]}
GUN_PROMPTS= {"TEST_CASE_1":["Issuing Gun rubber with two Locations","tts_please_say_the_location,|north bay,|or,|south bay","tts_thank_you_for_gun_running_to,|north bay","is_gun_running_to,|north bay","Location based gun runner for second Location","tts_thank_you_for_gun_running_to,|south bay","is_gun_running_to,|south bay"]}
ESC_PROMPTS= {"TEST_CASE_1":["Register Backup Escalation","backup_request_has_been_escalated_to_the,|managers","tts_backup_request_escalated_to,|managers","tts_thank_you_for_responding_to,|register_backup","is_responding_to,|register_backup"]}
COVERAGE_PROMPTS= {"TEST_CASE_1":["Issuing Store coverage command","flooring,2,available,0,engaged,front end,2,available,0,engaged,millwork,1,available,0,engaged"],
"Location":["millwork","front end","flooring","flooring","front end"],
"TEST_CASE_2":["Issuing Store coverage command for Dual_location","front end,2,available,0,engaged,millwork,3,available,0,engaged"],
"Dual_Location":["millwork","millwork|front end","front end|pumping","millwork","front end"]}
MT_PROMPTS= {"TEST_CASE_1":["Issuing Manager Thank you with 2 locations","tts_ok_you_are_responding_to,|manager_thank_you,|please_say,|north bay,|or,|south bay","tts_thank_you_for_responding_to_manager_thank_you,|north bay","is_responding_to_manager_thank_you,|north bay","Location based Managert Thank you for second Location","tts_thank_you_for_responding_to_manager_thank_you,|south bay","is_responding_to_manager_thank_you,|south bay"]}
MOD_PROMPTS={"non_member":"you_do_not_have_access_to_this_command",
			 "log_on":[",you_are_now_manager_on_duty",",is_now_the_manager_on_duty"],
			 "already_log_on":"you_are_already_logged_on_as,mod",
			 "MOD_not_checkin":"i_am_sorry_there_is_no,manageronduty,checked_in",
			 "who_is_MOD":",is_manageronduty",
			 "engaged_MOD":["manageronduty,",",is_engaged_with_a_customer_near"],
			 "busy_MOD":["manageronduty,",",is_in_a_conversation_near"],
			 "self_identify":"you_are,manageronduty",
			 "checkout":[",you_are_logging_off_as,mod,to_leave_a_handoff_message_for_the_next,mod,say_message,manageronduty","there_is_no_manager_on_duty_to_become_mod_please_say_checkin_manger_on_duty"],
			 "not_mod" :"you_are_not_a,mod",
			 "message_mod":"manageronduty,has_heard_your_message"}

RB_NBR_1 = {"initating_request":"requesting_all,3,available_associates,tap_out_to_cancel",
			"soundout":["register_backup","3_register_backup"],
			"accept1":[",is_responding_to_register_backup,issued_by,","thank_you_for_responding_to_register_backup,issued_by,"],
			"already_accept":"there_is_no_active_r_and_r",
			"initiator_accept":"im_sorry_you_can_not_respond_to_your_own_register_backup_request",
			"new_user_accept":"2_register_backup",
			"engaged_user_accept":"1_register_backup",
			"already_active":"backup_already_active",
			"no_user_available":"im_sorry_no_one_is_available_for_backup"}
LOCATE_PROMPTS={"non_logged_on":",is_not_logged_on",
				"available":",is_available_near,"+location,
				"engaged":",is_engaged_near,"+location,
				"self":"if_you_dont_know_where_you_are_how_should_i_know",
				"in_conversation":",is_in_a_conversation_near,"+location,
				"listening_message":",is_listening_to_a_message_near,"+location,
				"checking_inventory":[",please_say_the_SKU_number_one_digit_at_a_time",",is_checking_inventory_near"],
				"unreachable":",is_out_of_range"}
do_you_mean={"ask_for_full_name":"more_than_one,gary,is_available,please_say_full_name",
             "not_logged_on":"gary,is_not_logged_on"}
#AG_PROMPTS={"__COMMAND_LIST__":"listgroups,to_list_groups,listcommands,to_list_commands,listlocations,to_list_locations"}
AG_PROMPTS={"__COMMAND_LIST__":"listgroups,listlocations,listcommands"}

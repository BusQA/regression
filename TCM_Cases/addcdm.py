#!/usr/bin/env python
import pytest
import sqlite3
import requests
import argparse
import sys
import os
#from splinter import Browser
from selenium import webdriver
from config import *
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#from selenium import webdriver
#from selenium.webdriver.chrome.options import Options
from selenium.webdriver import ChromeOptions
from os import listdir
from config import driver_path

class AddUpdateDeleteCDM():
	def setup(self):
		try:
			chrome_options = ChromeOptions()
			#chrome_options.accept_untrusted_certs = True
			#chrome_options.assume_untrusted_cert_issuer = True
			chrome_options.add_argument("allow-file-access-from-files")
			chrome_options.add_argument("use-fake-device-for-media-stream")
			#chrome_options.add_argument("--disable-user-media-security=true")
			chrome_options.add_argument("use-fake-ui-for-media-stream")
			#opt.add_argument("--disable-extensions")
			chrome_options.add_argument("-disable-extensions")
			chrome_options.add_argument("-disable-infobars")
			chrome_options.add_argument("start-maximized")
			# Pass the argument 1 to allow and 2 to block
			chrome_options.add_experimental_option("prefs", { \
				"profile.default_content_setting_values.media_stream_mic": 1, 
    			"profile.default_content_setting_values.media_stream_camera": 1,
    			"profile.default_content_setting_values.geolocation": 1, 
    			"profile.default_content_setting_values.notifications": 1 
    			})
			self.browser = webdriver.Chrome(driver_path, chrome_options=chrome_options)
			self.browser.maximize_window()
			self.browser.get(url)

			#self.driver = webdriver.Chrome(chrome_options=opt, executable_path='/home/theatro/smoke_tool/TCM_Cases/chromedriver.exe')
			#self.driver.maximize_window()
			#self.driver.visit(url)

		except Exception as e:
			print "Exception while navigating to url"
			raise e


	def login_with_username_and_password(self,user=user,password=password):
		try:
			self.setup()
			WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'twLoginId')))

			if(self.browser.find_element_by_id('twLoginId').is_displayed()):		
				self.browser.find_element_by_name('twLoginId').send_keys(user)
				self.browser.find_element_by_id('twPassword').send_keys(password)
				self.browser.find_element_by_id('loginbtn').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'go-to-new-cdm')))
			else:
				print "Unable to Navigate to Theatro portal login page"
		

			if(self.browser.find_element_by_id('go-to-new-cdm').is_displayed()):
				self.browser.find_element_by_id('go-to-new-cdm').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-status-filter')))
				time.sleep(40)
				alertobj = self.browser.switch_to().alert()
				alertobj.dismiss()
				time.sleep(2)
				alertobj = self.browser.switch_to().alert()
				alertobj.dismiss()
				time.sleep(2)
				alertobj = self.browser.switch_to().alert()
				alertobj.dismiss()

			else:
				print "Unable to Navigate to role based login page"
		except Exception as e:
			raise e


	def add_cdm(self):
		try:
			if(self.browser.find_element_by_id('cdm-status-filter').is_displayed()):
				self.browser.find_element_by_xpath('''//*[@id="main-content"]/section/header//a''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-name')))
				self.browser.find_element_by_id('cdm-name').is_displayed()
				self.browser.find_element_by_id('cdm-name').send_keys(cdmname)
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="step-1"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', '''//*[@id="record"]/span[contains(text(),'Start Recording')]''')))
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Start Recording')]''').click()
				time.sleep(4)
				self.browser.find_element_by_xpath('''//*[@id="record"]/span[contains(text(),'Stop Recording')]''').click()
				self.browser.find_element_by_xpath('''//*[@id="step-2"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('xpath', "//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]")))
				self.browser.find_element_by_xpath("//*[@id='hierarchy-view']//label/strong[contains(text(),"+"'"+storeName+"'"+")]").click()
				self.browser.find_element_by_xpath('''//*[@id="step-3"]//button[@class='btn btn-lg empbtns cdm-navigation pull-right']''').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'immediate-cdm-schedule')))
				time.sleep(4)
				self.browser.find_element_by_id('immediate-cdm-schedule').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-action')))
				time.sleep(4)
				self.browser.find_element_by_id('cdm-action').click()
				WebDriverWait(self.browser, 60).until(EC.presence_of_element_located(('id', 'cdm-status-filter')))
				time.sleep(20)
				createdcdm = self.browser.find_element_by_xpath("//*[@id='cdm-grid']//a[@class='edit-cdm-basic-details']").text
				print(createdcdm)
				if(createdcdm==cdmname):
					return True
				else:
					return False
			else:
				print "Unable to find the create CDM element in CDM Home Page"
		except Exception as e:
			raise e

#t=AddUpdateDeleteCDM()
#t.setup()
#t.login_with_username_and_password('aayush','Developer342!')
#t.add_cdm()


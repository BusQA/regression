#!/usr/bin/env python
import sqlite3
import requests
import argparse
import sys
from splinter import Browser
from config import *


updated_url=url+'/#employees/{0}/{1}'.format(COMPANY_ID,STORE_ID)

class AddUpdateDeleteEmployee():
	def setUp(self):
		try:
			self.browser = Browser('firefox')
			self.browser.driver.maximize_window()
			self.browser.visit(url)
		except Exception as e:
			print "Exception while navigating to url"
			raise e

	def navigateToDiffStore(self):
		try:
			self.browser.visit("https://itg0.theatro.com/tsg/")
			self.browser.visit(updated_url)
		except Exception as e:
			print "Exception while navigating to different store"
			raise e
		
	def login_with_username_and_password(self):
		try:
			self.setUp()
			if(self.browser.is_element_present_by_id('twLoginId', wait_time=100)):
				self.browser.find_by_id('twLoginId').first.fill(user)
				self.browser.find_by_id('twPassword').first.fill(password)
				self.browser.find_by_id('loginbtn').first.click()
				"""if(self.browser.is_element_present_by_xpath('''.//*[@id='employeeList']/div''',wait_time=100)):
					dropdown = self.browser.find_by_id('merchant')
					for option in dropdown.find_by_tag('option'):
					    if option.text == all_rows.split('/')[0]:
					        option.click()
					        break
				if(self.browser.is_element_present_by_xpath('''.//*[@id='storeSelect']/option''',wait_time=100)):
					dropdown1 = self.browser.find_by_id('storeSelect')
					for option1 in dropdown1.find_by_tag('option'):
						if option1.text == all_rows.split('/')[1]:
						    option1.click()
						    break
				else:
					print "Unable to Login to Theatro portal"""
			else:
				print "Unable to Navigate to Theatro portal login page"
		except Exception as e:
				raise e

	def tearDown(self):
		self.browser.quit()

	def delete_employee(self):
		try:
			self.login_with_username_and_password()
			self.browser.is_element_present_by_xpath('''.//*[@id='employeeList']/div''',wait_time=1000)
			self.navigateToDiffStore()
			if(self.browser.is_element_visible_by_xpath('//p[contains(text(),\''+sys.argv[1]+' '+sys.argv[2]+'\')]',wait_time=100)):
				self.browser.find_by_xpath('//p[contains(text(),\''+sys.argv[1]+' '+sys.argv[2]+'\')]').first.click()
				if(self.browser.is_element_visible_by_xpath('''.//*[@id='empGroups']/li''', wait_time=30)):
					self.browser.find_by_id('deleteEmployee').first.click()
					if(self.browser.is_element_visible_by_xpath('''//div[contains(text(),'Are you sure you want to delete this employee?')]''',wait_time=30)):
						self.browser.find_by_xpath('''//button[@data-bb-handler='confirm']''').click()
					if(self.browser.is_element_visible_by_xpath('''//div[contains(text(),'Employee deleted successfullly')]''',wait_time=100)):
						self.browser.find_by_xpath('''//button[contains(text(),'OK')]''').click()
					else:
						print "Popup didnot appear after clicking on employee delete button"
			else:
				print "Unable to delete employee as employee doesn't exist"
			self.tearDown()
		except Exception as e:
				raise e

test = AddUpdateDeleteEmployee()
test.delete_employee()

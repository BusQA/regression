import sys
import subprocess, shlex
import os
from os.path import join
from config import WB_SUPPORT
from boto3.session import Session
from contextlib import closing

REGION = 'us-west-2'
ACCESS_KEY = 'AKIAIAJKBMFRTWUH7RHA'
SECRET_KEY = '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'

session = Session(aws_access_key_id=ACCESS_KEY,
                  aws_secret_access_key=SECRET_KEY,
                  region_name=REGION)

polly = session.client("polly")
                 
def run(cmd):

    try:
        output = subprocess.check_output(shlex.split(cmd))
    except subprocess.CalledProcessError as e:
        print 'Command {0} failed to execute. {1}'.format(cmd, repr(e))
        exit(1)

    return [out for out in output.split('\n') if out]
                     
def ensure_dir(f):
    d = os.path.dirname(f)
    if not os.path.exists(d):
        os.makedirs(d)
        
def make_audio(text, outputfilepath, TextType='ssml', outputFormat='pcm',
                filextension='raw', SampleRate=16000, convert2pcm16=False,
                convert2mulaw=False):
                
    # Not maintaining coding convention to align with Amazon's variable
    #naming convention
    #print polly.describe_voices(LanguageCode="fr-CA") # This tells you that there is a french voice called 'Chantal' for language fr-CA which I use in the next line.
    #voiceId  = 'Chantal' #fr-ca
    voiceId = 'Joanna'
    
    
    print '\n' + text
    response = polly.synthesize_speech(Text=text, TextType=TextType, VoiceId=voiceId, 
                OutputFormat=outputFormat, SampleRate=SampleRate)
    
        
    #print '\n\t', response
    
    with closing(response["AudioStream"]) as stream:
        try:
            # Open a file for writing the output as a binary stream
            with open('temp.pcm', "wb") as file:
                file.write(stream.read())

        except IOError as error:
            # Could not write to file, exit gracefully
            print(error)
            sys.exit(-1)
    
    if convert2pcm16:
        raw_pcm_to_pcm16('temp.pcm', outputfilepath)
    
    if convert2mulaw:
        raw_pcm_to_mulaw('temp.pcm', outputfilepath)
    
    
    os.remove('temp.pcm')
    
def raw_pcm_to_pcm16(rawpcmfilename, outputpcm16filename):
    
    run('''sox -r 16000 --bits 16 --encoding signed-integer --endian little -t raw %s "%s"''' % (rawpcmfilename, outputpcm16filename))
   
def raw_pcm_to_mulaw(rawpcmfilename, outputmulawfilename):
    
    outputpcm16filename = 'temp.wav'
    raw_pcm_to_pcm16(rawpcmfilename, outputpcm16filename)
    if WB_SUPPORT:
        run('sox %s -e u-law -c 1 -b 8 -r 16000 "%s"' % (outputpcm16filename, outputmulawfilename))
    else:
        run('sox %s -e u-law -c 1 -b 8 -r 8000 "%s"' % (outputpcm16filename, outputmulawfilename))
    
    os.remove(outputpcm16filename)

def demo_text():

    text = 'john smith'
    outputfilepath = join('output', 'john smith.wav')
    # set either one of convert2pcm16 or convert2mulaw to True not both
    make_audio(text, outputfilepath, TextType='text', outputFormat='pcm', filextension='pcm', SampleRate='16000', convert2pcm16=False, convert2mulaw=True)
    
def demo_ssml():
    
    ssml = ur'''<speak>
    Here is a number <w role="amazon:VBD">read</w> 
    as a cardinal number: 
    <say-as interpret-as="cardinal">12345</say-as>. 
    
    Here is a word spelled out: 
    <say-as interpret-as="spell-out">hello</say-as>. 
    
    this is how you use a custom pronunciation for the word coordinate <phoneme alphabet="x-sampa" ph='koUOrd@neIts'/>
    
    My name is <phoneme alphabet="x-sampa" ph='bV43T'/>
    
    I want to tell you a secret. 
    <amazon:effect name="whispered">I am not a real human.</amazon:effect>.
    Can you believe it?
    
    I already told you I 
    <emphasis level="strong">really</emphasis> 
    like that person.

</speak>'''

    outputfilepath = join('output', 'blab blah.wav')
    # set either one of convert2pcm16 or convert2mulaw to True not both
    make_audio(ssml, outputfilepath, TextType='ssml', outputFormat='pcm', filextension='pcm', SampleRate='16000', convert2pcm16=False, convert2mulaw=True)
        
def bulk(text,filepath):
    #outputfilepath = join('output', filename)
    text = text.strip().decode('latin-1').encode('utf-8')
    make_audio(text, filepath, TextType='text', outputFormat='pcm',filextension='pcm', SampleRate='16000', convert2pcm16=True,convert2mulaw=True)
    
if __name__ == "__main__":
    bulk(text=sys.argv[1])
